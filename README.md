Python 3.4

Django >1.7

Бібілотеки: lxml, requests

-----
### Щоб запустити сфінкс: ###

встановити сфінкс, інструкції звідси           
http://chakrygin.ru/2013/03/sphinx-install.html

далі - встановити sphinxit, слідувати інструкціям звідси
https://sphinxit.readthedocs.org/en/latest/preparation.html

конфігураційний файл берете на бітбакеті - etc/config.txt


### Команди ###
Запустити "C:\Program Files (x86)\MySQL\MySQL Server 5.6\bin\mysql" -h 127.0.0.1 -P 9306 --default-character-set=utf8

Оновити індекс - indexer -c D:\Programs\sphinx\data\config.txt --all --rotate 