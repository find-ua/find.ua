var scrollCount = 0;


$(document).ready(function () {
    //Скрыть PopUp при загрузке страницы
    PopUpHide();
});


$(document).ready(function () {
    $(document).on("submit", ".save-ad-form", function (event) {
        event.preventDefault();
        //alert("data submitted!") ; // sanity check
        SaveAd($(this));
    });
});
$(document).ready(function () {
    $('#save-params-button').on('click', function (event) {
        event.preventDefault();
        //alert("form submitted!") ; // sanity check
        SaveParams();
    });
});

$(document).ready(function () {
    $('.delete-ad-form').on('submit', function (event) {
        event.preventDefault();
        //alert("data submitted!") ; // sanity check
        DeleteAd($(this));
    });
});


$(document).ready(function () {
    $('.delete-param-form').on('click', function (event) {
        event.preventDefault();
        //alert("form submitted!") ; // sanity check
        DeleteParams($(this));
    });
});


//Функция отображения PopUp
function PopUpShow() {
    $("#popup1").show();
}
//Функция скрытия PopUp
function PopUpHide() {
    $("#popup1").hide();
}


function SaveParams() {
    var paramsForm = $("#params-form");
    $.ajax({
        url: "", // the endpoint
        type: "POST", // http method
        data: paramsForm.serialize() + '&save-params=', // data sent with the post request

        // handle a successful response
        success: function (json) {
            if (json['result'] == 'ok') alert('Ваші параметри збережено!');
            else {
                window.location.replace("/login");
            }
        },

        // handle a non-successful response
        error: function (xhr, errmsg, err) {
            alert("Щось пішло не так!");
        }
    });
}

function SaveAd(ad) {
    var adForm = ad;
    console.log(adForm);
    $.ajax({
        url: "", // the endpoint
        type: "POST", // http method
        data: adForm.serialize() + '&save-ad=', // data sent with the post request

        // handle a successful response
        success: function (json) {
            if (json['result'] == 'ok') {
                alert('Ваше оголошення збережено!');
            }
            else {
                window.location.replace("/login");
            }
        },

        // handle a non-successful response
        error: function (xhr, errmsg, err) {
            alert("Щось пішло не так!");
        }
    });
}

function DeleteParams(param) {
    $.ajax({
        url: "", // the endpoint
        type: "POST", // http method
        data: param.serialize() + '&delete-param=', // data sent with the post request

        // handle a successful response
        success: function (json) {
            $('#post-text').val(''); // remove the value from the input
            alert('Ваші параметри видалено!');
            param.parents(".row").hide();
        },

        // handle a non-successful response
        error: function (xhr, errmsg, err) {
            alert("Щось пішло не так!");
        }
    });
}

function DeleteAd(ad) {
    console.log(ad);
    $.ajax({
        url: "", // the endpoint
        type: "POST", // http method
        data: ad.serialize() + '&delete-ad=', // data sent with the post request

        // handle a successful response
        success: function (json) {
            $('#post-text').val(''); // remove the value from the input
            alert('Ваше оголошення видалено!');
            ad.parents(".row").hide();
        },

        // handle a non-successful response
        error: function (xhr, errmsg, err) {
            alert("Щось пішло не так!");
        }
    });
}


/* Кнопка, яка очищує всі поля форми */
$(document).ready(function () {
    $('#clear-form-data').on('click', function (event) {
        event.preventDefault();
        console.log('clear form data clicked');
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio, #location, #sort-select').val('');
        $(':checkbox, :radio').prop('checked', false);
    });
});

// Поле підкатегорії зникає, коли змінюється поле категорії
$(document).ready(function () {
    $('#category').on('change', function (event) {
        $('#subsubcategory').remove();
        $('.filter-wrapper').remove();
    });
});

$(document).ready(function () {
    $('#subsubcategory').on('change', function (event) {
        $('.filter-wrapper').remove();
    });
});


// Історія ціни
$(document).ready(function () {
    //$(".price-history-button").on("click", function(){
    $(document).on("click", ".price-history-button", function () {
        $(this).next(".price-history-detailed-info").toggle(350);
    });
});


// Сортування
$(document).ready(function () {
    $(document).on("change", "#sort-select", function () {
        scrollCount = 0;
        Sort(getSortMethod())
    })
});

function getSortMethod() {
    var sortMethod = $('#sort-select').find("option:selected").val();
    var path;
    switch (sortMethod) {
        case "date":
            path = '&sortby=date&order=desc';
            break;
        case "price_min":
            path = '&sortby=price&order=asc';
            break;
        case "price_max":
            path = '&sortby=price&order=desc';
            break;
    }
    return path;
}


function Sort(path) {

    $.ajax({
        url: $(location).attr('href'), // the endpoint
        type: "GET", // http method
        data: path, // data sent with the post request

        // handle a successful response
        success: function (data) {
            $('#ads-container').empty().append(data); // remove the value from the input
            $('#sort-select').val(method);
        },

        // handle a non-successful response
        error: function (xhr, errmsg, err) {
            alert("Щось пішло не так!");
        }
    });
}

// pagination

$(window).scroll(function () {
    var numberOfAds = $(".ad").length;
    if (($(window).scrollTop() == $(document).height() - $(window).height())
        && numberOfAds >= 40) {
        console.log("number of ads: ", numberOfAds);
        console.log("scroll count: ", scrollCount);
        scrollCount += 1;
        getResult(scrollCount);
    }
});

function getResult(scrollCount) {
    $.ajax({
        url: $(location).attr('href'),
        type: "GET",
        data: "&page=" + scrollCount.toString() + getSortMethod(),
        /*        beforeSend: function () {
         $('#loader-icon').show();
         },
         complete: function () {
         $('#loader-icon').hide();
         },*/
        success: function (data) {
            $("#ads-container").append(data);
        },
        error: function () {
            alert('Оголошення не завантажилися!')
        }
    });
}

// back to top button
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });

});


// footer to bottom
$(document).ready(function () {
    var forCenteredBottom = ($(document).width() / 2) - ($('#bottom').width() / 2);
    if ($('.page-wrapper').height() + $('.footer').height() < $(window).height()) {
        $('.footer').css({
            position: 'absolute',
            bottom: 0,
            left: forCenteredBottom,
        })
    } else {
        $('.footer').css({
            position: 'relative'
        })
    }

});