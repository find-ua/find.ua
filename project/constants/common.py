#-*- coding: utf-8 -*-
def genenum(**elements):
    """
    Function for generating enums
    """

    # Returned type is named 'MyEnum' as there are type
    # 'Enum' since python 3.4
    return type('MyEnum', (), elements)

class DB_KEYS:

    AD = genenum(ID='id',
                 CATEGORY='category',
                 URL='url',
                 SITE='site',
                 AD_ID='ad_id',
                 PRICE='price',
                 CURRENCY='currency',
                 LOCATION='location',
                 TITLE='title',
                 PHOTO_URLS='photo_urls',
                 DATE='date',
                 LAST_UPDATE='last_update')

    USER = genenum(ID='id',
                   USERNAME='username',
                   PASSWORD='password',
                   EMAIL='email',
                   FIRST_NAME='first_name',
                   LAST_NAME='last_name',
                   DATE_JOINED='date_joined',
                   LAST_LOGIN='last_login',
                   IS_ACTIVE='is_active')

    USER_PROFILE = genenum(ID='id',
                           USER='user',
                           GENDER='sex',
                           BIRTH_DATE='date_of_birth',
                           LOCATION='location',
                           IP='ip',
                           LANGUAGE='language',
                           PARAMS_ID='params_id',
                           URLS_ID='urls_id')

    PARAMS = genenum(ID='id',
                     CATEGORY='category',
                     KEYWORDS='q',
                     SITE='site',
                     PRICE_FROM='pmin',
                     PRICE_TO='pmax',
                     LOCATION='location',
                     CONDITION='condition',
                     SELLER_TYPE='seller',
                     DATE='date')

    NON_FILTERS = genenum(KEYWORDS='q',
                     SITE='site',
                     PRICE_FROM='pmin',
                     PRICE_TO='pmax',
                     CONDITION='condition',
                     SELLER_TYPE='seller',
                      LOCATION='location',
                      CATEGORY='category',
                      SUBCATEGORY='subcategory',
                      SUBSUBCATEGORY='subsubcategory',
                          ORDER='order',
                          SORTBY='sortby',
                          PAGE='page')

    # csrfmiddlewaretoken
    # FIELDS = genenum(CS='csrfmiddlewaretoken',
    #                  SITE='site',
    #                  PRICE_FROM='pmin',
    #                  PRICE_TO='pmax',
    #                  CONDITION='condition',
    #                  SELLER_TYPE='seller')
    FAVOURITE_AD = genenum(ID='id',
                           USER='user',
                           ADVERT='advert',
                           DESCRIPTION='description')

    PARAMS_LIST = genenum(ID='id',
                          USER='user',
                          PARAM='param',
                          LAST_UPDATE='last_update')

    LABEL_AD = genenum(ID='id',
                       PARAMS='params',
                       AD_ID='ad_id',
                       TITLE='title')


class DB_ENUMS:

    class GENERAL:
        EMPTY = ''
        OTHER = 'Other'
        ALL = 'all'

    class CATEGORY:
        MODA_I_STIL = 'moda-i-stil'
        NEDVIZHIMOST = 'nedvizhimost'
        TRANSPORT = 'transport'
        DETSKIY_MIR = 'detskiy-mir'
        USLUGI = 'uslugi'
        ELEKTRONIKA = 'elektronika'
        HOBBI_OTDYH_I_SPORT = 'hobbi-otdyh-i-sport'
        DOM_I_SAD = 'dom-i-sad'
        ZHIVOTNYE = 'zhivotnye'
        RABOTA = 'rabota'


    class SUBCATEGORY:

        ODEZHDA = 'odezhda'
        ZHENSKAYA_ODEZHDA = 'zhenskaya-odezhda'
        ZHENSKAYA_OBUV = 'zhenskaya-obuv'
        MUZHSKAYA_ODEZHDA = 'muzhskaya-odezhda'
        MUZHSKAYA_OBUV = 'muzhskaya-obuv'
        ZHENSKOE_BELE_KUPALNIKI = 'zhenskoe-bele-kupalniki'
        GOLOVNYE_UBORY = 'golovnye-ubory'
        ODEZHDA_DLYA_BEREMENNYH = 'odezhda-dlya-beremennyh'
        MUZHSKOE_BELE = 'muzhskoe-bele'


        AKSESSUARY = 'aksessuary'
        SUMKI = 'sumki'
        DRUGIE_AKSESSUARY = 'drugie-aksessuary'
        BIZHUTERIYA = 'bizhuteriya'
        YUVELIRNYE_IZDELIYA = 'yuvelirnye-izdeliya'


        NARUCHNYE_CHASY = 'naruchnye-chasy'


        KRASOTA_ZDOROVE = 'krasota-zdorove'
        PARFYUMERIYA = 'parfyumeriya'
        PROCHIE_TOVARY_DLYA_KRASOTY_I_ZDOROVYA = 'prochie-tovary-dlya-krasoty-i-zdorovya'
        SREDSTVA_PO_UHODU = 'sredstva-po-uhodu'
        KOSMETIKA = 'kosmetika'
        OBORUDOVANIE_PARIKMAHERSKIH_SALONOV_KRASOTY = 'oborudovanie-parikmaherskih-salonov-krasoty'
        TOVARY_DLYA_INVALIDOV = 'tovary-dlya-invalidov'


        PODARKI = 'podarki'


        DLYA_SVADBY = 'dlya-svadby'
        SVADEBNYE_PLATYA = 'svadebnye-platya'
        SVADEBNYE_AKSESSUARY = 'svadebnye-aksessuary'


        MODA_RAZNOE = 'moda-raznoe'


        PRODAZHA_KVARTIR = 'prodazha-kvartir'
        VTORICHNYY_RYNOK = 'vtorichnyy-rynok'
        NOVOSTROYKI = 'novostroyki'


        ARENDA_KVARTIR = 'arenda-kvartir'
        DOLGOSROCHNAYA_ARENDA_KVARTIR = 'dolgosrochnaya-arenda-kvartir'
        KVARTIRY_POSUTOCHNO = 'kvartiry-posutochno'
        KVARTIRY_S_POCHASOVOY_OPLATOY = 'kvartiry-s-pochasovoy-oplatoy'


        PRODAZHA_DOMOV = 'prodazha-domov'
        PRODAZHA_KOTTEDZHEY = 'prodazha-kottedzhey'
        PRODAZHA_DOMOV_V_DEREVNE = 'prodazha-domov-v-derevne'
        PRODAZHA_DACH = 'prodazha-dach'


        PRODAZHA_ZEMLI = 'prodazha-zemli'
        PRODAZHA_ZEMLI_POD_INDIVIDUALNOE_STROITELSTVO = 'prodazha-zemli-pod-individualnoe-stroitelstvo'
        PRODAZHA_ZEMLI_POD_SAD_OGOROD = 'prodazha-zemli-pod-sad-ogorod'
        PRODAZHA_ZEMLI_SELSKOHOZYAYSTVENNOGO_NAZNACHENIYA = 'prodazha-zemli-selskohozyaystvennogo-naznacheniya'
        PRODAZHA_ZEMLI_PROMYSHLENNOGO_NAZNACHENIYA = 'prodazha-zemli-promyshlennogo-naznacheniya'


        ARENDA_KOMNAT = 'arenda-komnat'
        DOLGOSROCHNAYA_ARENDA_KOMNAT = 'dolgosrochnaya-arenda-komnat'
        KOYKO_MESTA = 'koyko-mesta'
        KOMNATY_POSUTOCHNO = 'komnaty-posutochno'


        ARENDA_POMESCHENIY = 'arenda-pomescheniy'
        ARENDA_OFISOV = 'arenda-ofisov'
        ARENDA_MAGAZINOV_SALONOV = 'arenda-magazinov-salonov'
        ARENDA_POMESCHENIY_SVOBODNOGO_NAZNACHENIYA = 'arenda-pomescheniy-svobodnogo-naznacheniya'
        ARENDA_SKLADOV = 'arenda-skladov'
        ARENDA_RESTORANOV_BAROV = 'arenda-restoranov-barov'
        ARENDA_POMESCHENIY_PROMYSHLENNOGO_NAZNACHENIYA = 'arenda-pomescheniy-promyshlennogo-naznacheniya'
        ARENDA_OTDELNO_STOYASCHIH_ZDANIY = 'arenda-otdelno-stoyaschih-zdaniy'
        PROCHEE = 'prochee'
        ARENDA_BAZ_OTDYHA = 'arenda-baz-otdyha'


        ARENDA_DOMOV = 'arenda-domov'
        DOLGOSROCHNAYA_ARENDA_DOMOV = 'dolgosrochnaya-arenda-domov'
        DOMA_POSUTOCHNO_POCHASOVO = 'doma-posutochno-pochasovo'


        PRODAZHA_GARAZHEY_STOYANOK = 'prodazha-garazhey-stoyanok'


        OBMEN_NEDVIZHIMOSTI = 'obmen-nedvizhimosti'


        PROCHAJA_NEDVIZIMOST = 'prochaja-nedvizimost'


        PRODAZHA_KOMNAT = 'prodazha-komnat'


        PRODAZHA_POMESCHENIY = 'prodazha-pomescheniy'
        PRODAZHA_OFISOV = 'prodazha-ofisov'
        PRODAZHA_MAGAZINOV_SALONOV = 'prodazha-magazinov-salonov'
        PRODAZHA_POMESCHENIY_SVOBODNOGO_NAZNACHENIYA = 'prodazha-pomescheniy-svobodnogo-naznacheniya'
        PRODAZHA_OTDELNO_STOYASCHIH_ZDANIY = 'prodazha-otdelno-stoyaschih-zdaniy'
        PRODAZHA_POMESCHENIY_PROMYSHLENNOGO_NAZNACHENIYA = 'prodazha-pomescheniy-promyshlennogo-naznacheniya'
        PRODAZHA_RESTORANOV_BAROV = 'prodazha-restoranov-barov'
        PRODAZHA_BAZ_OTDYHA = 'prodazha-baz-otdyha'
        PRODAZHA_SKLADOV = 'prodazha-skladov'


        ARENDA_GARAZHEY_STOYANOK = 'arenda-garazhey-stoyanok'


        ARENDA_ZEMLI = 'arenda-zemli'


        ISCHU_KOMPANONA = 'ischu-kompanona'


        ZAPCHASTI_AKSESSUARY = 'zapchasti-aksessuary'
        AVTOZAPCHASTI = 'avtozapchasti'
        SHINY_DISKI = 'shiny-diski'
        AKSESSUARY_DLYA_AVTO = 'aksessuary-dlya-avto'
        ZAPCHASTI_DLYA_SPETS_SH_TEHNIKI = 'zapchasti-dlya-spets-sh-tehniki'
        MOTOZAPCHASTI = 'motozapchasti'
        AVTOZVUK = 'avtozvuk'
        PROCHIE_ZAPCHASTI = 'prochie-zapchasti'
        MOTO_AKSESSUARY = 'moto-aksessuary'
        TRANSPORT_NA_ZAPCHASTI = 'transport-na-zapchasti'
        GPS_NAVIGATORY_AVTOREGISTRATORY = 'gps-navigatory-avtoregistratory'


        SELHOZTEHNIKA = 'selhoztehnika'


        LEGKOVYE_AVTOMOBILI = 'legkovye-avtomobili'
        VAZ = 'vaz'
        VOLKSWAGEN = 'volkswagen'
        OPEL = 'opel'
        MERCEDES = 'mercedes'
        FORD = 'ford'
        DAEWOO = 'daewoo'
        RENAULT = 'renault'
        TOYOTA = 'toyota'
        MITSUBISHI = 'mitsubishi'
        ZAZ = 'zaz'
        CHEVROLET = 'chevrolet'
        HYUNDAI = 'hyundai'
        GAZ = 'gaz'
        BMW = 'bmw'
        NISSAN = 'nissan'
        AUDI = 'audi'
        FIAT = 'fiat'
        MAZDA = 'mazda'
        SKODA = 'skoda'
        PEUGEOT = 'peugeot'
        DRUGIE = 'drugie'
        KIA = 'kia'
        MOSKVICH = 'moskvich'
        CITROEN = 'citroen'
        HONDA = 'honda'
        CHERY = 'chery'
        GEELY = 'geely'
        SUBARU = 'subaru'
        UAZ = 'uaz'
        SUZUKI = 'suzuki'
        VOLVO = 'volvo'
        LEXUS = 'lexus'
        SEAT = 'seat'
        TAVRIYA = 'tavriya'
        SSANG_YONG = 'ssang-yong'
        IZH = 'izh'
        DACIA = 'dacia'
        GREAT_WALL = 'great-wall'
        LAND_ROVER = 'land-rover'
        CHRYSLER = 'chrysler'
        SMART = 'smart'
        ALFA_ROMEO = 'alfa-romeo'
        JEEP = 'jeep'
        DODGE = 'dodge'
        PORSCHE = 'porsche'
        ACURA = 'acura'
        ROEWE = 'roewe'
        OLDSMOBILE = 'oldsmobile'
        BRILLIANCE = 'brilliance'
        JMC = 'jmc'
        PROTON = 'proton'
        PAGANI = 'pagani'
        MICROCAR = 'microcar'


        GRUZOVYE_AVTOMOBILI = 'gruzovye-avtomobili'


        MOTO = 'moto'
        MOTOTSIKLY = 'mototsikly'
        MOPEDY_SKUTERY = 'mopedy-skutery'
        KVADROTSIKLY = 'kvadrotsikly'
        MOTO_PROCHEE = 'moto-prochee'


        VODNYY_TRANSPORT = 'vodnyy-transport'


        PRITSEPY = 'pritsepy'


        SPETSTEHNIKA = 'spetstehnika'
        BULDOZERY_TRAKTORY = 'buldozery-traktory'
        POGRUZCHIKI = 'pogruzchiki'
        STROITELNAYA_TEHNIKA = 'stroitelnaya-tehnika'
        PROCHAYA_SPETSTEHNIKA = 'prochaya-spetstehnika'
        OBORUDOVANIE_DLYA_SPETSTEHNIKI = 'oborudovanie-dlya-spetstehniki'
        EKSKAVATORY = 'ekskavatory'
        KOMMUNALNAYA_TEHNIKA = 'kommunalnaya-tehnika'
        SAMOSVALY = 'samosvaly'


        AVTOBUSY = 'avtobusy'


        DRUGOY_TRANSPORT = 'drugoy-transport'


        VOZDUSHNYY_TRANSPORT = 'vozdushnyy-transport'


        DETSKAYA_ODEZHDA = 'detskaya-odezhda'
        ODEZHDA_DLYA_DEVOCHEK = 'odezhda-dlya-devochek'
        ODEZHDA_DLYA_MALCHIKOV = 'odezhda-dlya-malchikov'
        ODEZHDA_DLYA_NOVOROZHDENNYH = 'odezhda-dlya-novorozhdennyh'


        DETSKAYA_OBUV = 'detskaya-obuv'


        IGRUSHKI = 'igrushki'


        PROCHIE_DETSKIE_TOVARY = 'prochie-detskie-tovary'


        DETSKAYA_MEBEL = 'detskaya-mebel'


        DETSKIE_KOLYASKI = 'detskie-kolyaski'


        DETSKIY_TRANSPORT = 'detskiy-transport'


        TOVARY_DLYA_SHKOLNIKOV = 'tovary-dlya-shkolnikov'


        KORMLENIE = 'kormlenie'


        DETSKIE_AVTOKRESLA = 'detskie-avtokresla'


        OBORUDOVANIE = 'oborudovanie'


        STROITELSTVO_OTDELKA_REMONT = 'stroitelstvo-otdelka-remont'
        OTDELKA_REMONT = 'otdelka-remont'
        STROITELNYE_USLUGI = 'stroitelnye-uslugi'
        BYTOVOY_REMONT_UBORKA = 'bytovoy-remont-uborka'
        OKNA_DVERI_BALKONY = 'okna-dveri-balkony'
        SANTEHNIKA_KOMMUNIKATSII = 'santehnika-kommunikatsii'
        GOTOVYE_KONSTRUKTSII = 'gotovye-konstruktsii'
        ELEKTRIKA = 'elektrika'
        DIZAYN_ARHITEKTURA = 'dizayn-arhitektura'
        VENTILYATSIYA_KONDITSIONIROVANIE = 'ventilyatsiya-konditsionirovanie'


        RAZVLECHENIE_FOTO_VIDEO = 'razvlechenie-foto-video'


        OBRAZOVANIE = 'obrazovanie'


        PEREVOZKI_ARENDA_TRANSPORTA = 'perevozki-arenda-transporta'


        SYRE_MATERIALY = 'syre-materialy'


        OBSLUZHIVANIE_REMONT_TEHNIKI = 'obsluzhivanie-remont-tehniki'


        REKLAMA_MARKETING_PR = 'reklama-marketing-pr'


        PROCHIE_USLUGI = 'prochie-uslugi'


        YURIDICHESKIE_USLUGI = 'yuridicheskie-uslugi'


        AVTO_MOTO_USLUGI = 'avto-moto-uslugi'


        PROKAT_TOVAROV = 'prokat-tovarov'


        KRASOTA_ZDOROVE = 'krasota-zdorove'
        MANIKYUR_NARASCHIVANIE_NOGTEY = 'manikyur-naraschivanie-nogtey'
        KRASOTA_ZDOROVE_PROCHEE = 'krasota-zdorove-prochee'
        MAKIYAZH_KOSMETOLOGIYA_NARASCHIVANIE_RESNITS = 'makiyazh-kosmetologiya-naraschivanie-resnits'
        MASSAZH = 'massazh'
        STRIZHKI_NARASCHIVANIE_VOLOS = 'strizhki-naraschivanie-volos'
        MEDITSINA = 'meditsina'
        PSIHOLOGIYA = 'psihologiya'


        TURIZM_IMMIGRATSIYA = 'turizm-immigratsiya'


        PRODAZHA_BIZNESA = 'prodazha-biznesa'


        SETEVOY_MARKETING = 'setevoy-marketing'


        FINANSOVYE_USLUGI = 'finansovye-uslugi'


        NYANI_SIDELKI = 'nyani-sidelki'


        USLUGI_DLYA_ZHIVOTNYH = 'uslugi-dlya-zhivotnyh'


        USLUGI_PEREVODCHIKOV_NABOR_TEKSTA = 'uslugi-perevodchikov-nabor-teksta'


        TELEFONY = 'telefony'
        AKSESSUARY = 'aksessuary'
        MOBILNYE_TELEFONY = 'mobilnye-telefony'
        STATSIONARNYE_TELEFONY = 'statsionarnye-telefony'
        SIM_KARTY_TARIFY_NOMERA = 'sim-karty-tarify-nomera'
        REMONT_PROSHIVKA = 'remont-proshivka'
        PROCHIE_TELEFONY = 'prochie-telefony'


        KOMPYUTERY = 'kompyutery'
        KOMPLEKTUYUSCHIE = 'komplektuyuschie'
        PERIFERIYNYE_USTROYSTVA = 'periferiynye-ustroystva'
        AKSESSUARY = 'aksessuary'
        NOUTBUKI = 'noutbuki'
        PLANSHETNYE_KOMPYUTERY = 'planshetnye-kompyutery'
        RASHODNYE_MATERIALY = 'rashodnye-materialy'
        NASTOLNYE = 'nastolnye'
        MONITORY = 'monitory'
        DRUGOE = 'drugoe'
        VNESHNIE_NAKOPITELI = 'vneshnie-nakopiteli'
        SERVERY = 'servery'


        AKSESSUARY_I_KOMPLEKTUYUSCHIE = 'aksessuary-i-komplektuyuschie'


        FOTO_VIDEO = 'foto-video'
        AKSESSUARY_DLYA_FOTO_VIDEOKAMER = 'aksessuary-dlya-foto-videokamer'
        TSIFROVYE_FOTOAPPARATY = 'tsifrovye-fotoapparaty'
        PLENOCHNYE_FOTOAPPARATY = 'plenochnye-fotoapparaty'
        VIDEOKAMERY = 'videokamery'
        OBEKTIVY = 'obektivy'
        TELESKOPY_BINOKLI = 'teleskopy-binokli'
        SHTATIVY_MONOPODY = 'shtativy-monopody'
        FOTOVSPYSHKI = 'fotovspyshki'


        KLIMATICHESKOE_OBORUDOVANIE = 'klimaticheskoe-oborudovanie'


        PROCHAJA_ELECTRONIKA = 'prochaja-electronika'


        TV_VIDEOTEHNIKA = 'tv-videotehnika'
        TELEVIZORY = 'televizory'
        AKSESSUARY_DLYA_TV_VIDEOTEHNIKI = 'aksessuary-dlya-tv-videotehniki'
        DVD_PLEERY = 'dvd-pleery'
        PROCHAYA_TV_VIDEOTEHNIKA = 'prochaya-tv-videotehnika'
        SPUTNIKOVOE_TV = 'sputnikovoe-tv'


        AUDIOTEHNIKA = 'audiotehnika'
        AKUSTICHESKIE_SISTEMY = 'akusticheskie-sistemy'
        NAUSHNIKI = 'naushniki'
        PROCHAYA_AUDIOTEHNIKA = 'prochaya-audiotehnika'
        USILITELI_RESIVERY = 'usiliteli-resivery'
        MP3_PLEERY = 'mp3-pleery'
        RADIOPRIEMNIKI = 'radiopriemniki'
        MAGNITOLY = 'magnitoly'
        CD_MD_VINILOVYE_PROIGRYVATELI = 'cd-md-vinilovye-proigryvateli'
        PORTATIVNAYA_AKUSTIKA = 'portativnaya-akustika'
        MUZYKALNYE_TSENTRY = 'muzykalnye-tsentry'


        TEHNIKA_DLYA_KUHNI = 'tehnika-dlya-kuhni'
        PROCHAYA_TEHNIKA_DLYA_KUHNI = 'prochaya-tehnika-dlya-kuhni'
        PLITY_PECHI = 'plity-pechi'
        HOLODILNIKI = 'holodilniki'
        KOFEVARKI_KOFEMOLKI = 'kofevarki-kofemolki'
        KUHONNYE_KOMBAYNY_I_IZMELCHITELI = 'kuhonnye-kombayny-i-izmelchiteli'
        PAROVARKI = 'parovarki'
        VYTYAZHKI = 'vytyazhki'
        ELEKTROCHAYNIKI = 'elektrochayniki'
        MIKROVOLNOVYE_PECHI = 'mikrovolnovye-pechi'
        POSUDOMOECHNYE_MASHINY = 'posudomoechnye-mashiny'
        HLEBOPECHKI = 'hlebopechki'


        TEHNIKA_DLYA_DOMA = 'tehnika-dlya-doma'
        PROCHAYA_TEHNIKA_DLYA_DOMA = 'prochaya-tehnika-dlya-doma'
        STIRALNYE_MASHINY = 'stiralnye-mashiny'
        SHVEYNYE_MASHINY_I_OVERLOKI = 'shveynye-mashiny-i-overloki'
        PYLESOSY = 'pylesosy'
        FILTRY_DLYA_VODY = 'filtry-dlya-vody'
        UTYUGI = 'utyugi'
        VYAZALNYE_MASHINY = 'vyazalnye-mashiny'


        IGRY_I_IGROVYE_PRISTAVKI = 'igry-i-igrovye-pristavki'
        IGRY_DLYA_PRISTAVOK = 'igry-dlya-pristavok'
        PRISTAVKI = 'pristavki'
        AKSESSUARY = 'aksessuary'
        IGRY_DLYA_PC = 'igry-dlya-pc'
        GEROI_IGR = 'geroi-igr'
        REMONT_PRISTAVOK = 'remont-pristavok'


        INDIVIDUALNYY_UHOD = 'individualnyy-uhod'
        BRITVY_EPILYATORY_MASHINKI_DLYA_STRIZHKI = 'britvy-epilyatory-mashinki-dlya-strizhki'
        FENY_UKLADKA_VOLOS = 'feny-ukladka-volos'
        PROCHAYA_TEHNIKA_DLYA_INDIVIDUALNOGO_UHODA = 'prochaya-tehnika-dlya-individualnogo-uhoda'
        VESY = 'vesy'


        KNIGI_ZHURNALY = 'knigi-zhurnaly'


        ANTIKVARIAT_KOLLEKTSII = 'antikvariat-kollektsii'
        KOLLEKTSIONIROVANIE = 'kollektsionirovanie'
        PODELKI_RUKODELIE = 'podelki-rukodelie'
        ZHIVOPIS = 'zhivopis'
        PREDMETY_ISKUSSTVA = 'predmety-iskusstva'
        BUKINISTIKA = 'bukinistika'
        ANTIKVARNAYA_MEBEL = 'antikvarnaya-mebel'


        CD_DVD_PLASTINKI = 'cd-dvd-plastinki'


        SPORT_OTDYH = 'sport-otdyh'
        VELO = 'velo'
        OHOTA_RYBALKA = 'ohota-rybalka'
        ATLETIKA_FITNES = 'atletika-fitnes'
        LYZHI_SNOUBORDY = 'lyzhi-snoubordy'
        TURIZM = 'turizm'
        PROCHIE_VIDY_SPORTA = 'prochie-vidy-sporta'
        FUTBOL = 'futbol'
        EDINOBORSTVA_BOKS = 'edinoborstva-boks'
        NASTOLNYE_IGRY = 'nastolnye-igry'
        VODNYE_VIDY_SPORTA = 'vodnye-vidy-sporta'
        ROLIKOVYE_KONKI = 'rolikovye-konki'
        KONKI = 'konki'
        IGRY_S_RAKETKOY = 'igry-s-raketkoy'
        HOKKEY = 'hokkey'


        MUZYKALNYE_INSTRUMENTY = 'muzykalnye-instrumenty'
        STUDIYNOE_OBORUDOVANIE = 'studiynoe-oborudovanie'
        PROCHEE = 'prochee'
        AKSESSUARY_DLYA_MUZYKALNYH_INSTRUMENTOV = 'aksessuary-dlya-muzykalnyh-instrumentov'
        PIANINO_FORTEPIANO_ROYALI = 'pianino-fortepiano-royali'
        ELEKTROGITARY = 'elektrogitary'
        AKUSTICHESKIE_GITARY = 'akusticheskie-gitary'
        SINTEZATORY = 'sintezatory'
        DUHOVYE_INSTRUMENTY = 'duhovye-instrumenty'
        UDARNYE_INSTRUMENTY = 'udarnye-instrumenty'
        KOMBOUSILITELI = 'kombousiliteli'
        BAS_GITARY = 'bas-gitary'


        DRUGOE = 'drugoe'


        BILETY = 'bilety'


        POISK_GRUPP_MUZYKANTOV = 'poisk-grupp-muzykantov'


        POISK_POPUTCHIKOV = 'poisk-poputchikov'


        PROCHIE_TOVARY_DLYA_DOMA = 'prochie-tovary-dlya-doma'


        POSUDA_KUHONNAYA_UTVAR = 'posuda-kuhonnaya-utvar'


        MEBEL = 'mebel'
        MEBEL_DLYA_GOSTINOY = 'mebel-dlya-gostinoy'
        MEBEL_NA_ZAKAZ = 'mebel-na-zakaz'
        MEBEL_DLYA_SPALNI = 'mebel-dlya-spalni'
        OFISNAYA_MEBEL = 'ofisnaya-mebel'
        KUHONNAYA_MEBEL = 'kuhonnaya-mebel'
        MEBEL_DLYA_PRIHOZHEY = 'mebel-dlya-prihozhey'
        MEBEL_DLYA_VANNOY_KOMNATY = 'mebel-dlya-vannoy-komnaty'


        SAD_OGOROD = 'sad-ogorod'


        PRODUKTY_PITANIYA_NAPITKI = 'produkty-pitaniya-napitki'


        STROITELSTVO_REMONT = 'stroitelstvo-remont'
        OTOPLENIE = 'otoplenie'
        PROCHIE_STROYMATERIALY = 'prochie-stroymaterialy'
        OKNA_STEKLO_ZERKALA = 'okna-steklo-zerkala'
        OTDELOCHNYE_I_OBLITSOVOCHNYE_MATERIALY = 'otdelochnye-i-oblitsovochnye-materialy'
        SANTEHNIKA = 'santehnika'
        ELEKTRIKA = 'elektrika'
        METALLOPROKAT_ARMATURA = 'metalloprokat-armatura'
        KIRPICH_BETON_PENOBLOKI = 'kirpich-beton-penobloki'
        PILOMATERIALY = 'pilomaterialy'
        ELEMENTY_KREPEZHA = 'elementy-krepezha'
        LAKOKRASOCHNYE_MATERIALY = 'lakokrasochnye-materialy'
        VENTILYATSIYA = 'ventilyatsiya'


        KOMNATNYE_RASTENIYA = 'komnatnye-rasteniya'


        HOZYAYSTVENNYY_INVENTAR = 'hozyaystvennyy-inventar'


        PREDMETY_INTERERA = 'predmety-interera'
        SVETILNIKI = 'svetilniki'
        TEKSTIL = 'tekstil'
        DEKOR_OKON = 'dekor-okon'


        INSTRUMENTY = 'instrumenty'
        ELEKTROINSTRUMENT = 'elektroinstrument'
        PROCHIY_INSTRUMENT = 'prochiy-instrument'
        RUCHNOY_INSTRUMENT = 'ruchnoy-instrument'
        BENZOINSTRUMENT = 'benzoinstrument'
        PNEVMOINSTRUMENT = 'pnevmoinstrument'


        KANTSTOVARY_RASHODNYE_MATERIALY = 'kantstovary-rashodnye-materialy'


        SADOVYY_INVENTAR = 'sadovyy-inventar'


        SOBAKI = 'sobaki'


        TOVARY_DLYA_ZHIVOTNYH = 'tovary-dlya-zhivotnyh'


        KOSHKI = 'koshki'


        VYAZKA = 'vyazka'


        SELSKOHOZYAYSTVENNYE_ZHIVOTNYE = 'selskohozyaystvennye-zhivotnye'


        AKVARIUMNYE_RYBKI = 'akvariumnye-rybki'


        PTITSY = 'ptitsy'


        DRUGIE_ZHIVOTNYE = 'drugie-zhivotnye'


        GRYZUNY = 'gryzuny'


        BYURO_NAHODOK = 'byuro-nahodok'


        REPTILII = 'reptilii'


        ROZNICHNAYA_TORGOVLYA_PRODAZHI = 'roznichnaya-torgovlya-prodazhi'


        STROITELSTVO = 'stroitelstvo'


        TRANSPORT_LOGISTIKA = 'transport-logistika'


        DRUGIE_SFERY_ZANYATIY = 'drugie-sfery-zanyatiy'


        DOMASHNIY_PERSONAL = 'domashniy-personal'


        BARY_RESTORANY_RAZVLECHENIYA = 'bary-restorany-razvlecheniya'


        KRASOTA_FITNES_SPORT = 'krasota-fitnes-sport'


        OBRAZOVANIE = 'obrazovanie'


        PROIZVODSTVO_ENERGETIKA = 'proizvodstvo-energetika'


        NACHALO_KARERY_STUDENTY = 'nachalo-karery-studenty'


        MARKETING_REKLAMA_DIZAYN = 'marketing-reklama-dizayn'


        YURISPRUDENTSIYA_I_BUHGALTERIYA = 'yurisprudentsiya-i-buhgalteriya'


        OHRANA_BEZOPASNOST = 'ohrana-bezopasnost'


        CEKRETARIAT_AHO = 'cekretariat-aho'


        SERVIS_I_BYT = 'servis-i-byt'


        IT_TELEKOM_KOMPYUTERY = 'it-telekom-kompyutery'


        NEDVIZHIMOST = 'nedvizhimost'


        MEDITSINA_FARMATSIYA = 'meditsina-farmatsiya'


        KULTURA_ISKUSSTVO = 'kultura-iskusstvo'


        TURIZM_OTDYH_RAZVLECHENIYA = 'turizm-otdyh-razvlecheniya'


    class AREA:
        UKRAINE = 'ukraine'
        VINNITSA_AREA = 'vin'
        VOLYNSK_AREA = 'vol'
        DNEPROPETROVSK_AREA = 'dnp'
        DONETSK_AREA = 'don'
        ZHITOMIR_AREA = 'zht'
        ZAKARPATSKA_AREA = 'zak'
        ZAPOROZHE_AREA = 'zap'
        IVANO_FRANKOVSK_AREA = 'if'
        KIEV_AREA = 'ko'
        KIROVOGRAD_AREA = 'kir'
        KRYM_AREA = 'cri'
        LUGANSK_AREA = 'lug'
        LVOV_AREA = 'lv'
        NIKOLAEV_AREA = 'nik'
        ODESSA_AREA = 'od'
        POLTAVA_AREA = 'pol'
        ROVNO_AREA = 'rov'
        SUMY_AREA = 'sum'
        TERNOPOL_AREA = 'ter'
        KHARKOV_AREA = 'kha'
        KHERSON_AREA = 'khe'
        KHMELNITSKIY_AREA = 'khm'
        CHERKASSY_AREA = 'chk'
        CHERNIGOV_AREA = 'chn'
        CHERNOVTSY_AREA = 'chv'

    class CITY:
        BAR = 'bar'
        BERSHAD = 'bershad'
        VINNITSA = 'vinnitsa'
        GAYSIN = 'gaysin'
        GNIVAN = 'gnivan'
        ZHMERINKA = 'zhmerinka'
        ILINTSY = 'ilintsy'
        KAZATIN = 'kazatin'
        KALINOVKA = 'kalinovka'
        KRYZHOPOL = 'kryzhopol'
        LADYZHIN = 'ladyzhin'
        LIPOVETS = 'lipovets'
        MOGILYEV_PODOLSKIY = 'mogilyev-podolskiy'
        NEMIROV = 'nemirov'
        POGREBISHCHE = 'pogrebishche'
        STRIZHAVKA = 'strizhavka'
        TULCHIN = 'tulchin'
        KHMELNIK = 'khmelnik'
        CHECHELNIK = 'chechelnik'
        SHARGOROD = 'shargorod'
        YAMPOL = 'yampol'
        BERESTECHKO = 'berestechko'
        VLADIMIR_VOLYNSKIY = 'vladimir-volynskiy'
        GOROKHOV = 'gorokhov'
        IVANICHI = 'ivanichi'
        KAMEN_KASHIRSKIY = 'kamen-kashirskiy'
        KIVERTSY = 'kivertsy'
        KOVEL = 'kovel'
        LUTSK = 'lutsk'
        LYUBESHOV = 'lyubeshov'
        LYUBOML = 'lyuboml'
        MANEVICHI = 'manevichi'
        NOVOVOLYNSK = 'novovolynsk'
        RATNO = 'ratno'
        ROZHISHCHE = 'rozhishche'
        STARAYA_VYZHEVKA = 'staraya-vyzhevka'
        TURIYSK = 'turiysk'
        USTILUG = 'ustilug'
        TSUMAN = 'tsuman'
        SHATSK = 'shatsk'
        APOSTOLOVO = 'apostolovo'
        VERKHNEDNEPROVSK = 'verkhnedneprovsk'
        VOLNOGORSK = 'volnogorsk'
        DNEPRODZERZHINSK = 'dneprodzerzhinsk'
        DNEPROPETROVSK = 'dnepropetrovsk'
        ZHYELTYE_VODY = 'zhyeltye-vody'
        KRIVOYROG = 'krivoyrog'
        MARGANETS = 'marganets'
        NIKOPOL = 'nikopol'
        NOVOMOSKOVSK = 'novomoskovsk'
        ORDZHONIKIDZE = 'ordzhonikidze'
        PAVLOGRAD = 'pavlograd'
        PERESHCHEPINO = 'pereshchepino'
        PERSHOTRAVENSK = 'pershotravensk'
        PODGORODNOE = 'podgorodnoe'
        PYATIKHATKI = 'pyatikhatki'
        SINELNIKOVO = 'sinelnikovo'
        TERNOVKA = 'ternovka'
        AVDEEVKA = 'avdeevka'
        ALEKSANDROVKA_DON = 'aleksandrovka_don'
        AMVROSIEVKA = 'amvrosievka'
        ARTYEMOVSK_DON = 'artyemovsk_don'
        VOLNOVAKHA = 'volnovakha'
        GORLOVKA = 'gorlovka'
        DEBALTSEVO = 'debaltsevo'
        DZERZHINSK = 'dzerzhinsk'
        DIMITROV = 'dimitrov'
        DOBROPOLE = 'dobropole'
        DOKUCHAEVSK = 'dokuchaevsk'
        DONETSK = 'donetsk'
        DRUZHKOVKA = 'druzhkovka'
        ENAKIEVO = 'enakievo'
        ZHDANOVKA = 'zhdanovka'
        ZUGRES = 'zugres'
        KIROVSKOE = 'kirovskoe'
        KONSTANTINOVKA = 'konstantinovka'
        KRAMATORSK = 'kramatorsk'
        KRASNOARMEYSK = 'krasnoarmeysk'
        KRASNYYLIMAN = 'krasnyyliman'
        MAYORSK = 'mayorsk'
        MAKEEVKA = 'makeevka'
        MARIUPOL = 'mariupol'
        MARINKA = 'marinka'
        NOVOAZOVSK = 'novoazovsk'
        NOVOGRODOVKA = 'novogrodovka'
        SELIDOVO = 'selidovo'
        SLAVYANSK = 'slavyansk'
        SNEZHNOE = 'snezhnoe'
        SOLEDAR = 'soledar'
        STAROBESHEVO = 'starobeshevo'
        TOREZ = 'torez'
        UGLEDAR = 'ugledar'
        KHARTSYZSK = 'khartsyzsk'
        SHAKHTYERSK = 'shakhtyersk'
        YASINOVATAYA = 'yasinovataya'
        ANDRUSHEVKA = 'andrushevka'
        BARANOVKA = 'baranovka'
        BERDICHEV = 'berdichev'
        VOLODARSK_VOLYNSKIY = 'volodarsk-volynskiy'
        YEMILCHINO = 'yemilchino'
        ZHITOMIR = 'zhitomir'
        IRSHANSK = 'irshansk'
        KOROSTEN = 'korosten'
        KOROSTYSHEV = 'korostyshev'
        MALIN = 'malin'
        NOVOGRAD_VOLYNSKIY = 'novograd-volynskiy'
        OVRUCH = 'ovruch'
        OLEVSK = 'olevsk'
        POPELNYA = 'popelnya'
        RADOMYSHL = 'radomyshl'
        ROMANOV = 'romanov'
        CHERNYAHOV = 'chernyahov'
        BEREGOVO = 'beregovo'
        BUSHTYNA = 'bushtyna'
        VELIKIY_BYCHKOV = 'velikiy-bychkov'
        VINOGRADOV = 'vinogradov'
        VYSHKOVO = 'vyshkovo'
        DUBOVOYE = 'dubovoye'
        IRSHAVA = 'irshava'
        KOROLEVO = 'korolevo'
        MEZHGORYE = 'mezhgorye'
        MUKACHEVO = 'mukachevo'
        PERECHIN = 'perechin'
        RAKHOV = 'rakhov'
        SVALYAVA = 'svalyava'
        SOLOTVINA = 'solotvina'
        TYACHEV = 'tyachev'
        UZHGOROD = 'uzhgorod'
        KHUST = 'khust'
        CHOP = 'chop'
        AKIMOVKA = 'akimovka'
        BERDYANSK = 'berdyansk'
        VASILEVKA = 'vasilevka'
        VESELOE = 'veseloe'
        VOLNYANSK = 'volnyansk'
        GULYAYPOLE = 'gulyaypole'
        DNEPRORUDNOE = 'dneprorudnoe'
        ZAPOROZHE = 'zaporozhe'
        KAMENKA_DNEPROVSKAYA = 'kamenka-dneprovskaya'
        KUYBYSHEVO = 'kuybyshevo'
        KUSHUGUM = 'kushugum'
        MELITOPOL = 'melitopol'
        MIHAYLOVKA = 'mihaylovka'
        MOLOCHANSK = 'molochansk'
        OREKHOV = 'orekhov'
        POLOGI = 'pologi'
        PRIMORSK = 'primorsk'
        ROZOVKA = 'rozovka'
        TOKMAK = 'tokmak'
        ENERGODAR = 'energodar'
        BOGORODCHANY = 'bogorodchany'
        BOLEKHOV = 'bolekhov'
        BURSHTYN = 'burshtyn'
        GALICH = 'galich'
        GORODENKA = 'gorodenka'
        DELYATIN = 'delyatin'
        DOLINA = 'dolina'
        IVANO_FRANKOVSK = 'ivano-frankovsk'
        KALUSH = 'kalush'
        KOLOMYYA = 'kolomyya'
        KOSOV = 'kosov'
        LANCHIN = 'lanchin'
        NADVORNAYA = 'nadvornaya'
        PEREGINSKOYE = 'pereginskoye'
        ROGATIN = 'rogatin'
        SNYATYN = 'snyatyn'
        TLUMACH = 'tlumach'
        TYSMENITSA = 'tysmenitsa'
        YAREMCHE = 'yaremche'
        BARYSHEVKA = 'baryshevka'
        BELAYATSERKOV = 'belayatserkov'
        BEREZAN = 'berezan'
        BOGUSLAV = 'boguslav'
        BORISPOL = 'borispol'
        BORODYANKA = 'borodyanka'
        BOYARKA = 'boyarka'
        BROVARY = 'brovary'
        BUCHA = 'bucha'
        VASILKOV = 'vasilkov'
        VISHNYEVOE = 'vishnyevoe'
        VOLODARKA = 'volodarka'
        VYSHGOROD = 'vyshgorod'
        GLEVAXA = 'glevaxa'
        GOSTOMEL = 'gostomel'
        IVANKOV = 'ivankov'
        IRPEN = 'irpen'
        KAGARLYK = 'kagarlyk'
        KIEV = 'kiev'
        KOTSYUBINSKOE = 'kotsyubinskoe'
        MAKAROV = 'makarov'
        MIRONOVKA = 'mironovka'
        OBUKHOV = 'obukhov'
        PEREYASLAV_KHMELNITSKIY = 'pereyaslav-khmelnitskiy'
        PRIPYAT = 'pripyat'
        RZHISHCHEV = 'rzhishchev'
        ROKITNOE_KIEV = 'rokitnoe_kiev'
        SKVIRA = 'skvira'
        SLAVUTICH = 'slavutich'
        TARASHCHA = 'tarashcha'
        TETIEV = 'tetiev'
        UZIN = 'uzin'
        UKRAINKA = 'ukrainka'
        FASTOV = 'fastov'
        CHERNOBYL = 'chernobyl'
        YAGOTIN = 'yagotin'
        ALEKSANDRIYA = 'aleksandriya'
        BOBRINETS = 'bobrinets'
        VLASOVKA = 'vlasovka'
        GAYVORON = 'gayvoron'
        DOLINSKAYA = 'dolinskaya'
        ZNAMENKA = 'znamenka'
        KIROVOGRAD = 'kirovograd'
        MALAYAVISKA = 'malayaviska'
        NOVAYA_PRAGA = 'novaya-praga'
        NOVOARKHANGELSK = 'novoarkhangelsk'
        NOVOYE = 'novoye'
        NOVOMIRGOROD = 'novomirgorod'
        NOVOUKRAINKA = 'novoukrainka'
        PETROVO = 'petrovo'
        POMOSHNAYA = 'pomoshnaya'
        SVETLOVODSK = 'svetlovodsk'
        SMOLINO = 'smolino'
        ALUPKA = 'alupka'
        ALUSHTA = 'alushta'
        ARMYANSK = 'armyansk'
        BAKHCHISARAY = 'bakhchisaray'
        BELOGORSK = 'belogorsk'
        DZHANKOY = 'dzhankoy'
        EVPATORIYA = 'evpatoriya'
        INKERMAN = 'inkerman'
        KERCH = 'kerch'
        KRASNOGVARDEISKOE = 'krasnogvardeiskoe'
        KRASNOPEREKOPSK = 'krasnoperekopsk'
        RAZDOLNOYE = 'razdolnoye'
        SAKI = 'saki'
        SEVASTOPOL = 'sevastopol'
        SIMFEROPOL = 'simferopol'
        STARYY_KRYM = 'staryy-krym'
        SUDAK = 'sudak'
        FEODOSIYA = 'feodosiya'
        CHERNOMORSKOYE = 'chernomorskoye'
        SHCHYELKINO = 'shchyelkino'
        YALTA = 'yalta'
        ALEKSANDROVSK = 'aleksandrovsk'
        ALMAZNAYA = 'almaznaya'
        ALCHEVSK = 'alchevsk'
        ANTRATSIT = 'antratsit'
        ARTYEMOVSK_LUG = 'artyemovsk_lug'
        BRYANKA = 'bryanka'
        VAKHRUSHEVO = 'vakhrushevo'
        GORNOE = 'gornoe'
        GORSKOE = 'gorskoe'
        ZIMOGORE = 'zimogore'
        ZOLOTOE = 'zolotoe'
        ZORINSK = 'zorinsk'
        KIROVSK = 'kirovsk'
        KRASNODON = 'krasnodon'
        KRASNOPARTIZANSK = 'krasnopartizansk'
        KRASNYYLUCH = 'krasnyyluch'
        KREMENNAYA = 'kremennaya'
        LISICHANSK = 'lisichansk'
        LUGANSK = 'lugansk'
        LUTUGINO = 'lutugino'
        MIUSINSK = 'miusinsk'
        MOLODOGVARDEYSK = 'molodogvardeysk'
        NOVODRUZHESK = 'novodruzhesk'
        NOVOPSKOV = 'novopskov'
        PERVOMAYSK_LUG = 'pervomaysk_lug'
        PEREVALSK = 'perevalsk'
        PETROVSKOE = 'petrovskoe'
        POPASNAYA = 'popasnaya'
        PRIVOLE = 'privole'
        ROVENKI = 'rovenki'
        RUBEZHNOE = 'rubezhnoe'
        SVATOVO = 'svatovo'
        SVERDLOVSK = 'sverdlovsk'
        SEVERODONETSK = 'severodonetsk'
        STANICA_LUGANSKAYA = 'stanica-luganskaya'
        STAROBELSK = 'starobelsk'
        STAKHANOV = 'stakhanov'
        SUKHODOLSK = 'sukhodolsk'
        SCHASTE = 'schaste'
        TEPLOGORSK = 'teplogorsk'
        CHERVONOPARTIZANSK = 'chervonopartizansk'
        BELZ = 'belz'
        BOBRKA = 'bobrka'
        BORISLAV = 'borislav'
        BRODY = 'brody'
        BUSK = 'busk'
        VELIKIE_MOSTY = 'velikie-mosty'
        VINNIKI = 'vinniki'
        GLINYANY = 'glinyany'
        GORODOK_LV = 'gorodok_lv'
        DOBROMIL = 'dobromil'
        DROGOBYCH = 'drogobych'
        DUBLYANY = 'dublyany'
        ZHIDACHEV = 'zhidachev'
        ZHOLKVA = 'zholkva'
        ZOLOCHEV = 'zolochev'
        KAMENKA_BUGSKAYA = 'kamenka-bugskaya'
        LVOV = 'lvov'
        MOSTISKA = 'mostiska'
        NIKOLAEV_LV = 'nikolaev_lv'
        NOVOYAVOROVSK = 'novoyavorovsk'
        NOVYY_ROZDOL = 'novyy-rozdol'
        PEREMYSHLYANE = 'peremyshlyane'
        PUSTOMYTY = 'pustomyty'
        RAVA_RUSSKAYA = 'rava-russkaya'
        RADEKHOV = 'radekhov'
        RUDKI = 'rudki'
        SAMBOR = 'sambor'
        SKOLE = 'skole'
        SOKAL = 'sokal'
        SOSNOVKA = 'sosnovka'
        STARYY_SAMBOR = 'staryy-sambor'
        STEBNIK = 'stebnik'
        STRYY = 'stryy'
        TRUSKAVETS = 'truskavets'
        UGNEV = 'ugnev'
        KHYROV = 'khyrov'
        CHERVONOGRAD = 'chervonograd'
        YAVOROV = 'yavorov'
        ALEKSANDROVKA_NIK = 'aleksandrovka_nik'
        ARBUZINKA = 'arbuzinka'
        BASHTANKA = 'bashtanka'
        BEREZNEGOVATOYE = 'bereznegovatoye'
        BRATSKOYE = 'bratskoye'
        VESELINOVO = 'veselinovo'
        VOZNESENSK = 'voznesensk'
        VRADIEVKA = 'vradievka'
        DOMANEVKA = 'domanevka'
        YELANETS = 'yelanets'
        KAZANKA = 'kazanka'
        KRIVOE_OZERO = 'krivoe-ozero'
        NIKOLAEV = 'nikolaev'
        NOVAYA_ODESSA = 'novaya-odessa'
        NOVYYBUG = 'novyybug'
        OCHAKOV = 'ochakov'
        PERVOMAYSK_NIK = 'pervomaysk_nik'
        SNIGIREVKA = 'snigirevka'
        YUZHNOUKRAINSK = 'yuzhnoukrainsk'
        ANANEV = 'ananev'
        ARTSIZ = 'artsiz'
        BALTA = 'balta'
        BELGOROD_DNESTROVSKIY = 'belgorod-dnestrovskiy'
        BELYAEVKA = 'belyaevka'
        BEREZOVKA = 'berezovka'
        BOLGRAD = 'bolgrad'
        VELIKODOLINSKOE = 'velikodolinskoe'
        IZMAIL = 'izmail'
        ILICHYEVSK = 'ilichyevsk'
        KILIYA = 'kiliya'
        KODYMA = 'kodyma'
        KOTOVSK = 'kotovsk'
        LYUBASHEVKA = 'lyubashevka'
        OVIDIOPOL = 'ovidiopol'
        ODESSA = 'odessa'
        RAZDELNAYA = 'razdelnaya'
        RENI = 'reni'
        TATARBUNARY = 'tatarbunary'
        TEPLODAR = 'teplodar'
        SHIRYAEVO = 'shiryaevo'
        YUZHNOE = 'yuzhnoe'
        GADYACH = 'gadyach'
        GLOBINO = 'globino'
        GRADIZHSK = 'gradizhsk'
        GREBYENKA = 'grebyenka'
        DIKANKA = 'dikanka'
        ZENKOV = 'zenkov'
        KARLOVKA = 'karlovka'
        KOBELYAKI = 'kobelyaki'
        KOMSOMOLSK = 'komsomolsk'
        KOTELVA = 'kotelva'
        KREMENCHUG = 'kremenchug'
        LOKHVITSA = 'lokhvitsa'
        LUBNY = 'lubny'
        MIRGOROD = 'mirgorod'
        NOVYYE_SANZHARY = 'novyye-sanzhary'
        PIRYATIN = 'piryatin'
        POLTAVA = 'poltava'
        RESHETILOVKA = 'reshetilovka'
        KHOROL = 'khorol'
        CHERVONOZAVODSKOE = 'chervonozavodskoe'
        CHUTOVO = 'chutovo'
        BEREZNE = 'berezne'
        VLADIMIRETS = 'vladimirets'
        DUBNO = 'dubno'
        DUBROVITSA = 'dubrovitsa'
        ZARECHNOYE = 'zarechnoye'
        ZDOLBUNOV = 'zdolbunov'
        KVASILOV = 'kvasilov'
        KLEVAN = 'klevan'
        KORETS = 'korets'
        KOSTOPOL = 'kostopol'
        KUZNETSOVSK = 'kuznetsovsk'
        MLINOV = 'mlinov'
        OSTROG = 'ostrog'
        RADIVILOV = 'radivilov'
        ROVNO = 'rovno'
        ROKITNOE_ROV = 'rokitnoe_rov'
        SARNY = 'sarny'
        AKHTYRKA = 'akhtyrka'
        BELOPOLE = 'belopole'
        BURYN = 'buryn'
        VOROZHBA = 'vorozhba'
        VORONEZH = 'voronezh'
        GLUKHOV = 'glukhov'
        DRUZHBA = 'druzhba'
        KONOTOP = 'konotop'
        KRASNOPOLYE = 'krasnopolye'
        KROLEVETS = 'krolevets'
        LEBEDIN = 'lebedin'
        PUTIVL = 'putivl'
        ROMNY = 'romny'
        SVESSA = 'svessa'
        SEREDINA_BUDA = 'seredina-buda'
        SUMY = 'sumy'
        TROSTYANETS = 'trostyanets'
        SHOSTKA = 'shostka'
        BEREZHANY = 'berezhany'
        BORSHCHYEV = 'borshchyev'
        BUCHACH = 'buchach'
        VELIKAYA_BEREZOVITSA = 'velikaya-berezovitsa'
        GUSYATIN = 'gusyatin'
        ZALESHCHIKI = 'zaleshchiki'
        ZBARAZH = 'zbarazh'
        ZBOROV = 'zborov'
        KOZOVA = 'kozova'
        KOPYCHINTSY = 'kopychintsy'
        KREMENETS = 'kremenets'
        LANOVTSY = 'lanovtsy'
        MONASTYRISKA = 'monastyriska'
        PODVOLOCHISK = 'podvolochisk'
        PODGAYTSY = 'podgaytsy'
        POCHAEV = 'pochaev'
        SKALAT = 'skalat'
        TEREBOVLYA = 'terebovlya'
        TERNOPOL = 'ternopol'
        KHOROSTKOV = 'khorostkov'
        CHORTKOV = 'chortkov'
        SHUMSK = 'shumsk'
        BALAKLEYA = 'balakleya'
        BARVENKOVO = 'barvenkovo'
        BOGODUKHOV = 'bogodukhov'
        VALKI = 'valki'
        VELIKIY_BURLUK = 'velikiy-burluk'
        VOLCHANSK = 'volchansk'
        VYSOKIY = 'vysokiy'
        DERGACHI = 'dergachi'
        ZMIEV = 'zmiev'
        IZYUM = 'izyum'
        KOMSOMOLSKOYE = 'komsomolskoye'
        KRASNOGRAD = 'krasnograd'
        KUPYANSK = 'kupyansk'
        LOZOVAYA = 'lozovaya'
        LYUBOTIN = 'lyubotin'
        MEREFA = 'merefa'
        NOVAYA_VODOLAGA = 'novaya-vodolaga'
        PERVOMAYSKIY = 'pervomayskiy'
        PESOCHIN = 'pesochin'
        SOLONITSEVKA = 'solonitsevka'
        KHARKOV = 'kharkov'
        CHUGUEV = 'chuguev'
        ANTONOVKA = 'antonovka'
        BELOZERKA = 'belozerka'
        BERISLAV = 'berislav'
        VELIKAYA_ALEKSANDROVKA = 'velikaya-aleksandrovka'
        VELIKAYA_LEPETIKHA = 'velikaya-lepetikha'
        GENICHESK = 'genichesk'
        GOLAYA_PRISTAN = 'golaya-pristan'
        KALANCHAK = 'kalanchak'
        KAMYSHANY = 'kamyshany'
        KAKHOVKA = 'kakhovka'
        NOVAYA_KAKHOVKA = 'novaya-kakhovka'
        NOVAYA_MAYACHKA = 'novaya-mayachka'
        NOVOALEKSEYEVKA = 'novoalekseyevka'
        NOVOTROITSKOYE = 'novotroitskoye'
        SKADOVSK = 'skadovsk'
        TAVRIYSK = 'tavriysk'
        KHERSON = 'kherson'
        TSYURUPINSK = 'tsyurupinsk'
        CHAPLINKA = 'chaplinka'
        VINKOVTSY = 'vinkovtsy'
        VOLOCHISK = 'volochisk'
        GORODOK_KHM = 'gorodok_khm'
        DERAZHNYA = 'derazhnya'
        DUNAEVTSY = 'dunaevtsy'
        IZYASLAV = 'izyaslav'
        KAMENETS_PODOLSKIY = 'kamenets-podolskiy'
        KRASILOV = 'krasilov'
        LETICHEV = 'letichev'
        NETESHIN = 'neteshin'
        POLONNOE = 'polonnoe'
        PONINKA = 'poninka'
        SLAVUTA = 'slavuta'
        STAROKONSTANTINOV = 'starokonstantinov'
        TEOFIPOL = 'teofipol'
        KHMELNITSKIY = 'khmelnitskiy'
        SHEPETOVKA = 'shepetovka'
        VATUTINO = 'vatutino'
        GORODISHCHE = 'gorodishche'
        DRABOV = 'drabov'
        ZHASHKOV = 'zhashkov'
        ZVENIGORODKA = 'zvenigorodka'
        ZOLOTONOSHA = 'zolotonosha'
        KAMENKA = 'kamenka'
        KANEV = 'kanev'
        KORSUN_SHEVCHENKOVSKIY = 'korsun-shevchenkovskiy'
        LYSYANKA = 'lysyanka'
        MANKOVKA = 'mankovka'
        MONASTYRISHCHE = 'monastyrishche'
        SMELA = 'smela'
        TALNOE = 'talnoe'
        UMAN = 'uman'
        KHRISTINOVKA = 'khristinovka'
        CHERKASSY = 'cherkassy'
        CHERNOBAY = 'chernobay'
        CHIGIRIN = 'chigirin'
        SHPOLA = 'shpola'
        BAKHMACH = 'bakhmach'
        BOBROVITSA = 'bobrovitsa'
        BORZNA = 'borzna'
        GORODNYA = 'gorodnya'
        DESNA = 'desna'
        ICHNYA = 'ichnya'
        KOZELETS = 'kozelets'
        KORYUKOVKA = 'koryukovka'
        MENA = 'mena'
        NEZHIN = 'nezhin'
        NOVGOROD_SEVERSKIY = 'novgorod-severskiy'
        NOSOVKA = 'nosovka'
        PRILUKI = 'priluki'
        SEDNEV = 'sednev'
        SEMYENOVKA = 'semyenovka'
        CHERNIGOV = 'chernigov'
        SHCHORS = 'shchors'
        BEREGOMET = 'beregomet'
        VASHKOVTSY = 'vashkovtsy'
        VIZHNITSA = 'vizhnitsa'
        GERTSA = 'gertsa'
        GLYBOKAYA = 'glybokaya'
        ZASTAVNA = 'zastavna'
        KELMENTSY = 'kelmentsy'
        KITSMAN = 'kitsman'
        KRASNOILSK = 'krasnoilsk'
        NOVODNESTROVSK = 'novodnestrovsk'
        NOVOSELITSA = 'novoselitsa'
        PUTILA = 'putila'
        SOKIRYANY = 'sokiryany'
        STOROZHINETS = 'storozhinets'
        KHOTIN = 'khotin'
        CHERNOVTSY = 'chernovtsy'


    class CURRENCY:
        UAH = 'uah'
        DOLLAR = 'dollar'

    class GENDER:
        MALE = 'male'
        FEMALE = 'female'

    class LANGUAGE:
        UA = 'ua'
        RU = 'ru'

    class CONDITION:
        NEW = 'new'
        USED = 'used'

    class SELLER_TYPE:
        BUSINESS = 'business'
        PRIVATE = 'private'

    class SITE:
        OLX = 'olx.ua'

    class FREQUENCY:
        HOURS24 = '24 hr'
        HOURS6 = '6 hr'
        MINUTE1 = '1 min'


class SETS:
    CITY = set([DB_ENUMS.CITY.__dict__[attr] for attr in dir(DB_ENUMS.CITY) if not attr.startswith('__')])
    AREA = set([DB_ENUMS.AREA.__dict__[attr] for attr in dir(DB_ENUMS.AREA) if not attr.startswith('__')])
    LOCATION = CITY.union(AREA)
    CATEGORY = set([DB_ENUMS.CATEGORY.__dict__[attr] for attr in dir(DB_ENUMS.CATEGORY) if not attr.startswith('__')])
    SUBCATEGORY = set([DB_ENUMS.SUBCATEGORY.__dict__[attr] for attr in dir(DB_ENUMS.SUBCATEGORY)
                       if not attr.startswith('__')])
    GENDER = set([DB_ENUMS.GENDER.__dict__[attr] for attr in dir(DB_ENUMS.GENDER) if not attr.startswith('__')])
    CONDITION = set([DB_ENUMS.CATEGORY.__dict__[attr] for attr in dir(DB_ENUMS.CATEGORY) if not attr.startswith('__')])
    NON_FILTERS = set([DB_KEYS.NON_FILTERS.__dict__[attr] for attr in dir(DB_KEYS.NON_FILTERS) if not attr.startswith('__')])
    CATEGORY_TYPES = {'category', 'subcategory', 'subsubcategory'}
    FIELDS = {'q', 'pmin', 'pmax', 'seller'}


STANDART_PARAMS = {'location', 'category', 'subcategory', 'subsubcategory', 'q', 'pmin', 'pmax', 'sostoyanie',
                   'save-params', 'csrfmiddlewaretoken'}.union(SETS.NON_FILTERS)