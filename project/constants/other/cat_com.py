#-*- coding: utf-8 -*- 
#!/usr/bin/env python
#-*- coding: utf-8 -*-

catg = (
('Bytovaya technika', ( #NOT nullable
    ('Klimaticheskaya tehnika', ( #NOT nullable
        ('Kondicionery'), #nullable
        ('Ventilyatory'),
        ('Otoplenie'),
        ('Vozduhoochistiteli, uvlazhniteli'),
        ('Drugaya tehnika'),
        ('Aksessuary i komplektuyushchie') ) 
    ),
    ('Televizory / Videotehnika', ( #NOT nullable
        ('Proigryvateli'), #nullable
        ('Televizory', (
            ('LG'), #filter
            ('Panasonic'),
            ('Philips'),
            ('Samsung'),
            ('Sony'),
            ('Toshiba'),
            ('Drugaya marka') )
        ),
        ('Aksessuary dlya TV / videotehniki'),
        ('Sputnikovoe TV'),
        ('Drugaya TV / videotehnika') )
    ),
    ('Tehnika dlya kuhni', (#NOT nullable
        ('Mikrovolnovye pechi'), #nullable
        ('Holodilniki'),
        ('Plity / pechi'),
        ('Elektrochajniki'),
        ('Kofevarki / kofemolki'),
        ('Kuhonnye kombajny i izmelchiteli'),
        ('Parovarki, multivarki'),
        ('Hlebopechki'),
        ('Posudomoechnye mashiny'),
        ('Vytyazhki'),
        ('Drugaya tehnika dlya kuhni') )
    ),
    ('Tehnika dlya doma', ( #NOT nullable
        ('Pylesosy'), #nullable
        ('Utyugi'),
        ('Stiralnye mashiny'),
        ('SHvejnye mashiny i overloki'),
        ('vyazalnye mashiny'),
        ('Filtry dlya vody'),
        ('Drugaya tehnika dlya doma'),
        ('Vodonagrevateli'),
        ('Aksessuary') )
    ),
    ('Tehnika dlya uhoda za soboj', ( #NOT nullable
        ('Britvy, epilyatory, mashinki dlya strizhki'),
        ('Feny, ukladka volos'),
        ('Vesy'),
        ('Drugaya tehnika dlya individualnoguhoda') )
    ),
    ('Prochee / drugaya tehnika') ) #NOT nullable
),
('Elektronika', ( #NOT nullable
    ('audiotehnika', ( #NOT nullable
        ('Mp3 pleery', (#nullable
            ('Ergo'), #filter
            ('iPod'),
            ('iRiver'),
            ('Samsung'),
            ('Sony'),
            ('TeXet'),
            ('Transcend'),
            ('Drugaya marka') )
        ),
        ('Magnitoly'),
        ('Muzykalnye centry'),
        ('Akusticheskie sistemy'),
        ('Naushniki'),
        ('Radiopriemniki'),
        ('Portativnaya akustika'),
        ('Usiliteli / resivery'),
        ('Cd / md / vinilovye proigryvateli'),
        ('Prochaya audiotehnika') )
    ),
    ('Fot/ videotehnika', ( #NOT nullable
        ('Plenochnye fotoapparaty'), #nullable
        ('Cifrovye fotoapparaty'),
        ('Videokamery'),
        ('Obektivy'),
        ('SHtativy / monopody'),
        ('Fotovspyshki'),
        ('Fotoramki, fotoalbomy'),
        ('Aksessuary, oborudovanie'),
        ('Teleskopy / binokli'),
        ('Zapchasti i komplektuyushchie'),
        ('Drugoe') ) 
    ),
    ('Igry, pristavki, PO', (#NOT nullable
        ('Igry dlya pristavok'), #nullable
        ('tehnika'),
        ('Aksessuary'),
        ('Remont pristavok'),
        ('Drugoe'),
        ('Igry dlya PK'),
        ('Operacionnye sistemy'),
        ('Ofisnye prilozheniya'),
        ('Utility, antivirusy') ) 
    ),
    ('Komplektuyushchie i aksessuary', ( #NOT nullable
        ('Komplektuyushchie'), #nullable
        ('Moduli pamyati'),
        ('Videokarty'),
        ('Zvukovye karty'),
        ('Materinskie platy'),
        ('Opticheskie privody'),
        ('ZHestkie diski'),
        ('Korpusa'),
        ('Processory'),
        ('TV-tyunery'),
        ('Bloki pitaniya'),
        ('Sistemy ohlazhdeniya'),
        ('Operativnaya pamyat') ) 
    ),
    ('Noutbuki', (#NOT nullable
#where is no subcategory! that's no good
        ('Acer'), #filter
        ('Apple'),
        ('Asus'),
        ('Dell'),
        ('Fujitsu'),
        ('HP'),
        ('Lenovo'),
        ('LG'),
        ('MSI'),
        ('Panasonic'),
        ('Samsung'),
        ('Sony'),
        ('Toshiba'),
        ('Drugie modeli') ) 
    ),
    ('PK i orgtehnika', ( #NOT nullable
        ('PK'), #nullable
        ('Printery, skanery, faksy'),
        ('Myshki, klaviatura'),
        ('Fleshki, zhestkie diski'),
        ('Setevoe oborudovanie'),
        ('Monitory') ) 
    ),
    ('Elektronnye knigi', ( #NOT nullable
        ('KPK'),
        ('Aksessuary') ) 
    ),
    ('Telefony i planshety', ( #NOT nullable
        ('Planshety', ( #nullable
            ('Acer'), #filter
            ('Ainol'),
            ('Archos'),
            ('Asus'),
            ('Cube'),
            ('Dell'),
            ('Flytouch'),
            ('FreeLander'),
            ('HP'),
            ('HTC'),
            ('Hyundai'),
            ('ICOO'),
            ('Impression'),
            ('iPad'),
            ('Lenovo'),
            ('LG'),
            ('Onda'),
            ('Pipo'),
            ('Prestigio'),
            ('Samsung'),
            ('Sanei'),
            ('Sony'),
            ('Zenithink'),
            ('Drugie modeli') ) 
        ),
        ('Remont i proshivka'),
        ('Aksessuary', ( #nullable
            ('Akkumulyatory'), #UFO
            ('Antenny, usiliteli'),
            ('Dinamiki, mikrofony'),
            ('Displei, sensornye ekrany'),
            ('Drugoe'),
            ('Zaryadnye ustrojstva'),
            ('Kabeli'),
            ('Karty pamyati'),
            ('Klaviatury'),
            ('Korpusa'),
            ('Mikroshemy'),
            ('Plenki dlya ekranov'),
            ('Provodnye garnitury, naushniki'),
            ('Razemy, gnezda'),
            ('CHehly i futlyary'),
            ('SHlejfy, perehodniki') ) 
        ),
        ('Mobilnye telefony', (#nullable
            ('Alcatel'), #filter
            ('Apple iPhone'),
            ('Asus'),
            ('BenQ'),
            ('Siemens'),
            ('BlackBerry'),
            ('Cubot'),
            ('Fly'),
            ('HTC'),
            ('Huawei'),
            ('Jiayu'),
            ('Lenovo'),
            ('LG'),
            ('Motorola'),
            ('Nokia'),
            ('Pantech'),
            ('Philips'),
            ('Prestigio'),
            ('Samsung'),
            ('Sony, Sony Ericsson'),
            ('ThL'),
            ('Xiaomi'),
            ('ZTE'),
            ('Drugie modeli') ) 
    )
    ),
    ('Stacionarnye telefony'), #NOT nullable
    ('Sim-karty / tarify / nomera', ( #NOT nullable
        ('Beeline'), #i think it's ok enough to be nullable subcategory
        ('Life'),
        ('PEOPLEnet'),
        ('Utel'),
        ('Drugoe'),
        ('Kievstar, Djuice'),
        ('MTS') ) 
    ),
    ('Drugie telefony, faksy') ) ) #NOT nullable
),
('Odezhda, obuv', (#NOT nullable
    ('ZHenskaya odezhda', (#NOT nullable
        ('Odezhda dlya beremennyh'), #nullable
        ('Bluzki, rubashki'),
        ('Bryuki'),
        ('Verhnyaya odezhda'),
        ('ZHilety'),
        ('Kostyumy'),
        ('kofty'),
        ('Kupalniki, pareo'),
        ('Losiny, legginsy'),
        ('Majki'),
        ('Nizhnee belyo'),
        ('Pidzhaki'),
        ('Platya'),
        ('Sarafany'),
        ('Svitera'),
        ('Topy'),
        ('Tuniki'),
        ('Futbolki'),
        ('Hudi, tolstovki'),
        ('Futbolka'),
        ('YUbki'),
        ('Drugoe'),
        ('Sportivnye kostyumy'),
        ('Dlya svadby') ) 
    ),
    ('Muzhskaya odezhda', (#NOT nullable
        ('Nizhnee bele, plavki'), #nullable
        ('Futbolki, majki'),
        ('Rubashki'),
        ('kofty'),
        ('Svitera'),
        ('Tolstovki'),
        ('Kostyumy, pidzhaki, zhilety'),
        ('Bryuki'),
        ('Futbolka'),
        ('Verhnyaya odezhda'),
        ('Drugoe'),
        ('Sportivnye kostyumy') ) 
    ),
    ('ZHenskaya obuv', (#NOT nullable
        ('Baletki'), #nullable
        ('Mokasiny'),
        ('Tufli'),
        ('Bosonozhki'),
        ('SHlepancy'),
        ('Botinki'),
        ('Sapogi'),
        ('Drugoe'),
        ('Sportivnaya obuv') ) 
    ),
    ('Muzhskaya obuv', (#NOT nullable
        ('Sapogi, botinki'), #nullable
        ('Tufli, mokasiny'),
        ('Sandalii, shlepancy'),
        ('Sportivnaya obuv'),
        ('Drugaya obuv'),
        ('Golovnye ubory') ) 
    )
),
('Krasota i aksessuary', (#NOT nullable
    ('Sumki, ryukzaki, koshelki'), #NOT nullable
    ('Bizhuteriya i ukrasheniya'),
    ('CHasy'),
    ('YUvelirnye izdeliya', ( #NOT nullable
        ('Kolca'), #nullable
        ('Sergi'),
        ('Braslety'),
        ('Kulony / Podveski'),
        ('Broshi'),
        ('Cepochki'),
        ('Drugie yuvelirnye izdeliya') ) 
    ),
    ('Kosmetika i uhod', ( #NOT nullable
        ('Vsydlya volos'), #nullable
        ('Vsydlya nogtej'),
        ('Dekorativnaya kosmetika'),
        ('Drugoe'),
        ('Kosmetika solncezashchitnaya, dlya solyariya'),
        ('Parfyumeriya'),
        ('Sredstva dlya britya / depilyacii'),
        ('Uslugi'),
        ('Uhod za licom i telom'),
        ('Uhod za polostyu rta'),
        ('Pribory i aksessuary'),
        ('Sredstva dlya volos') ) 
    ),
    ('Parfyumeriya'), #NOT nullable
    ('Ochki'), 
    ('Prochee') ) )
),
('Detskii mir', ( #NOT nullable
    ('Detskaya odezhda', ( #NOT nullable
        ('Verhnyaya odezhda'), #nullable
        ('Golovnye ubory, sharfy'),
        ('Dlya mladencev'),
        ('Drugoe'),
        ('Karnavalnye kostyumy'),
        ('Kolgotki, noski, golfy'),
        ('kofty, reglany'),
        ('Kupalniki, plavki'),
        ('Nizhnee bele'),
        ('Perchatki, varezhki'),
        ('Pidzhaki, rubashki'),
        ('Pizhamy, halaty'),
        ('Platya, sarafany'),
        ('Sportivnye kostyumy'),
        ('Futbolki, majki'),
        ('SHkolnaya forma'),
        ('SHorty, bridzhi, kapri'),
        ('SHtany'),
        ('YUbki') ) 
    ),
    ('Detskaya obuv', (#NOT nullable
        ('Drugoe'), #nullable
        ('Krossovki, kedy, mokasiny, butsy'),
        ('Sandalii, bosonozhki, shlepancy'),
        ('Sapogi, botinki'),
        ('Tapochki, cheshki'),
        ('Tufli') ) 
    ),
    ('Aksessuary dlya detej', (#NOT nullable
        ('Galstuki, babochki'), #nullable
        ('Drugoe'),
        ('Zonty, dozhdeviki'),
        ('Kosmetika i ukrasheniya dlya devochek'),
        ('Ochki i aksessuary'),
        ('Rezinki, zakolki, obruchi'),
        ('Remni, poyasa, podtyazhki'),
        ('Ryukzaki, sumki') )
    ),
    ('Detskie igrushki i igry'), #NOT nullable
    ('Detskaya literatura'),
    ('Dlya samyh malenkih'),
    ('Dlya shkolnikov'),
    ('Dlya mam'),
    ('Detskoe pitanie'),
    ('Detskii transport'),
    ('Detskaya mebel'),
    ('Detskie kolyaski i avtokresla'),
    ('Zdorove i gigiena', ( #NOT nullable
        ('Vse dlya kormleniya'), #nullable
        ('Vse dlya kupaniya'),
        ('Gorshki, nakladki na unitaz'),
        ('Gradusniki, termometry, vesy'),
        ('Drugoe'),
        ('Krema, masla, prisypka'),
        ('Podguzniki, salfetki') ) 
    ),
    ('Prochee') ) 
),
('Sport i zdorove', ( #NOT nullable
    ('Sportinventar', ( #NOT nullable
        ('Alpinizm, speleologiya'), #nullable
        ('arbalety, luki, strely'),
        ('Bilyard'),
        ('Boevye iskusstva'),
        ('Vodnyi sport'),
        ('Ganteli, giri, shtangi'),
        ('Darts'),
        ('Drugoe'),
        ('Zimnii sport'),
        ('Igry s myachom'),
        ('Nastolnye igry'),
        ('Pejntbol, strajkbol'),
        ('Roliki, skejty'),
        ('Sportivnye ugolki'),
        ('Tennis, badminton'),
        ('Fitnes, joga, aerobika, gimnastika') ) 
    ),
    ('Vsydlya otdyha, turizm', ( #NOT nullable
        ('Dorozhnye sumki'), #nullable
        ('Drugoe'),
        ('Mangaly, gril, shampura'),
        ('Metalloiskateli'),
        ('Naduvnye bassejny'),
        ('Nozhi, multituly'),
        ('Palatki'),
        ('Posuda, flyagi, termosy'),
        ('Pohodnyi inventar'),
        ('putyovki, tury'),
        ('Ryukzaki'),
        ('Spalnye meshki, gamaki'),
        ('Fonari, fonariki') ) 
    ),
    ('Pribory i ustrojstva', ( #NOT nullable
        ('Vesy'), #nullable
        ('Drugoe'),
        ('Kardiopribory, pribory dlya pulsa'),
        ('Massazhery'),
        ('Poyasa, miostimulyatory'),
        ('Pribory magnitoterapii'),
        ('Sluhovye apparaty'),
        ('Termometry, gradusniki'),
        ('Tonometry'),
        ('Filtry') ) 
    ),
    ('Trenazhery', ( #NOT nullable
        ('Begovye dorozhki'), #nullable
        ('Velotrenazhyory'),
        ('Drugie'),
        ('Orbitreki'),
        ('Silovye trenazhyory'),
        ('skami i stojki'),
        ('steppery') ) 
    ), 
    ('Zdorove', ( #NOT nullable
        ('Aromaterapiya, svechi, lampy'), #nullable
        ('Lekarstva'),
        ('Sredstva dlya pohudeniya'),
        ('Drugoe'),
        ('Netradicionnaya medicina'),
        ('Ochki, linzy, aksessuary') ) 
    ),
    ('Velosipedy i aksessuary k nim', ( #NOT nullable
        ('Smartfony'), #nullable
        ('Drugoe'),
        ('Komplektuyushchie'),
        ('Snaryazhenie, aksessuary'),
        ('Ekipirovka dlya velosporta') ) 
    ),
    ('Specodezhda, obuv', ( #NOT nullable
        ('Drugoe'),
        ('Ohota, rybolovstvo'),
        ('Turizm') ) 
    ),
    ('Drugoe'), #NOT nullable
    ('Sportivnoe pitanie'),
    ('Tovary dlya invalidov') ) 
),
('Hobbi i dosug', ( #NOT nullable
    ('Knigi, pressa'), #NOT nullable
    ('CD / DVD plastinki'),
    ('Muzykalnye instrumenty', ( #NOT nullable
        ('Studiinoe oborudovanie'), #nullable
        ('Drugoe'),
        ('Aksessuary dlya muzykalnyh instrumentov'),
        ('Pianin/ fortepian/ royali'),
        ('Elektrogitary'),
        ('Akusticheskie gitary'),
        ('vyhodnoj'),
        ('duhovye'),
        ('Udarnye instrumenty'),
        ('Kombousiliteli'),
        ('Bas-gitary') ) 
    ),
    ('Ohota'), #NOT nullable
    ('Rybolovstvo', ( #NOT nullable
        ('Drugoe'), #nullable
        ('Katushki'),
        ('Lodki, katamarany'),
        ('Podvodnaya ohota'),
        ('Rybackii inventar'),
        ('Udilishcha'),
        ('Eholoty, radary') ) 
    ),
    ('Kollekcionirovanie / antikvariat', ( #NOT nullable
        ('Numizmatika'), #nullable
        ('Drugie kategorii'),
        ('Bonistika'),
        ('Stolovoe serebro, antikvarnaya kuhonnaya utvar'),
        ('Filateliya'),
        ('Skulptury'),
        ('Kartiny, antikvarnaya mebel, predmety interera'),
        ('Masshtabnye modeli'),
        ('Militariya'),
        ('Medali, znachki, zhetony'),
        ('Ikony, skladni, metalloplastika'),
        ('Knigi'),
        ('Plastinki'),
        ('Monety Ukrainy iz drag.splavov'),
        ('Kollekcionnoe oruzhie') )
    ),
    ('Prochee') ) #NOT nullable
),
('Zhivotnye i dlya zhivotnyh', ( #NOT nullable
    ('Koshki'), #NOT nullable
    ('Sobaki'),
    ('Akvariumy'),
    ('Pernatye'),
    ('Reptilii'),
    ('Drugie domashnie pitomcy'),
    ('Selhoz zhivotnye'),
    ('Pitanie i aksessuary'),
    ('Veterinarnye Zootovary'),
    ('Uhod za zhivotnymi'),
    ('Vyazka'),
    ('Otdam besplatno'),
    ('Drugoe'),
    ('Byurnahodok') ) 
),
('Dom i sad', ( #NOT nullable
    ('Vse dlya kuhni'), #NOT nullable
    ('Vse dlya vannoj komnaty'),
    ('Predmety Interera, osveshchenie'),
    ('Mebel', ( #NOT nullable
        ('Mebel dlya gostinoj'), #nullable
        ('Mebel na zakaz'),
        ('Mebel dlya spalni'),
        ('Ofisnaya mebel'),
        ('Mebel'),
        ('Mebel dlya prihozhej'),
        ('Mebel dlya vannoj komnaty') ) 
    ),
    ('Instrumenty / inventar', ( #NOT nullable
        ('Elektroinstrument'), #nullable
        ('Ruchnoj instrument'),
        ('Drugoj instrument'),
        ('Benzoinstrument'),
        ('Komplektuyushchie, rashodnye materialy') ) 
    ),
    ('Tekstil'),
    ('Bytovaya himiya'),
    ('Rasteniya', ( #NOT nullable
        ('Gorshki, zemlya, udobreniya'), #nullable
        ('Rasteniya') ) 
    ),
    ('Sad i ogorod', ( #NOT nullable
        ('Vse dlya poliva'), #nullable
        ('Drugoe'),
        ('Sadovaya mebel, shatry, besedki'),
        ('Sadovaya tehnika'),
        ('Sadovye svetilniki'),
        ('Sadovyi instrument'),
        ('Sredstva ot nasekomyh'),
        ('Udobreniya i himikaty') ) 
    ),
    ('Produkty pitaniya'),
    ('Kanctovary'),
    ('Prochee') ) 
),
('Remont i stroitelstvo', ( #NOT nullable
    ('Otoplenie'), #NOT nullable
    ('Drugie strojmaterialy'),
    ('Okna / dveri / stekl/ zerkala'),
    ('Otdelochnye i oblicovochnye materialy'),
    ('Elektrichestvo'),
    ('Santehnika'),
    ('Metalloprokat / armatura'),
    ('Kirpich / beton / penobloki'),
    ('Pilomaterialy'),
    ('Elementy krepleniya'),
    ('Lakokrasochnye materialy'),
    ('Ventilyaciya'),
    ('Uslugi') ) 
),
('Transport', ( #NOT nullable
    ('Zapchasti / aksessuary', ( #NOT nullable
        ('Avtozapchasti'), #nullable
        ('SHiny / diski'),
        ('Aksessuary dlya avto'),
        ('Zapchasti dlya spec / s.h.tehniki'),
        ('Motozapchasti'),
        ('Avtozvuk'),
        ('Drugie zapchasti'),
        ('Motaksessuary'),
        ('Transport na zapchasti'),
        ('GPS-navigatory / avtoregistratory') ) 
    ),
    ('Selhoztehnika'),
    ('Legkovye avtomobili', ( #NOT nullable
	#there is no nullable subcategory
        ('VAZ'), #filter
        ('Volkswagen'),
        ('Opel'),
        ('Mercedes'),
        ('Ford'),
        ('Renault'),
        ('Daewoo'),
        ('ZAZ'),
        ('Toyota'),
        ('Chevrolet'),
        ('Hyundai'),
        ('GAZ'),
        ('Mitsubishi'),
        ('BMW'),
        ('Nissan'),
        ('Audi'),
        ('Fiat'),
        ('Mazda'),
        ('Skoda'),
        ('Peugeot'),
        ('Drugie'),
        ('Kia'),
        ('Moskvich'),
        ('Citroen'),
        ('Honda'),
        ('Chery'),
        ('Geely'),
        ('Subaru'),
        ('UAZ'),
        ('Lexus'),
        ('Suzuki'),
        ('Volvo'),
        ('Tavriya') ) 
    ),
    ('Gruzovye avtomobili'),
    ('Moto', ( #NOT nullable
        ('Mopedy / skutery'), #nullable
        ('Motocikly'),
        ('Kvadrocikly'),
        ('Mot-drugoe') ) 
    ),
    ('Vodnyi transport'),
    ('Spectehnika', ( #NOT nullable
        ('Buldozery / traktory'), #nullable
        ('Pogruzchiki'),
        ('Stroitelnaya tehnika'),
        ('Drugaya spectehnika'),
        ('Oborudovanie dlya spectehniki'),
        ('Ekskavatory'),
        ('Kommunalnaya tehnika'),
        ('Samosvaly') ) 
    ),
    ('Pricepy'), #NOT nullable
    ('Avtobusy'),
    ('Drugoj transport') ) 
),
('Uslugi', (#NOT nullable
    ('Remontnye uslugi'), #NOT nullable
    ('Uborka', ( #NOT nullable
        ('Otdelka / remont'), #nullable
        ('Stroitelnye uslugi'),
        ('Bytovoj remont / uborka'),
        ('Okna / dveri / balkony'),
        ('Gotovye konstrukcii'),
        ('Santehnika / kommunikacii'),
        ('Elektrichestvo'),
        ('Dizajn / arhitektura'),
        ('Ventilyaciya / kondicionirovanie') ) 
    ),
    ('Dostavka'), #NOT nullable
    ('Transportnye uslugi / Avtarenda'),
    ('Uslugi perevoda'),
    ('YUridicheskie, notarialnye uslugi'),
    ('Finansy, kredit, strahovanie'),
    ('Informacionnye tehnologii, internet'),
    ('Zdorove, krasota', ( #NOT nullable
        ('Manikyur / narashchivanie nogtej'), #nullable
        ('Makiyazh / kosmetologiya / narashchivanie resnic'),
        ('Krasota / zdorove '),
        ('prochee'),
        ('Massazh'),
        ('Strizhki / narashchivanie volos'),
        ('Medicina'),
        ('Uslugi psihologa') ) 
    ),
    ('Organizaciya, obsluzhivanie meropriyatii'), #NOT nullable
    ('Turizm, immigraciya'),
    ('Reklama, marketing'),
    ('Bezopasnost, ohrana'),
    ('Torgovlya'),
    ('Poligrafiya, izdatelstvo'),
    ('Uslugi puhodu za zhivotnymi'),
    ('Razvlecheniya, foto, video'),
    ('Prokat'),
    ('Nyani, sidelki'),
    ('Obrazovanie'),
    ('Pomoshch ekstrasensov'),
    ('Sport'),
    ('Selskoe hozyajstvo'),
    ('Prochee') ) 
),
('Obshchenie', ( #NOT nullable
    ('', ( #NOT nullable
        ('Poteri'), #nullable
        ('Nahodki'),
        ('Bilety na meropriyatiya'),
        ('Poisk poputchikov'),
        ('Poisk grupp / muzykantov'),
        ('Poisk lyudej') ) 
    )
),
('Drugoe', (#NOT nullable
    ('', ( #NOT nullable
        ('Tovary dlya vzroslyh'), #nullable
        ('Podarki i suveniry'),
        ('Otdam besplatno'),
        ('Obmen') ) 
    )
)
)
