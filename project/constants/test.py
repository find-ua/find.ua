import unittest
from constants.constants_functions import translate_filter_name, params_to_olx_urls


class ParamsToOlxUrlsTest(unittest.TestCase):
    def test_params_to_olx_urls(self):
        # self.assertEqual('', params_to_olx_urls({'category': 'elektronika',
        #                   'subcategory':'noutbuki',
        #                   # 'subsubcategory':'plenochnye-fotoapparaty',
        #                   'q':'рубашка',
        #                   'pmin':'4',
        #                   'pmax':'5',
        #                   'location':'ilintsy',
        #                   'sostoyanie':'b-u',
        #                     'diagonal-ekrana':'13"-14"',
        #                   'marka-noutbuka':'fujitsu-siemens'
        #                   }))
        # self.assertEqual([], params_to_olx_urls({'category': 'elektronika',
        #                   'subcategory':'noutbuki',
        #                   'subsubcategory':'plenochnye-fotoapparaty',
        #                   'q':'рубашка',
        #                   'pmin':'4',
        #                   'pmax':'5',
        #                   'location':'ilintsy',
        #                   'sostoyanie':'b-u',
        #                     'diagonal-ekrana':'13"-14"',
        #                   'marka-noutbuka':'fujitsu-siemens'
        #                   }))
        self.assertEqual(['ukraine.olx.ua/elektronika/kompyutery/noutbuki/q-рубашка/?search[filter_enum_state][]=b-u&search[filter_enum_laptop_manufacturer][]=2113&'], params_to_olx_urls({'diagonal-ekrana': '', 'search': '', 'pmax': '', 'sostoyanie': 'b-u', 'pmin': '', 'csrfmiddlewaretoken': 'NQSS9NVQC9EkXFo20ipedLIp4HuyUErD', 'category': 'elektronika', 'subcategory':'noutbuki', 'location': 'ukraine', 'q': 'рубашка', 'marka-noutbuka': 'asus'}))

        # self.assertEqual('http://olx.ua/elektronika/kompyutery/noutbuki/?search[filter_enum_laptop_manufacturer][]=Acer',
        #                  params_to_olx_urls({'category': 'elektronika',
        #                   'subcategory':'noutbuki',
        #                   # 'subsubcategory':'plenochnye-fotoapparaty',
        #
        #                   'marka-noutbuka':'Acer'
        #                   }))


class TranslateFilterNameTest(unittest.TestCase):
    def test_filter_name(self):
        self.assertEqual(('Проигрыватели', 'Марка плеера'), translate_filter_name('marka-pleera', 'bytovaya-tehnika',
                                                               'televizory-videotehnika', 'proigryvateli', ))
