# -*- coding: utf-8 -*-
"""
Файл із допоміжними функціями
"""

from pprint import PrettyPrinter
# from constants.cat_full import categories
from constants.areas import AREAS
from constants.categories_choices import CATEGORIES_CHOICES
from constants.categories_db import get_subsubcategory_id, get_subcategory_id
from constants.common import STANDART_PARAMS
from constants.constants_ultimate import CONSTANTS
from constants.db_enums_ru import DB_ENUMS_RU
# from constants.db_num_to_rus import DB_NUM_TO_CATEGORIES_SUBCATEGORIES_TRANSLIT
from constants.filters import extract_filters
from constants.find_categories_to_olx import FIND_SUBCATEGORIES_TO_OLX, FIND_SUBSUBCATEGORIES_TO_OLX
from constants.olx_to_find_categories import OLX, NEW_OLX
from constants.other.filters_olx import FILTERS_OLX
from constants.other_site_to_find import TEMPLATE_OTHER_SITE_TO_FIND
from dev.cat_and_filt import join_categories_and_filters
from string import printable

categories = CONSTANTS


def strings_equal(s1, s2):
    s1 = s1.replace(',', '-')
    s2 = s2.replace(',', '-')
    return s1.casefold() == s2.casefold()


def dict_translit(dic):
    """
    Функція переводить у трансліт словник, якщо значення ключа - список
    """
    ndic = {}
    for key in dic:
        temp_value = []
        for value in dic[key]:
            temp_value.append((translit(value), value))
        key = (translit(key), key)
        ndic[key] = temp_value
    return ndic


def key_translit(dic):
    """
    Функція переводить у трансліт та нижній регістр ключі словника
    """
    ndic = {}
    for key in dic:
        nkey = translit(key)
        ndic[nkey] = dic[key]
    return ndic


def translit(s):
    """
    Функція переводить у трансліт та нижній регістр рядок
    """
    DICT = {
        'а': 'a',
        'б': 'b',
        'в': 'v',
        'г': 'g',
        'д': 'd',
        'е': 'e',
        'ж': 'zh',
        'з': 'z',
        'и': 'i',
        'й': 'j',
        'к': 'k',
        'л': 'l',
        'м': 'm',
        'н': 'n',
        'о': 'o',
        'п': 'p',
        'р': 'r',
        'с': 's',
        'т': 't',
        'у': 'u',
        'ф': 'f',
        'х': 'h',
        'ц': 'c',
        'ч': 'ch',
        'ш': 'sh',
        'щ': 'sch',
        'ь': '',
        'ю': 'yu',
        'я': 'ya',
        'ы': 'y',
        'ъ': 'j',
        'э': 'e',
        'ё': 'e',
        ' ': '-',
        '/': '-',
        ',': '-'
    }
    res = ''
    if s:
        s = s.lower()
        for letter in s:
            if letter in DICT.keys():
                res += DICT[letter]
            else:
                res += letter
        res = res.replace('---', '-')
        res = res.replace('--', '-')
    return res


# for category in categories:
#     for category_name in category[0]:
#         category_name = category_name.lower()
#     for subcategory in category[1]:
#         for subcategory_name in subcategory[0]:
#             subcategory_name = subcategory_name.lower()
#         if len(subcategory) > 1:
#             for subsubcategory in subcategory[1]:
#                 print(subsubcategory)
#                 subsubcategory[0] = subsubcategory[0].lower()
#                 if len(subsubcategory) > 1:
#                     for key in subsubcategory[1]:
#                         for value in subsubcategory[1][key]:
#                             value = value.lower()
#                         value = subsubcategory[1][key]
#                         subsubcategory[1].pop(key)
#                         key = key.lower()
#                         subsubcategory[1][key] = value


def translit_category_to_rus(s, *args):
    """
    Функція повертає російську назву транслітерованої категорії, підкатегорії, підпідкатегорії
    - в залежності від того, що зазначено в *args, проте зазначати треба все, наприклад,
    якщо перекласти треба фільтр - в args треба передати 'category', 'subcategory', 'subsubcategory',
    'filter'
    """
    for category in categories:
        for category_name in category[0]:
            if category_name == s:
                return category[0][1]
        if 'subcategory' in args:
            for subcategory in category[1]:
                for subcategory_name in subcategory[0]:
                    if subcategory_name == s:
                        return subcategory[0][1]
                if 'subsubcategory' in args:
                    if len(subcategory) > 1:
                        for subsubcategory in subcategory[1]:
                            # print(subsubcategory)
                            if subsubcategory[0] == s:
                                return subsubcategory[1]
                            if 'filter' in args:
                                if len(subsubcategory) > 1:
                                    for key in subsubcategory[1]:
                                        pass


def translit_area_to_rus(s):
    for area in DB_ENUMS_RU.AREA_CHOICES:
        if type(area[1]) is tuple:
            for city in area[1]:
                if city[0] == s:
                    return city[1]


def eng_and_rus_categories(categories):
    """
    Функція переводить СONSTANTS у трансліт
    """
    for category in categories:
        # for category_name in category[0]:
        # category_name = category[0]
        # print('cat_name before', category_name)
        category[0] = (translit(category[0]), category[0])
        # print('cat_name after', category_name)
        for subcategory in category[1]:
            subcategory[0] = (translit(subcategory[0]), subcategory[0])
            if len(subcategory) > 1:
                if type(subcategory[1]) is list:
                    for subsubcategory in subcategory[1]:
                        subsubcategory[0] = (translit(subsubcategory[0]), subsubcategory[0])
                        if len(subsubcategory) > 1:
                            subsubcategory[1] = dict_translit(subsubcategory[1])
                            # for key in subsubcategory[1]:
                            #     for value in subsubcategory[1][key]:
                            #         value = value.lower()
                            #     value = subsubcategory[1][key]
                            #     subsubcategory[1].pop(key)
                            #     key = key.lower()
                            #     subsubcategory[1][key] = value
                elif type(subcategory[1]) is dict:
                    subcategory[1] = dict_translit(subcategory[1])
    p = PrettyPrinter(indent=3)
    p.pprint(categories)


def find_filters(kwargs):
    """
    Функція повертає словник фільтрів для даної підкатегорії або підпідкатегорії
    """
    print('ffinlters', kwargs)
    filters = {}
    if 'category' in kwargs:
        for category in categories:
            if category[0][0] == kwargs['category'] and 'subcategory' in kwargs:
                for subcategory in category[1]:
                    if subcategory[0][0] == kwargs['subcategory']:
                        if len(subcategory) > 1:
                            if type(subcategory[1]) is dict:
                                filters.update(subcategory[1])
                            elif type(subcategory[1]) is list and 'subsubcategory' in kwargs:
                                for subsubcategory in subcategory[1]:
                                    if subsubcategory[0][0] == kwargs['subsubcategory']:
                                        if len(subsubcategory) > 1:
                                            filters.update(subsubcategory[1])
    return filters


def translate_filter_name(filter_name, _category, _subcategory, _subsubcategory=None):
    """
    :param args: назва фільтру транслітом, категорія, підкатегорія, (підпідкатегорія)
    :return: назва породжуючої підкатегорії, фільтру російською
        Example:
        >>> translate_filter_name('marka-pleera', 'bytovaya-tehnika','televizory-videotehnika', 'proigryvateli')
        ('Проигрыватели', 'Марка плеера')
    """

    def translate_key_name(dic):
        for key in dic:
            if key[0] == filter_name:
                return key[1]

    if _category:
        for category in categories:
            # for category_name in category[0]:
            # category_name = category[0]
            # print('cat_name before', category_name)
            if category[0][0] == _category and _subcategory:
                # print('cat_name after', category_name)
                for subcategory in category[1]:
                    if subcategory[0][0] == _subcategory:
                        if len(subcategory) > 1:
                            if type(subcategory[1]) is dict:
                                subcategory_name = subcategory[0][1]
                                key = translate_key_name(subcategory[1])
                                return subcategory_name, key
                            elif type(subcategory[1]) is list and _subsubcategory:
                                for subsubcategory in subcategory[1]:
                                    if subsubcategory[0][0] == _subsubcategory:
                                        if len(subsubcategory) > 1:
                                            subcategory_name = subsubcategory[0][1]
                                            key = translate_key_name(subsubcategory[1])
                                            return subcategory_name, key


def split_category_and_subcategory(category_value):
    """
    Функція визначає, чи рядок є категорією або підкатегорією і в залежності від цього повертає
    або None, або категорію, або категорію і підкатегорію
    """
    if not category_value:
        return None
    for category in CATEGORIES_CHOICES:
        for i, cat in enumerate(category[1]):
            if category_value == cat[0]:
                # if 'category' is category
                if i == 0:
                    return category_value
                # if 'category' is subcategory
                else:
                    return category[1][0][0], category_value


def params_to_olx_urls(params):
    """
    :param params: параметри find.org.ua
    :return: список посилань на olx, які відповідають нашим параметрам
    """
    params = {k: v for k, v in params.items() if v != ''}
    category = subcategory = subsubcategory = ''
    olx_urls = []
    olx_url = 'olx.ua/'
    if 'location' in params:
        olx_url = city_to_olx_url(params.get('location')) + olx_url
    if 'category' in params:
        # поки що не реалізую відслідковування, якщо у запиті тільки категорії
        category = params['category']
        if 'subcategory' in params:
            subcategory = params['subcategory']
            if 'subsubcategory' in params:
                subsubcategory = params['subsubcategory']
                subsubcategory_id = get_subsubcategory_id(category, subcategory, subsubcategory)
                olx_categories = FIND_SUBSUBCATEGORIES_TO_OLX.get(subsubcategory_id)
            else:
                subcategory_id = get_subcategory_id(category, subcategory)
                olx_categories = FIND_SUBCATEGORIES_TO_OLX.get(subcategory_id)
            # Якщо користувач обирає несумісні категорії, наприклад Електроніка|Ноутбуки|Дитяче взуття, повертаємо None
            if not olx_categories:
                return None
            olx_urls = [olx_url + olx_category_to_olx_url(olx_category) for olx_category in olx_categories]
    else:
        olx_url += 'list/'
        olx_urls = [olx_url]
    if 'q' in params:
        olx_urls = list(map(lambda s: s + 'q-' + params['q'] + '/?', olx_urls))
    else:
        olx_urls = list(map(lambda s: s + '?', olx_urls))
    if 'pmin' in params:
        olx_urls = list(map(lambda s: s + 'search[filter_float_price:from]=' + params['pmin'] + '&', olx_urls))
    if 'pmax' in params:
        olx_urls = list(map(lambda s: s + 'search[filter_float_price:to]=' + params['pmax'] + '&', olx_urls))
    if 'sostoyanie' in params:
        olx_urls = list(map(lambda s: s + 'search[filter_enum_state][]=' + params['sostoyanie'] + '&', olx_urls))
    filters_pairs = extract_filters(params)
    if filters_pairs:
        for key in filters_pairs:
            subsubcategory_rus, rus_key = translate_filter_name(key, category, subcategory, subsubcategory)
            # key - 'marka-pleera', rus_key - 'Марка плеера'
            for key_olx, value in FILTERS_OLX[
                subsubcategory_rus].items():  # key_olx - "Состояние", "Марка плеера" і т.д
                if strings_equal(key_olx, rus_key):
                    olx_name = value[0]
                    for val in value[1]:
                        if strings_equal(params[key], val[1]):
                            olx_value = val[0]
                            break
                else:
                    continue
                olx_urls = list(map(lambda s: s + olx_name + '=' + olx_value + '&', olx_urls))
    return olx_urls


def city_to_olx_url(city):
    """Transform city or area name from find.ua to olx.u url.

    Args:
        :param city (str): city or area name from find.ua

    Returns:
        :returns: (str)

    Example:
        >>> city_to_olx_url('bar')
        'bar.vin.'
    """
    for area in AREAS.keys():
        if city in AREAS[area]:
            return "{}.{}.".format(city, area)

    # if not in AREAS then city looks like "rokitnoe_rov" or city is an area (e.g. "ko")
    if len(city.split('_')) == 2:
        city, area = city.split('_')
        return "{}.{}.".format()
    # if city is an area
    elif len(city.split('_')) == 1:
        return "{}.".format(city)


def olx_category_to_olx_url(key):
    """
    :param key: Электроника|Аудиотехника|Cd / md / виниловые проигрыватели'
    :return: elektronika/audiotehnika/cd-md-vinilovye-proigryvateli/
    """
    olx_url = ''
    cat_list = list(key.split('|'))
    category_value = cat_list[0]
    subcategory_value = cat_list[1]
    subsubcategory_value = cat_list[2] if 2 < len(cat_list) else None
    for category in DB_ENUMS_RU.CATEGORY_CHOICES:
        if strings_equal(category_value, category[0]):
            olx_url += category[1][0][0] + '/'
            for subcategory in category[1]:
                if type(subcategory[1]) is tuple and strings_equal(subcategory_value, subcategory[0]):
                    olx_url += subcategory[1][0][0] + '/'
                    if subsubcategory_value:
                        for subsubcategory in subcategory[1]:
                            if strings_equal(subsubcategory_value, subsubcategory[1]):
                                olx_url += subsubcategory[0] + '/'
                                return olx_url
                elif type(subcategory[1]) is str and strings_equal(subcategory_value, subcategory[1]):
                    olx_url += subcategory[0] + '/'
                    return olx_url


def find_categories_to_site(site):
    """
    :params: словник ключі - категорії цільового сайту, значення - наші категорії
    :return: два словники
    1) ключі - номери підкатегорій в бд, значення - список відповідних підкатегорій на olx
    2) ключі - номери підпідкатегорій в бд, значення - список відповідних підпідкатегорій на olx
    """
    DB_SUBCATEGORIES_TO_SITE = {}
    DB_SUBSUBCATEGORIES_TO_SITE = {}
    for key, value in site.items():
        # кщо ключ написаний латинськими символами, це означає, що відповідної категорії на
        # цільовому сайті немає. Наступний рядок перевіряє, чи в ключі є кирилиця
        if not all(map(lambda c: c in printable, key)):
            last_element_in_value = value[2]
            if type(last_element_in_value) is int:
                if last_element_in_value in DB_SUBCATEGORIES_TO_SITE:
                    DB_SUBCATEGORIES_TO_SITE[last_element_in_value].add(key)
                else:
                    DB_SUBCATEGORIES_TO_SITE[last_element_in_value] = {key}
            elif type(last_element_in_value) is dict:
                number_of_subsubcategory = value[1]
                if number_of_subsubcategory in DB_SUBSUBCATEGORIES_TO_SITE:
                    DB_SUBSUBCATEGORIES_TO_SITE[number_of_subsubcategory].add(key)
                else:
                    DB_SUBSUBCATEGORIES_TO_SITE[number_of_subsubcategory] = {key}

                number_of_subcategory = list(last_element_in_value.values())[0][1]
                if number_of_subcategory in DB_SUBCATEGORIES_TO_SITE:
                    DB_SUBCATEGORIES_TO_SITE[number_of_subcategory].add(key)
                else:
                    DB_SUBCATEGORIES_TO_SITE[number_of_subcategory] = {key}
        else:
            continue
    return DB_SUBCATEGORIES_TO_SITE, DB_SUBSUBCATEGORIES_TO_SITE


def replace_incorrect_dbnumbers_in_other_site_to_olx():
    olx_dict = {}
    for key in OLX:
        v = OLX[key]
        for k, va in TEMPLATE_OTHER_SITE_TO_FIND.items():
            if type(v[2]) is int:
                if type(va[2]) is int:
                    if v[0] == va[0] and v[1] == va[1]:
                        v[2] = va[2]
                        olx_dict[key] = v
                        break
                    else:
                        continue
                else:
                    continue
            elif type(v[2]) is dict:
                if type(va[2]) is dict:
                    if v[0] == va[0] and list(v[2].keys()) == list(va[2].keys()):
                        v[1] = va[1]
                        v[2][list(v[2].keys())[0]][1] = va[2][list(va[2].keys())[0]][1]
                        olx_dict[key] = v
                        break
                    else:
                        continue
                else:
                    continue
            else:
                print('??????')
        if not key in olx_dict.keys():
            olx_dict[key] = v
    return olx_dict


# j = translit_area_to_rus('Kiev-area')
# j = split_category_and_subcategory('zhenskaya-odezhda')
# print(j)

# d = dict_translit(DB_NUM_TO_CATEGORIES_SUBCATEGORIES_TRANSLIT)
# s, ss = find_categories_to_site(NEW_OLX)
# s = replace_incorrect_dbnumbers_in_other_site_to_olx()
p = PrettyPrinter()
# p.pprint(s)
# p.pprint(ss)
# print(olx_category_to_olx_url('Электроника|Аудиотехника|Cd / md / виниловые проигрыватели'))
# p.pprint(eng_and_rus_categories(join_categories_and_filters()))
# print(find_filters(category='elektronika', subcategory='noutbuki'))
# print(params_to_olx_urls({'category': 'elektronika',
#                           'csrfmiddlewaretoken': 'oBaI2RT7FsqwoWV1pQe7nOSRt7GhnZl9',
#                           'diagonal-ekrana': '15"-15-6"',
#                           'location': 'mogilyev-podolskiy',
#                           'marka-noutbuka': 'dell',
#                           'pmax': '243432',
#                           'pmin': '42',
#                           'q': 'iphone 5s 8gb',
#                           'sostoyanie': 'novyj',
#                           'subcategory': 'noutbuki'}))



# TODO: у базі даних замінити усі назви категорій на транслітеровані й нормалізовані з цих констант
