# -*- coding: utf-8 -*-
OLX = {
    'Bytovaya technika|Prochee / drugaya tehnika': ['Prochee / drugaya tehnika', 'Bytovaya technika', 6],
    'Электроника|Телефоны|Стационарные телефоны': ['Stacionarnye telefony', 'Elektronika', 16],
    'Электроника|Телефоны|Прочие телефоны': ['Drugie telefony, faksy', 'Elektronika', 18],
    'Мода и стиль|Аксессуары|Сумки': ['Sumki, ryukzaki, koshelki', 'Krasota i aksessuary', 23],
    'Мода и стиль|Аксессуары|Бижутерия': ['Bizhuteriya i ukrasheniya', 'Krasota i aksessuary', 24],
    'Мода и стиль|Наручные часы': ['CHasy', 'Krasota i aksessuary', 25],
    'Мода и стиль|Красота / здоровье|Парфюмерия': ['Parfyumeriya', 'Krasota i aksessuary', 28],
    'Krasota i aksessuary|Ochki': ['Ochki', 'Krasota i aksessuary', 29],
    # прочие аксессуары
    # ОДЕЖДА ОБУВЬ ПРОЧЕЕ
    # прочие товары для красоты и здоровья
    # Мода и стиль|Красота / здоровье|Оборудование парикмахерских / салонов красоты
    'Мода и стиль|Красота / здоровье|Прочие товары для красоты и здоровья': ['Prochee', 'Krasota i aksessuary', 30],
    'Мода и стиль|Красота / здоровье|Оборудование парикмахерских / салонов красоты': ['Prochee', 'Krasota i aksessuary',
                                                                                      30],
    'Мода и стиль|Аксессуары|Другие аксессуары': ['Prochee', 'Krasota i aksessuary', 30],
    'Мода и стиль|Мода разное': ['Prochee', 'Krasota i aksessuary', 30],
    'Детский мир|Игрушки': ['Detskie igrushki i igry', 'Detskii mir', 34],
    'Detskii mir|Detskaya literatura': ['Detskaya literatura', 'Detskii mir', 35],
    'Detskii mir|Dlya samyh malenkih': ['Dlya samyh malenkih', 'Detskii mir', 36],
    'Детский мир|Товары для школьников': ['Dlya shkolnikov', 'Detskii mir', 37],
    'Detskii mir|Dlya mam': ['Dlya mam', 'Detskii mir', 38],
    'Детский мир|Товары для кормления': ['Detskoe pitanie', 'Detskii mir', 39],
    'Детский мир|Детский транспорт': ['Detskii transport', 'Detskii mir', 40],
    'Детский мир|Детская мебель': ['Detskaya mebel', 'Detskii mir', 41],
    'Детский мир|Детские коляски': ['Detskie kolyaski i avtokresla', 'Detskii mir', 42],
    'Детский мир|Детские автокресла': ['Detskie kolyaski i avtokresla', 'Detskii mir', 42],
    'Детский мир|Прочие детские товары': ['Prochee', 'Detskii mir', 44],
    'Sport i zdorove|Drugoe': ['Drugoe', 'Sport i zdorove', 52],
    'Sport i zdorove|Sportivnoe pitanie': ['Sportivnoe pitanie', 'Sport i zdorove', 53],
    'Мода и стиль|Красота / здоровье|Товары для инвалидов': ['Tovary dlya invalidov', 'Sport i zdorove', 54],
    'Хобби, отдых и спорт|Книги / журналы': ['Knigi, pressa', 'Hobbi i dosug', 55],
    'Хобби, отдых и спорт|CD / DVD / пластинки / кассеты': ['CD / DVD plastinki', 'Hobbi i dosug', 56],
    'Hobbi i dosug|Ohota': ['Ohota', 'Hobbi i dosug', 58],
    'Хобби, отдых и спорт|Другое': ['Prochee', 'Hobbi i dosug', 61],
    'Животные|Кошки': ['Koshki', 'Zhivotnye i dlya zhivotnyh', 62],
    'Животные|Собаки': ['Sobaki', 'Zhivotnye i dlya zhivotnyh', 63],
    'Животные|Аквариумистика': ['Akvariumy', 'Zhivotnye i dlya zhivotnyh', 64],
    'Животные|Птицы': ['Pernatye', 'Zhivotnye i dlya zhivotnyh', 65],
    'Животные|Рептилии': ['Reptilii', 'Zhivotnye i dlya zhivotnyh', 66],
    # грызуны
    'Животные|Другие  животные': [
        'Drugie domashnie pitomcy', 'Zhivotnye i dlya zhivotnyh', 67],
    'Животные|Грызуны': [
        'Drugie domashnie pitomcy', 'Zhivotnye i dlya zhivotnyh', 67],
    'Животные|Сельхоз животные': ['Selhoz zhivotnye', 'Zhivotnye i dlya zhivotnyh', 68],
    'Zhivotnye i dlya zhivotnyh|Pitanie i aksessuary': ['Pitanie i aksessuary', 'Zhivotnye i dlya zhivotnyh', 69],
    'Животные|Зоотовары': ['Veterinarnye Zootovary', 'Zhivotnye i dlya zhivotnyh', 70],
    'Zhivotnye i dlya zhivotnyh|Uhod za zhivotnymi': ['Uhod za zhivotnymi', 'Zhivotnye i dlya zhivotnyh', 71],
    'Животные|Вязка': ['Vyazka', 'Zhivotnye i dlya zhivotnyh', 72],
    'Zhivotnye i dlya zhivotnyh|Otdam besplatno': ['Otdam besplatno', 'Zhivotnye i dlya zhivotnyh', 73],
    'Zhivotnye i dlya zhivotnyh|Drugoe': ['Drugoe', 'Zhivotnye i dlya zhivotnyh', 74],
    'Животные|Бюро находок': ['Byurnahodok', 'Zhivotnye i dlya zhivotnyh', 75],
    'Дом и сад|Посуда / кухонная утварь': ['Vse dlya kuhni', 'Dom i sad', 76],
    'Dom i sad|Vse dlya vannoj komnaty': ['Vse dlya vannoj komnaty', 'Dom i sad', 77],
    'Дом и сад|Предметы интерьера|Светильники': ['Predmety Interera, osveshchenie', 'Dom i sad', 78],
    'Дом и сад|Предметы интерьера|Декор окон': ['Predmety Interera, osveshchenie', 'Dom i sad', 78],
    'Дом и сад|Предметы интерьера|Текстиль': ['Tekstil', 'Dom i sad', 81],
    'Dom i sad|Bytovaya himiya': ['Bytovaya himiya', 'Dom i sad', 82],
    'Дом и сад|Продукты питания / напитки': ['Produkty pitaniya', 'Dom i sad', 85],
    'Дом и сад|Канцтовары / расходные материалы': ['Kanctovary', 'Dom i sad', 86],
    'Дом и сад|Прочие товары для дома': ['Prochee', 'Dom i sad', 87],
    'Дом и сад|Строительство / ремонт|Отопление': ['Otoplenie', 'Remont i stroitelstvo', 88],
    'Дом и сад|Строительство / ремонт|Прочие стройматериалы': ['Drugie strojmaterialy', 'Remont i stroitelstvo', 89],
    'Дом и сад|Строительство / ремонт|Окна / двери / стеклo / зеркала': [
        'Okna / dveri / stekl/ zerkala', 'Remont i stroitelstvo', 90],
    'Дом и сад|Строительство / ремонт|Отделочные и облицовочные материалы': [
        'Otdelochnye i oblicovochnye materialy', 'Remont i stroitelstvo', 91],
    'Дом и сад|Строительство / ремонт|Электрика': ['Elektrichestvo', 'Remont i stroitelstvo', 92],
    'Дом и сад|Строительство / ремонт|Сантехника': ['Santehnika', 'Remont i stroitelstvo', 93],
    'Дом и сад|Строительство / ремонт|Металлопрокат / арматура': ['Metalloprokat / armatura', 'Remont i stroitelstvo',
                                                                  94],
    'Дом и сад|Строительство / ремонт|Кирпич / бетон / пеноблоки': ['Kirpich / beton / penobloki',
                                                                    'Remont i stroitelstvo', 95],
    'Дом и сад|Строительство / ремонт|Пиломатериалы': ['Pilomaterialy', 'Remont i stroitelstvo', 96],
    'Дом и сад|Строительство / ремонт|Элементы крепежа': ['Elementy krepleniya', 'Remont i stroitelstvo', 97],
    'Дом и сад|Строительство / ремонт|Лакокрасочные материалы': ['Lakokrasochnye materialy', 'Remont i stroitelstvo',
                                                                 98],
    'Дом и сад|Строительство / ремонт|Вентиляция': ['Ventilyaciya', 'Remont i stroitelstvo', 99],
    'Remont i stroitelstvo|Uslugi': ['Uslugi', 'Remont i stroitelstvo', 100],
    'Транспорт|Сельхозтехника': ['Selhoztehnika', 'Transport', 102],
    'Транспорт|Грузовые автомобили': ['Gruzovye avtomobili', 'Transport', 104],
    'Транспорт|Водный транспорт ': ['Vodnyi transport', 'Transport', 106],
    'Транспорт|Прицепы': ['Pricepy', 'Transport', 108],
    'Транспорт|Автобусы': ['Avtobusy', 'Transport', 109],
    'Транспорт|Другой транспорт': ['Drugoj transport', 'Transport', 110],
    'Транспорт|Воздушный транспорт': ['Drugoj transport', 'Transport', 110],
    'Uslugi|Remontnye uslugi': ['Remontnye uslugi', 'Uslugi', 111],
    'Uslugi|Dostavka': ['Dostavka', 'Uslugi', 113],
    'Бизнес и услуги|Перевозки / аренда транспорта': ['Transportnye uslugi / Avtarenda', 'Uslugi', 114],
    'Бизнес и услуги|Услуги переводчиков / набор текста': ['Uslugi perevoda', 'Uslugi', 115],
    'Бизнес и услуги|Юридические услуги': ['YUridicheskie, notarialnye uslugi', 'Uslugi', 116],
    'Бизнес и услуги|Финансовые услуги / партнерство': ['Finansy, kredit, strahovanie', 'Uslugi', 117],
    'Uslugi|Informacionnye tehnologii, internet': ['Informacionnye tehnologii, internet', 'Uslugi', 118],
    'Uslugi|Organizaciya, obsluzhivanie meropriyatii': ['Organizaciya, obsluzhivanie meropriyatii', 'Uslugi', 120],
    'Бизнес и услуги|Туризм / иммиграция': ['Turizm, immigraciya', 'Uslugi', 121],
    'Uslugi|Reklama, marketing': ['Reklama, marketing', 'Uslugi', 122],
    'Uslugi|Bezopasnost, ohrana': ['Bezopasnost, ohrana', 'Uslugi', 123],
    'Электроника|Техника для кухни|Микроволновые печи': [
        'Mikrovolnovye pechi', 12, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Холодильники': [
        'Holodilniki', 13, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Плиты / печи': [
        'Plity / pechi', 14, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Электрочайники': [
        'Elektrochajniki', 15, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Кофеварки / кофемолки': [
        'Kofevarki / kofemolki', 16, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Кухонные комбайны / измельчители': [
        'Kuhonnye kombajny i izmelchiteli', 17, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Пароварки / мультиварки': [
        'Parovarki, multivarki', 18, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Хлебопечки': [
        'Hlebopechki', 19, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Посудомоечные машины': [
        'Posudomoechnye mashiny', 20, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Вытяжки': [
        'Vytyazhki', 21, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для кухни|Прочая техника для кухни': [
        'Drugaya tehnika dlya kuhni', 22, {'Tehnika dlya kuhni': ['Tehnika dlya kuhni', 3]}],
    'Электроника|Техника для дома|Пылесосы': ['Pylesosy', 23, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Электроника|Техника для дома|Утюги': ['Utyugi', 24, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Электроника|Техника для дома|Стиральные машины': [
        'Stiralnye mashiny', 25, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Электроника|Техника для дома|Швейные машины и оверлоки': [
        'SHvejnye mashiny i overloki', 26, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Электроника|Техника для дома|Вязальные машины': [
        'vyazalnye mashiny', 27, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Электроника|Техника для дома|Фильтры для воды': [
        'Filtry dlya vody', 28, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Электроника|Техника для дома|Прочая техника для дома': [
        'Drugaya tehnika dlya doma', 29, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Bytovaya technika|Tehnika dlya doma|Vodonagrevateli': [
        'Vodonagrevateli', 30, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Bytovaya technika|Tehnika dlya doma|Aksessuary': [
        'Aksessuary', 31, {'Tehnika dlya doma': ['Tehnika dlya doma', 4]}],
    'Электроника|Индивидуальный уход|Бритвы, эпиляторы, машинки для стрижки': [
        'Britvy, epilyatory, mashinki dlya strizhki', 32,
        {'Tehnika dlya uhoda za soboj': ['Tehnika dlya uhoda za soboj', 5]}],
    'Электроника|Индивидуальный уход|Фены, укладка волос': [
        'Feny, ukladka volos', 33, {'Tehnika dlya uhoda za soboj': ['Tehnika dlya uhoda za soboj', 5]}],
    'Электроника|Индивидуальный уход|Весы': [
        'Vesy', 34, {'Tehnika dlya uhoda za soboj': ['Tehnika dlya uhoda za soboj', 5]}],
    'Электроника|Индивидуальный уход|Прочая техника для индивидуального ухода': [
        'Drugaya tehnika dlya individualnoguhoda', 35,
        {'Tehnika dlya uhoda za soboj': ['Tehnika dlya uhoda za soboj', 5]}],
    'Электроника|Аудиотехника|Mp3 плееры': ['Mp3 pleery', 36, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Магнитолы': ['Magnitoly', 37, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Музыкальные центры': ['Muzykalnye centry', 38, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Акустические системы': [
        'Akusticheskie sistemy', 39, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Наушники': ['Naushniki', 40, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Радиоприемники': ['Radiopriemniki', 41, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Портативная акустика': [
        'Portativnaya akustika', 42, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Усилители / ресиверы': [
        'Usiliteli / resivery', 43, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Cd / md / виниловые проигрыватели': [
        'Cd / md / vinilovye proigryvateli', 44, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Прочая аудиотехника': [
        'Prochaya audiotehnika', 45, {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Фото / видео|Пленочные фотоаппараты': [
        'Plenochnye fotoapparaty', 46, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Электроника|Фото / видео|Цифровые фотоаппараты': [
        'Cifrovye fotoapparaty', 47, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Электроника|Фото / видео|Видеокамеры': [
        'Videokamery', 48, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Электроника|Фото / видео|Объективы': ['Obektivy', 49, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Электроника|Фото / видео|Штативы / моноподы': [
        'SHtativy / monopody', 50, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Электроника|Фото / видео|Фотовспышки': [
        'Fotovspyshki', 51, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Elektronika|Foto/ videotehnika|Fotoramki, fotoalbomy': [
        'Fotoramki, fotoalbomy', 52, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Электроника|Фото / видео|Аксессуары для фото / видеокамер': [
        'Aksessuary, oborudovanie', 53, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Электроника|Фото / видео|Телескопы / бинокли': [
        'Teleskopy / binokli', 54, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Elektronika|Foto/ videotehnika|Zapchasti i komplektuyushchie': [
        'Zapchasti i komplektuyushchie', 55, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Elektronika|Foto/ videotehnika|Drugoe': ['Drugoe', 56, {'Foto/ videotehnika': ['Foto/ videotehnika', 8]}],
    'Электроника|Игры и игровые приставки|Игры для приставок': [
        'Igry dlya pristavok', 57, {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Электроника|Игры и игровые приставки|Приставки': ['tehnika', 58,
                                                       {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Электроника|Игры и игровые приставки|Аксессуары': [
        'Aksessuary', 59, {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Электроника|Игры и игровые приставки|Ремонт приставок': [
        'Remont pristavok', 60, {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Elektronika|Igry, pristavki, PO|Drugoe': ['Drugoe', 61, {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Электроника|Игры и игровые приставки|Игры для PC': [
        'Igry dlya PK', 62, {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Elektronika|Igry, pristavki, PO|Operacionnye sistemy': [
        'Operacionnye sistemy', 63, {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Elektronika|Igry, pristavki, PO|Ofisnye prilozheniya': [
        'Ofisnye prilozheniya', 64, {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Elektronika|Igry, pristavki, PO|Utility, antivirusy': [
        'Utility, antivirusy', 65, {'Igry, pristavki, PO': ['Igry, pristavki, PO', 9]}],
    'Электроника|Компьютеры|Комплектующие': [
        'Komplektuyushchie', 66, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Электроника|Прочая электроника': [
        'Moduli pamyati', 67, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Videokarty': [
        'Videokarty', 68, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Zvukovye karty': [
        'Zvukovye karty', 69, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Materinskie platy': [
        'Materinskie platy', 70, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Opticheskie privody': [
        'Opticheskie privody', 71, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|ZHestkie diski': [
        'ZHestkie diski', 72, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Korpusa': [
        'Korpusa', 73, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Processory': [
        'Processory', 74, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|TV-tyunery': [
        'TV-tyunery', 75, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Bloki pitaniya': [
        'Bloki pitaniya', 76, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Sistemy ohlazhdeniya': [
        'Sistemy ohlazhdeniya', 77, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    # SIC!!!
    'Электроника|Аксессуары и комплектующие': [
        'Operativnaya pamyat', 78, {'Komplektuyushchie i aksessuary': ['Komplektuyushchie i aksessuary', 10]}],
    'Электроника|Компьютеры|Ноутбуки': ['V', 79, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|s': ['s', 80, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|e': ['e', 81, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki| ': [' ', 82, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|n': ['n', 83, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|o': ['o', 84, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|t': ['t', 85, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|b': ['b', 86, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|i': ['i', 87, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|k': ['k', 88, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|i': ['i', 89, {'Noutbuki': ['Noutbuki', 11]}],
    'Электроника|Компьютеры|Настольные': ['PK', 90, {'PK i orgtehnika': ['PK i orgtehnika', 12]}],
    'Электроника|Компьютеры|Другое': ['PK', 90, {'PK i orgtehnika': ['PK i orgtehnika', 12]}],
    'Электроника|Компьютеры|Периферийные устройства': [
        'Printery, skanery, faksy', 91, {'PK i orgtehnika': ['PK i orgtehnika', 12]}],
    'Электроника|Компьютеры|Расходные материалы': [
        'Printery, skanery, faksy', 91, {'PK i orgtehnika': ['PK i orgtehnika', 12]}],
    # компьютеры - аксессуары
    # расходные материалы
    # другое
    'Электроника|Компьютеры|Аксессуары': [
        'Myshki, klaviatura', 92, {'PK i orgtehnika': ['PK i orgtehnika', 12]}],
    'Электроника|Компьютеры|Внешние накопители': [
        'Fleshki, zhestkie diski', 93, {'PK i orgtehnika': ['PK i orgtehnika', 12]}],
    'Электроника|Компьютеры|Серверы': [
        'Setevoe oborudovanie', 94, {'PK i orgtehnika': ['PK i orgtehnika', 12]}],
    'Электроника|Компьютеры|Мониторы': ['Monitory', 95, {'PK i orgtehnika': ['PK i orgtehnika', 12]}],
    'Электроника|Компьютеры|Планшетные компьютеры': [
        'Planshety', 98, {'Telefony i planshety': ['Telefony i planshety', 14]}],
    'Электроника|Телефоны|Ремонт / прошивка': [
        'Remont i proshivka', 99, {'Telefony i planshety': ['Telefony i planshety', 14]}],
    'Электроника|Телефоны|Мобильные телефоны': [
        'Mobilnye telefony', 100, {'Telefony i planshety': ['Telefony i planshety', 14]}],
    'Elektronika|Aksessuary dlya telefoniv|Akkumulyatory': [
        'Akkumulyatory', 101, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Antenny, usiliteli': [
        'Antenny, usiliteli', 102, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Dinamiki, mikrofony': [
        'Dinamiki, mikrofony', 103, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Displei, sensornye ekrany': [
        'Displei, sensornye ekrany', 104, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Электроника|Телефоны|Аксессуары / запчасти': [
        'Drugoe', 105, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Zaryadnye ustrojstva': [
        'Zaryadnye ustrojstva', 106, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Kabeli': [
        'Kabeli', 107, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Karty pamyati': [
        'Karty pamyati', 108, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Klaviatury': [
        'Klaviatury', 109, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Korpusa': [
        'Korpusa', 110, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Mikroshemy': [
        'Mikroshemy', 111, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Plenki dlya ekranov': [
        'Plenki dlya ekranov', 112, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Provodnye garnitury, naushniki': [
        'Provodnye garnitury, naushniki', 113, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Razemy, gnezda': [
        'Razemy, gnezda', 114, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|CHehly i futlyary': [
        'CHehly i futlyary', 115, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Aksessuary dlya telefoniv|SHlejfy, perehodniki': [
        'SHlejfy, perehodniki', 116, {'Aksessuary dlya telefoniv': ['Aksessuary dlya telefoniv', 15]}],
    'Elektronika|Sim-karty / tarify / nomera|Beeline': [
        'Beeline', 117, {'Sim-karty / tarify / nomera': ['Sim-karty / tarify / nomera', 17]}],
    'Elektronika|Sim-karty / tarify / nomera|Life': [
        'Life', 118, {'Sim-karty / tarify / nomera': ['Sim-karty / tarify / nomera', 17]}],
    'Elektronika|Sim-karty / tarify / nomera|PEOPLEnet': [
        'PEOPLEnet', 119, {'Sim-karty / tarify / nomera': ['Sim-karty / tarify / nomera', 17]}],
    'Elektronika|Sim-karty / tarify / nomera|Utel': [
        'Utel', 120, {'Sim-karty / tarify / nomera': ['Sim-karty / tarify / nomera', 17]}],
    'Электроника|Телефоны|Сим-карты / тарифы / номера': [
        'Drugoe', 121, {'Sim-karty / tarify / nomera': ['Sim-karty / tarify / nomera', 17]}],
    'Elektronika|Sim-karty / tarify / nomera|Kievstar, Djuice': [
        'Kievstar, Djuice', 122, {'Sim-karty / tarify / nomera': ['Sim-karty / tarify / nomera', 17]}],
    'Elektronika|Sim-karty / tarify / nomera|MTS': [
        'MTS', 123, {'Sim-karty / tarify / nomera': ['Sim-karty / tarify / nomera', 17]}],
    'Мода и стиль|Одежда/обувь|Одежда для беременных': [
        'Odezhda dlya beremennyh', 124, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Bluzki, rubashki': [
        'Bluzki, rubashki', 125, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Bryuki': ['Bryuki', 126, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Verhnyaya odezhda': [
        'Verhnyaya odezhda', 127, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|ZHilety': ['ZHilety', 128, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Kostyumy': ['Kostyumy', 129, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|kofty': ['kofty', 130, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Мода и стиль|Одежда/обувь|Женское белье, купальники': [
        'Kupalniki, pareo', 131, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Losiny, legginsy': [
        'Losiny, legginsy', 132, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Majki': ['Majki', 133, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Nizhnee belyo': [
        'Nizhnee belyo', 134, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Pidzhaki': ['Pidzhaki', 135, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Platya': ['Platya', 136, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Sarafany': ['Sarafany', 137, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Svitera': ['Svitera', 138, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Topy': ['Topy', 139, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Tuniki': ['Tuniki', 140, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Futbolki': ['Futbolki', 141, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Hudi, tolstovki': [
        'Hudi, tolstovki', 142, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Futbolka': ['Futbolka', 143, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|YUbki': ['YUbki', 144, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Мода и стиль|Одежда/обувь|Женская одежда': ['Drugoe', 145, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Sportivnye kostyumy': [
        'Sportivnye kostyumy', 146, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Мода и стиль|Для свадьбы|Свадебные платья/костюмы': [
        'Мода и стиль|Для свадьбы|Свадебные аксессуары', 147, {'ZHenskaya odezhda': ['ZHenskaya odezhda', 19]}],
    'Мода и стиль|Одежда/обувь|Мужское белье': [
        'Nizhnee bele, plavki', 148, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Futbolki, majki': [
        'Futbolki, majki', 149, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Rubashki': ['Rubashki', 150, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|kofty': ['kofty', 151, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Svitera': ['Svitera', 152, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Tolstovki': ['Tolstovki', 153, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Kostyumy, pidzhaki, zhilety': [
        'Kostyumy, pidzhaki, zhilety', 154, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Bryuki': ['Bryuki', 155, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Futbolka': ['Futbolka', 156, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Verhnyaya odezhda': [
        'Verhnyaya odezhda', 157, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Мода и стиль|Одежда/обувь|Мужская одежда': ['Drugoe', 158, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Sportivnye kostyumy': [
        'Sportivnye kostyumy', 159, {'Muzhskaya odezhda': ['Muzhskaya odezhda', 20]}],
    'Odezhda, obuv|ZHenskaya obuv|Baletki': ['Baletki', 160, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Odezhda, obuv|ZHenskaya obuv|Mokasiny': ['Mokasiny', 161, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Odezhda, obuv|ZHenskaya obuv|Tufli': ['Tufli', 162, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Odezhda, obuv|ZHenskaya obuv|Bosonozhki': ['Bosonozhki', 163, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Odezhda, obuv|ZHenskaya obuv|SHlepancy': ['SHlepancy', 164, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Odezhda, obuv|ZHenskaya obuv|Botinki': ['Botinki', 165, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Odezhda, obuv|ZHenskaya obuv|Sapogi': ['Sapogi', 166, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Мода и стиль|Одежда/обувь|Женская обувь': ['Drugoe', 167, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Odezhda, obuv|ZHenskaya obuv|Sportivnaya obuv': [
        'Sportivnaya obuv', 168, {'ZHenskaya obuv': ['ZHenskaya obuv', 21]}],
    'Odezhda, obuv|Muzhskaya obuv|Sapogi, botinki': [
        'Sapogi, botinki', 169, {'Muzhskaya obuv': ['Muzhskaya obuv', 22]}],
    'Odezhda, obuv|Muzhskaya obuv|Tufli, mokasiny': [
        'Tufli, mokasiny', 170, {'Muzhskaya obuv': ['Muzhskaya obuv', 22]}],
    'Odezhda, obuv|Muzhskaya obuv|Sandalii, shlepancy': [
        'Sandalii, shlepancy', 171, {'Muzhskaya obuv': ['Muzhskaya obuv', 22]}],
    'Odezhda, obuv|Muzhskaya obuv|Sportivnaya obuv': [
        'Sportivnaya obuv', 172, {'Muzhskaya obuv': ['Muzhskaya obuv', 22]}],
    'Мода и стиль|Одежда/обувь|Мужская обувь': ['Drugaya obuv', 173, {'Muzhskaya obuv': ['Muzhskaya obuv', 22]}],
    # ЯКОГО ДІДЬКА? ВИНЕСТИ
    'Мода и стиль|Одежда/обувь|Головные уборы': ['Golovnye ubory', 174, {'Muzhskaya obuv': ['Muzhskaya obuv', 22]}],
    'Krasota i aksessuary|Kosmetika i uhod|Vsydlya volos': [
        'Vsydlya volos', 182, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Vsydlya nogtej': [
        'Vsydlya nogtej', 183, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Мода и стиль|Красота / здоровье|Косметика': [
        'Dekorativnaya kosmetika', 184, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Мода и стиль|Красота / здоровье|Средства по уходу': ['Drugoe', 185,
                                                          {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Kosmetika solncezashchitnaya, dlya solyariya': [
        'Kosmetika solncezashchitnaya, dlya solyariya', 186, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    '': [
        'Parfyumeriya', 187, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Sredstva dlya britya / depilyacii': [
        'Sredstva dlya britya / depilyacii', 188, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Uslugi': ['Uslugi', 189, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Uhod za licom i telom': [
        'Uhod za licom i telom', 190, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Uhod za polostyu rta': [
        'Uhod za polostyu rta', 191, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Pribory i aksessuary': [
        'Pribory i aksessuary', 192, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Sredstva dlya volos': [
        'Sredstva dlya volos', 193, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Detskii mir|Detskaya odezhda|Verhnyaya odezhda': [
        'Verhnyaya odezhda', 194, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Golovnye ubory, sharfy': [
        'Golovnye ubory, sharfy', 195, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Детский мир|Детская одежда|Одежда для новорожденных': [
        'Dlya mladencev', 196, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Детский мир|Детская одежда|Одежда для девочек': ['Drugoe', 197, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Детский мир|Детская одежда|Одежда для мальчиков': ['Drugoe', 197, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Детский мир|Детская одежда|Одежда для девочек': ['Drugoe', 197, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Karnavalnye kostyumy': [
        'Karnavalnye kostyumy', 198, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Kolgotki, noski, golfy': [
        'Kolgotki, noski, golfy', 199, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|kofty, reglany': [
        'kofty, reglany', 200, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Kupalniki, plavki': [
        'Kupalniki, plavki', 201, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Nizhnee bele': ['Nizhnee bele', 202, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Perchatki, varezhki': [
        'Perchatki, varezhki', 203, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Pidzhaki, rubashki': [
        'Pidzhaki, rubashki', 204, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Pizhamy, halaty': [
        'Pizhamy, halaty', 205, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Platya, sarafany': [
        'Platya, sarafany', 206, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Sportivnye kostyumy': [
        'Sportivnye kostyumy', 207, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|Futbolki, majki': [
        'Futbolki, majki', 208, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|SHkolnaya forma': [
        'SHkolnaya forma', 209, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|SHorty, bridzhi, kapri': [
        'SHorty, bridzhi, kapri', 210, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|SHtany': ['SHtany', 211, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Detskii mir|Detskaya odezhda|YUbki': ['YUbki', 212, {'Detskaya odezhda': ['Detskaya odezhda', 31]}],
    'Детский мир|Детская обувь': ['Drugoe', 213, {'Detskaya obuv': ['Detskaya obuv', 32]}],
    'Detskii mir|Detskaya obuv|Krossovki, kedy, mokasiny, butsy': [
        'Krossovki, kedy, mokasiny, butsy', 214, {'Detskaya obuv': ['Detskaya obuv', 32]}],
    'Detskii mir|Detskaya obuv|Sandalii, bosonozhki, shlepancy': [
        'Sandalii, bosonozhki, shlepancy', 215, {'Detskaya obuv': ['Detskaya obuv', 32]}],
    'Detskii mir|Detskaya obuv|Sapogi, botinki': ['Sapogi, botinki', 216, {'Detskaya obuv': ['Detskaya obuv', 32]}],
    'Detskii mir|Detskaya obuv|Tapochki, cheshki': ['Tapochki, cheshki', 217, {'Detskaya obuv': ['Detskaya obuv', 32]}],
    'Detskii mir|Detskaya obuv|Tufli': ['Tufli', 218, {'Detskaya obuv': ['Detskaya obuv', 32]}],
    'Detskii mir|Aksessuary dlya detej|Galstuki, babochki': [
        'Galstuki, babochki', 219, {'Aksessuary dlya detej': ['Aksessuary dlya detej', 33]}],
    'Detskii mir|Aksessuary dlya detej|Drugoe': [
        'Drugoe', 220, {'Aksessuary dlya detej': ['Aksessuary dlya detej', 33]}],
    'Detskii mir|Aksessuary dlya detej|Zonty, dozhdeviki': [
        'Zonty, dozhdeviki', 221, {'Aksessuary dlya detej': ['Aksessuary dlya detej', 33]}],
    'Detskii mir|Aksessuary dlya detej|Kosmetika i ukrasheniya dlya devochek': [
        'Kosmetika i ukrasheniya dlya devochek', 222, {'Aksessuary dlya detej': ['Aksessuary dlya detej', 33]}],
    'Detskii mir|Aksessuary dlya detej|Ochki i aksessuary': [
        'Ochki i aksessuary', 223, {'Aksessuary dlya detej': ['Aksessuary dlya detej', 33]}],
    'Detskii mir|Aksessuary dlya detej|Rezinki, zakolki, obruchi': [
        'Rezinki, zakolki, obruchi', 224, {'Aksessuary dlya detej': ['Aksessuary dlya detej', 33]}],
    'Detskii mir|Aksessuary dlya detej|Remni, poyasa, podtyazhki': [
        'Remni, poyasa, podtyazhki', 225, {'Aksessuary dlya detej': ['Aksessuary dlya detej', 33]}],
    'Detskii mir|Aksessuary dlya detej|Ryukzaki, sumki': [
        'Ryukzaki, sumki', 226, {'Aksessuary dlya detej': ['Aksessuary dlya detej', 33]}],
    'Detskii mir|Zdorove i gigiena|Vse dlya kormleniya': [
        'Vse dlya kormleniya', 227, {'Zdorove i gigiena': ['Zdorove i gigiena', 43]}],
    'Detskii mir|Zdorove i gigiena|Vse dlya kupaniya': [
        'Vse dlya kupaniya', 228, {'Zdorove i gigiena': ['Zdorove i gigiena', 43]}],
    'Detskii mir|Zdorove i gigiena|Gorshki, nakladki na unitaz': [
        'Gorshki, nakladki na unitaz', 229, {'Zdorove i gigiena': ['Zdorove i gigiena', 43]}],
    'Detskii mir|Zdorove i gigiena|Gradusniki, termometry, vesy': [
        'Gradusniki, termometry, vesy', 230, {'Zdorove i gigiena': ['Zdorove i gigiena', 43]}],
    'Detskii mir|Zdorove i gigiena|Drugoe': ['Drugoe', 231, {'Zdorove i gigiena': ['Zdorove i gigiena', 43]}],
    'Detskii mir|Zdorove i gigiena|Krema, masla, prisypka': [
        'Krema, masla, prisypka', 232, {'Zdorove i gigiena': ['Zdorove i gigiena', 43]}],
    'Detskii mir|Zdorove i gigiena|Podguzniki, salfetki': [
        'Podguzniki, salfetki', 233, {'Zdorove i gigiena': ['Zdorove i gigiena', 43]}],
    'Sport i zdorove|Sportinventar|Alpinizm, speleologiya': [
        'Alpinizm, speleologiya', 234, {'Sportinventar': ['Sportinventar', 45]}],
    'Sport i zdorove|Sportinventar|arbalety, luki, strely': [
        'arbalety, luki, strely', 235, {'Sportinventar': ['Sportinventar', 45]}],
    'Sport i zdorove|Sportinventar|Bilyard': ['Bilyard', 236, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Единоборства / бокс': [
        'Boevye iskusstva', 237, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Водные виды спорта': ['Vodnyi sport', 238,
                                                              {'Sportinventar': ['Sportinventar', 45]}],
    'Sport i zdorove|Sportinventar|Ganteli, giri, shtangi': [
        'Ganteli, giri, shtangi', 239, {'Sportinventar': ['Sportinventar', 45]}],
    'Sport i zdorove|Sportinventar|Darts': ['Darts', 240, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Прочие виды спорта': ['Drugoe', 241, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Лыжи / сноуборды': ['Zimnii sport', 242,
                                                            {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Коньки': ['Zimnii sport', 242, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Футбол': ['Igry s myachom', 243, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Настольные игры': ['Nastolnye igry', 244,
                                                           {'Sportinventar': ['Sportinventar', 45]}],
    'Sport i zdorove|Sportinventar|Pejntbol, strajkbol': [
        'Pejntbol, strajkbol', 245, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Роликовые коньки': ['Roliki, skejty', 246,
                                                            {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Хоккей': [
        'Sportivnye ugolki', 247, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Игры с ракеткой': [
        'Tennis, badminton', 248, {'Sportinventar': ['Sportinventar', 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Атлетика / фитнес': [
        'Fitnes, joga, aerobika, gimnastika', 249, {'Sportinventar': ['Sportinventar', 45]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Dorozhnye sumki': [
        'Dorozhnye sumki', 250, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Хобби, отдых и спорт|Спорт / отдых|Туризм': [
        'Drugoe', 251, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Mangaly, gril, shampura': [
        'Mangaly, gril, shampura', 252, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Metalloiskateli': [
        'Metalloiskateli', 253, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Naduvnye bassejny': [
        'Naduvnye bassejny', 254, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Nozhi, multituly': [
        'Nozhi, multituly', 255, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Palatki': [
        'Palatki', 256, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Posuda, flyagi, termosy': [
        'Posuda, flyagi, termosy', 257, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Pohodnyi inventar': [
        'Pohodnyi inventar', 258, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|putyovki, tury': [
        'putyovki, tury', 259, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Ryukzaki': [
        'Ryukzaki', 260, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Spalnye meshki, gamaki': [
        'Spalnye meshki, gamaki', 261, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Fonari, fonariki': [
        'Fonari, fonariki', 262, {'Vsydlya otdyha, turizm': ['Vsydlya otdyha, turizm', 46]}],
    'Sport i zdorove|Pribory i ustrojstva|Vesy': ['Vesy', 263, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Drugoe': [
        'Drugoe', 264, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Kardiopribory, pribory dlya pulsa': [
        'Kardiopribory, pribory dlya pulsa', 265, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Massazhery': [
        'Massazhery', 266, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Poyasa, miostimulyatory': [
        'Poyasa, miostimulyatory', 267, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Pribory magnitoterapii': [
        'Pribory magnitoterapii', 268, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Sluhovye apparaty': [
        'Sluhovye apparaty', 269, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Termometry, gradusniki': [
        'Termometry, gradusniki', 270, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Tonometry': [
        'Tonometry', 271, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Filtry': [
        'Filtry', 272, {'Pribory i ustrojstva': ['Pribory i ustrojstva', 47]}],
    'Sport i zdorove|Trenazhery|Begovye dorozhki': ['Begovye dorozhki', 273, {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Trenazhery|Velotrenazhyory': ['Velotrenazhyory', 274, {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Trenazhery|Drugie': ['Drugie', 275, {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Trenazhery|Orbitreki': ['Orbitreki', 276, {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Trenazhery|Silovye trenazhyory': ['Silovye trenazhyory', 277, {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Trenazhery|skami i stojki': ['skami i stojki', 278, {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Trenazhery|steppery': ['steppery', 279, {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Velosipedy i aksessuary k nim|Smartfony': [
        'Smartfony', 286, {'Velosipedy i aksessuary k nim': ['Velosipedy i aksessuary k nim', 50]}],
    'Хобби, отдых и спорт|Спорт / отдых|Вело': [
        'Drugoe', 287, {'Velosipedy i aksessuary k nim': ['Velosipedy i aksessuary k nim', 50]}],
    'Sport i zdorove|Velosipedy i aksessuary k nim|Komplektuyushchie': [
        'Komplektuyushchie', 288, {'Velosipedy i aksessuary k nim': ['Velosipedy i aksessuary k nim', 50]}],
    'Sport i zdorove|Velosipedy i aksessuary k nim|Snaryazhenie, aksessuary': [
        'Snaryazhenie, aksessuary', 289, {'Velosipedy i aksessuary k nim': ['Velosipedy i aksessuary k nim', 50]}],
    'Sport i zdorove|Velosipedy i aksessuary k nim|Ekipirovka dlya velosporta': [
        'Ekipirovka dlya velosporta', 290, {'Velosipedy i aksessuary k nim': ['Velosipedy i aksessuary k nim', 50]}],
    'Sport i zdorove|Specodezhda, obuv|Drugoe': ['Drugoe', 291, {'Specodezhda, obuv': ['Specodezhda, obuv', 51]}],
    'Sport i zdorove|Specodezhda, obuv|Ohota, rybolovstvo': [
        'Ohota, rybolovstvo', 292, {'Specodezhda, obuv': ['Specodezhda, obuv', 51]}],
    'Sport i zdorove|Specodezhda, obuv|Turizm': ['Turizm', 293, {'Specodezhda, obuv': ['Specodezhda, obuv', 51]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Студийное оборудование': [
        'Studiinoe oborudovanie', 294, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Прочее': [
        'Drugoe', 295, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Аксессуары для музыкальных инструментов': [
        'Aksessuary dlya muzykalnyh instrumentov', 296, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Пианино / фортепиано / рояли': [
        'Pianin/ fortepian/ royali', 297, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Электрогитары': [
        'Elektrogitary', 298, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Акустические гитары': [
        'Akusticheskie gitary', 299, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Синтезаторы': [
        'vyhodnoj', 300, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Духовые инструменты': [
        'duhovye', 301, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Ударные инструменты': [
        'Udarnye instrumenty', 302, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Комбоусилители': [
        'Kombousiliteli', 303, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Бас-гитары': [
        'Bas-gitary', 304, {'Muzykalnye instrumenty': ['Muzykalnye instrumenty', 57]}],
    'Хобби, отдых и спорт|Спорт / отдых|Охота / рыбалка': ['Drugoe', 305, {'Rybolovstvo': ['Rybolovstvo', 59]}],
    'Hobbi i dosug|Rybolovstvo|Katushki': ['Katushki', 306, {'Rybolovstvo': ['Rybolovstvo', 59]}],
    'Hobbi i dosug|Rybolovstvo|Lodki, katamarany': ['Lodki, katamarany', 307, {'Rybolovstvo': ['Rybolovstvo', 59]}],
    'Hobbi i dosug|Rybolovstvo|Podvodnaya ohota': ['Podvodnaya ohota', 308, {'Rybolovstvo': ['Rybolovstvo', 59]}],
    'Hobbi i dosug|Rybolovstvo|Rybackii inventar': ['Rybackii inventar', 309, {'Rybolovstvo': ['Rybolovstvo', 59]}],
    'Hobbi i dosug|Rybolovstvo|Udilishcha': ['Udilishcha', 310, {'Rybolovstvo': ['Rybolovstvo', 59]}],
    'Hobbi i dosug|Rybolovstvo|Eholoty, radary': ['Eholoty, radary', 311, {'Rybolovstvo': ['Rybolovstvo', 59]}],
    'Дом и сад|Мебель|Мебель для гостиной': ['Mebel dlya gostinoj', 327, {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель на заказ': ['Mebel na zakaz', 328, {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель для спальни': ['Mebel dlya spalni', 329, {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Офисная мебель': ['Ofisnaya mebel', 330, {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Кухонная мебель': ['Mebel', 331, {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель для прихожей': ['Mebel dlya prihozhej', 332, {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель для ванной комнаты': ['Mebel dlya vannoj komnaty', 333, {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Инструменты|Электроинструмент': [
        'Elektroinstrument', 334, {'Instrumenty / inventar': ['Instrumenty / inventar', 80]}],
    'Дом и сад|Инструменты|Ручной инструмент': [
        'Ruchnoj instrument', 335, {'Instrumenty / inventar': ['Instrumenty / inventar', 80]}],
    'Дом и сад|Инструменты|Прочий инструмент': [
        'Drugoj instrument', 336, {'Instrumenty / inventar': ['Instrumenty / inventar', 80]}],
    'Дом и сад|Инструменты|Пневмоинструмент': [
        'Drugoj instrument', 336, {'Instrumenty / inventar': ['Instrumenty / inventar', 80]}],
    'Дом и сад|Инструменты|Бензоинструмент': [
        'Benzoinstrument', 337, {'Instrumenty / inventar': ['Instrumenty / inventar', 80]}],
    # перейменувати
    'Дом и сад|Хозяйственный инвентарь / бытовая химия': [
        'Komplektuyushchie, rashodnye materialy', 338, {'Instrumenty / inventar': ['Instrumenty / inventar', 80]}],
    'Dom i sad|Rasteniya|Gorshki, zemlya, udobreniya': [
        'Gorshki, zemlya, udobreniya', 339, {'Rasteniya': ['Rasteniya', 83]}],
    'Дом и сад|Комнатные растения': ['Rasteniya', 340, {'Rasteniya': ['Rasteniya', 83]}],
    'Dom i sad|Sad i ogorod|Vse dlya poliva': ['Vse dlya poliva', 341, {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Дом и сад|Сад / огород': ['Drugoe', 342, {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Dom i sad|Sad i ogorod|Sadovaya mebel, shatry, besedki': [
        'Sadovaya mebel, shatry, besedki', 343, {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Dom i sad|Sad i ogorod|Sadovaya tehnika': ['Sadovaya tehnika', 344, {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Dom i sad|Sad i ogorod|Sadovye svetilniki': ['Sadovye svetilniki', 345, {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Дом и сад|Садовый инвентарь': ['Sadovyi instrument', 346, {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Dom i sad|Sad i ogorod|Sredstva ot nasekomyh': [
        'Sredstva ot nasekomyh', 347, {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Dom i sad|Sad i ogorod|Udobreniya i himikaty': [
        'Udobreniya i himikaty', 348, {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Транспорт|Запчасти / аксессуары|Автозапчасти': [
        'Avtozapchasti', 349, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|Шины / диски': [
        'SHiny / diski', 350, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|Аксессуары для авто': [
        'Aksessuary dlya avto', 351, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|Запчасти для спец / с.х. техники': [
        'Zapchasti dlya spec / s.h.tehniki', 352, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|Мотозапчасти': [
        'Motozapchasti', 353, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|Автозвук': [
        'Avtozvuk', 354, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|Прочие запчасти': [
        'Drugie zapchasti', 355, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|Мото аксессуары': [
        'Motaksessuary', 356, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|Транспорт на запчасти': [
        'Transport na zapchasti', 357, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Транспорт|Запчасти / аксессуары|GPS-навигаторы / авторегистраторы': [
        'GPS-navigatory / avtoregistratory', 358, {'Zapchasti / aksessuary': ['Zapchasti / aksessuary', 101]}],
    'Transport|Legkovye avtomobili|V': ['V', 359, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|s': ['s', 360, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|e': ['e', 361, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili| ': [' ', 362, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|L': ['L', 363, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|e': ['e', 364, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|g': ['g', 365, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|k': ['k', 366, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|o': ['o', 367, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|v': ['v', 368, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|y': ['y', 369, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|e': ['e', 370, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili| ': [' ', 371, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|a': ['a', 372, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|v': ['v', 373, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|t': ['t', 374, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|o': ['o', 375, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|m': ['m', 376, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|o': ['o', 377, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|b': ['b', 378, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|i': ['i', 379, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|l': ['l', 380, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Transport|Legkovye avtomobili|i': ['i', 381, {'Legkovye avtomobili': ['Legkovye avtomobili', 103]}],
    'Транспорт|Мото|Мопеды / скутеры': ['Mopedy / skutery', 382, {'Moto': ['Moto', 105]}],
    'Транспорт|Мото|Мотоциклы': ['Motocikly', 383, {'Moto': ['Moto', 105]}],
    'Транспорт|Мото|Квадроциклы': ['Kvadrocikly', 384, {'Moto': ['Moto', 105]}],
    'Транспорт|Мото|Мото - прочее': ['Mot-drugoe', 385, {'Moto': ['Moto', 105]}],
    'Транспорт|Спецтехника|Бульдозеры / тракторы': ['Buldozery / traktory', 386, {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Погрузчики': ['Pogruzchiki', 387, {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Строительная техника': ['Stroitelnaya tehnika', 388, {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Прочая спецтехника': ['Drugaya spectehnika', 389, {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Оборудование для спецтехники': [
        'Oborudovanie dlya spectehniki', 390, {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Экскаваторы': ['Ekskavatory', 391, {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Коммунальная техника': ['Kommunalnaya tehnika', 392, {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Самосвалы': ['Samosvaly', 393, {'Spectehnika': ['Spectehnika', 107]}],
    # UBORKA???
    'Бизнес и услуги|Строительство / ремонт / уборка|Отделка / ремонт': ['Otdelka / remont', 394,
                                                                         {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Cтроительные услуги': ['Stroitelnye uslugi', 395,
                                                                            {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Бытовой ремонт / уборка': ['Bytovoj remont / uborka', 396,
                                                                                {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Окна / двери / балконы': ['Okna / dveri / balkony', 397,
                                                                               {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Готовые конструкции': ['Gotovye konstrukcii', 398,
                                                                            {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Сантехника / коммуникации': ['Santehnika / kommunikacii', 399,
                                                                                  {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Электрика': ['Elektrichestvo', 400, {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Дизайн / архитектура': ['Dizajn / arhitektura', 401,
                                                                             {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Вентиляция / кондиционирование': [
        'Ventilyaciya / kondicionirovanie', 402, {'Uborka': ['Uborka', 112]}],
    'Бизнес и услуги|Красота / здоровье|Маникюр / наращивание ногтей': [
        'Manikyur / narashchivanie nogtej', 403, {'Zdorove, krasota': ['Zdorove, krasota', 119]}],
    'Бизнес и услуги|Красота / здоровье|Макияж / косметология / наращивание ресниц': [
        'Makiyazh / kosmetologiya / narashchivanie resnic', 404, {'Zdorove, krasota': ['Zdorove, krasota', 119]}],
    'Uslugi|Zdorove, krasota|Krasota / zdorove ': [
        'Krasota / zdorove ', 405, {'Zdorove, krasota': ['Zdorove, krasota', 119]}],
    'Бизнес и услуги|Красота / здоровье|Красота / здоровье - прочее': ['prochee', 406,
                                                                       {'Zdorove, krasota': ['Zdorove, krasota', 119]}],
    'Бизнес и услуги|Красота / здоровье|Массаж': ['Massazh', 407, {'Zdorove, krasota': ['Zdorove, krasota', 119]}],
    'Бизнес и услуги|Красота / здоровье|Стрижки / наращивание волосs': [
        'Strizhki / narashchivanie volos', 408, {'Zdorove, krasota': ['Zdorove, krasota', 119]}],
    'Бизнес и услуги|Красота / здоровье|Медицина': ['Medicina', 409, {'Zdorove, krasota': ['Zdorove, krasota', 119]}],
    'Бизнес и услуги|Красота / здоровье|Услуги психолога': [
        'Uslugi psihologa', 410, {'Zdorove, krasota': ['Zdorove, krasota', 119]}],
    'Бизнес и услуги|Продажа бизнеса': ['Torgovlya', 'Uslugi', 124],
    'Uslugi|Poligrafiya, izdatelstvo': ['Poligrafiya, izdatelstvo', 'Uslugi', 125],
    'Бизнес и услуги|Услуги для животных': ['Uslugi puhodu za zhivotnymi', 'Uslugi', 126],
    'Бизнес и услуги|Развлечения / Искусство / Фото / Видео': ['Razvlecheniya, foto, video', 'Uslugi', 127],
    'Бизнес и услуги|Прокат товаров': ['Prokat', 'Uslugi', 128],
    'Бизнес и услуги|Няни / сиделки': ['Nyani, sidelki', 'Uslugi', 129],
    'Бизнес и услуги|Образование / Спорт': ['Obrazovanie', 'Uslugi', 130],
    'Uslugi|Pomoshch ekstrasensov': ['Pomoshch ekstrasensov', 'Uslugi', 131],
    'Uslugi|Sport': ['Sport', 'Uslugi', 132],
    'Uslugi|Selskoe hozyajstvo': ['Selskoe hozyajstvo', 'Uslugi', 133],
    'Бизнес и услуги|Прочие услуги': ['Prochee', 'Uslugi', 134],
    'Электроника|Климатическое оборудование': [
        'Kondicionery', 1, {'Klimaticheskaya tehnika': ['Klimaticheskaya tehnika', 1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Ventilyatory': [
        'Ventilyatory', 2, {'Klimaticheskaya tehnika': ['Klimaticheskaya tehnika', 1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Otoplenie': [
        'Otoplenie', 3, {'Klimaticheskaya tehnika': ['Klimaticheskaya tehnika', 1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Vozduhoochistiteli, uvlazhniteli': [
        'Vozduhoochistiteli, uvlazhniteli', 4, {'Klimaticheskaya tehnika': ['Klimaticheskaya tehnika', 1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Drugaya tehnika': [
        'Drugaya tehnika', 5, {'Klimaticheskaya tehnika': ['Klimaticheskaya tehnika', 1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Aksessuary i komplektuyushchie': [
        'Aksessuary i komplektuyushchie', 6, {'Klimaticheskaya tehnika': ['Klimaticheskaya tehnika', 1]}],
    'Электроника|Тв / видеотехника|Медиа проигрыватели': [
        'Proigryvateli', 7, {'Televizory / Videotehnika': ['Televizory / Videotehnika', 2]}],
    'Электроника|Тв / видеотехника|Телевизоры': [
        'Televizory', 8, {'Televizory / Videotehnika': ['Televizory / Videotehnika', 2]}],
    'Электроника|Тв / видеотехника|Аксессуары для ТВ / видео': [
        'Aksessuary dlya TV / videotehniki', 9, {'Televizory / Videotehnika': ['Televizory / Videotehnika', 2]}],
    'Электроника|Тв / видеотехника|Спутниковое ТВ': [
        'Sputnikovoe TV', 10, {'Televizory / Videotehnika': ['Televizory / Videotehnika', 2]}],
    'Электроника|Тв / видеотехника|Прочая ТВ / видеотехника': [
        'Drugaya TV / videotehnika', 11, {'Televizory / Videotehnika': ['Televizory / Videotehnika', 2]}],
    'Elektronika|Elektronnye knigi|KPK': ['KPK', 96, {'Elektronnye knigi': ['Elektronnye knigi', 13]}],
    'Elektronika|Elektronnye knigi|Aksessuary': ['Aksessuary', 97, {'Elektronnye knigi': ['Elektronnye knigi', 13]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Kolca': [
        'Kolca', 175, {'YUvelirnye izdeliya': ['YUvelirnye izdeliya', 26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Sergi': [
        'Sergi', 176, {'YUvelirnye izdeliya': ['YUvelirnye izdeliya', 26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Braslety': [
        'Braslety', 177, {'YUvelirnye izdeliya': ['YUvelirnye izdeliya', 26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Kulony / Podveski': [
        'Kulony / Podveski', 178, {'YUvelirnye izdeliya': ['YUvelirnye izdeliya', 26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Broshi': [
        'Broshi', 179, {'YUvelirnye izdeliya': ['YUvelirnye izdeliya', 26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Cepochki': [
        'Cepochki', 180, {'YUvelirnye izdeliya': ['YUvelirnye izdeliya', 26]}],
    'Мода и стиль|Аксессуары|Ювелирные изделия': [
        'Drugie yuvelirnye izdeliya', 181, {'YUvelirnye izdeliya': ['YUvelirnye izdeliya', 26]}],
    'Sport i zdorove|Zdorove|Aromaterapiya, svechi, lampy': [
        'Aromaterapiya, svechi, lampy', 280, {'Zdorove': ['Zdorove', 49]}],
    'Sport i zdorove|Zdorove|Lekarstva': ['Lekarstva', 281, {'Zdorove': ['Zdorove', 49]}],
    'Sport i zdorove|Zdorove|Sredstva dlya pohudeniya': ['Sredstva dlya pohudeniya', 282, {'Zdorove': ['Zdorove', 49]}],
    'Sport i zdorove|Zdorove|Drugoe': ['Drugoe', 283, {'Zdorove': ['Zdorove', 49]}],
    'Sport i zdorove|Zdorove|Netradicionnaya medicina': ['Netradicionnaya medicina', 284, {'Zdorove': ['Zdorove', 49]}],
    'Sport i zdorove|Zdorove|Ochki, linzy, aksessuary': ['Ochki, linzy, aksessuary', 285, {'Zdorove': ['Zdorove', 49]}],
    #   колекціонування переробити
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Numizmatika': [
        'Numizmatika', 312, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Коллекционирование': [
        'Drugie kategorii', 313, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Bonistika': [
        'Bonistika', 314, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Stolovoe serebro, antikvarnaya kuhonnaya utvar': [
        'Stolovoe serebro, antikvarnaya kuhonnaya utvar', 315,
        {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Filateliya': [
        'Filateliya', 316, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Живопись': [
        'Skulptury', 317, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Антикварная мебель': [
        'Kartiny, antikvarnaya mebel, predmety interera', 318,
        {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Поделки / рукоделие': [
        'Masshtabnye modeli', 319, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Militariya': [
        'Militariya', 320, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Medali, znachki, zhetony': [
        'Medali, znachki, zhetony', 321,
        {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Предметы искусства': [
        'Ikony, skladni, metalloplastika', 322,
        {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Букинистика': [
        'Knigi', 323, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Plastinki': [
        'Plastinki', 324, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Monety Ukrainy iz drag.splavov': [
        'Monety Ukrainy iz drag.splavov', 325,
        {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Kollekcionnoe oruzhie': [
        'Kollekcionnoe oruzhie', 326, {'Kollekcionirovanie / antikvariat': ['Kollekcionirovanie / antikvariat', 60]}],
    'Obshchenie|None|Poteri': ['Poteri', 411, {'None': ['None', 135]}],
    'Obshchenie|None|Nahodki': ['Nahodki', 412, {'None': ['None', 135]}],
    'Хобби, отдых и спорт|Билеты': ['Bilety na meropriyatiya', 413, {'None': ['None', 135]}],
    'Хобби, отдых и спорт|Поиск попутчиков': ['Poisk poputchikov', 414, {'None': ['None', 135]}],
    'Хобби, отдых и спорт|Поиск групп / музыкантов': ['Poisk grupp / muzykantov', 415, {'None': ['None', 135]}],
    'Obshchenie|None|Poisk lyudej': ['Poisk lyudej', 416, {'None': ['None', 135]}],
    'Drugoe|None|Tovary dlya vzroslyh': ['Tovary dlya vzroslyh', 417, {'None': ['None', 136]}],
    'Мода и стиль|Подарки': ['Podarki i suveniry', 418, {'None': ['None', 136]}],
    'Drugoe|None|Otdam besplatno': ['Otdam besplatno', 419, {'None': ['None', 136]}],
    'Drugoe|None|Obmen': ['Obmen', 420, {'None': ['None', 136]}]
}

NEW_OLX = {
    '': ['Parfyumeriya', 177, {'Kosmetika i uhod': ['Kosmetika i uhod', 27]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Aksessuary i komplektuyushchie': ['Aksessuary '
                                                                                 'i '
                                                                                 'komplektuyushchie',
                                                                                 6,
                                                                                 {'Klimaticheskaya tehnika': [
                                                                                     'Klimaticheskaya '
                                                                                     'tehnika',
                                                                                     1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Drugaya tehnika': ['Drugaya '
                                                                  'tehnika',
                                                                  5,
                                                                  {'Klimaticheskaya tehnika': ['Klimaticheskaya '
                                                                                               'tehnika',
                                                                                               1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Otoplenie': ['Otoplenie',
                                                            3,
                                                            {'Klimaticheskaya tehnika': ['Klimaticheskaya '
                                                                                         'tehnika',
                                                                                         1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Ventilyatory': ['Ventilyatory',
                                                               2,
                                                               {'Klimaticheskaya tehnika': ['Klimaticheskaya '
                                                                                            'tehnika',
                                                                                            1]}],
    'Bytovaya technika|Klimaticheskaya tehnika|Vozduhoochistiteli, uvlazhniteli': ['Vozduhoochistiteli, '
                                                                                   'uvlazhniteli',
                                                                                   4,
                                                                                   {'Klimaticheskaya tehnika': [
                                                                                       'Klimaticheskaya '
                                                                                       'tehnika',
                                                                                       1]}],
    'Bytovaya technika|Prochee / drugaya tehnika': ['Prochee / drugaya tehnika',
                                                    'Bytovaya technika',
                                                    6],
    'Bytovaya technika|Tehnika dlya doma|Aksessuary': ['Aksessuary',
                                                       31,
                                                       {'Tehnika dlya doma': ['Tehnika '
                                                                              'dlya '
                                                                              'doma',
                                                                              4]}],
    'Bytovaya technika|Tehnika dlya doma|Vodonagrevateli': ['Vodonagrevateli',
                                                            30,
                                                            {'Tehnika dlya doma': ['Tehnika '
                                                                                   'dlya '
                                                                                   'doma',
                                                                                   4]}],
    'Detskii mir|Aksessuary dlya detej|Drugoe': ['Drugoe',
                                                 210,
                                                 {'Aksessuary dlya detej': ['Aksessuary '
                                                                            'dlya '
                                                                            'detej',
                                                                            33]}],
    'Detskii mir|Aksessuary dlya detej|Galstuki, babochki': ['Galstuki, '
                                                             'babochki',
                                                             209,
                                                             {'Aksessuary dlya detej': ['Aksessuary '
                                                                                        'dlya '
                                                                                        'detej',
                                                                                        33]}],
    'Detskii mir|Aksessuary dlya detej|Kosmetika i ukrasheniya dlya devochek': ['Kosmetika '
                                                                                'i '
                                                                                'ukrasheniya '
                                                                                'dlya '
                                                                                'devochek',
                                                                                212,
                                                                                {'Aksessuary dlya detej': ['Aksessuary '
                                                                                                           'dlya '
                                                                                                           'detej',
                                                                                                           33]}],
    'Detskii mir|Aksessuary dlya detej|Ochki i aksessuary': ['Ochki i '
                                                             'aksessuary',
                                                             213,
                                                             {'Aksessuary dlya detej': ['Aksessuary '
                                                                                        'dlya '
                                                                                        'detej',
                                                                                        33]}],
    'Detskii mir|Aksessuary dlya detej|Remni, poyasa, podtyazhki': ['Remni, '
                                                                    'poyasa, '
                                                                    'podtyazhki',
                                                                    215,
                                                                    {'Aksessuary dlya detej': ['Aksessuary '
                                                                                               'dlya '
                                                                                               'detej',
                                                                                               33]}],
    'Detskii mir|Aksessuary dlya detej|Rezinki, zakolki, obruchi': ['Rezinki, '
                                                                    'zakolki, '
                                                                    'obruchi',
                                                                    214,
                                                                    {'Aksessuary dlya detej': ['Aksessuary '
                                                                                               'dlya '
                                                                                               'detej',
                                                                                               33]}],
    'Detskii mir|Aksessuary dlya detej|Ryukzaki, sumki': ['Ryukzaki, sumki',
                                                          216,
                                                          {'Aksessuary dlya detej': ['Aksessuary '
                                                                                     'dlya '
                                                                                     'detej',
                                                                                     33]}],
    'Detskii mir|Aksessuary dlya detej|Zonty, dozhdeviki': ['Zonty, dozhdeviki',
                                                            211,
                                                            {'Aksessuary dlya detej': ['Aksessuary '
                                                                                       'dlya '
                                                                                       'detej',
                                                                                       33]}],
    'Detskii mir|Detskaya literatura': ['Detskaya literatura', 'Detskii mir', 35],
    'Detskii mir|Detskaya obuv|Krossovki, kedy, mokasiny, butsy': ['Krossovki, '
                                                                   'kedy, '
                                                                   'mokasiny, '
                                                                   'butsy',
                                                                   204,
                                                                   {'Detskaya obuv': ['Detskaya '
                                                                                      'obuv',
                                                                                      32]}],
    'Detskii mir|Detskaya obuv|Sandalii, bosonozhki, shlepancy': ['Sandalii, '
                                                                  'bosonozhki, '
                                                                  'shlepancy',
                                                                  205,
                                                                  {'Detskaya obuv': ['Detskaya '
                                                                                     'obuv',
                                                                                     32]}],
    'Detskii mir|Detskaya obuv|Sapogi, botinki': ['Sapogi, botinki',
                                                  206,
                                                  {'Detskaya obuv': ['Detskaya '
                                                                     'obuv',
                                                                     32]}],
    'Detskii mir|Detskaya obuv|Tapochki, cheshki': ['Tapochki, cheshki',
                                                    207,
                                                    {'Detskaya obuv': ['Detskaya '
                                                                       'obuv',
                                                                       32]}],
    'Detskii mir|Detskaya obuv|Tufli': ['Tufli',
                                        208,
                                        {'Detskaya obuv': ['Detskaya obuv', 32]}],
    'Detskii mir|Detskaya odezhda|Futbolki, majki': ['Futbolki, majki',
                                                     198,
                                                     {'Detskaya odezhda': ['Detskaya '
                                                                           'odezhda',
                                                                           31]}],
    'Detskii mir|Detskaya odezhda|Golovnye ubory, sharfy': ['Golovnye ubory, '
                                                            'sharfy',
                                                            185,
                                                            {'Detskaya odezhda': ['Detskaya '
                                                                                  'odezhda',
                                                                                  31]}],
    'Detskii mir|Detskaya odezhda|Karnavalnye kostyumy': ['Karnavalnye kostyumy',
                                                          188,
                                                          {'Detskaya odezhda': ['Detskaya '
                                                                                'odezhda',
                                                                                31]}],
    'Detskii mir|Detskaya odezhda|Kolgotki, noski, golfy': ['Kolgotki, noski, '
                                                            'golfy',
                                                            189,
                                                            {'Detskaya odezhda': ['Detskaya '
                                                                                  'odezhda',
                                                                                  31]}],
    'Detskii mir|Detskaya odezhda|Kupalniki, plavki': ['Kupalniki, plavki',
                                                       191,
                                                       {'Detskaya odezhda': ['Detskaya '
                                                                             'odezhda',
                                                                             31]}],
    'Detskii mir|Detskaya odezhda|Nizhnee bele': ['Nizhnee bele',
                                                  192,
                                                  {'Detskaya odezhda': ['Detskaya '
                                                                        'odezhda',
                                                                        31]}],
    'Detskii mir|Detskaya odezhda|Perchatki, varezhki': ['Perchatki, varezhki',
                                                         193,
                                                         {'Detskaya odezhda': ['Detskaya '
                                                                               'odezhda',
                                                                               31]}],
    'Detskii mir|Detskaya odezhda|Pidzhaki, rubashki': ['Pidzhaki, rubashki',
                                                        194,
                                                        {'Detskaya odezhda': ['Detskaya '
                                                                              'odezhda',
                                                                              31]}],
    'Detskii mir|Detskaya odezhda|Pizhamy, halaty': ['Pizhamy, halaty',
                                                     195,
                                                     {'Detskaya odezhda': ['Detskaya '
                                                                           'odezhda',
                                                                           31]}],
    'Detskii mir|Detskaya odezhda|Platya, sarafany': ['Platya, sarafany',
                                                      196,
                                                      {'Detskaya odezhda': ['Detskaya '
                                                                            'odezhda',
                                                                            31]}],
    'Detskii mir|Detskaya odezhda|SHkolnaya forma': ['SHkolnaya forma',
                                                     199,
                                                     {'Detskaya odezhda': ['Detskaya '
                                                                           'odezhda',
                                                                           31]}],
    'Detskii mir|Detskaya odezhda|SHorty, bridzhi, kapri': ['SHorty, bridzhi, '
                                                            'kapri',
                                                            200,
                                                            {'Detskaya odezhda': ['Detskaya '
                                                                                  'odezhda',
                                                                                  31]}],
    'Detskii mir|Detskaya odezhda|SHtany': ['SHtany',
                                            201,
                                            {'Detskaya odezhda': ['Detskaya '
                                                                  'odezhda',
                                                                  31]}],
    'Detskii mir|Detskaya odezhda|Sportivnye kostyumy': ['Sportivnye kostyumy',
                                                         197,
                                                         {'Detskaya odezhda': ['Detskaya '
                                                                               'odezhda',
                                                                               31]}],
    'Detskii mir|Detskaya odezhda|Verhnyaya odezhda': ['Verhnyaya odezhda',
                                                       184,
                                                       {'Detskaya odezhda': ['Detskaya '
                                                                             'odezhda',
                                                                             31]}],
    'Detskii mir|Detskaya odezhda|YUbki': ['YUbki',
                                           202,
                                           {'Detskaya odezhda': ['Detskaya '
                                                                 'odezhda',
                                                                 31]}],
    'Detskii mir|Detskaya odezhda|kofty, reglany': ['kofty, reglany',
                                                    190,
                                                    {'Detskaya odezhda': ['Detskaya '
                                                                          'odezhda',
                                                                          31]}],
    'Detskii mir|Dlya mam': ['Dlya mam', 'Detskii mir', 38],
    'Detskii mir|Dlya samyh malenkih': ['Dlya samyh malenkih', 'Detskii mir', 36],
    'Detskii mir|Zdorove i gigiena|Drugoe': ['Drugoe',
                                             221,
                                             {'Zdorove i gigiena': ['Zdorove '
                                                                    'i '
                                                                    'gigiena',
                                                                    43]}],
    'Detskii mir|Zdorove i gigiena|Gorshki, nakladki na unitaz': ['Gorshki, '
                                                                  'nakladki na '
                                                                  'unitaz',
                                                                  219,
                                                                  {'Zdorove i gigiena': ['Zdorove '
                                                                                         'i '
                                                                                         'gigiena',
                                                                                         43]}],
    'Detskii mir|Zdorove i gigiena|Gradusniki, termometry, vesy': ['Gradusniki, '
                                                                   'termometry, '
                                                                   'vesy',
                                                                   220,
                                                                   {'Zdorove i gigiena': ['Zdorove '
                                                                                          'i '
                                                                                          'gigiena',
                                                                                          43]}],
    'Detskii mir|Zdorove i gigiena|Krema, masla, prisypka': ['Krema, masla, '
                                                             'prisypka',
                                                             222,
                                                             {'Zdorove i gigiena': ['Zdorove '
                                                                                    'i '
                                                                                    'gigiena',
                                                                                    43]}],
    'Detskii mir|Zdorove i gigiena|Podguzniki, salfetki': ['Podguzniki, '
                                                           'salfetki',
                                                           223,
                                                           {'Zdorove i gigiena': ['Zdorove '
                                                                                  'i '
                                                                                  'gigiena',
                                                                                  43]}],
    'Detskii mir|Zdorove i gigiena|Vse dlya kormleniya': ['Vse dlya kormleniya',
                                                          217,
                                                          {'Zdorove i gigiena': ['Zdorove '
                                                                                 'i '
                                                                                 'gigiena',
                                                                                 43]}],
    'Detskii mir|Zdorove i gigiena|Vse dlya kupaniya': ['Vse dlya kupaniya',
                                                        218,
                                                        {'Zdorove i gigiena': ['Zdorove '
                                                                               'i '
                                                                               'gigiena',
                                                                               43]}],
    'Dom i sad|Bytovaya himiya': ['Bytovaya himiya', 'Dom i sad', 82],
    'Dom i sad|Rasteniya|Gorshki, zemlya, udobreniya': ['Gorshki, zemlya, '
                                                        'udobreniya',
                                                        329,
                                                        {'Rasteniya': ['Rasteniya',
                                                                       83]}],
    'Dom i sad|Sad i ogorod|Sadovaya mebel, shatry, besedki': ['Sadovaya '
                                                               'mebel, shatry, '
                                                               'besedki',
                                                               333,
                                                               {'Sad i ogorod': ['Sad '
                                                                                 'i '
                                                                                 'ogorod',
                                                                                 84]}],
    'Dom i sad|Sad i ogorod|Sadovaya tehnika': ['Sadovaya tehnika',
                                                334,
                                                {'Sad i ogorod': ['Sad i '
                                                                  'ogorod',
                                                                  84]}],
    'Dom i sad|Sad i ogorod|Sadovye svetilniki': ['Sadovye svetilniki',
                                                  335,
                                                  {'Sad i ogorod': ['Sad i '
                                                                    'ogorod',
                                                                    84]}],
    'Dom i sad|Sad i ogorod|Sredstva ot nasekomyh': ['Sredstva ot nasekomyh',
                                                     337,
                                                     {'Sad i ogorod': ['Sad '
                                                                       'i '
                                                                       'ogorod',
                                                                       84]}],
    'Dom i sad|Sad i ogorod|Udobreniya i himikaty': ['Udobreniya i himikaty',
                                                     338,
                                                     {'Sad i ogorod': ['Sad '
                                                                       'i '
                                                                       'ogorod',
                                                                       84]}],
    'Dom i sad|Sad i ogorod|Vse dlya poliva': ['Vse dlya poliva',
                                               331,
                                               {'Sad i ogorod': ['Sad i '
                                                                 'ogorod',
                                                                 84]}],
    'Dom i sad|Vse dlya vannoj komnaty': ['Vse dlya vannoj komnaty',
                                          'Dom i sad',
                                          77],
    'Drugoe|None|Obmen': ['Obmen', 420, {'None': ['None', 136]}],
    'Drugoe|None|Otdam besplatno': ['Otdam besplatno',
                                    419,
                                    {'None': ['None', 136]}],
    'Drugoe|None|Tovary dlya vzroslyh': ['Tovary dlya vzroslyh',
                                         417,
                                         {'None': ['None', 136]}],
    'Elektronika|Aksessuary dlya telefoniv|Akkumulyatory': ['Akkumulyatory',
                                                            91,
                                                            {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                           'dlya '
                                                                                           'telefoniv',
                                                                                           15]}],
    'Elektronika|Aksessuary dlya telefoniv|Antenny, usiliteli': ['Antenny, '
                                                                 'usiliteli',
                                                                 92,
                                                                 {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                                'dlya '
                                                                                                'telefoniv',
                                                                                                15]}],
    'Elektronika|Aksessuary dlya telefoniv|CHehly i futlyary': ['CHehly i '
                                                                'futlyary',
                                                                105,
                                                                {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                               'dlya '
                                                                                               'telefoniv',
                                                                                               15]}],
    'Elektronika|Aksessuary dlya telefoniv|Dinamiki, mikrofony': ['Dinamiki, '
                                                                  'mikrofony',
                                                                  93,
                                                                  {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                                 'dlya '
                                                                                                 'telefoniv',
                                                                                                 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Displei, sensornye ekrany': ['Displei, '
                                                                        'sensornye '
                                                                        'ekrany',
                                                                        94,
                                                                        {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                                       'dlya '
                                                                                                       'telefoniv',
                                                                                                       15]}],
    'Elektronika|Aksessuary dlya telefoniv|Kabeli': ['Kabeli',
                                                     97,
                                                     {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                    'dlya '
                                                                                    'telefoniv',
                                                                                    15]}],
    'Elektronika|Aksessuary dlya telefoniv|Karty pamyati': ['Karty pamyati',
                                                            98,
                                                            {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                           'dlya '
                                                                                           'telefoniv',
                                                                                           15]}],
    'Elektronika|Aksessuary dlya telefoniv|Klaviatury': ['Klaviatury',
                                                         99,
                                                         {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                        'dlya '
                                                                                        'telefoniv',
                                                                                        15]}],
    'Elektronika|Aksessuary dlya telefoniv|Korpusa': ['Korpusa',
                                                      100,
                                                      {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                     'dlya '
                                                                                     'telefoniv',
                                                                                     15]}],
    'Elektronika|Aksessuary dlya telefoniv|Mikroshemy': ['Mikroshemy',
                                                         101,
                                                         {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                        'dlya '
                                                                                        'telefoniv',
                                                                                        15]}],
    'Elektronika|Aksessuary dlya telefoniv|Plenki dlya ekranov': ['Plenki dlya '
                                                                  'ekranov',
                                                                  102,
                                                                  {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                                 'dlya '
                                                                                                 'telefoniv',
                                                                                                 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Provodnye garnitury, naushniki': ['Provodnye '
                                                                             'garnitury, '
                                                                             'naushniki',
                                                                             103,
                                                                             {'Aksessuary dlya telefoniv': [
                                                                                 'Aksessuary '
                                                                                 'dlya '
                                                                                 'telefoniv',
                                                                                 15]}],
    'Elektronika|Aksessuary dlya telefoniv|Razemy, gnezda': ['Razemy, gnezda',
                                                             104,
                                                             {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                            'dlya '
                                                                                            'telefoniv',
                                                                                            15]}],
    'Elektronika|Aksessuary dlya telefoniv|SHlejfy, perehodniki': ['SHlejfy, '
                                                                   'perehodniki',
                                                                   106,
                                                                   {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                                  'dlya '
                                                                                                  'telefoniv',
                                                                                                  15]}],
    'Elektronika|Aksessuary dlya telefoniv|Zaryadnye ustrojstva': ['Zaryadnye '
                                                                   'ustrojstva',
                                                                   96,
                                                                   {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                                  'dlya '
                                                                                                  'telefoniv',
                                                                                                  15]}],
    'Elektronika|Elektronnye knigi|Aksessuary': ['Aksessuary',
                                                 87,
                                                 {'Elektronnye knigi': ['Elektronnye '
                                                                        'knigi',
                                                                        13]}],
    'Elektronika|Elektronnye knigi|KPK': ['KPK',
                                          86,
                                          {'Elektronnye knigi': ['Elektronnye '
                                                                 'knigi',
                                                                 13]}],
    'Elektronika|Foto/ videotehnika|Drugoe': ['Drugoe',
                                              56,
                                              {'Foto/ videotehnika': ['Foto/ '
                                                                      'videotehnika',
                                                                      8]}],
    'Elektronika|Foto/ videotehnika|Fotoramki, fotoalbomy': ['Fotoramki, '
                                                             'fotoalbomy',
                                                             52,
                                                             {'Foto/ videotehnika': ['Foto/ '
                                                                                     'videotehnika',
                                                                                     8]}],
    'Elektronika|Foto/ videotehnika|Zapchasti i komplektuyushchie': ['Zapchasti '
                                                                     'i '
                                                                     'komplektuyushchie',
                                                                     55,
                                                                     {'Foto/ videotehnika': ['Foto/ '
                                                                                             'videotehnika',
                                                                                             8]}],
    'Elektronika|Igry, pristavki, PO|Drugoe': ['Drugoe',
                                               61,
                                               {'Igry, pristavki, PO': ['Igry, '
                                                                        'pristavki, '
                                                                        'PO',
                                                                        9]}],
    'Elektronika|Igry, pristavki, PO|Ofisnye prilozheniya': ['Ofisnye '
                                                             'prilozheniya',
                                                             64,
                                                             {'Igry, pristavki, PO': ['Igry, '
                                                                                      'pristavki, '
                                                                                      'PO',
                                                                                      9]}],
    'Elektronika|Igry, pristavki, PO|Operacionnye sistemy': ['Operacionnye '
                                                             'sistemy',
                                                             63,
                                                             {'Igry, pristavki, PO': ['Igry, '
                                                                                      'pristavki, '
                                                                                      'PO',
                                                                                      9]}],
    'Elektronika|Igry, pristavki, PO|Utility, antivirusy': ['Utility, '
                                                            'antivirusy',
                                                            65,
                                                            {'Igry, pristavki, PO': ['Igry, '
                                                                                     'pristavki, '
                                                                                     'PO',
                                                                                     9]}],
    'Elektronika|Komplektuyushchie i aksessuary|Bloki pitaniya': ['Bloki '
                                                                  'pitaniya',
                                                                  76,
                                                                  {'Komplektuyushchie i aksessuary': [
                                                                      'Komplektuyushchie '
                                                                      'i '
                                                                      'aksessuary',
                                                                      10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Korpusa': ['Korpusa',
                                                           73,
                                                           {'Komplektuyushchie i aksessuary': ['Komplektuyushchie '
                                                                                               'i '
                                                                                               'aksessuary',
                                                                                               10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Materinskie platy': ['Materinskie '
                                                                     'platy',
                                                                     70,
                                                                     {'Komplektuyushchie i aksessuary': [
                                                                         'Komplektuyushchie '
                                                                         'i '
                                                                         'aksessuary',
                                                                         10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Opticheskie privody': ['Opticheskie '
                                                                       'privody',
                                                                       71,
                                                                       {'Komplektuyushchie i aksessuary': [
                                                                           'Komplektuyushchie '
                                                                           'i '
                                                                           'aksessuary',
                                                                           10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Processory': ['Processory',
                                                              74,
                                                              {'Komplektuyushchie i aksessuary': ['Komplektuyushchie '
                                                                                                  'i '
                                                                                                  'aksessuary',
                                                                                                  10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Sistemy ohlazhdeniya': ['Sistemy '
                                                                        'ohlazhdeniya',
                                                                        77,
                                                                        {'Komplektuyushchie i aksessuary': [
                                                                            'Komplektuyushchie '
                                                                            'i '
                                                                            'aksessuary',
                                                                            10]}],
    'Elektronika|Komplektuyushchie i aksessuary|TV-tyunery': ['TV-tyunery',
                                                              75,
                                                              {'Komplektuyushchie i aksessuary': ['Komplektuyushchie '
                                                                                                  'i '
                                                                                                  'aksessuary',
                                                                                                  10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Videokarty': ['Videokarty',
                                                              68,
                                                              {'Komplektuyushchie i aksessuary': ['Komplektuyushchie '
                                                                                                  'i '
                                                                                                  'aksessuary',
                                                                                                  10]}],
    'Elektronika|Komplektuyushchie i aksessuary|ZHestkie diski': ['ZHestkie '
                                                                  'diski',
                                                                  72,
                                                                  {'Komplektuyushchie i aksessuary': [
                                                                      'Komplektuyushchie '
                                                                      'i '
                                                                      'aksessuary',
                                                                      10]}],
    'Elektronika|Komplektuyushchie i aksessuary|Zvukovye karty': ['Zvukovye '
                                                                  'karty',
                                                                  69,
                                                                  {'Komplektuyushchie i aksessuary': [
                                                                      'Komplektuyushchie '
                                                                      'i '
                                                                      'aksessuary',
                                                                      10]}],
    'Elektronika|Noutbuki| ': [' ', 82, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|b': ['b', 86, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|e': ['e', 81, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|i': ['i', 89, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|k': ['k', 88, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|n': ['n', 83, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|o': ['o', 84, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|s': ['s', 80, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Noutbuki|t': ['t', 85, {'Noutbuki': ['Noutbuki', 11]}],
    'Elektronika|Sim-karty / tarify / nomera|Beeline': ['Beeline',
                                                        107,
                                                        {'Sim-karty / tarify / nomera': ['Sim-karty '
                                                                                         '/ '
                                                                                         'tarify '
                                                                                         '/ '
                                                                                         'nomera',
                                                                                         17]}],
    'Elektronika|Sim-karty / tarify / nomera|Kievstar, Djuice': ['Kievstar, '
                                                                 'Djuice',
                                                                 112,
                                                                 {'Sim-karty / tarify / nomera': ['Sim-karty '
                                                                                                  '/ '
                                                                                                  'tarify '
                                                                                                  '/ '
                                                                                                  'nomera',
                                                                                                  17]}],
    'Elektronika|Sim-karty / tarify / nomera|Life': ['Life',
                                                     108,
                                                     {'Sim-karty / tarify / nomera': ['Sim-karty '
                                                                                      '/ '
                                                                                      'tarify '
                                                                                      '/ '
                                                                                      'nomera',
                                                                                      17]}],
    'Elektronika|Sim-karty / tarify / nomera|MTS': ['MTS',
                                                    113,
                                                    {'Sim-karty / tarify / nomera': ['Sim-karty '
                                                                                     '/ '
                                                                                     'tarify '
                                                                                     '/ '
                                                                                     'nomera',
                                                                                     17]}],
    'Elektronika|Sim-karty / tarify / nomera|PEOPLEnet': ['PEOPLEnet',
                                                          109,
                                                          {'Sim-karty / tarify / nomera': ['Sim-karty '
                                                                                           '/ '
                                                                                           'tarify '
                                                                                           '/ '
                                                                                           'nomera',
                                                                                           17]}],
    'Elektronika|Sim-karty / tarify / nomera|Utel': ['Utel',
                                                     110,
                                                     {'Sim-karty / tarify / nomera': ['Sim-karty '
                                                                                      '/ '
                                                                                      'tarify '
                                                                                      '/ '
                                                                                      'nomera',
                                                                                      17]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Bonistika': ['Bonistika',
                                                                 304,
                                                                 {'Kollekcionirovanie / antikvariat': [
                                                                     'Kollekcionirovanie '
                                                                     '/ '
                                                                     'antikvariat',
                                                                     60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Filateliya': ['Filateliya',
                                                                  306,
                                                                  {'Kollekcionirovanie / antikvariat': [
                                                                      'Kollekcionirovanie '
                                                                      '/ '
                                                                      'antikvariat',
                                                                      60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Kollekcionnoe oruzhie': ['Kollekcionnoe '
                                                                             'oruzhie',
                                                                             316,
                                                                             {'Kollekcionirovanie / antikvariat': [
                                                                                 'Kollekcionirovanie '
                                                                                 '/ '
                                                                                 'antikvariat',
                                                                                 60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Medali, znachki, zhetony': ['Medali, '
                                                                                'znachki, '
                                                                                'zhetony',
                                                                                311,
                                                                                {'Kollekcionirovanie / antikvariat': [
                                                                                    'Kollekcionirovanie '
                                                                                    '/ '
                                                                                    'antikvariat',
                                                                                    60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Militariya': ['Militariya',
                                                                  310,
                                                                  {'Kollekcionirovanie / antikvariat': [
                                                                      'Kollekcionirovanie '
                                                                      '/ '
                                                                      'antikvariat',
                                                                      60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Monety Ukrainy iz drag.splavov': ['Monety '
                                                                                      'Ukrainy '
                                                                                      'iz '
                                                                                      'drag.splavov',
                                                                                      315,
                                                                                      {
                                                                                          'Kollekcionirovanie / antikvariat': [
                                                                                              'Kollekcionirovanie '
                                                                                              '/ '
                                                                                              'antikvariat',
                                                                                              60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Numizmatika': ['Numizmatika',
                                                                   302,
                                                                   {'Kollekcionirovanie / antikvariat': [
                                                                       'Kollekcionirovanie '
                                                                       '/ '
                                                                       'antikvariat',
                                                                       60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Plastinki': ['Plastinki',
                                                                 314,
                                                                 {'Kollekcionirovanie / antikvariat': [
                                                                     'Kollekcionirovanie '
                                                                     '/ '
                                                                     'antikvariat',
                                                                     60]}],
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Stolovoe serebro, antikvarnaya kuhonnaya utvar': ['Stolovoe '
                                                                                                      'serebro, '
                                                                                                      'antikvarnaya '
                                                                                                      'kuhonnaya '
                                                                                                      'utvar',
                                                                                                      305,
                                                                                                      {
                                                                                                          'Kollekcionirovanie / antikvariat': [
                                                                                                              'Kollekcionirovanie '
                                                                                                              '/ '
                                                                                                              'antikvariat',
                                                                                                              60]}],
    'Hobbi i dosug|Ohota': ['Ohota', 'Hobbi i dosug', 58],
    'Hobbi i dosug|Rybolovstvo|Eholoty, radary': ['Eholoty, radary',
                                                  301,
                                                  {'Rybolovstvo': ['Rybolovstvo',
                                                                   59]}],
    'Hobbi i dosug|Rybolovstvo|Katushki': ['Katushki',
                                           296,
                                           {'Rybolovstvo': ['Rybolovstvo', 59]}],
    'Hobbi i dosug|Rybolovstvo|Lodki, katamarany': ['Lodki, katamarany',
                                                    297,
                                                    {'Rybolovstvo': ['Rybolovstvo',
                                                                     59]}],
    'Hobbi i dosug|Rybolovstvo|Podvodnaya ohota': ['Podvodnaya ohota',
                                                   298,
                                                   {'Rybolovstvo': ['Rybolovstvo',
                                                                    59]}],
    'Hobbi i dosug|Rybolovstvo|Rybackii inventar': ['Rybackii inventar',
                                                    299,
                                                    {'Rybolovstvo': ['Rybolovstvo',
                                                                     59]}],
    'Hobbi i dosug|Rybolovstvo|Udilishcha': ['Udilishcha',
                                             300,
                                             {'Rybolovstvo': ['Rybolovstvo',
                                                              59]}],
    'Krasota i aksessuary|Kosmetika i uhod|Kosmetika solncezashchitnaya, dlya solyariya': ['Kosmetika '
                                                                                           'solncezashchitnaya, '
                                                                                           'dlya '
                                                                                           'solyariya',
                                                                                           176,
                                                                                           {'Kosmetika i uhod': [
                                                                                               'Kosmetika '
                                                                                               'i '
                                                                                               'uhod',
                                                                                               27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Pribory i aksessuary': ['Pribory i '
                                                                   'aksessuary',
                                                                   182,
                                                                   {'Kosmetika i uhod': ['Kosmetika '
                                                                                         'i '
                                                                                         'uhod',
                                                                                         27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Sredstva dlya britya / depilyacii': ['Sredstva '
                                                                                'dlya '
                                                                                'britya '
                                                                                '/ '
                                                                                'depilyacii',
                                                                                178,
                                                                                {'Kosmetika i uhod': ['Kosmetika '
                                                                                                      'i '
                                                                                                      'uhod',
                                                                                                      27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Sredstva dlya volos': ['Sredstva '
                                                                  'dlya volos',
                                                                  183,
                                                                  {'Kosmetika i uhod': ['Kosmetika '
                                                                                        'i '
                                                                                        'uhod',
                                                                                        27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Uhod za licom i telom': ['Uhod za '
                                                                    'licom i '
                                                                    'telom',
                                                                    180,
                                                                    {'Kosmetika i uhod': ['Kosmetika '
                                                                                          'i '
                                                                                          'uhod',
                                                                                          27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Uhod za polostyu rta': ['Uhod za '
                                                                   'polostyu '
                                                                   'rta',
                                                                   181,
                                                                   {'Kosmetika i uhod': ['Kosmetika '
                                                                                         'i '
                                                                                         'uhod',
                                                                                         27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Uslugi': ['Uslugi',
                                                     179,
                                                     {'Kosmetika i uhod': ['Kosmetika '
                                                                           'i '
                                                                           'uhod',
                                                                           27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Vsydlya nogtej': ['Vsydlya nogtej',
                                                             173,
                                                             {'Kosmetika i uhod': ['Kosmetika '
                                                                                   'i '
                                                                                   'uhod',
                                                                                   27]}],
    'Krasota i aksessuary|Kosmetika i uhod|Vsydlya volos': ['Vsydlya volos',
                                                            172,
                                                            {'Kosmetika i uhod': ['Kosmetika '
                                                                                  'i '
                                                                                  'uhod',
                                                                                  27]}],
    'Krasota i aksessuary|Ochki': ['Ochki', 'Krasota i aksessuary', 29],
    'Krasota i aksessuary|YUvelirnye izdeliya|Braslety': ['Braslety',
                                                          167,
                                                          {'YUvelirnye izdeliya': ['YUvelirnye '
                                                                                   'izdeliya',
                                                                                   26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Broshi': ['Broshi',
                                                        169,
                                                        {'YUvelirnye izdeliya': ['YUvelirnye '
                                                                                 'izdeliya',
                                                                                 26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Cepochki': ['Cepochki',
                                                          170,
                                                          {'YUvelirnye izdeliya': ['YUvelirnye '
                                                                                   'izdeliya',
                                                                                   26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Kolca': ['Kolca',
                                                       165,
                                                       {'YUvelirnye izdeliya': ['YUvelirnye '
                                                                                'izdeliya',
                                                                                26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Kulony / Podveski': ['Kulony / '
                                                                   'Podveski',
                                                                   168,
                                                                   {'YUvelirnye izdeliya': ['YUvelirnye '
                                                                                            'izdeliya',
                                                                                            26]}],
    'Krasota i aksessuary|YUvelirnye izdeliya|Sergi': ['Sergi',
                                                       166,
                                                       {'YUvelirnye izdeliya': ['YUvelirnye '
                                                                                'izdeliya',
                                                                                26]}],
    'Obshchenie|None|Nahodki': ['Nahodki', 412, {'None': ['None', 135]}],
    'Obshchenie|None|Poisk lyudej': ['Poisk lyudej',
                                     416,
                                     {'None': ['None', 135]}],
    'Obshchenie|None|Poteri': ['Poteri', 411, {'None': ['None', 135]}],
    'Odezhda, obuv|Muzhskaya obuv|Sandalii, shlepancy': ['Sandalii, shlepancy',
                                                         161,
                                                         {'Muzhskaya obuv': ['Muzhskaya '
                                                                             'obuv',
                                                                             22]}],
    'Odezhda, obuv|Muzhskaya obuv|Sapogi, botinki': ['Sapogi, botinki',
                                                     159,
                                                     {'Muzhskaya obuv': ['Muzhskaya '
                                                                         'obuv',
                                                                         22]}],
    'Odezhda, obuv|Muzhskaya obuv|Sportivnaya obuv': ['Sportivnaya obuv',
                                                      162,
                                                      {'Muzhskaya obuv': ['Muzhskaya '
                                                                          'obuv',
                                                                          22]}],
    'Odezhda, obuv|Muzhskaya obuv|Tufli, mokasiny': ['Tufli, mokasiny',
                                                     160,
                                                     {'Muzhskaya obuv': ['Muzhskaya '
                                                                         'obuv',
                                                                         22]}],
    'Odezhda, obuv|Muzhskaya odezhda|Bryuki': ['Bryuki',
                                               145,
                                               {'Muzhskaya odezhda': ['Muzhskaya '
                                                                      'odezhda',
                                                                      20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Futbolka': ['Futbolka',
                                                 146,
                                                 {'Muzhskaya odezhda': ['Muzhskaya '
                                                                        'odezhda',
                                                                        20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Futbolki, majki': ['Futbolki, majki',
                                                        139,
                                                        {'Muzhskaya odezhda': ['Muzhskaya '
                                                                               'odezhda',
                                                                               20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Kostyumy, pidzhaki, zhilety': ['Kostyumy, '
                                                                    'pidzhaki, '
                                                                    'zhilety',
                                                                    144,
                                                                    {'Muzhskaya odezhda': ['Muzhskaya '
                                                                                           'odezhda',
                                                                                           20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Rubashki': ['Rubashki',
                                                 140,
                                                 {'Muzhskaya odezhda': ['Muzhskaya '
                                                                        'odezhda',
                                                                        20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Sportivnye kostyumy': ['Sportivnye '
                                                            'kostyumy',
                                                            149,
                                                            {'Muzhskaya odezhda': ['Muzhskaya '
                                                                                   'odezhda',
                                                                                   20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Svitera': ['Svitera',
                                                142,
                                                {'Muzhskaya odezhda': ['Muzhskaya '
                                                                       'odezhda',
                                                                       20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Tolstovki': ['Tolstovki',
                                                  143,
                                                  {'Muzhskaya odezhda': ['Muzhskaya '
                                                                         'odezhda',
                                                                         20]}],
    'Odezhda, obuv|Muzhskaya odezhda|Verhnyaya odezhda': ['Verhnyaya odezhda',
                                                          147,
                                                          {'Muzhskaya odezhda': ['Muzhskaya '
                                                                                 'odezhda',
                                                                                 20]}],
    'Odezhda, obuv|Muzhskaya odezhda|kofty': ['kofty',
                                              141,
                                              {'Muzhskaya odezhda': ['Muzhskaya '
                                                                     'odezhda',
                                                                     20]}],
    'Odezhda, obuv|ZHenskaya obuv|Baletki': ['Baletki',
                                             150,
                                             {'ZHenskaya obuv': ['ZHenskaya '
                                                                 'obuv',
                                                                 21]}],
    'Odezhda, obuv|ZHenskaya obuv|Bosonozhki': ['Bosonozhki',
                                                153,
                                                {'ZHenskaya obuv': ['ZHenskaya '
                                                                    'obuv',
                                                                    21]}],
    'Odezhda, obuv|ZHenskaya obuv|Botinki': ['Botinki',
                                             155,
                                             {'ZHenskaya obuv': ['ZHenskaya '
                                                                 'obuv',
                                                                 21]}],
    'Odezhda, obuv|ZHenskaya obuv|Mokasiny': ['Mokasiny',
                                              151,
                                              {'ZHenskaya obuv': ['ZHenskaya '
                                                                  'obuv',
                                                                  21]}],
    'Odezhda, obuv|ZHenskaya obuv|SHlepancy': ['SHlepancy',
                                               154,
                                               {'ZHenskaya obuv': ['ZHenskaya '
                                                                   'obuv',
                                                                   21]}],
    'Odezhda, obuv|ZHenskaya obuv|Sapogi': ['Sapogi',
                                            156,
                                            {'ZHenskaya obuv': ['ZHenskaya '
                                                                'obuv',
                                                                21]}],
    'Odezhda, obuv|ZHenskaya obuv|Sportivnaya obuv': ['Sportivnaya obuv',
                                                      158,
                                                      {'ZHenskaya obuv': ['ZHenskaya '
                                                                          'obuv',
                                                                          21]}],
    'Odezhda, obuv|ZHenskaya obuv|Tufli': ['Tufli',
                                           152,
                                           {'ZHenskaya obuv': ['ZHenskaya '
                                                               'obuv',
                                                               21]}],
    'Odezhda, obuv|ZHenskaya odezhda|Bluzki, rubashki': ['Bluzki, rubashki',
                                                         115,
                                                         {'ZHenskaya odezhda': ['ZHenskaya '
                                                                                'odezhda',
                                                                                19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Bryuki': ['Bryuki',
                                               116,
                                               {'ZHenskaya odezhda': ['ZHenskaya '
                                                                      'odezhda',
                                                                      19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Futbolka': ['Futbolka',
                                                 133,
                                                 {'ZHenskaya odezhda': ['ZHenskaya '
                                                                        'odezhda',
                                                                        19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Futbolki': ['Futbolki',
                                                 131,
                                                 {'ZHenskaya odezhda': ['ZHenskaya '
                                                                        'odezhda',
                                                                        19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Hudi, tolstovki': ['Hudi, tolstovki',
                                                        132,
                                                        {'ZHenskaya odezhda': ['ZHenskaya '
                                                                               'odezhda',
                                                                               19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Kostyumy': ['Kostyumy',
                                                 119,
                                                 {'ZHenskaya odezhda': ['ZHenskaya '
                                                                        'odezhda',
                                                                        19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Losiny, legginsy': ['Losiny, legginsy',
                                                         122,
                                                         {'ZHenskaya odezhda': ['ZHenskaya '
                                                                                'odezhda',
                                                                                19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Majki': ['Majki',
                                              123,
                                              {'ZHenskaya odezhda': ['ZHenskaya '
                                                                     'odezhda',
                                                                     19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Nizhnee belyo': ['Nizhnee belyo',
                                                      124,
                                                      {'ZHenskaya odezhda': ['ZHenskaya '
                                                                             'odezhda',
                                                                             19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Pidzhaki': ['Pidzhaki',
                                                 125,
                                                 {'ZHenskaya odezhda': ['ZHenskaya '
                                                                        'odezhda',
                                                                        19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Platya': ['Platya',
                                               126,
                                               {'ZHenskaya odezhda': ['ZHenskaya '
                                                                      'odezhda',
                                                                      19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Sarafany': ['Sarafany',
                                                 127,
                                                 {'ZHenskaya odezhda': ['ZHenskaya '
                                                                        'odezhda',
                                                                        19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Sportivnye kostyumy': ['Sportivnye '
                                                            'kostyumy',
                                                            136,
                                                            {'ZHenskaya odezhda': ['ZHenskaya '
                                                                                   'odezhda',
                                                                                   19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Svitera': ['Svitera',
                                                128,
                                                {'ZHenskaya odezhda': ['ZHenskaya '
                                                                       'odezhda',
                                                                       19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Topy': ['Topy',
                                             129,
                                             {'ZHenskaya odezhda': ['ZHenskaya '
                                                                    'odezhda',
                                                                    19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Tuniki': ['Tuniki',
                                               130,
                                               {'ZHenskaya odezhda': ['ZHenskaya '
                                                                      'odezhda',
                                                                      19]}],
    'Odezhda, obuv|ZHenskaya odezhda|Verhnyaya odezhda': ['Verhnyaya odezhda',
                                                          117,
                                                          {'ZHenskaya odezhda': ['ZHenskaya '
                                                                                 'odezhda',
                                                                                 19]}],
    'Odezhda, obuv|ZHenskaya odezhda|YUbki': ['YUbki',
                                              134,
                                              {'ZHenskaya odezhda': ['ZHenskaya '
                                                                     'odezhda',
                                                                     19]}],
    'Odezhda, obuv|ZHenskaya odezhda|ZHilety': ['ZHilety',
                                                118,
                                                {'ZHenskaya odezhda': ['ZHenskaya '
                                                                       'odezhda',
                                                                       19]}],
    'Odezhda, obuv|ZHenskaya odezhda|kofty': ['kofty',
                                              120,
                                              {'ZHenskaya odezhda': ['ZHenskaya '
                                                                     'odezhda',
                                                                     19]}],
    'Remont i stroitelstvo|Uslugi': ['Uslugi', 'Remont i stroitelstvo', 100],
    'Sport i zdorove|Drugoe': ['Drugoe', 'Sport i zdorove', 52],
    'Sport i zdorove|Pribory i ustrojstva|Drugoe': ['Drugoe',
                                                    254,
                                                    {'Pribory i ustrojstva': ['Pribory '
                                                                              'i '
                                                                              'ustrojstva',
                                                                              47]}],
    'Sport i zdorove|Pribory i ustrojstva|Filtry': ['Filtry',
                                                    262,
                                                    {'Pribory i ustrojstva': ['Pribory '
                                                                              'i '
                                                                              'ustrojstva',
                                                                              47]}],
    'Sport i zdorove|Pribory i ustrojstva|Kardiopribory, pribory dlya pulsa': ['Kardiopribory, '
                                                                               'pribory '
                                                                               'dlya '
                                                                               'pulsa',
                                                                               255,
                                                                               {'Pribory i ustrojstva': ['Pribory '
                                                                                                         'i '
                                                                                                         'ustrojstva',
                                                                                                         47]}],
    'Sport i zdorove|Pribory i ustrojstva|Massazhery': ['Massazhery',
                                                        256,
                                                        {'Pribory i ustrojstva': ['Pribory '
                                                                                  'i '
                                                                                  'ustrojstva',
                                                                                  47]}],
    'Sport i zdorove|Pribory i ustrojstva|Poyasa, miostimulyatory': ['Poyasa, '
                                                                     'miostimulyatory',
                                                                     257,
                                                                     {'Pribory i ustrojstva': ['Pribory '
                                                                                               'i '
                                                                                               'ustrojstva',
                                                                                               47]}],
    'Sport i zdorove|Pribory i ustrojstva|Pribory magnitoterapii': ['Pribory '
                                                                    'magnitoterapii',
                                                                    258,
                                                                    {'Pribory i ustrojstva': ['Pribory '
                                                                                              'i '
                                                                                              'ustrojstva',
                                                                                              47]}],
    'Sport i zdorove|Pribory i ustrojstva|Sluhovye apparaty': ['Sluhovye '
                                                               'apparaty',
                                                               259,
                                                               {'Pribory i ustrojstva': ['Pribory '
                                                                                         'i '
                                                                                         'ustrojstva',
                                                                                         47]}],
    'Sport i zdorove|Pribory i ustrojstva|Termometry, gradusniki': ['Termometry, '
                                                                    'gradusniki',
                                                                    260,
                                                                    {'Pribory i ustrojstva': ['Pribory '
                                                                                              'i '
                                                                                              'ustrojstva',
                                                                                              47]}],
    'Sport i zdorove|Pribory i ustrojstva|Tonometry': ['Tonometry',
                                                       261,
                                                       {'Pribory i ustrojstva': ['Pribory '
                                                                                 'i '
                                                                                 'ustrojstva',
                                                                                 47]}],
    'Sport i zdorove|Pribory i ustrojstva|Vesy': ['Vesy',
                                                  253,
                                                  {'Pribory i ustrojstva': ['Pribory '
                                                                            'i '
                                                                            'ustrojstva',
                                                                            47]}],
    'Sport i zdorove|Specodezhda, obuv|Drugoe': ['Drugoe',
                                                 281,
                                                 {'Specodezhda, obuv': ['Specodezhda, '
                                                                        'obuv',
                                                                        51]}],
    'Sport i zdorove|Specodezhda, obuv|Ohota, rybolovstvo': ['Ohota, '
                                                             'rybolovstvo',
                                                             282,
                                                             {'Specodezhda, obuv': ['Specodezhda, '
                                                                                    'obuv',
                                                                                    51]}],
    'Sport i zdorove|Specodezhda, obuv|Turizm': ['Turizm',
                                                 283,
                                                 {'Specodezhda, obuv': ['Specodezhda, '
                                                                        'obuv',
                                                                        51]}],
    'Sport i zdorove|Sportinventar|Alpinizm, speleologiya': ['Alpinizm, '
                                                             'speleologiya',
                                                             224,
                                                             {'Sportinventar': ['Sportinventar',
                                                                                45]}],
    'Sport i zdorove|Sportinventar|Bilyard': ['Bilyard',
                                              226,
                                              {'Sportinventar': ['Sportinventar',
                                                                 45]}],
    'Sport i zdorove|Sportinventar|Darts': ['Darts',
                                            230,
                                            {'Sportinventar': ['Sportinventar',
                                                               45]}],
    'Sport i zdorove|Sportinventar|Ganteli, giri, shtangi': ['Ganteli, giri, '
                                                             'shtangi',
                                                             229,
                                                             {'Sportinventar': ['Sportinventar',
                                                                                45]}],
    'Sport i zdorove|Sportinventar|Pejntbol, strajkbol': ['Pejntbol, strajkbol',
                                                          235,
                                                          {'Sportinventar': ['Sportinventar',
                                                                             45]}],
    'Sport i zdorove|Sportinventar|arbalety, luki, strely': ['arbalety, luki, '
                                                             'strely',
                                                             225,
                                                             {'Sportinventar': ['Sportinventar',
                                                                                45]}],
    'Sport i zdorove|Sportivnoe pitanie': ['Sportivnoe pitanie',
                                           'Sport i zdorove',
                                           53],
    'Sport i zdorove|Trenazhery|Begovye dorozhki': ['Begovye dorozhki',
                                                    263,
                                                    {'Trenazhery': ['Trenazhery',
                                                                    48]}],
    'Sport i zdorove|Trenazhery|Drugie': ['Drugie',
                                          265,
                                          {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Trenazhery|Orbitreki': ['Orbitreki',
                                             266,
                                             {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Trenazhery|Silovye trenazhyory': ['Silovye trenazhyory',
                                                       267,
                                                       {'Trenazhery': ['Trenazhery',
                                                                       48]}],
    'Sport i zdorove|Trenazhery|Velotrenazhyory': ['Velotrenazhyory',
                                                   264,
                                                   {'Trenazhery': ['Trenazhery',
                                                                   48]}],
    'Sport i zdorove|Trenazhery|skami i stojki': ['skami i stojki',
                                                  268,
                                                  {'Trenazhery': ['Trenazhery',
                                                                  48]}],
    'Sport i zdorove|Trenazhery|steppery': ['steppery',
                                            269,
                                            {'Trenazhery': ['Trenazhery', 48]}],
    'Sport i zdorove|Velosipedy i aksessuary k nim|Ekipirovka dlya velosporta': ['Ekipirovka '
                                                                                 'dlya '
                                                                                 'velosporta',
                                                                                 280,
                                                                                 {'Velosipedy i aksessuary k nim': [
                                                                                     'Velosipedy '
                                                                                     'i '
                                                                                     'aksessuary '
                                                                                     'k '
                                                                                     'nim',
                                                                                     50]}],
    'Sport i zdorove|Velosipedy i aksessuary k nim|Komplektuyushchie': ['Komplektuyushchie',
                                                                        278,
                                                                        {'Velosipedy i aksessuary k nim': ['Velosipedy '
                                                                                                           'i '
                                                                                                           'aksessuary '
                                                                                                           'k '
                                                                                                           'nim',
                                                                                                           50]}],
    'Sport i zdorove|Velosipedy i aksessuary k nim|Smartfony': ['Smartfony',
                                                                276,
                                                                {'Velosipedy i aksessuary k nim': ['Velosipedy '
                                                                                                   'i '
                                                                                                   'aksessuary '
                                                                                                   'k '
                                                                                                   'nim',
                                                                                                   50]}],
    'Sport i zdorove|Velosipedy i aksessuary k nim|Snaryazhenie, aksessuary': ['Snaryazhenie, '
                                                                               'aksessuary',
                                                                               279,
                                                                               {'Velosipedy i aksessuary k nim': [
                                                                                   'Velosipedy '
                                                                                   'i '
                                                                                   'aksessuary '
                                                                                   'k '
                                                                                   'nim',
                                                                                   50]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Dorozhnye sumki': ['Dorozhnye sumki',
                                                               240,
                                                               {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                           'otdyha, '
                                                                                           'turizm',
                                                                                           46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Fonari, fonariki': ['Fonari, '
                                                                'fonariki',
                                                                252,
                                                                {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                            'otdyha, '
                                                                                            'turizm',
                                                                                            46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Mangaly, gril, shampura': ['Mangaly, '
                                                                       'gril, '
                                                                       'shampura',
                                                                       242,
                                                                       {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                                   'otdyha, '
                                                                                                   'turizm',
                                                                                                   46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Metalloiskateli': ['Metalloiskateli',
                                                               243,
                                                               {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                           'otdyha, '
                                                                                           'turizm',
                                                                                           46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Naduvnye bassejny': ['Naduvnye '
                                                                 'bassejny',
                                                                 244,
                                                                 {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                             'otdyha, '
                                                                                             'turizm',
                                                                                             46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Nozhi, multituly': ['Nozhi, '
                                                                'multituly',
                                                                245,
                                                                {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                            'otdyha, '
                                                                                            'turizm',
                                                                                            46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Palatki': ['Palatki',
                                                       246,
                                                       {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                   'otdyha, '
                                                                                   'turizm',
                                                                                   46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Pohodnyi inventar': ['Pohodnyi '
                                                                 'inventar',
                                                                 248,
                                                                 {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                             'otdyha, '
                                                                                             'turizm',
                                                                                             46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Posuda, flyagi, termosy': ['Posuda, '
                                                                       'flyagi, '
                                                                       'termosy',
                                                                       247,
                                                                       {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                                   'otdyha, '
                                                                                                   'turizm',
                                                                                                   46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Ryukzaki': ['Ryukzaki',
                                                        250,
                                                        {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                    'otdyha, '
                                                                                    'turizm',
                                                                                    46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|Spalnye meshki, gamaki': ['Spalnye '
                                                                      'meshki, '
                                                                      'gamaki',
                                                                      251,
                                                                      {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                                  'otdyha, '
                                                                                                  'turizm',
                                                                                                  46]}],
    'Sport i zdorove|Vsydlya otdyha, turizm|putyovki, tury': ['putyovki, tury',
                                                              249,
                                                              {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                                          'otdyha, '
                                                                                          'turizm',
                                                                                          46]}],
    'Sport i zdorove|Zdorove|Aromaterapiya, svechi, lampy': ['Aromaterapiya, '
                                                             'svechi, lampy',
                                                             270,
                                                             {'Zdorove': ['Zdorove',
                                                                          49]}],
    'Sport i zdorove|Zdorove|Drugoe': ['Drugoe',
                                       273,
                                       {'Zdorove': ['Zdorove', 49]}],
    'Sport i zdorove|Zdorove|Lekarstva': ['Lekarstva',
                                          271,
                                          {'Zdorove': ['Zdorove', 49]}],
    'Sport i zdorove|Zdorove|Netradicionnaya medicina': ['Netradicionnaya '
                                                         'medicina',
                                                         274,
                                                         {'Zdorove': ['Zdorove',
                                                                      49]}],
    'Sport i zdorove|Zdorove|Ochki, linzy, aksessuary': ['Ochki, linzy, '
                                                         'aksessuary',
                                                         275,
                                                         {'Zdorove': ['Zdorove',
                                                                      49]}],
    'Sport i zdorove|Zdorove|Sredstva dlya pohudeniya': ['Sredstva dlya '
                                                         'pohudeniya',
                                                         272,
                                                         {'Zdorove': ['Zdorove',
                                                                      49]}],
    'Transport|Legkovye avtomobili| ': [' ',
                                        371,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|L': ['L',
                                        363,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|V': ['V',
                                        359,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|a': ['a',
                                        372,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|b': ['b',
                                        378,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|e': ['e',
                                        370,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|g': ['g',
                                        365,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|i': ['i',
                                        381,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|k': ['k',
                                        366,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|l': ['l',
                                        380,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|m': ['m',
                                        376,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|o': ['o',
                                        377,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|s': ['s',
                                        360,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|t': ['t',
                                        374,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|v': ['v',
                                        373,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Transport|Legkovye avtomobili|y': ['y',
                                        369,
                                        {'Legkovye avtomobili': ['Legkovye '
                                                                 'avtomobili',
                                                                 103]}],
    'Uslugi|Bezopasnost, ohrana': ['Bezopasnost, ohrana', 'Uslugi', 123],
    'Uslugi|Dostavka': ['Dostavka', 'Uslugi', 113],
    'Uslugi|Informacionnye tehnologii, internet': ['Informacionnye tehnologii, '
                                                   'internet',
                                                   'Uslugi',
                                                   118],
    'Uslugi|Organizaciya, obsluzhivanie meropriyatii': ['Organizaciya, '
                                                        'obsluzhivanie '
                                                        'meropriyatii',
                                                        'Uslugi',
                                                        120],
    'Uslugi|Poligrafiya, izdatelstvo': ['Poligrafiya, izdatelstvo',
                                        'Uslugi',
                                        125],
    'Uslugi|Pomoshch ekstrasensov': ['Pomoshch ekstrasensov', 'Uslugi', 131],
    'Uslugi|Reklama, marketing': ['Reklama, marketing', 'Uslugi', 122],
    'Uslugi|Remontnye uslugi': ['Remontnye uslugi', 'Uslugi', 111],
    'Uslugi|Selskoe hozyajstvo': ['Selskoe hozyajstvo', 'Uslugi', 133],
    'Uslugi|Sport': ['Sport', 'Uslugi', 132],
    'Uslugi|Zdorove, krasota|Krasota / zdorove ': ['Krasota / zdorove ',
                                                   373,
                                                   {'Zdorove, krasota': ['Zdorove, '
                                                                         'krasota',
                                                                         119]}],
    'Zhivotnye i dlya zhivotnyh|Drugoe': ['Drugoe',
                                          'Zhivotnye i dlya zhivotnyh',
                                          74],
    'Zhivotnye i dlya zhivotnyh|Otdam besplatno': ['Otdam besplatno',
                                                   'Zhivotnye i dlya zhivotnyh',
                                                   73],
    'Zhivotnye i dlya zhivotnyh|Pitanie i aksessuary': ['Pitanie i aksessuary',
                                                        'Zhivotnye i dlya '
                                                        'zhivotnyh',
                                                        69],
    'Zhivotnye i dlya zhivotnyh|Uhod za zhivotnymi': ['Uhod za zhivotnymi',
                                                      'Zhivotnye i dlya '
                                                      'zhivotnyh',
                                                      71],
    'Бизнес и услуги|Красота / здоровье|Красота / здоровье - прочее': ['prochee',
                                                                       374,
                                                                       {'Zdorove, krasota': ['Zdorove, '
                                                                                             'krasota',
                                                                                             119]}],
    'Бизнес и услуги|Красота / здоровье|Макияж / косметология / наращивание ресниц': ['Makiyazh '
                                                                                      '/ '
                                                                                      'kosmetologiya '
                                                                                      '/ '
                                                                                      'narashchivanie '
                                                                                      'resnic',
                                                                                      372,
                                                                                      {'Zdorove, krasota': ['Zdorove, '
                                                                                                            'krasota',
                                                                                                            119]}],
    'Бизнес и услуги|Красота / здоровье|Маникюр / наращивание ногтей': ['Manikyur '
                                                                        '/ '
                                                                        'narashchivanie '
                                                                        'nogtej',
                                                                        371,
                                                                        {'Zdorove, krasota': ['Zdorove, '
                                                                                              'krasota',
                                                                                              119]}],
    'Бизнес и услуги|Красота / здоровье|Массаж': ['Massazh',
                                                  375,
                                                  {'Zdorove, krasota': ['Zdorove, '
                                                                        'krasota',
                                                                        119]}],
    'Бизнес и услуги|Красота / здоровье|Медицина': ['Medicina',
                                                    377,
                                                    {'Zdorove, krasota': ['Zdorove, '
                                                                          'krasota',
                                                                          119]}],
    'Бизнес и услуги|Красота / здоровье|Стрижки / наращивание волосs': ['Strizhki '
                                                                        '/ '
                                                                        'narashchivanie '
                                                                        'volos',
                                                                        376,
                                                                        {'Zdorove, krasota': ['Zdorove, '
                                                                                              'krasota',
                                                                                              119]}],
    'Бизнес и услуги|Красота / здоровье|Услуги психолога': ['Uslugi psihologa',
                                                            378,
                                                            {'Zdorove, krasota': ['Zdorove, '
                                                                                  'krasota',
                                                                                  119]}],
    'Бизнес и услуги|Няни / сиделки': ['Nyani, sidelki', 'Uslugi', 129],
    'Бизнес и услуги|Образование / Спорт': ['Obrazovanie', 'Uslugi', 130],
    'Бизнес и услуги|Перевозки / аренда транспорта': ['Transportnye uslugi / '
                                                      'Avtarenda',
                                                      'Uslugi',
                                                      114],
    'Бизнес и услуги|Продажа бизнеса': ['Torgovlya', 'Uslugi', 124],
    'Бизнес и услуги|Прокат товаров': ['Prokat', 'Uslugi', 128],
    'Бизнес и услуги|Прочие услуги': ['Prochee', 'Uslugi', 134],
    'Бизнес и услуги|Развлечения / Искусство / Фото / Видео': ['Razvlecheniya, '
                                                               'foto, video',
                                                               'Uslugi',
                                                               127],
    'Бизнес и услуги|Строительство / ремонт / уборка|Cтроительные услуги': ['Stroitelnye '
                                                                            'uslugi',
                                                                            363,
                                                                            {'Uborka': ['Uborka',
                                                                                        112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Бытовой ремонт / уборка': ['Bytovoj '
                                                                                'remont '
                                                                                '/ '
                                                                                'uborka',
                                                                                364,
                                                                                {'Uborka': ['Uborka',
                                                                                            112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Вентиляция / кондиционирование': ['Ventilyaciya '
                                                                                       '/ '
                                                                                       'kondicionirovanie',
                                                                                       370,
                                                                                       {'Uborka': ['Uborka',
                                                                                                   112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Готовые конструкции': ['Gotovye '
                                                                            'konstrukcii',
                                                                            366,
                                                                            {'Uborka': ['Uborka',
                                                                                        112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Дизайн / архитектура': ['Dizajn '
                                                                             '/ '
                                                                             'arhitektura',
                                                                             369,
                                                                             {'Uborka': ['Uborka',
                                                                                         112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Окна / двери / балконы': ['Okna '
                                                                               '/ '
                                                                               'dveri '
                                                                               '/ '
                                                                               'balkony',
                                                                               365,
                                                                               {'Uborka': ['Uborka',
                                                                                           112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Отделка / ремонт': ['Otdelka '
                                                                         '/ '
                                                                         'remont',
                                                                         362,
                                                                         {'Uborka': ['Uborka',
                                                                                     112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Сантехника / коммуникации': ['Santehnika '
                                                                                  '/ '
                                                                                  'kommunikacii',
                                                                                  367,
                                                                                  {'Uborka': ['Uborka',
                                                                                              112]}],
    'Бизнес и услуги|Строительство / ремонт / уборка|Электрика': ['Elektrichestvo',
                                                                  368,
                                                                  {'Uborka': ['Uborka',
                                                                              112]}],
    'Бизнес и услуги|Туризм / иммиграция': ['Turizm, immigraciya', 'Uslugi', 121],
    'Бизнес и услуги|Услуги для животных': ['Uslugi puhodu za zhivotnymi',
                                            'Uslugi',
                                            126],
    'Бизнес и услуги|Услуги переводчиков / набор текста': ['Uslugi perevoda',
                                                           'Uslugi',
                                                           115],
    'Бизнес и услуги|Финансовые услуги / партнерство': ['Finansy, kredit, '
                                                        'strahovanie',
                                                        'Uslugi',
                                                        117],
    'Бизнес и услуги|Юридические услуги': ['YUridicheskie, notarialnye uslugi',
                                           'Uslugi',
                                           116],
    'Детский мир|Детская мебель': ['Detskaya mebel', 'Detskii mir', 41],
    'Детский мир|Детская обувь': ['Drugoe',
                                  203,
                                  {'Detskaya obuv': ['Detskaya obuv', 32]}],
    'Детский мир|Детская одежда|Одежда для девочек': ['Drugoe',
                                                      187,
                                                      {'Detskaya odezhda': ['Detskaya '
                                                                            'odezhda',
                                                                            31]}],
    'Детский мир|Детская одежда|Одежда для мальчиков': ['Drugoe',
                                                        187,
                                                        {'Detskaya odezhda': ['Detskaya '
                                                                              'odezhda',
                                                                              31]}],
    'Детский мир|Детская одежда|Одежда для новорожденных': ['Dlya mladencev',
                                                            186,
                                                            {'Detskaya odezhda': ['Detskaya '
                                                                                  'odezhda',
                                                                                  31]}],
    'Детский мир|Детские автокресла': ['Detskie kolyaski i avtokresla',
                                       'Detskii mir',
                                       42],
    'Детский мир|Детские коляски': ['Detskie kolyaski i avtokresla',
                                    'Detskii mir',
                                    42],
    'Детский мир|Детский транспорт': ['Detskii transport', 'Detskii mir', 40],
    'Детский мир|Игрушки': ['Detskie igrushki i igry', 'Detskii mir', 34],
    'Детский мир|Прочие детские товары': ['Prochee', 'Detskii mir', 44],
    'Детский мир|Товары для кормления': ['Detskoe pitanie', 'Detskii mir', 39],
    'Детский мир|Товары для школьников': ['Dlya shkolnikov', 'Detskii mir', 37],
    'Дом и сад|Инструменты|Бензоинструмент': ['Benzoinstrument',
                                              327,
                                              {'Instrumenty / inventar': ['Instrumenty '
                                                                          '/ '
                                                                          'inventar',
                                                                          80]}],
    'Дом и сад|Инструменты|Пневмоинструмент': ['Drugoj instrument',
                                               326,
                                               {'Instrumenty / inventar': ['Instrumenty '
                                                                           '/ '
                                                                           'inventar',
                                                                           80]}],
    'Дом и сад|Инструменты|Прочий инструмент': ['Drugoj instrument',
                                                326,
                                                {'Instrumenty / inventar': ['Instrumenty '
                                                                            '/ '
                                                                            'inventar',
                                                                            80]}],
    'Дом и сад|Инструменты|Ручной инструмент': ['Ruchnoj instrument',
                                                325,
                                                {'Instrumenty / inventar': ['Instrumenty '
                                                                            '/ '
                                                                            'inventar',
                                                                            80]}],
    'Дом и сад|Инструменты|Электроинструмент': ['Elektroinstrument',
                                                324,
                                                {'Instrumenty / inventar': ['Instrumenty '
                                                                            '/ '
                                                                            'inventar',
                                                                            80]}],
    'Дом и сад|Канцтовары / расходные материалы': ['Kanctovary', 'Dom i sad', 86],
    'Дом и сад|Комнатные растения': ['Rasteniya',
                                     330,
                                     {'Rasteniya': ['Rasteniya', 83]}],
    'Дом и сад|Мебель|Кухонная мебель': ['Mebel', 321, {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель для ванной комнаты': ['Mebel dlya vannoj komnaty',
                                                   323,
                                                   {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель для гостиной': ['Mebel dlya gostinoj',
                                             317,
                                             {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель для прихожей': ['Mebel dlya prihozhej',
                                             322,
                                             {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель для спальни': ['Mebel dlya spalni',
                                            319,
                                            {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Мебель на заказ': ['Mebel na zakaz',
                                         318,
                                         {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Мебель|Офисная мебель': ['Ofisnaya mebel',
                                        320,
                                        {'Mebel': ['Mebel', 79]}],
    'Дом и сад|Посуда / кухонная утварь': ['Vse dlya kuhni', 'Dom i sad', 76],
    'Дом и сад|Предметы интерьера|Декор окон': ['Predmety Interera, '
                                                'osveshchenie',
                                                'Dom i sad',
                                                78],
    'Дом и сад|Предметы интерьера|Светильники': ['Predmety Interera, '
                                                 'osveshchenie',
                                                 'Dom i sad',
                                                 78],
    'Дом и сад|Предметы интерьера|Текстиль': ['Tekstil', 'Dom i sad', 81],
    'Дом и сад|Продукты питания / напитки': ['Produkty pitaniya',
                                             'Dom i sad',
                                             85],
    'Дом и сад|Прочие товары для дома': ['Prochee', 'Dom i sad', 87],
    'Дом и сад|Сад / огород': ['Drugoe',
                               332,
                               {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Дом и сад|Садовый инвентарь': ['Sadovyi instrument',
                                    336,
                                    {'Sad i ogorod': ['Sad i ogorod', 84]}],
    'Дом и сад|Строительство / ремонт|Вентиляция': ['Ventilyaciya',
                                                    'Remont i stroitelstvo',
                                                    99],
    'Дом и сад|Строительство / ремонт|Кирпич / бетон / пеноблоки': ['Kirpich / '
                                                                    'beton / '
                                                                    'penobloki',
                                                                    'Remont i '
                                                                    'stroitelstvo',
                                                                    95],
    'Дом и сад|Строительство / ремонт|Лакокрасочные материалы': ['Lakokrasochnye '
                                                                 'materialy',
                                                                 'Remont i '
                                                                 'stroitelstvo',
                                                                 98],
    'Дом и сад|Строительство / ремонт|Металлопрокат / арматура': ['Metalloprokat '
                                                                  '/ armatura',
                                                                  'Remont i '
                                                                  'stroitelstvo',
                                                                  94],
    'Дом и сад|Строительство / ремонт|Окна / двери / стеклo / зеркала': ['Okna '
                                                                         '/ '
                                                                         'dveri '
                                                                         '/ '
                                                                         'stekl/ '
                                                                         'zerkala',
                                                                         'Remont '
                                                                         'i '
                                                                         'stroitelstvo',
                                                                         90],
    'Дом и сад|Строительство / ремонт|Отделочные и облицовочные материалы': ['Otdelochnye '
                                                                             'i '
                                                                             'oblicovochnye '
                                                                             'materialy',
                                                                             'Remont '
                                                                             'i '
                                                                             'stroitelstvo',
                                                                             91],
    'Дом и сад|Строительство / ремонт|Отопление': ['Otoplenie',
                                                   'Remont i stroitelstvo',
                                                   88],
    'Дом и сад|Строительство / ремонт|Пиломатериалы': ['Pilomaterialy',
                                                       'Remont i stroitelstvo',
                                                       96],
    'Дом и сад|Строительство / ремонт|Прочие стройматериалы': ['Drugie '
                                                               'strojmaterialy',
                                                               'Remont i '
                                                               'stroitelstvo',
                                                               89],
    'Дом и сад|Строительство / ремонт|Сантехника': ['Santehnika',
                                                    'Remont i stroitelstvo',
                                                    93],
    'Дом и сад|Строительство / ремонт|Электрика': ['Elektrichestvo',
                                                   'Remont i stroitelstvo',
                                                   92],
    'Дом и сад|Строительство / ремонт|Элементы крепежа': ['Elementy krepleniya',
                                                          'Remont i '
                                                          'stroitelstvo',
                                                          97],
    'Дом и сад|Хозяйственный инвентарь / бытовая химия': ['Komplektuyushchie, '
                                                          'rashodnye materialy',
                                                          328,
                                                          {'Instrumenty / inventar': ['Instrumenty '
                                                                                      '/ '
                                                                                      'inventar',
                                                                                      80]}],
    'Животные|Аквариумистика': ['Akvariumy', 'Zhivotnye i dlya zhivotnyh', 64],
    'Животные|Бюро находок': ['Byurnahodok', 'Zhivotnye i dlya zhivotnyh', 75],
    'Животные|Вязка': ['Vyazka', 'Zhivotnye i dlya zhivotnyh', 72],
    'Животные|Грызуны': ['Drugie domashnie pitomcy',
                         'Zhivotnye i dlya zhivotnyh',
                         67],
    'Животные|Другие  животные': ['Drugie domashnie pitomcy',
                                  'Zhivotnye i dlya zhivotnyh',
                                  67],
    'Животные|Зоотовары': ['Veterinarnye Zootovary',
                           'Zhivotnye i dlya zhivotnyh',
                           70],
    'Животные|Кошки': ['Koshki', 'Zhivotnye i dlya zhivotnyh', 62],
    'Животные|Птицы': ['Pernatye', 'Zhivotnye i dlya zhivotnyh', 65],
    'Животные|Рептилии': ['Reptilii', 'Zhivotnye i dlya zhivotnyh', 66],
    'Животные|Сельхоз животные': ['Selhoz zhivotnye',
                                  'Zhivotnye i dlya zhivotnyh',
                                  68],
    'Животные|Собаки': ['Sobaki', 'Zhivotnye i dlya zhivotnyh', 63],
    'Мода и стиль|Аксессуары|Бижутерия': ['Bizhuteriya i ukrasheniya',
                                          'Krasota i aksessuary',
                                          24],
    'Мода и стиль|Аксессуары|Другие аксессуары': ['Prochee',
                                                  'Krasota i aksessuary',
                                                  30],
    'Мода и стиль|Аксессуары|Сумки': ['Sumki, ryukzaki, koshelki',
                                      'Krasota i aksessuary',
                                      23],
    'Мода и стиль|Аксессуары|Ювелирные изделия': ['Drugie yuvelirnye izdeliya',
                                                  171,
                                                  {'YUvelirnye izdeliya': ['YUvelirnye '
                                                                           'izdeliya',
                                                                           26]}],
    'Мода и стиль|Для свадьбы|Свадебные платья/костюмы': ['Мода и стиль|Для '
                                                          'свадьбы|Свадебные '
                                                          'аксессуары',
                                                          147,
                                                          {'ZHenskaya odezhda': ['ZHenskaya '
                                                                                 'odezhda',
                                                                                 19]}],
    'Мода и стиль|Красота / здоровье|Косметика': ['Dekorativnaya kosmetika',
                                                  174,
                                                  {'Kosmetika i uhod': ['Kosmetika '
                                                                        'i '
                                                                        'uhod',
                                                                        27]}],
    'Мода и стиль|Красота / здоровье|Оборудование парикмахерских / салонов красоты': ['Prochee',
                                                                                      'Krasota '
                                                                                      'i '
                                                                                      'aksessuary',
                                                                                      30],
    'Мода и стиль|Красота / здоровье|Парфюмерия': ['Parfyumeriya',
                                                   'Krasota i aksessuary',
                                                   28],
    'Мода и стиль|Красота / здоровье|Прочие товары для красоты и здоровья': ['Prochee',
                                                                             'Krasota '
                                                                             'i '
                                                                             'aksessuary',
                                                                             30],
    'Мода и стиль|Красота / здоровье|Средства по уходу': ['Drugoe',
                                                          175,
                                                          {'Kosmetika i uhod': ['Kosmetika '
                                                                                'i '
                                                                                'uhod',
                                                                                27]}],
    'Мода и стиль|Красота / здоровье|Товары для инвалидов': ['Tovary dlya '
                                                             'invalidov',
                                                             'Sport i zdorove',
                                                             54],
    'Мода и стиль|Мода разное': ['Prochee', 'Krasota i aksessuary', 30],
    'Мода и стиль|Наручные часы': ['CHasy', 'Krasota i aksessuary', 25],
    'Мода и стиль|Одежда/обувь|Головные уборы': ['Golovnye ubory',
                                                 164,
                                                 {'Muzhskaya obuv': ['Muzhskaya '
                                                                     'obuv',
                                                                     22]}],
    'Мода и стиль|Одежда/обувь|Женская обувь': ['Drugoe',
                                                157,
                                                {'ZHenskaya obuv': ['ZHenskaya '
                                                                    'obuv',
                                                                    21]}],
    'Мода и стиль|Одежда/обувь|Женская одежда': ['Drugoe',
                                                 135,
                                                 {'ZHenskaya odezhda': ['ZHenskaya '
                                                                        'odezhda',
                                                                        19]}],
    'Мода и стиль|Одежда/обувь|Женское белье, купальники': ['Kupalniki, pareo',
                                                            121,
                                                            {'ZHenskaya odezhda': ['ZHenskaya '
                                                                                   'odezhda',
                                                                                   19]}],
    'Мода и стиль|Одежда/обувь|Мужская обувь': ['Drugaya obuv',
                                                163,
                                                {'Muzhskaya obuv': ['Muzhskaya '
                                                                    'obuv',
                                                                    22]}],
    'Мода и стиль|Одежда/обувь|Мужская одежда': ['Drugoe',
                                                 148,
                                                 {'Muzhskaya odezhda': ['Muzhskaya '
                                                                        'odezhda',
                                                                        20]}],
    'Мода и стиль|Одежда/обувь|Мужское белье': ['Nizhnee bele, plavki',
                                                138,
                                                {'Muzhskaya odezhda': ['Muzhskaya '
                                                                       'odezhda',
                                                                       20]}],
    'Мода и стиль|Одежда/обувь|Одежда для беременных': ['Odezhda dlya '
                                                        'beremennyh',
                                                        114,
                                                        {'ZHenskaya odezhda': ['ZHenskaya '
                                                                               'odezhda',
                                                                               19]}],
    'Мода и стиль|Подарки': ['Podarki i suveniry', 418, {'None': ['None', 136]}],
    'Транспорт|Автобусы': ['Avtobusy', 'Transport', 109],
    'Транспорт|Водный транспорт ': ['Vodnyi transport', 'Transport', 106],
    'Транспорт|Воздушный транспорт': ['Drugoj transport', 'Transport', 110],
    'Транспорт|Грузовые автомобили': ['Gruzovye avtomobili', 'Transport', 104],
    'Транспорт|Другой транспорт': ['Drugoj transport', 'Transport', 110],
    'Транспорт|Запчасти / аксессуары|GPS-навигаторы / авторегистраторы': ['GPS-navigatory '
                                                                          '/ '
                                                                          'avtoregistratory',
                                                                          348,
                                                                          {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                                      '/ '
                                                                                                      'aksessuary',
                                                                                                      101]}],
    'Транспорт|Запчасти / аксессуары|Автозапчасти': ['Avtozapchasti',
                                                     339,
                                                     {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                 '/ '
                                                                                 'aksessuary',
                                                                                 101]}],
    'Транспорт|Запчасти / аксессуары|Автозвук': ['Avtozvuk',
                                                 344,
                                                 {'Zapchasti / aksessuary': ['Zapchasti '
                                                                             '/ '
                                                                             'aksessuary',
                                                                             101]}],
    'Транспорт|Запчасти / аксессуары|Аксессуары для авто': ['Aksessuary dlya '
                                                            'avto',
                                                            341,
                                                            {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                        '/ '
                                                                                        'aksessuary',
                                                                                        101]}],
    'Транспорт|Запчасти / аксессуары|Запчасти для спец / с.х. техники': ['Zapchasti '
                                                                         'dlya '
                                                                         'spec '
                                                                         '/ '
                                                                         's.h.tehniki',
                                                                         342,
                                                                         {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                                     '/ '
                                                                                                     'aksessuary',
                                                                                                     101]}],
    'Транспорт|Запчасти / аксессуары|Мото аксессуары': ['Motaksessuary',
                                                        346,
                                                        {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                    '/ '
                                                                                    'aksessuary',
                                                                                    101]}],
    'Транспорт|Запчасти / аксессуары|Мотозапчасти': ['Motozapchasti',
                                                     343,
                                                     {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                 '/ '
                                                                                 'aksessuary',
                                                                                 101]}],
    'Транспорт|Запчасти / аксессуары|Прочие запчасти': ['Drugie zapchasti',
                                                        345,
                                                        {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                    '/ '
                                                                                    'aksessuary',
                                                                                    101]}],
    'Транспорт|Запчасти / аксессуары|Транспорт на запчасти': ['Transport na '
                                                              'zapchasti',
                                                              347,
                                                              {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                          '/ '
                                                                                          'aksessuary',
                                                                                          101]}],
    'Транспорт|Запчасти / аксессуары|Шины / диски': ['SHiny / diski',
                                                     340,
                                                     {'Zapchasti / aksessuary': ['Zapchasti '
                                                                                 '/ '
                                                                                 'aksessuary',
                                                                                 101]}],
    'Транспорт|Мото|Квадроциклы': ['Kvadrocikly', 352, {'Moto': ['Moto', 105]}],
    'Транспорт|Мото|Мопеды / скутеры': ['Mopedy / skutery',
                                        350,
                                        {'Moto': ['Moto', 105]}],
    'Транспорт|Мото|Мото - прочее': ['Mot-drugoe', 353, {'Moto': ['Moto', 105]}],
    'Транспорт|Мото|Мотоциклы': ['Motocikly', 351, {'Moto': ['Moto', 105]}],
    'Транспорт|Прицепы': ['Pricepy', 'Transport', 108],
    'Транспорт|Сельхозтехника': ['Selhoztehnika', 'Transport', 102],
    'Транспорт|Спецтехника|Бульдозеры / тракторы': ['Buldozery / traktory',
                                                    354,
                                                    {'Spectehnika': ['Spectehnika',
                                                                     107]}],
    'Транспорт|Спецтехника|Коммунальная техника': ['Kommunalnaya tehnika',
                                                   360,
                                                   {'Spectehnika': ['Spectehnika',
                                                                    107]}],
    'Транспорт|Спецтехника|Оборудование для спецтехники': ['Oborudovanie dlya '
                                                           'spectehniki',
                                                           358,
                                                           {'Spectehnika': ['Spectehnika',
                                                                            107]}],
    'Транспорт|Спецтехника|Погрузчики': ['Pogruzchiki',
                                         355,
                                         {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Прочая спецтехника': ['Drugaya spectehnika',
                                                 357,
                                                 {'Spectehnika': ['Spectehnika',
                                                                  107]}],
    'Транспорт|Спецтехника|Самосвалы': ['Samosvaly',
                                        361,
                                        {'Spectehnika': ['Spectehnika', 107]}],
    'Транспорт|Спецтехника|Строительная техника': ['Stroitelnaya tehnika',
                                                   356,
                                                   {'Spectehnika': ['Spectehnika',
                                                                    107]}],
    'Транспорт|Спецтехника|Экскаваторы': ['Ekskavatory',
                                          359,
                                          {'Spectehnika': ['Spectehnika', 107]}],
    'Хобби, отдых и спорт|CD / DVD / пластинки / кассеты': ['CD / DVD plastinki',
                                                            'Hobbi i dosug',
                                                            56],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Антикварная мебель': ['Kartiny, '
                                                                        'antikvarnaya '
                                                                        'mebel, '
                                                                        'predmety '
                                                                        'interera',
                                                                        308,
                                                                        {'Kollekcionirovanie / antikvariat': [
                                                                            'Kollekcionirovanie '
                                                                            '/ '
                                                                            'antikvariat',
                                                                            60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Букинистика': ['Knigi',
                                                                 313,
                                                                 {'Kollekcionirovanie / antikvariat': [
                                                                     'Kollekcionirovanie '
                                                                     '/ '
                                                                     'antikvariat',
                                                                     60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Живопись': ['Skulptury',
                                                              307,
                                                              {'Kollekcionirovanie / antikvariat': [
                                                                  'Kollekcionirovanie '
                                                                  '/ '
                                                                  'antikvariat',
                                                                  60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Коллекционирование': ['Drugie '
                                                                        'kategorii',
                                                                        303,
                                                                        {'Kollekcionirovanie / antikvariat': [
                                                                            'Kollekcionirovanie '
                                                                            '/ '
                                                                            'antikvariat',
                                                                            60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Поделки / рукоделие': ['Masshtabnye '
                                                                         'modeli',
                                                                         309,
                                                                         {'Kollekcionirovanie / antikvariat': [
                                                                             'Kollekcionirovanie '
                                                                             '/ '
                                                                             'antikvariat',
                                                                             60]}],
    'Хобби, отдых и спорт|Антиквариат / коллекции|Предметы искусства': ['Ikony, '
                                                                        'skladni, '
                                                                        'metalloplastika',
                                                                        312,
                                                                        {'Kollekcionirovanie / antikvariat': [
                                                                            'Kollekcionirovanie '
                                                                            '/ '
                                                                            'antikvariat',
                                                                            60]}],
    'Хобби, отдых и спорт|Билеты': ['Bilety na meropriyatiya',
                                    413,
                                    {'None': ['None', 135]}],
    'Хобби, отдых и спорт|Другое': ['Prochee', 'Hobbi i dosug', 61],
    'Хобби, отдых и спорт|Книги / журналы': ['Knigi, pressa',
                                             'Hobbi i dosug',
                                             55],
    'Хобби, отдых и спорт|Музыкальные инструменты|Аксессуары для музыкальных инструментов': ['Aksessuary '
                                                                                             'dlya '
                                                                                             'muzykalnyh '
                                                                                             'instrumentov',
                                                                                             286,
                                                                                             {
                                                                                                 'Muzykalnye instrumenty': [
                                                                                                     'Muzykalnye '
                                                                                                     'instrumenty',
                                                                                                     57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Акустические гитары': ['Akusticheskie '
                                                                         'gitary',
                                                                         289,
                                                                         {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                                     'instrumenty',
                                                                                                     57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Бас-гитары': ['Bas-gitary',
                                                                294,
                                                                {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                            'instrumenty',
                                                                                            57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Духовые инструменты': ['duhovye',
                                                                         291,
                                                                         {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                                     'instrumenty',
                                                                                                     57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Комбоусилители': ['Kombousiliteli',
                                                                    293,
                                                                    {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                                'instrumenty',
                                                                                                57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Пианино / фортепиано / рояли': ['Pianin/ '
                                                                                  'fortepian/ '
                                                                                  'royali',
                                                                                  287,
                                                                                  {'Muzykalnye instrumenty': [
                                                                                      'Muzykalnye '
                                                                                      'instrumenty',
                                                                                      57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Прочее': ['Drugoe',
                                                            285,
                                                            {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                        'instrumenty',
                                                                                        57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Синтезаторы': ['vyhodnoj',
                                                                 290,
                                                                 {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                             'instrumenty',
                                                                                             57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Студийное оборудование': ['Studiinoe '
                                                                            'oborudovanie',
                                                                            284,
                                                                            {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                                        'instrumenty',
                                                                                                        57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Ударные инструменты': ['Udarnye '
                                                                         'instrumenty',
                                                                         292,
                                                                         {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                                     'instrumenty',
                                                                                                     57]}],
    'Хобби, отдых и спорт|Музыкальные инструменты|Электрогитары': ['Elektrogitary',
                                                                   288,
                                                                   {'Muzykalnye instrumenty': ['Muzykalnye '
                                                                                               'instrumenty',
                                                                                               57]}],
    'Хобби, отдых и спорт|Поиск групп / музыкантов': ['Poisk grupp / muzykantov',
                                                      415,
                                                      {'None': ['None', 135]}],
    'Хобби, отдых и спорт|Поиск попутчиков': ['Poisk poputchikov',
                                              414,
                                              {'None': ['None', 135]}],
    'Хобби, отдых и спорт|Спорт / отдых|Атлетика / фитнес': ['Fitnes, joga, '
                                                             'aerobika, '
                                                             'gimnastika',
                                                             239,
                                                             {'Sportinventar': ['Sportinventar',
                                                                                45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Вело': ['Drugoe',
                                                277,
                                                {'Velosipedy i aksessuary k nim': ['Velosipedy '
                                                                                   'i '
                                                                                   'aksessuary '
                                                                                   'k '
                                                                                   'nim',
                                                                                   50]}],
    'Хобби, отдых и спорт|Спорт / отдых|Водные виды спорта': ['Vodnyi sport',
                                                              228,
                                                              {'Sportinventar': ['Sportinventar',
                                                                                 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Единоборства / бокс': ['Boevye '
                                                               'iskusstva',
                                                               227,
                                                               {'Sportinventar': ['Sportinventar',
                                                                                  45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Игры с ракеткой': ['Tennis, badminton',
                                                           238,
                                                           {'Sportinventar': ['Sportinventar',
                                                                              45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Коньки': ['Zimnii sport',
                                                  232,
                                                  {'Sportinventar': ['Sportinventar',
                                                                     45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Лыжи / сноуборды': ['Zimnii sport',
                                                            232,
                                                            {'Sportinventar': ['Sportinventar',
                                                                               45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Настольные игры': ['Nastolnye igry',
                                                           234,
                                                           {'Sportinventar': ['Sportinventar',
                                                                              45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Охота / рыбалка': ['Drugoe',
                                                           295,
                                                           {'Rybolovstvo': ['Rybolovstvo',
                                                                            59]}],
    'Хобби, отдых и спорт|Спорт / отдых|Прочие виды спорта': ['Drugoe',
                                                              231,
                                                              {'Sportinventar': ['Sportinventar',
                                                                                 45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Роликовые коньки': ['Roliki, skejty',
                                                            236,
                                                            {'Sportinventar': ['Sportinventar',
                                                                               45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Туризм': ['Drugoe',
                                                  241,
                                                  {'Vsydlya otdyha, turizm': ['Vsydlya '
                                                                              'otdyha, '
                                                                              'turizm',
                                                                              46]}],
    'Хобби, отдых и спорт|Спорт / отдых|Футбол': ['Igry s myachom',
                                                  233,
                                                  {'Sportinventar': ['Sportinventar',
                                                                     45]}],
    'Хобби, отдых и спорт|Спорт / отдых|Хоккей': ['Sportivnye ugolki',
                                                  237,
                                                  {'Sportinventar': ['Sportinventar',
                                                                     45]}],
    'Электроника|Аксессуары и комплектующие': ['Operativnaya pamyat',
                                               78,
                                               {'Komplektuyushchie i aksessuary': ['Komplektuyushchie '
                                                                                   'i '
                                                                                   'aksessuary',
                                                                                   10]}],
    'Электроника|Аудиотехника|Cd / md / виниловые проигрыватели': ['Cd / md / '
                                                                   'vinilovye '
                                                                   'proigryvateli',
                                                                   44,
                                                                   {'audiotehnika': ['audiotehnika',
                                                                                     7]}],
    'Электроника|Аудиотехника|Mp3 плееры': ['Mp3 pleery',
                                            36,
                                            {'audiotehnika': ['audiotehnika',
                                                              7]}],
    'Электроника|Аудиотехника|Акустические системы': ['Akusticheskie sistemy',
                                                      39,
                                                      {'audiotehnika': ['audiotehnika',
                                                                        7]}],
    'Электроника|Аудиотехника|Магнитолы': ['Magnitoly',
                                           37,
                                           {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Музыкальные центры': ['Muzykalnye centry',
                                                    38,
                                                    {'audiotehnika': ['audiotehnika',
                                                                      7]}],
    'Электроника|Аудиотехника|Наушники': ['Naushniki',
                                          40,
                                          {'audiotehnika': ['audiotehnika', 7]}],
    'Электроника|Аудиотехника|Портативная акустика': ['Portativnaya akustika',
                                                      42,
                                                      {'audiotehnika': ['audiotehnika',
                                                                        7]}],
    'Электроника|Аудиотехника|Прочая аудиотехника': ['Prochaya audiotehnika',
                                                     45,
                                                     {'audiotehnika': ['audiotehnika',
                                                                       7]}],
    'Электроника|Аудиотехника|Радиоприемники': ['Radiopriemniki',
                                                41,
                                                {'audiotehnika': ['audiotehnika',
                                                                  7]}],
    'Электроника|Аудиотехника|Усилители / ресиверы': ['Usiliteli / resivery',
                                                      43,
                                                      {'audiotehnika': ['audiotehnika',
                                                                        7]}],
    'Электроника|Игры и игровые приставки|Аксессуары': ['Aksessuary',
                                                        59,
                                                        {'Igry, pristavki, PO': ['Igry, '
                                                                                 'pristavki, '
                                                                                 'PO',
                                                                                 9]}],
    'Электроника|Игры и игровые приставки|Игры для PC': ['Igry dlya PK',
                                                         62,
                                                         {'Igry, pristavki, PO': ['Igry, '
                                                                                  'pristavki, '
                                                                                  'PO',
                                                                                  9]}],
    'Электроника|Игры и игровые приставки|Игры для приставок': ['Igry dlya '
                                                                'pristavok',
                                                                57,
                                                                {'Igry, pristavki, PO': ['Igry, '
                                                                                         'pristavki, '
                                                                                         'PO',
                                                                                         9]}],
    'Электроника|Игры и игровые приставки|Приставки': ['tehnika',
                                                       58,
                                                       {'Igry, pristavki, PO': ['Igry, '
                                                                                'pristavki, '
                                                                                'PO',
                                                                                9]}],
    'Электроника|Игры и игровые приставки|Ремонт приставок': ['Remont pristavok',
                                                              60,
                                                              {'Igry, pristavki, PO': ['Igry, '
                                                                                       'pristavki, '
                                                                                       'PO',
                                                                                       9]}],
    'Электроника|Индивидуальный уход|Бритвы, эпиляторы, машинки для стрижки': ['Britvy, '
                                                                               'epilyatory, '
                                                                               'mashinki '
                                                                               'dlya '
                                                                               'strizhki',
                                                                               32,
                                                                               {'Tehnika dlya uhoda za soboj': [
                                                                                   'Tehnika '
                                                                                   'dlya '
                                                                                   'uhoda '
                                                                                   'za '
                                                                                   'soboj',
                                                                                   5]}],
    'Электроника|Индивидуальный уход|Весы': ['Vesy',
                                             34,
                                             {'Tehnika dlya uhoda za soboj': ['Tehnika '
                                                                              'dlya '
                                                                              'uhoda '
                                                                              'za '
                                                                              'soboj',
                                                                              5]}],
    'Электроника|Индивидуальный уход|Прочая техника для индивидуального ухода': ['Drugaya '
                                                                                 'tehnika '
                                                                                 'dlya '
                                                                                 'individualnoguhoda',
                                                                                 35,
                                                                                 {'Tehnika dlya uhoda za soboj': [
                                                                                     'Tehnika '
                                                                                     'dlya '
                                                                                     'uhoda '
                                                                                     'za '
                                                                                     'soboj',
                                                                                     5]}],
    'Электроника|Индивидуальный уход|Фены, укладка волос': ['Feny, ukladka '
                                                            'volos',
                                                            33,
                                                            {'Tehnika dlya uhoda za soboj': ['Tehnika '
                                                                                             'dlya '
                                                                                             'uhoda '
                                                                                             'za '
                                                                                             'soboj',
                                                                                             5]}],
    'Электроника|Климатическое оборудование': ['Kondicionery',
                                               1,
                                               {'Klimaticheskaya tehnika': ['Klimaticheskaya '
                                                                            'tehnika',
                                                                            1]}],
    'Электроника|Компьютеры|Аксессуары': ['Myshki, klaviatura',
                                          82,
                                          {'PK i orgtehnika': ['PK i '
                                                               'orgtehnika',
                                                               12]}],
    'Электроника|Компьютеры|Внешние накопители': ['Fleshki, zhestkie diski',
                                                  83,
                                                  {'PK i orgtehnika': ['PK i '
                                                                       'orgtehnika',
                                                                       12]}],
    'Электроника|Компьютеры|Другое': ['PK',
                                      80,
                                      {'PK i orgtehnika': ['PK i orgtehnika',
                                                           12]}],
    'Электроника|Компьютеры|Комплектующие': ['Komplektuyushchie',
                                             66,
                                             {'Komplektuyushchie i aksessuary': ['Komplektuyushchie '
                                                                                 'i '
                                                                                 'aksessuary',
                                                                                 10]}],
    'Электроника|Компьютеры|Мониторы': ['Monitory',
                                        85,
                                        {'PK i orgtehnika': ['PK i orgtehnika',
                                                             12]}],
    'Электроника|Компьютеры|Настольные': ['PK',
                                          80,
                                          {'PK i orgtehnika': ['PK i '
                                                               'orgtehnika',
                                                               12]}],
    'Электроника|Компьютеры|Ноутбуки': ['V', 79, {'Noutbuki': ['Noutbuki', 11]}],
    'Электроника|Компьютеры|Периферийные устройства': ['Printery, skanery, '
                                                       'faksy',
                                                       81,
                                                       {'PK i orgtehnika': ['PK '
                                                                            'i '
                                                                            'orgtehnika',
                                                                            12]}],
    'Электроника|Компьютеры|Планшетные компьютеры': ['Planshety',
                                                     88,
                                                     {'Telefony i planshety': ['Telefony '
                                                                               'i '
                                                                               'planshety',
                                                                               14]}],
    'Электроника|Компьютеры|Расходные материалы': ['Printery, skanery, faksy',
                                                   81,
                                                   {'PK i orgtehnika': ['PK '
                                                                        'i '
                                                                        'orgtehnika',
                                                                        12]}],
    'Электроника|Компьютеры|Серверы': ['Setevoe oborudovanie',
                                       84,
                                       {'PK i orgtehnika': ['PK i orgtehnika',
                                                            12]}],
    'Электроника|Прочая электроника': ['Moduli pamyati',
                                       67,
                                       {'Komplektuyushchie i aksessuary': ['Komplektuyushchie '
                                                                           'i '
                                                                           'aksessuary',
                                                                           10]}],
    'Электроника|Тв / видеотехника|Аксессуары для ТВ / видео': ['Aksessuary '
                                                                'dlya TV / '
                                                                'videotehniki',
                                                                9,
                                                                {'Televizory / Videotehnika': ['Televizory '
                                                                                               '/ '
                                                                                               'Videotehnika',
                                                                                               2]}],
    'Электроника|Тв / видеотехника|Медиа проигрыватели': ['Proigryvateli',
                                                          7,
                                                          {'Televizory / Videotehnika': ['Televizory '
                                                                                         '/ '
                                                                                         'Videotehnika',
                                                                                         2]}],
    'Электроника|Тв / видеотехника|Прочая ТВ / видеотехника': ['Drugaya TV / '
                                                               'videotehnika',
                                                               11,
                                                               {'Televizory / Videotehnika': ['Televizory '
                                                                                              '/ '
                                                                                              'Videotehnika',
                                                                                              2]}],
    'Электроника|Тв / видеотехника|Спутниковое ТВ': ['Sputnikovoe TV',
                                                     10,
                                                     {'Televizory / Videotehnika': ['Televizory '
                                                                                    '/ '
                                                                                    'Videotehnika',
                                                                                    2]}],
    'Электроника|Тв / видеотехника|Телевизоры': ['Televizory',
                                                 8,
                                                 {'Televizory / Videotehnika': ['Televizory '
                                                                                '/ '
                                                                                'Videotehnika',
                                                                                2]}],
    'Электроника|Телефоны|Аксессуары / запчасти': ['Drugoe',
                                                   95,
                                                   {'Aksessuary dlya telefoniv': ['Aksessuary '
                                                                                  'dlya '
                                                                                  'telefoniv',
                                                                                  15]}],
    'Электроника|Телефоны|Мобильные телефоны': ['Mobilnye telefony',
                                                90,
                                                {'Telefony i planshety': ['Telefony '
                                                                          'i '
                                                                          'planshety',
                                                                          14]}],
    'Электроника|Телефоны|Прочие телефоны': ['Drugie telefony, faksy',
                                             'Elektronika',
                                             18],
    'Электроника|Телефоны|Ремонт / прошивка': ['Remont i proshivka',
                                               89,
                                               {'Telefony i planshety': ['Telefony '
                                                                         'i '
                                                                         'planshety',
                                                                         14]}],
    'Электроника|Телефоны|Сим-карты / тарифы / номера': ['Drugoe',
                                                         111,
                                                         {'Sim-karty / tarify / nomera': ['Sim-karty '
                                                                                          '/ '
                                                                                          'tarify '
                                                                                          '/ '
                                                                                          'nomera',
                                                                                          17]}],
    'Электроника|Телефоны|Стационарные телефоны': ['Stacionarnye telefony',
                                                   'Elektronika',
                                                   16],
    'Электроника|Техника для дома|Вязальные машины': ['vyazalnye mashiny',
                                                      27,
                                                      {'Tehnika dlya doma': ['Tehnika '
                                                                             'dlya '
                                                                             'doma',
                                                                             4]}],
    'Электроника|Техника для дома|Прочая техника для дома': ['Drugaya tehnika '
                                                             'dlya doma',
                                                             29,
                                                             {'Tehnika dlya doma': ['Tehnika '
                                                                                    'dlya '
                                                                                    'doma',
                                                                                    4]}],
    'Электроника|Техника для дома|Пылесосы': ['Pylesosy',
                                              23,
                                              {'Tehnika dlya doma': ['Tehnika '
                                                                     'dlya '
                                                                     'doma',
                                                                     4]}],
    'Электроника|Техника для дома|Стиральные машины': ['Stiralnye mashiny',
                                                       25,
                                                       {'Tehnika dlya doma': ['Tehnika '
                                                                              'dlya '
                                                                              'doma',
                                                                              4]}],
    'Электроника|Техника для дома|Утюги': ['Utyugi',
                                           24,
                                           {'Tehnika dlya doma': ['Tehnika '
                                                                  'dlya doma',
                                                                  4]}],
    'Электроника|Техника для дома|Фильтры для воды': ['Filtry dlya vody',
                                                      28,
                                                      {'Tehnika dlya doma': ['Tehnika '
                                                                             'dlya '
                                                                             'doma',
                                                                             4]}],
    'Электроника|Техника для дома|Швейные машины и оверлоки': ['SHvejnye '
                                                               'mashiny i '
                                                               'overloki',
                                                               26,
                                                               {'Tehnika dlya doma': ['Tehnika '
                                                                                      'dlya '
                                                                                      'doma',
                                                                                      4]}],
    'Электроника|Техника для кухни|Вытяжки': ['Vytyazhki',
                                              21,
                                              {'Tehnika dlya kuhni': ['Tehnika '
                                                                      'dlya '
                                                                      'kuhni',
                                                                      3]}],
    'Электроника|Техника для кухни|Кофеварки / кофемолки': ['Kofevarki / '
                                                            'kofemolki',
                                                            16,
                                                            {'Tehnika dlya kuhni': ['Tehnika '
                                                                                    'dlya '
                                                                                    'kuhni',
                                                                                    3]}],
    'Электроника|Техника для кухни|Кухонные комбайны / измельчители': ['Kuhonnye '
                                                                       'kombajny '
                                                                       'i '
                                                                       'izmelchiteli',
                                                                       17,
                                                                       {'Tehnika dlya kuhni': ['Tehnika '
                                                                                               'dlya '
                                                                                               'kuhni',
                                                                                               3]}],
    'Электроника|Техника для кухни|Микроволновые печи': ['Mikrovolnovye pechi',
                                                         12,
                                                         {'Tehnika dlya kuhni': ['Tehnika '
                                                                                 'dlya '
                                                                                 'kuhni',
                                                                                 3]}],
    'Электроника|Техника для кухни|Пароварки / мультиварки': ['Parovarki, '
                                                              'multivarki',
                                                              18,
                                                              {'Tehnika dlya kuhni': ['Tehnika '
                                                                                      'dlya '
                                                                                      'kuhni',
                                                                                      3]}],
    'Электроника|Техника для кухни|Плиты / печи': ['Plity / pechi',
                                                   14,
                                                   {'Tehnika dlya kuhni': ['Tehnika '
                                                                           'dlya '
                                                                           'kuhni',
                                                                           3]}],
    'Электроника|Техника для кухни|Посудомоечные машины': ['Posudomoechnye '
                                                           'mashiny',
                                                           20,
                                                           {'Tehnika dlya kuhni': ['Tehnika '
                                                                                   'dlya '
                                                                                   'kuhni',
                                                                                   3]}],
    'Электроника|Техника для кухни|Прочая техника для кухни': ['Drugaya '
                                                               'tehnika dlya '
                                                               'kuhni',
                                                               22,
                                                               {'Tehnika dlya kuhni': ['Tehnika '
                                                                                       'dlya '
                                                                                       'kuhni',
                                                                                       3]}],
    'Электроника|Техника для кухни|Хлебопечки': ['Hlebopechki',
                                                 19,
                                                 {'Tehnika dlya kuhni': ['Tehnika '
                                                                         'dlya '
                                                                         'kuhni',
                                                                         3]}],
    'Электроника|Техника для кухни|Холодильники': ['Holodilniki',
                                                   13,
                                                   {'Tehnika dlya kuhni': ['Tehnika '
                                                                           'dlya '
                                                                           'kuhni',
                                                                           3]}],
    'Электроника|Техника для кухни|Электрочайники': ['Elektrochajniki',
                                                     15,
                                                     {'Tehnika dlya kuhni': ['Tehnika '
                                                                             'dlya '
                                                                             'kuhni',
                                                                             3]}],
    'Электроника|Фото / видео|Аксессуары для фото / видеокамер': ['Aksessuary, '
                                                                  'oborudovanie',
                                                                  53,
                                                                  {'Foto/ videotehnika': ['Foto/ '
                                                                                          'videotehnika',
                                                                                          8]}],
    'Электроника|Фото / видео|Видеокамеры': ['Videokamery',
                                             48,
                                             {'Foto/ videotehnika': ['Foto/ '
                                                                     'videotehnika',
                                                                     8]}],
    'Электроника|Фото / видео|Объективы': ['Obektivy',
                                           49,
                                           {'Foto/ videotehnika': ['Foto/ '
                                                                   'videotehnika',
                                                                   8]}],
    'Электроника|Фото / видео|Пленочные фотоаппараты': ['Plenochnye '
                                                        'fotoapparaty',
                                                        46,
                                                        {'Foto/ videotehnika': ['Foto/ '
                                                                                'videotehnika',
                                                                                8]}],
    'Электроника|Фото / видео|Телескопы / бинокли': ['Teleskopy / binokli',
                                                     54,
                                                     {'Foto/ videotehnika': ['Foto/ '
                                                                             'videotehnika',
                                                                             8]}],
    'Электроника|Фото / видео|Фотовспышки': ['Fotovspyshki',
                                             51,
                                             {'Foto/ videotehnika': ['Foto/ '
                                                                     'videotehnika',
                                                                     8]}],
    'Электроника|Фото / видео|Цифровые фотоаппараты': ['Cifrovye fotoapparaty',
                                                       47,
                                                       {'Foto/ videotehnika': ['Foto/ '
                                                                               'videotehnika',
                                                                               8]}],
    'Электроника|Фото / видео|Штативы / моноподы': ['SHtativy / monopody',
                                                    50,
                                                    {'Foto/ videotehnika': ['Foto/ '
                                                                            'videotehnika',
                                                                            8]}]}
