MONTHES_NAMES_UA = {
    1: 'січ',
    2: 'лют',
    3: 'бер',
    4: 'кві',
    5: 'тра',
    6: 'чер',
    7: 'лип',
    8: 'сер',
    9: 'вер',
    10: 'жов',
    11: 'лис',
    12: 'гру',
}