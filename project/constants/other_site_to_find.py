#-*- coding: utf-8 -*- 
TEMPLATE_OTHER_SITE_TO_FIND = {
    'Bytovaya technika|Prochee / drugaya tehnika': ('Prochee / drugaya tehnika', 'Bytovaya technika', 6),
    'Elektronika|Stacionarnye telefony': ('Stacionarnye telefony', 'Elektronika', 16),
    'Elektronika|Drugie telefony, faksy': ('Drugie telefony, faksy', 'Elektronika', 18),
    'Krasota i aksessuary|Sumki, ryukzaki, koshelki': ('Sumki, ryukzaki, koshelki', 'Krasota i aksessuary', 23),
    'Krasota i aksessuary|Bizhuteriya i ukrasheniya': ('Bizhuteriya i ukrasheniya', 'Krasota i aksessuary', 24),
    'Krasota i aksessuary|CHasy': ('CHasy', 'Krasota i aksessuary', 25),
    'Krasota i aksessuary|Parfyumeriya': ('Parfyumeriya', 'Krasota i aksessuary', 28),
    'Krasota i aksessuary|Ochki': ('Ochki', 'Krasota i aksessuary', 29),
    'Krasota i aksessuary|Prochee': ('Prochee', 'Krasota i aksessuary', 30),
    'Detskii mir|Detskie igrushki i igry': ('Detskie igrushki i igry', 'Detskii mir', 34),
    'Detskii mir|Detskaya literatura': ('Detskaya literatura', 'Detskii mir', 35),
    'Detskii mir|Dlya samyh malenkih': ('Dlya samyh malenkih', 'Detskii mir', 36),
    'Detskii mir|Dlya shkolnikov': ('Dlya shkolnikov', 'Detskii mir', 37),
    'Detskii mir|Dlya mam': ('Dlya mam', 'Detskii mir', 38),
    'Detskii mir|Detskoe pitanie': ('Detskoe pitanie', 'Detskii mir', 39),
    'Detskii mir|Detskii transport': ('Detskii transport', 'Detskii mir', 40),
    'Detskii mir|Detskaya mebel': ('Detskaya mebel', 'Detskii mir', 41),
    'Detskii mir|Detskie kolyaski i avtokresla': ('Detskie kolyaski i avtokresla', 'Detskii mir', 42),
    'Detskii mir|Prochee': ('Prochee', 'Detskii mir', 44),
    'Sport i zdorove|Drugoe': ('Drugoe', 'Sport i zdorove', 52),
    'Sport i zdorove|Sportivnoe pitanie': ('Sportivnoe pitanie', 'Sport i zdorove', 53),
    'Sport i zdorove|Tovary dlya invalidov': ('Tovary dlya invalidov', 'Sport i zdorove', 54),
    'Hobbi i dosug|Knigi, pressa': ('Knigi, pressa', 'Hobbi i dosug', 55),
    'Hobbi i dosug|CD / DVD plastinki': ('CD / DVD plastinki', 'Hobbi i dosug', 56),
    'Hobbi i dosug|Ohota': ('Ohota', 'Hobbi i dosug', 58),
    'Hobbi i dosug|Prochee': ('Prochee', 'Hobbi i dosug', 61),
    'Zhivotnye i dlya zhivotnyh|Koshki': ('Koshki', 'Zhivotnye i dlya zhivotnyh', 62),
    'Zhivotnye i dlya zhivotnyh|Sobaki': ('Sobaki', 'Zhivotnye i dlya zhivotnyh', 63),
    'Zhivotnye i dlya zhivotnyh|Akvariumy': ('Akvariumy', 'Zhivotnye i dlya zhivotnyh', 64),
    'Zhivotnye i dlya zhivotnyh|Pernatye': ('Pernatye', 'Zhivotnye i dlya zhivotnyh', 65),
    'Zhivotnye i dlya zhivotnyh|Reptilii': ('Reptilii', 'Zhivotnye i dlya zhivotnyh', 66),
    'Zhivotnye i dlya zhivotnyh|Drugie domashnie pitomcy': (
    'Drugie domashnie pitomcy', 'Zhivotnye i dlya zhivotnyh', 67),
    'Zhivotnye i dlya zhivotnyh|Selhoz zhivotnye': ('Selhoz zhivotnye', 'Zhivotnye i dlya zhivotnyh', 68),
    'Zhivotnye i dlya zhivotnyh|Pitanie i aksessuary': ('Pitanie i aksessuary', 'Zhivotnye i dlya zhivotnyh', 69),
    'Zhivotnye i dlya zhivotnyh|Veterinarnye Zootovary': ('Veterinarnye Zootovary', 'Zhivotnye i dlya zhivotnyh', 70),
    'Zhivotnye i dlya zhivotnyh|Uhod za zhivotnymi': ('Uhod za zhivotnymi', 'Zhivotnye i dlya zhivotnyh', 71),
    'Zhivotnye i dlya zhivotnyh|Vyazka': ('Vyazka', 'Zhivotnye i dlya zhivotnyh', 72),
    'Zhivotnye i dlya zhivotnyh|Otdam besplatno': ('Otdam besplatno', 'Zhivotnye i dlya zhivotnyh', 73),
    'Zhivotnye i dlya zhivotnyh|Drugoe': ('Drugoe', 'Zhivotnye i dlya zhivotnyh', 74),
    'Zhivotnye i dlya zhivotnyh|Byurnahodok': ('Byurnahodok', 'Zhivotnye i dlya zhivotnyh', 75),
    'Dom i sad|Vse dlya kuhni': ('Vse dlya kuhni', 'Dom i sad', 76),
    'Dom i sad|Vse dlya vannoj komnaty': ('Vse dlya vannoj komnaty', 'Dom i sad', 77),
    'Dom i sad|Predmety Interera, osveshchenie': ('Predmety Interera, osveshchenie', 'Dom i sad', 78),
    'Dom i sad|Tekstil': ('Tekstil', 'Dom i sad', 81),
    'Dom i sad|Bytovaya himiya': ('Bytovaya himiya', 'Dom i sad', 82),
    'Dom i sad|Produkty pitaniya': ('Produkty pitaniya', 'Dom i sad', 85),
    'Dom i sad|Kanctovary': ('Kanctovary', 'Dom i sad', 86),
    'Dom i sad|Prochee': ('Prochee', 'Dom i sad', 87),
    'Remont i stroitelstvo|Otoplenie': ('Otoplenie', 'Remont i stroitelstvo', 88),
    'Remont i stroitelstvo|Drugie strojmaterialy': ('Drugie strojmaterialy', 'Remont i stroitelstvo', 89),
    'Remont i stroitelstvo|Okna / dveri / stekl/ zerkala': (
    'Okna / dveri / stekl/ zerkala', 'Remont i stroitelstvo', 90),
    'Remont i stroitelstvo|Otdelochnye i oblicovochnye materialy': (
    'Otdelochnye i oblicovochnye materialy', 'Remont i stroitelstvo', 91),
    'Remont i stroitelstvo|Elektrichestvo': ('Elektrichestvo', 'Remont i stroitelstvo', 92),
    'Remont i stroitelstvo|Santehnika': ('Santehnika', 'Remont i stroitelstvo', 93),
    'Remont i stroitelstvo|Metalloprokat / armatura': ('Metalloprokat / armatura', 'Remont i stroitelstvo', 94),
    'Remont i stroitelstvo|Kirpich / beton / penobloki': ('Kirpich / beton / penobloki', 'Remont i stroitelstvo', 95),
    'Remont i stroitelstvo|Pilomaterialy': ('Pilomaterialy', 'Remont i stroitelstvo', 96),
    'Remont i stroitelstvo|Elementy krepleniya': ('Elementy krepleniya', 'Remont i stroitelstvo', 97),
    'Remont i stroitelstvo|Lakokrasochnye materialy': ('Lakokrasochnye materialy', 'Remont i stroitelstvo', 98),
    'Remont i stroitelstvo|Ventilyaciya': ('Ventilyaciya', 'Remont i stroitelstvo', 99),
    'Remont i stroitelstvo|Uslugi': ('Uslugi', 'Remont i stroitelstvo', 100),
    'Transport|Selhoztehnika': ('Selhoztehnika', 'Transport', 102),
    'Transport|Gruzovye avtomobili': ('Gruzovye avtomobili', 'Transport', 104),
    'Transport|Vodnyi transport': ('Vodnyi transport', 'Transport', 106),
    'Transport|Pricepy': ('Pricepy', 'Transport', 108),
    'Transport|Avtobusy': ('Avtobusy', 'Transport', 109),
    'Transport|Drugoj transport': ('Drugoj transport', 'Transport', 110),
    'Uslugi|Remontnye uslugi': ('Remontnye uslugi', 'Uslugi', 111),
    'Uslugi|Dostavka': ('Dostavka', 'Uslugi', 113),
    'Uslugi|Transportnye uslugi / Avtarenda': ('Transportnye uslugi / Avtarenda', 'Uslugi', 114),
    'Uslugi|Uslugi perevoda': ('Uslugi perevoda', 'Uslugi', 115),
    'Uslugi|YUridicheskie, notarialnye uslugi': ('YUridicheskie, notarialnye uslugi', 'Uslugi', 116),
    'Uslugi|Finansy, kredit, strahovanie': ('Finansy, kredit, strahovanie', 'Uslugi', 117),
    'Uslugi|Informacionnye tehnologii, internet': ('Informacionnye tehnologii, internet', 'Uslugi', 118),
    'Uslugi|Organizaciya, obsluzhivanie meropriyatii': ('Organizaciya, obsluzhivanie meropriyatii', 'Uslugi', 120),
    'Uslugi|Turizm, immigraciya': ('Turizm, immigraciya', 'Uslugi', 121),
    'Uslugi|Reklama, marketing': ('Reklama, marketing', 'Uslugi', 122),
    'Uslugi|Bezopasnost, ohrana': ('Bezopasnost, ohrana', 'Uslugi', 123),
    'Bytovaya technika|Tehnika dlya uhoda za soboj|Britvy, epilyatory, mashinki dlya strizhki': (
    'Britvy, epilyatory, mashinki dlya strizhki', 32,
    {'Tehnika dlya uhoda za soboj': ('Tehnika dlya uhoda za soboj', 5)}),
    'Bytovaya technika|Tehnika dlya uhoda za soboj|Feny, ukladka volos': (
    'Feny, ukladka volos', 33, {'Tehnika dlya uhoda za soboj': ('Tehnika dlya uhoda za soboj', 5)}),
    'Bytovaya technika|Tehnika dlya uhoda za soboj|Vesy': (
    'Vesy', 34, {'Tehnika dlya uhoda za soboj': ('Tehnika dlya uhoda za soboj', 5)}),
    'Bytovaya technika|Tehnika dlya uhoda za soboj|Drugaya tehnika dlya individualnoguhoda': (
    'Drugaya tehnika dlya individualnoguhoda', 35, {'Tehnika dlya uhoda za soboj': ('Tehnika dlya uhoda za soboj', 5)}),
    'Elektronika|audiotehnika|Mp3 pleery': ('Mp3 pleery', 36, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Magnitoly': ('Magnitoly', 37, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Muzykalnye centry': ('Muzykalnye centry', 38, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Akusticheskie sistemy': (
    'Akusticheskie sistemy', 39, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Naushniki': ('Naushniki', 40, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Radiopriemniki': ('Radiopriemniki', 41, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Portativnaya akustika': (
    'Portativnaya akustika', 42, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Usiliteli / resivery': (
    'Usiliteli / resivery', 43, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Cd / md / vinilovye proigryvateli': (
    'Cd / md / vinilovye proigryvateli', 44, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|audiotehnika|Prochaya audiotehnika': (
    'Prochaya audiotehnika', 45, {'audiotehnika': ('audiotehnika', 7)}),
    'Elektronika|Foto/ videotehnika|Plenochnye fotoapparaty': (
    'Plenochnye fotoapparaty', 46, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Cifrovye fotoapparaty': (
    'Cifrovye fotoapparaty', 47, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Videokamery': (
    'Videokamery', 48, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Obektivy': ('Obektivy', 49, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|SHtativy / monopody': (
    'SHtativy / monopody', 50, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Fotovspyshki': (
    'Fotovspyshki', 51, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Fotoramki, fotoalbomy': (
    'Fotoramki, fotoalbomy', 52, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Aksessuary, oborudovanie': (
    'Aksessuary, oborudovanie', 53, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Teleskopy / binokli': (
    'Teleskopy / binokli', 54, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Zapchasti i komplektuyushchie': (
    'Zapchasti i komplektuyushchie', 55, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Foto/ videotehnika|Drugoe': ('Drugoe', 56, {'Foto/ videotehnika': ('Foto/ videotehnika', 8)}),
    'Elektronika|Igry, pristavki, PO|Igry dlya pristavok': (
    'Igry dlya pristavok', 57, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Igry, pristavki, PO|tehnika': ('tehnika', 58, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Igry, pristavki, PO|Aksessuary': (
    'Aksessuary', 59, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Igry, pristavki, PO|Remont pristavok': (
    'Remont pristavok', 60, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Igry, pristavki, PO|Drugoe': ('Drugoe', 61, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Igry, pristavki, PO|Igry dlya PK': (
    'Igry dlya PK', 62, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Igry, pristavki, PO|Operacionnye sistemy': (
    'Operacionnye sistemy', 63, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Igry, pristavki, PO|Ofisnye prilozheniya': (
    'Ofisnye prilozheniya', 64, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Igry, pristavki, PO|Utility, antivirusy': (
    'Utility, antivirusy', 65, {'Igry, pristavki, PO': ('Igry, pristavki, PO', 9)}),
    'Elektronika|Komplektuyushchie i aksessuary|Komplektuyushchie': (
    'Komplektuyushchie', 66, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Moduli pamyati': (
    'Moduli pamyati', 67, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Videokarty': (
    'Videokarty', 68, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Zvukovye karty': (
    'Zvukovye karty', 69, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Materinskie platy': (
    'Materinskie platy', 70, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Opticheskie privody': (
    'Opticheskie privody', 71, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|ZHestkie diski': (
    'ZHestkie diski', 72, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Korpusa': (
    'Korpusa', 73, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Processory': (
    'Processory', 74, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|TV-tyunery': (
    'TV-tyunery', 75, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Bloki pitaniya': (
    'Bloki pitaniya', 76, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Sistemy ohlazhdeniya': (
    'Sistemy ohlazhdeniya', 77, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Komplektuyushchie i aksessuary|Operativnaya pamyat': (
    'Operativnaya pamyat', 78, {'Komplektuyushchie i aksessuary': ('Komplektuyushchie i aksessuary', 10)}),
    'Elektronika|Noutbuki|Vse noutbuki': ('Vse noutbuki', 79, {'Noutbuki': ('Noutbuki', 11)}),
    'Bytovaya technika|Tehnika dlya kuhni|Mikrovolnovye pechi': (
    'Mikrovolnovye pechi', 12, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Holodilniki': (
    'Holodilniki', 13, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Plity / pechi': (
    'Plity / pechi', 14, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Elektrochajniki': (
    'Elektrochajniki', 15, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Kofevarki / kofemolki': (
    'Kofevarki / kofemolki', 16, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Kuhonnye kombajny i izmelchiteli': (
    'Kuhonnye kombajny i izmelchiteli', 17, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Parovarki, multivarki': (
    'Parovarki, multivarki', 18, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Hlebopechki': (
    'Hlebopechki', 19, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Posudomoechnye mashiny': (
    'Posudomoechnye mashiny', 20, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Vytyazhki': (
    'Vytyazhki', 21, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Bytovaya technika|Tehnika dlya kuhni|Drugaya tehnika dlya kuhni': (
    'Drugaya tehnika dlya kuhni', 22, {'Tehnika dlya kuhni': ('Tehnika dlya kuhni', 3)}),
    'Elektronika|PK i orgtehnika|PK': ('PK', 80, {'PK i orgtehnika': ('PK i orgtehnika', 12)}),
    'Elektronika|PK i orgtehnika|Printery, skanery, faksy': (
    'Printery, skanery, faksy', 81, {'PK i orgtehnika': ('PK i orgtehnika', 12)}),
    'Elektronika|PK i orgtehnika|Myshki, klaviatura': (
    'Myshki, klaviatura', 82, {'PK i orgtehnika': ('PK i orgtehnika', 12)}),
    'Elektronika|PK i orgtehnika|Fleshki, zhestkie diski': (
    'Fleshki, zhestkie diski', 83, {'PK i orgtehnika': ('PK i orgtehnika', 12)}),
    'Elektronika|PK i orgtehnika|Setevoe oborudovanie': (
    'Setevoe oborudovanie', 84, {'PK i orgtehnika': ('PK i orgtehnika', 12)}),
    'Elektronika|PK i orgtehnika|Monitory': ('Monitory', 85, {'PK i orgtehnika': ('PK i orgtehnika', 12)}),
    'Elektronika|Telefony i planshety|Planshety': (
    'Planshety', 88, {'Telefony i planshety': ('Telefony i planshety', 14)}),
    'Elektronika|Telefony i planshety|Remont i proshivka': (
    'Remont i proshivka', 89, {'Telefony i planshety': ('Telefony i planshety', 14)}),
    'Elektronika|Telefony i planshety|Mobilnye telefony': (
    'Mobilnye telefony', 90, {'Telefony i planshety': ('Telefony i planshety', 14)}),
    'Elektronika|Aksessuary dlya telefoniv|Akkumulyatory': (
    'Akkumulyatory', 91, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Antenny, usiliteli': (
    'Antenny, usiliteli', 92, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Dinamiki, mikrofony': (
    'Dinamiki, mikrofony', 93, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Displei, sensornye ekrany': (
    'Displei, sensornye ekrany', 94, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Drugoe': (
    'Drugoe', 95, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Zaryadnye ustrojstva': (
    'Zaryadnye ustrojstva', 96, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Kabeli': (
    'Kabeli', 97, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Karty pamyati': (
    'Karty pamyati', 98, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Klaviatury': (
    'Klaviatury', 99, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Korpusa': (
    'Korpusa', 100, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Mikroshemy': (
    'Mikroshemy', 101, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Plenki dlya ekranov': (
    'Plenki dlya ekranov', 102, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Provodnye garnitury, naushniki': (
    'Provodnye garnitury, naushniki', 103, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|Razemy, gnezda': (
    'Razemy, gnezda', 104, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|CHehly i futlyary': (
    'CHehly i futlyary', 105, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Aksessuary dlya telefoniv|SHlejfy, perehodniki': (
    'SHlejfy, perehodniki', 106, {'Aksessuary dlya telefoniv': ('Aksessuary dlya telefoniv', 15)}),
    'Elektronika|Sim-karty / tarify / nomera|Beeline': (
    'Beeline', 107, {'Sim-karty / tarify / nomera': ('Sim-karty / tarify / nomera', 17)}),
    'Elektronika|Sim-karty / tarify / nomera|Life': (
    'Life', 108, {'Sim-karty / tarify / nomera': ('Sim-karty / tarify / nomera', 17)}),
    'Elektronika|Sim-karty / tarify / nomera|PEOPLEnet': (
    'PEOPLEnet', 109, {'Sim-karty / tarify / nomera': ('Sim-karty / tarify / nomera', 17)}),
    'Elektronika|Sim-karty / tarify / nomera|Utel': (
    'Utel', 110, {'Sim-karty / tarify / nomera': ('Sim-karty / tarify / nomera', 17)}),
    'Elektronika|Sim-karty / tarify / nomera|Drugoe': (
    'Drugoe', 111, {'Sim-karty / tarify / nomera': ('Sim-karty / tarify / nomera', 17)}),
    'Elektronika|Sim-karty / tarify / nomera|Kievstar, Djuice': (
    'Kievstar, Djuice', 112, {'Sim-karty / tarify / nomera': ('Sim-karty / tarify / nomera', 17)}),
    'Elektronika|Sim-karty / tarify / nomera|MTS': (
    'MTS', 113, {'Sim-karty / tarify / nomera': ('Sim-karty / tarify / nomera', 17)}),
    'Odezhda, obuv|ZHenskaya odezhda|Odezhda dlya beremennyh': (
    'Odezhda dlya beremennyh', 114, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Bluzki, rubashki': (
    'Bluzki, rubashki', 115, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Bryuki': ('Bryuki', 116, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Verhnyaya odezhda': (
    'Verhnyaya odezhda', 117, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|ZHilety': ('ZHilety', 118, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Kostyumy': ('Kostyumy', 119, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|kofty': ('kofty', 120, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Kupalniki, pareo': (
    'Kupalniki, pareo', 121, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Losiny, legginsy': (
    'Losiny, legginsy', 122, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Majki': ('Majki', 123, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Nizhnee belyo': (
    'Nizhnee belyo', 124, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Pidzhaki': ('Pidzhaki', 125, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Platya': ('Platya', 126, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Sarafany': ('Sarafany', 127, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Svitera': ('Svitera', 128, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Topy': ('Topy', 129, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Tuniki': ('Tuniki', 130, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Futbolki': ('Futbolki', 131, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Hudi, tolstovki': (
    'Hudi, tolstovki', 132, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Futbolka': ('Futbolka', 133, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|YUbki': ('YUbki', 134, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Drugoe': ('Drugoe', 135, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Sportivnye kostyumy': (
    'Sportivnye kostyumy', 136, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|ZHenskaya odezhda|Dlya svadby': (
    'Dlya svadby', 137, {'ZHenskaya odezhda': ('ZHenskaya odezhda', 19)}),
    'Odezhda, obuv|Muzhskaya odezhda|Nizhnee bele, plavki': (
    'Nizhnee bele, plavki', 138, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Futbolki, majki': (
    'Futbolki, majki', 139, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Rubashki': ('Rubashki', 140, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|kofty': ('kofty', 141, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Svitera': ('Svitera', 142, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Tolstovki': ('Tolstovki', 143, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Kostyumy, pidzhaki, zhilety': (
    'Kostyumy, pidzhaki, zhilety', 144, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Bryuki': ('Bryuki', 145, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Futbolka': ('Futbolka', 146, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Verhnyaya odezhda': (
    'Verhnyaya odezhda', 147, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Drugoe': ('Drugoe', 148, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|Muzhskaya odezhda|Sportivnye kostyumy': (
    'Sportivnye kostyumy', 149, {'Muzhskaya odezhda': ('Muzhskaya odezhda', 20)}),
    'Odezhda, obuv|ZHenskaya obuv|Baletki': ('Baletki', 150, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|ZHenskaya obuv|Mokasiny': ('Mokasiny', 151, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|ZHenskaya obuv|Tufli': ('Tufli', 152, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|ZHenskaya obuv|Bosonozhki': ('Bosonozhki', 153, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|ZHenskaya obuv|SHlepancy': ('SHlepancy', 154, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|ZHenskaya obuv|Botinki': ('Botinki', 155, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|ZHenskaya obuv|Sapogi': ('Sapogi', 156, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|ZHenskaya obuv|Drugoe': ('Drugoe', 157, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|ZHenskaya obuv|Sportivnaya obuv': (
    'Sportivnaya obuv', 158, {'ZHenskaya obuv': ('ZHenskaya obuv', 21)}),
    'Odezhda, obuv|Muzhskaya obuv|Sapogi, botinki': (
    'Sapogi, botinki', 159, {'Muzhskaya obuv': ('Muzhskaya obuv', 22)}),
    'Odezhda, obuv|Muzhskaya obuv|Tufli, mokasiny': (
    'Tufli, mokasiny', 160, {'Muzhskaya obuv': ('Muzhskaya obuv', 22)}),
    'Odezhda, obuv|Muzhskaya obuv|Sandalii, shlepancy': (
    'Sandalii, shlepancy', 161, {'Muzhskaya obuv': ('Muzhskaya obuv', 22)}),
    'Odezhda, obuv|Muzhskaya obuv|Sportivnaya obuv': (
    'Sportivnaya obuv', 162, {'Muzhskaya obuv': ('Muzhskaya obuv', 22)}),
    'Odezhda, obuv|Muzhskaya obuv|Drugaya obuv': ('Drugaya obuv', 163, {'Muzhskaya obuv': ('Muzhskaya obuv', 22)}),
    'Odezhda, obuv|Muzhskaya obuv|Golovnye ubory': ('Golovnye ubory', 164, {'Muzhskaya obuv': ('Muzhskaya obuv', 22)}),
    'Krasota i aksessuary|Kosmetika i uhod|Vsydlya volos': (
    'Vsydlya volos', 172, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Vsydlya nogtej': (
    'Vsydlya nogtej', 173, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Dekorativnaya kosmetika': (
    'Dekorativnaya kosmetika', 174, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Drugoe': ('Drugoe', 175, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Kosmetika solncezashchitnaya, dlya solyariya': (
    'Kosmetika solncezashchitnaya, dlya solyariya', 176, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Parfyumeriya': (
    'Parfyumeriya', 177, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Sredstva dlya britya / depilyacii': (
    'Sredstva dlya britya / depilyacii', 178, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Uslugi': ('Uslugi', 179, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Uhod za licom i telom': (
    'Uhod za licom i telom', 180, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Uhod za polostyu rta': (
    'Uhod za polostyu rta', 181, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Pribory i aksessuary': (
    'Pribory i aksessuary', 182, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Krasota i aksessuary|Kosmetika i uhod|Sredstva dlya volos': (
    'Sredstva dlya volos', 183, {'Kosmetika i uhod': ('Kosmetika i uhod', 27)}),
    'Detskii mir|Detskaya odezhda|Verhnyaya odezhda': (
    'Verhnyaya odezhda', 184, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Golovnye ubory, sharfy': (
    'Golovnye ubory, sharfy', 185, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Dlya mladencev': (
    'Dlya mladencev', 186, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Drugoe': ('Drugoe', 187, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Karnavalnye kostyumy': (
    'Karnavalnye kostyumy', 188, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Kolgotki, noski, golfy': (
    'Kolgotki, noski, golfy', 189, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|kofty, reglany': (
    'kofty, reglany', 190, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Kupalniki, plavki': (
    'Kupalniki, plavki', 191, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Nizhnee bele': ('Nizhnee bele', 192, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Perchatki, varezhki': (
    'Perchatki, varezhki', 193, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Pidzhaki, rubashki': (
    'Pidzhaki, rubashki', 194, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Pizhamy, halaty': (
    'Pizhamy, halaty', 195, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Platya, sarafany': (
    'Platya, sarafany', 196, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Sportivnye kostyumy': (
    'Sportivnye kostyumy', 197, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|Futbolki, majki': (
    'Futbolki, majki', 198, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|SHkolnaya forma': (
    'SHkolnaya forma', 199, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|SHorty, bridzhi, kapri': (
    'SHorty, bridzhi, kapri', 200, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|SHtany': ('SHtany', 201, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya odezhda|YUbki': ('YUbki', 202, {'Detskaya odezhda': ('Detskaya odezhda', 31)}),
    'Detskii mir|Detskaya obuv|Drugoe': ('Drugoe', 203, {'Detskaya obuv': ('Detskaya obuv', 32)}),
    'Detskii mir|Detskaya obuv|Krossovki, kedy, mokasiny, butsy': (
    'Krossovki, kedy, mokasiny, butsy', 204, {'Detskaya obuv': ('Detskaya obuv', 32)}),
    'Detskii mir|Detskaya obuv|Sandalii, bosonozhki, shlepancy': (
    'Sandalii, bosonozhki, shlepancy', 205, {'Detskaya obuv': ('Detskaya obuv', 32)}),
    'Detskii mir|Detskaya obuv|Sapogi, botinki': ('Sapogi, botinki', 206, {'Detskaya obuv': ('Detskaya obuv', 32)}),
    'Detskii mir|Detskaya obuv|Tapochki, cheshki': ('Tapochki, cheshki', 207, {'Detskaya obuv': ('Detskaya obuv', 32)}),
    'Detskii mir|Detskaya obuv|Tufli': ('Tufli', 208, {'Detskaya obuv': ('Detskaya obuv', 32)}),
    'Detskii mir|Aksessuary dlya detej|Galstuki, babochki': (
    'Galstuki, babochki', 209, {'Aksessuary dlya detej': ('Aksessuary dlya detej', 33)}),
    'Detskii mir|Aksessuary dlya detej|Drugoe': (
    'Drugoe', 210, {'Aksessuary dlya detej': ('Aksessuary dlya detej', 33)}),
    'Detskii mir|Aksessuary dlya detej|Zonty, dozhdeviki': (
    'Zonty, dozhdeviki', 211, {'Aksessuary dlya detej': ('Aksessuary dlya detej', 33)}),
    'Detskii mir|Aksessuary dlya detej|Kosmetika i ukrasheniya dlya devochek': (
    'Kosmetika i ukrasheniya dlya devochek', 212, {'Aksessuary dlya detej': ('Aksessuary dlya detej', 33)}),
    'Detskii mir|Aksessuary dlya detej|Ochki i aksessuary': (
    'Ochki i aksessuary', 213, {'Aksessuary dlya detej': ('Aksessuary dlya detej', 33)}),
    'Detskii mir|Aksessuary dlya detej|Rezinki, zakolki, obruchi': (
    'Rezinki, zakolki, obruchi', 214, {'Aksessuary dlya detej': ('Aksessuary dlya detej', 33)}),
    'Detskii mir|Aksessuary dlya detej|Remni, poyasa, podtyazhki': (
    'Remni, poyasa, podtyazhki', 215, {'Aksessuary dlya detej': ('Aksessuary dlya detej', 33)}),
    'Detskii mir|Aksessuary dlya detej|Ryukzaki, sumki': (
    'Ryukzaki, sumki', 216, {'Aksessuary dlya detej': ('Aksessuary dlya detej', 33)}),
    'Detskii mir|Zdorove i gigiena|Vse dlya kormleniya': (
    'Vse dlya kormleniya', 217, {'Zdorove i gigiena': ('Zdorove i gigiena', 43)}),
    'Detskii mir|Zdorove i gigiena|Vse dlya kupaniya': (
    'Vse dlya kupaniya', 218, {'Zdorove i gigiena': ('Zdorove i gigiena', 43)}),
    'Detskii mir|Zdorove i gigiena|Gorshki, nakladki na unitaz': (
    'Gorshki, nakladki na unitaz', 219, {'Zdorove i gigiena': ('Zdorove i gigiena', 43)}),
    'Detskii mir|Zdorove i gigiena|Gradusniki, termometry, vesy': (
    'Gradusniki, termometry, vesy', 220, {'Zdorove i gigiena': ('Zdorove i gigiena', 43)}),
    'Detskii mir|Zdorove i gigiena|Drugoe': ('Drugoe', 221, {'Zdorove i gigiena': ('Zdorove i gigiena', 43)}),
    'Detskii mir|Zdorove i gigiena|Krema, masla, prisypka': (
    'Krema, masla, prisypka', 222, {'Zdorove i gigiena': ('Zdorove i gigiena', 43)}),
    'Detskii mir|Zdorove i gigiena|Podguzniki, salfetki': (
    'Podguzniki, salfetki', 223, {'Zdorove i gigiena': ('Zdorove i gigiena', 43)}),
    'Sport i zdorove|Sportinventar|Alpinizm, speleologiya': (
    'Alpinizm, speleologiya', 224, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|arbalety, luki, strely': (
    'arbalety, luki, strely', 225, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Bilyard': ('Bilyard', 226, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Boevye iskusstva': (
    'Boevye iskusstva', 227, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Vodnyi sport': ('Vodnyi sport', 228, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Ganteli, giri, shtangi': (
    'Ganteli, giri, shtangi', 229, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Darts': ('Darts', 230, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Drugoe': ('Drugoe', 231, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Zimnii sport': ('Zimnii sport', 232, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Igry s myachom': ('Igry s myachom', 233, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Nastolnye igry': ('Nastolnye igry', 234, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Pejntbol, strajkbol': (
    'Pejntbol, strajkbol', 235, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Roliki, skejty': ('Roliki, skejty', 236, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Sportivnye ugolki': (
    'Sportivnye ugolki', 237, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Tennis, badminton': (
    'Tennis, badminton', 238, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Sportinventar|Fitnes, joga, aerobika, gimnastika': (
    'Fitnes, joga, aerobika, gimnastika', 239, {'Sportinventar': ('Sportinventar', 45)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Dorozhnye sumki': (
    'Dorozhnye sumki', 240, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Drugoe': (
    'Drugoe', 241, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Mangaly, gril, shampura': (
    'Mangaly, gril, shampura', 242, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Metalloiskateli': (
    'Metalloiskateli', 243, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Naduvnye bassejny': (
    'Naduvnye bassejny', 244, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Nozhi, multituly': (
    'Nozhi, multituly', 245, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Palatki': (
    'Palatki', 246, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Posuda, flyagi, termosy': (
    'Posuda, flyagi, termosy', 247, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Pohodnyi inventar': (
    'Pohodnyi inventar', 248, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|putyovki, tury': (
    'putyovki, tury', 249, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Ryukzaki': (
    'Ryukzaki', 250, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Spalnye meshki, gamaki': (
    'Spalnye meshki, gamaki', 251, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Vsydlya otdyha, turizm|Fonari, fonariki': (
    'Fonari, fonariki', 252, {'Vsydlya otdyha, turizm': ('Vsydlya otdyha, turizm', 46)}),
    'Sport i zdorove|Pribory i ustrojstva|Vesy': ('Vesy', 253, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Drugoe': (
    'Drugoe', 254, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Kardiopribory, pribory dlya pulsa': (
    'Kardiopribory, pribory dlya pulsa', 255, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Massazhery': (
    'Massazhery', 256, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Poyasa, miostimulyatory': (
    'Poyasa, miostimulyatory', 257, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Pribory magnitoterapii': (
    'Pribory magnitoterapii', 258, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Sluhovye apparaty': (
    'Sluhovye apparaty', 259, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Termometry, gradusniki': (
    'Termometry, gradusniki', 260, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Tonometry': (
    'Tonometry', 261, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Pribory i ustrojstva|Filtry': (
    'Filtry', 262, {'Pribory i ustrojstva': ('Pribory i ustrojstva', 47)}),
    'Sport i zdorove|Trenazhery|Begovye dorozhki': ('Begovye dorozhki', 263, {'Trenazhery': ('Trenazhery', 48)}),
    'Sport i zdorove|Trenazhery|Velotrenazhyory': ('Velotrenazhyory', 264, {'Trenazhery': ('Trenazhery', 48)}),
    'Sport i zdorove|Trenazhery|Drugie': ('Drugie', 265, {'Trenazhery': ('Trenazhery', 48)}),
    'Sport i zdorove|Trenazhery|Orbitreki': ('Orbitreki', 266, {'Trenazhery': ('Trenazhery', 48)}),
    'Sport i zdorove|Trenazhery|Silovye trenazhyory': ('Silovye trenazhyory', 267, {'Trenazhery': ('Trenazhery', 48)}),
    'Sport i zdorove|Trenazhery|skami i stojki': ('skami i stojki', 268, {'Trenazhery': ('Trenazhery', 48)}),
    'Sport i zdorove|Trenazhery|steppery': ('steppery', 269, {'Trenazhery': ('Trenazhery', 48)}),
    'Sport i zdorove|Velosipedy i aksessuary k nim|Smartfony': (
    'Smartfony', 276, {'Velosipedy i aksessuary k nim': ('Velosipedy i aksessuary k nim', 50)}),
    'Sport i zdorove|Velosipedy i aksessuary k nim|Drugoe': (
    'Drugoe', 277, {'Velosipedy i aksessuary k nim': ('Velosipedy i aksessuary k nim', 50)}),
    'Sport i zdorove|Velosipedy i aksessuary k nim|Komplektuyushchie': (
    'Komplektuyushchie', 278, {'Velosipedy i aksessuary k nim': ('Velosipedy i aksessuary k nim', 50)}),
    'Sport i zdorove|Velosipedy i aksessuary k nim|Snaryazhenie, aksessuary': (
    'Snaryazhenie, aksessuary', 279, {'Velosipedy i aksessuary k nim': ('Velosipedy i aksessuary k nim', 50)}),
    'Sport i zdorove|Velosipedy i aksessuary k nim|Ekipirovka dlya velosporta': (
    'Ekipirovka dlya velosporta', 280, {'Velosipedy i aksessuary k nim': ('Velosipedy i aksessuary k nim', 50)}),
    'Sport i zdorove|Specodezhda, obuv|Drugoe': ('Drugoe', 281, {'Specodezhda, obuv': ('Specodezhda, obuv', 51)}),
    'Sport i zdorove|Specodezhda, obuv|Ohota, rybolovstvo': (
    'Ohota, rybolovstvo', 282, {'Specodezhda, obuv': ('Specodezhda, obuv', 51)}),
    'Sport i zdorove|Specodezhda, obuv|Turizm': ('Turizm', 283, {'Specodezhda, obuv': ('Specodezhda, obuv', 51)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Studiinoe oborudovanie': (
    'Studiinoe oborudovanie', 284, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Drugoe': (
    'Drugoe', 285, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Aksessuary dlya muzykalnyh instrumentov': (
    'Aksessuary dlya muzykalnyh instrumentov', 286, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Pianin/ fortepian/ royali': (
    'Pianin/ fortepian/ royali', 287, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Elektrogitary': (
    'Elektrogitary', 288, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Akusticheskie gitary': (
    'Akusticheskie gitary', 289, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|vyhodnoj': (
    'vyhodnoj', 290, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|duhovye': (
    'duhovye', 291, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Udarnye instrumenty': (
    'Udarnye instrumenty', 292, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Kombousiliteli': (
    'Kombousiliteli', 293, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Muzykalnye instrumenty|Bas-gitary': (
    'Bas-gitary', 294, {'Muzykalnye instrumenty': ('Muzykalnye instrumenty', 57)}),
    'Hobbi i dosug|Rybolovstvo|Drugoe': ('Drugoe', 295, {'Rybolovstvo': ('Rybolovstvo', 59)}),
    'Hobbi i dosug|Rybolovstvo|Katushki': ('Katushki', 296, {'Rybolovstvo': ('Rybolovstvo', 59)}),
    'Hobbi i dosug|Rybolovstvo|Lodki, katamarany': ('Lodki, katamarany', 297, {'Rybolovstvo': ('Rybolovstvo', 59)}),
    'Hobbi i dosug|Rybolovstvo|Podvodnaya ohota': ('Podvodnaya ohota', 298, {'Rybolovstvo': ('Rybolovstvo', 59)}),
    'Hobbi i dosug|Rybolovstvo|Rybackii inventar': ('Rybackii inventar', 299, {'Rybolovstvo': ('Rybolovstvo', 59)}),
    'Hobbi i dosug|Rybolovstvo|Udilishcha': ('Udilishcha', 300, {'Rybolovstvo': ('Rybolovstvo', 59)}),
    'Hobbi i dosug|Rybolovstvo|Eholoty, radary': ('Eholoty, radary', 301, {'Rybolovstvo': ('Rybolovstvo', 59)}),
    'Dom i sad|Mebel|Mebel dlya gostinoj': ('Mebel dlya gostinoj', 317, {'Mebel': ('Mebel', 79)}),
    'Dom i sad|Mebel|Mebel na zakaz': ('Mebel na zakaz', 318, {'Mebel': ('Mebel', 79)}),
    'Dom i sad|Mebel|Mebel dlya spalni': ('Mebel dlya spalni', 319, {'Mebel': ('Mebel', 79)}),
    'Dom i sad|Mebel|Ofisnaya mebel': ('Ofisnaya mebel', 320, {'Mebel': ('Mebel', 79)}),
    'Dom i sad|Mebel|Mebel': ('Mebel', 321, {'Mebel': ('Mebel', 79)}),
    'Dom i sad|Mebel|Mebel dlya prihozhej': ('Mebel dlya prihozhej', 322, {'Mebel': ('Mebel', 79)}),
    'Dom i sad|Mebel|Mebel dlya vannoj komnaty': ('Mebel dlya vannoj komnaty', 323, {'Mebel': ('Mebel', 79)}),
    'Dom i sad|Instrumenty / inventar|Elektroinstrument': (
    'Elektroinstrument', 324, {'Instrumenty / inventar': ('Instrumenty / inventar', 80)}),
    'Dom i sad|Instrumenty / inventar|Ruchnoj instrument': (
    'Ruchnoj instrument', 325, {'Instrumenty / inventar': ('Instrumenty / inventar', 80)}),
    'Dom i sad|Instrumenty / inventar|Drugoj instrument': (
    'Drugoj instrument', 326, {'Instrumenty / inventar': ('Instrumenty / inventar', 80)}),
    'Dom i sad|Instrumenty / inventar|Benzoinstrument': (
    'Benzoinstrument', 327, {'Instrumenty / inventar': ('Instrumenty / inventar', 80)}),
    'Dom i sad|Instrumenty / inventar|Komplektuyushchie, rashodnye materialy': (
    'Komplektuyushchie, rashodnye materialy', 328, {'Instrumenty / inventar': ('Instrumenty / inventar', 80)}),
    'Dom i sad|Rasteniya|Gorshki, zemlya, udobreniya': (
    'Gorshki, zemlya, udobreniya', 329, {'Rasteniya': ('Rasteniya', 83)}),
    'Dom i sad|Rasteniya|Rasteniya': ('Rasteniya', 330, {'Rasteniya': ('Rasteniya', 83)}),
    'Dom i sad|Sad i ogorod|Vse dlya poliva': ('Vse dlya poliva', 331, {'Sad i ogorod': ('Sad i ogorod', 84)}),
    'Dom i sad|Sad i ogorod|Drugoe': ('Drugoe', 332, {'Sad i ogorod': ('Sad i ogorod', 84)}),
    'Dom i sad|Sad i ogorod|Sadovaya mebel, shatry, besedki': (
    'Sadovaya mebel, shatry, besedki', 333, {'Sad i ogorod': ('Sad i ogorod', 84)}),
    'Dom i sad|Sad i ogorod|Sadovaya tehnika': ('Sadovaya tehnika', 334, {'Sad i ogorod': ('Sad i ogorod', 84)}),
    'Dom i sad|Sad i ogorod|Sadovye svetilniki': ('Sadovye svetilniki', 335, {'Sad i ogorod': ('Sad i ogorod', 84)}),
    'Dom i sad|Sad i ogorod|Sadovyi instrument': ('Sadovyi instrument', 336, {'Sad i ogorod': ('Sad i ogorod', 84)}),
    'Dom i sad|Sad i ogorod|Sredstva ot nasekomyh': (
    'Sredstva ot nasekomyh', 337, {'Sad i ogorod': ('Sad i ogorod', 84)}),
    'Dom i sad|Sad i ogorod|Udobreniya i himikaty': (
    'Udobreniya i himikaty', 338, {'Sad i ogorod': ('Sad i ogorod', 84)}),
    'Transport|Zapchasti / aksessuary|Avtozapchasti': (
    'Avtozapchasti', 339, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|SHiny / diski': (
    'SHiny / diski', 340, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|Aksessuary dlya avto': (
    'Aksessuary dlya avto', 341, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|Zapchasti dlya spec / s.h.tehniki': (
    'Zapchasti dlya spec / s.h.tehniki', 342, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|Motozapchasti': (
    'Motozapchasti', 343, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|Avtozvuk': (
    'Avtozvuk', 344, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|Drugie zapchasti': (
    'Drugie zapchasti', 345, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|Motaksessuary': (
    'Motaksessuary', 346, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|Transport na zapchasti': (
    'Transport na zapchasti', 347, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Zapchasti / aksessuary|GPS-navigatory / avtoregistratory': (
    'GPS-navigatory / avtoregistratory', 348, {'Zapchasti / aksessuary': ('Zapchasti / aksessuary', 101)}),
    'Transport|Moto|Mopedy / skutery': ('Mopedy / skutery', 350, {'Moto': ('Moto', 105)}),
    'Transport|Moto|Motocikly': ('Motocikly', 351, {'Moto': ('Moto', 105)}),
    'Transport|Moto|Kvadrocikly': ('Kvadrocikly', 352, {'Moto': ('Moto', 105)}),
    'Transport|Moto|Mot-drugoe': ('Mot-drugoe', 353, {'Moto': ('Moto', 105)}),
    'Transport|Legkovye avtomobili|Vse Legkovye avtomobili': (
    'Vse Legkovye avtomobili', 349, {'Legkovye avtomobili': ('Legkovye avtomobili', 103)}),
    'Transport|Spectehnika|Buldozery / traktory': ('Buldozery / traktory', 354, {'Spectehnika': ('Spectehnika', 107)}),
    'Transport|Spectehnika|Pogruzchiki': ('Pogruzchiki', 355, {'Spectehnika': ('Spectehnika', 107)}),
    'Transport|Spectehnika|Stroitelnaya tehnika': ('Stroitelnaya tehnika', 356, {'Spectehnika': ('Spectehnika', 107)}),
    'Transport|Spectehnika|Drugaya spectehnika': ('Drugaya spectehnika', 357, {'Spectehnika': ('Spectehnika', 107)}),
    'Transport|Spectehnika|Oborudovanie dlya spectehniki': (
    'Oborudovanie dlya spectehniki', 358, {'Spectehnika': ('Spectehnika', 107)}),
    'Transport|Spectehnika|Ekskavatory': ('Ekskavatory', 359, {'Spectehnika': ('Spectehnika', 107)}),
    'Transport|Spectehnika|Kommunalnaya tehnika': ('Kommunalnaya tehnika', 360, {'Spectehnika': ('Spectehnika', 107)}),
    'Transport|Spectehnika|Samosvaly': ('Samosvaly', 361, {'Spectehnika': ('Spectehnika', 107)}),
    'Uslugi|Uborka|Otdelka / remont': ('Otdelka / remont', 362, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Uborka|Stroitelnye uslugi': ('Stroitelnye uslugi', 363, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Uborka|Bytovoj remont / uborka': ('Bytovoj remont / uborka', 364, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Uborka|Okna / dveri / balkony': ('Okna / dveri / balkony', 365, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Uborka|Gotovye konstrukcii': ('Gotovye konstrukcii', 366, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Uborka|Santehnika / kommunikacii': ('Santehnika / kommunikacii', 367, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Uborka|Elektrichestvo': ('Elektrichestvo', 368, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Uborka|Dizajn / arhitektura': ('Dizajn / arhitektura', 369, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Uborka|Ventilyaciya / kondicionirovanie': (
    'Ventilyaciya / kondicionirovanie', 370, {'Uborka': ('Uborka', 112)}),
    'Uslugi|Zdorove, krasota|Manikyur / narashchivanie nogtej': (
    'Manikyur / narashchivanie nogtej', 371, {'Zdorove, krasota': ('Zdorove, krasota', 119)}),
    'Uslugi|Zdorove, krasota|Makiyazh / kosmetologiya / narashchivanie resnic': (
    'Makiyazh / kosmetologiya / narashchivanie resnic', 372, {'Zdorove, krasota': ('Zdorove, krasota', 119)}),
    'Uslugi|Zdorove, krasota|Krasota / zdorove ': (
    'Krasota / zdorove ', 373, {'Zdorove, krasota': ('Zdorove, krasota', 119)}),
    'Uslugi|Zdorove, krasota|prochee': ('prochee', 374, {'Zdorove, krasota': ('Zdorove, krasota', 119)}),
    'Uslugi|Zdorove, krasota|Massazh': ('Massazh', 375, {'Zdorove, krasota': ('Zdorove, krasota', 119)}),
    'Uslugi|Zdorove, krasota|Strizhki / narashchivanie volos': (
    'Strizhki / narashchivanie volos', 376, {'Zdorove, krasota': ('Zdorove, krasota', 119)}),
    'Uslugi|Zdorove, krasota|Medicina': ('Medicina', 377, {'Zdorove, krasota': ('Zdorove, krasota', 119)}),
    'Uslugi|Zdorove, krasota|Uslugi psihologa': (
    'Uslugi psihologa', 378, {'Zdorove, krasota': ('Zdorove, krasota', 119)}),
    'Bytovaya technika|Tehnika dlya doma|Pylesosy': ('Pylesosy', 23, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Bytovaya technika|Tehnika dlya doma|Utyugi': ('Utyugi', 24, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Bytovaya technika|Tehnika dlya doma|Stiralnye mashiny': (
    'Stiralnye mashiny', 25, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Bytovaya technika|Tehnika dlya doma|SHvejnye mashiny i overloki': (
    'SHvejnye mashiny i overloki', 26, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Bytovaya technika|Tehnika dlya doma|vyazalnye mashiny': (
    'vyazalnye mashiny', 27, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Bytovaya technika|Tehnika dlya doma|Filtry dlya vody': (
    'Filtry dlya vody', 28, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Bytovaya technika|Tehnika dlya doma|Drugaya tehnika dlya doma': (
    'Drugaya tehnika dlya doma', 29, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Bytovaya technika|Tehnika dlya doma|Vodonagrevateli': (
    'Vodonagrevateli', 30, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Bytovaya technika|Tehnika dlya doma|Aksessuary': (
    'Aksessuary', 31, {'Tehnika dlya doma': ('Tehnika dlya doma', 4)}),
    'Uslugi|Torgovlya': ('Torgovlya', 'Uslugi', 124),
    'Uslugi|Poligrafiya, izdatelstvo': ('Poligrafiya, izdatelstvo', 'Uslugi', 125),
    'Uslugi|Uslugi puhodu za zhivotnymi': ('Uslugi puhodu za zhivotnymi', 'Uslugi', 126),
    'Uslugi|Razvlecheniya, foto, video': ('Razvlecheniya, foto, video', 'Uslugi', 127),
    'Uslugi|Prokat': ('Prokat', 'Uslugi', 128),
    'Uslugi|Nyani, sidelki': ('Nyani, sidelki', 'Uslugi', 129),
    'Uslugi|Obrazovanie': ('Obrazovanie', 'Uslugi', 130),
    'Uslugi|Pomoshch ekstrasensov': ('Pomoshch ekstrasensov', 'Uslugi', 131),
    'Uslugi|Sport': ('Sport', 'Uslugi', 132),
    'Uslugi|Selskoe hozyajstvo': ('Selskoe hozyajstvo', 'Uslugi', 133),
    'Uslugi|Prochee': ('Prochee', 'Uslugi', 134),
    'Bytovaya technika|Klimaticheskaya tehnika|Kondicionery': (
    'Kondicionery', 1, {'Klimaticheskaya tehnika': ('Klimaticheskaya tehnika', 1)}),
    'Bytovaya technika|Klimaticheskaya tehnika|Ventilyatory': (
    'Ventilyatory', 2, {'Klimaticheskaya tehnika': ('Klimaticheskaya tehnika', 1)}),
    'Bytovaya technika|Klimaticheskaya tehnika|Otoplenie': (
    'Otoplenie', 3, {'Klimaticheskaya tehnika': ('Klimaticheskaya tehnika', 1)}),
    'Bytovaya technika|Klimaticheskaya tehnika|Vozduhoochistiteli, uvlazhniteli': (
    'Vozduhoochistiteli, uvlazhniteli', 4, {'Klimaticheskaya tehnika': ('Klimaticheskaya tehnika', 1)}),
    'Bytovaya technika|Klimaticheskaya tehnika|Drugaya tehnika': (
    'Drugaya tehnika', 5, {'Klimaticheskaya tehnika': ('Klimaticheskaya tehnika', 1)}),
    'Bytovaya technika|Klimaticheskaya tehnika|Aksessuary i komplektuyushchie': (
    'Aksessuary i komplektuyushchie', 6, {'Klimaticheskaya tehnika': ('Klimaticheskaya tehnika', 1)}),
    'Bytovaya technika|Televizory / Videotehnika|Proigryvateli': (
    'Proigryvateli', 7, {'Televizory / Videotehnika': ('Televizory / Videotehnika', 2)}),
    'Bytovaya technika|Televizory / Videotehnika|Televizory': (
    'Televizory', 8, {'Televizory / Videotehnika': ('Televizory / Videotehnika', 2)}),
    'Bytovaya technika|Televizory / Videotehnika|Aksessuary dlya TV / videotehniki': (
    'Aksessuary dlya TV / videotehniki', 9, {'Televizory / Videotehnika': ('Televizory / Videotehnika', 2)}),
    'Bytovaya technika|Televizory / Videotehnika|Sputnikovoe TV': (
    'Sputnikovoe TV', 10, {'Televizory / Videotehnika': ('Televizory / Videotehnika', 2)}),
    'Bytovaya technika|Televizory / Videotehnika|Drugaya TV / videotehnika': (
    'Drugaya TV / videotehnika', 11, {'Televizory / Videotehnika': ('Televizory / Videotehnika', 2)}),
    'Elektronika|Elektronnye knigi|KPK': ('KPK', 86, {'Elektronnye knigi': ('Elektronnye knigi', 13)}),
    'Elektronika|Elektronnye knigi|Aksessuary': ('Aksessuary', 87, {'Elektronnye knigi': ('Elektronnye knigi', 13)}),
    'Krasota i aksessuary|YUvelirnye izdeliya|Kolca': (
    'Kolca', 165, {'YUvelirnye izdeliya': ('YUvelirnye izdeliya', 26)}),
    'Krasota i aksessuary|YUvelirnye izdeliya|Sergi': (
    'Sergi', 166, {'YUvelirnye izdeliya': ('YUvelirnye izdeliya', 26)}),
    'Krasota i aksessuary|YUvelirnye izdeliya|Braslety': (
    'Braslety', 167, {'YUvelirnye izdeliya': ('YUvelirnye izdeliya', 26)}),
    'Krasota i aksessuary|YUvelirnye izdeliya|Kulony / Podveski': (
    'Kulony / Podveski', 168, {'YUvelirnye izdeliya': ('YUvelirnye izdeliya', 26)}),
    'Krasota i aksessuary|YUvelirnye izdeliya|Broshi': (
    'Broshi', 169, {'YUvelirnye izdeliya': ('YUvelirnye izdeliya', 26)}),
    'Krasota i aksessuary|YUvelirnye izdeliya|Cepochki': (
    'Cepochki', 170, {'YUvelirnye izdeliya': ('YUvelirnye izdeliya', 26)}),
    'Krasota i aksessuary|YUvelirnye izdeliya|Drugie yuvelirnye izdeliya': (
    'Drugie yuvelirnye izdeliya', 171, {'YUvelirnye izdeliya': ('YUvelirnye izdeliya', 26)}),
    'Sport i zdorove|Zdorove|Aromaterapiya, svechi, lampy': (
    'Aromaterapiya, svechi, lampy', 270, {'Zdorove': ('Zdorove', 49)}),
    'Sport i zdorove|Zdorove|Lekarstva': ('Lekarstva', 271, {'Zdorove': ('Zdorove', 49)}),
    'Sport i zdorove|Zdorove|Sredstva dlya pohudeniya': ('Sredstva dlya pohudeniya', 272, {'Zdorove': ('Zdorove', 49)}),
    'Sport i zdorove|Zdorove|Drugoe': ('Drugoe', 273, {'Zdorove': ('Zdorove', 49)}),
    'Sport i zdorove|Zdorove|Netradicionnaya medicina': ('Netradicionnaya medicina', 274, {'Zdorove': ('Zdorove', 49)}),
    'Sport i zdorove|Zdorove|Ochki, linzy, aksessuary': ('Ochki, linzy, aksessuary', 275, {'Zdorove': ('Zdorove', 49)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Numizmatika': (
    'Numizmatika', 302, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Drugie kategorii': (
    'Drugie kategorii', 303, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Bonistika': (
    'Bonistika', 304, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Stolovoe serebro, antikvarnaya kuhonnaya utvar': (
    'Stolovoe serebro, antikvarnaya kuhonnaya utvar', 305,
    {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Filateliya': (
    'Filateliya', 306, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Skulptury': (
    'Skulptury', 307, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Kartiny, antikvarnaya mebel, predmety interera': (
    'Kartiny, antikvarnaya mebel, predmety interera', 308,
    {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Masshtabnye modeli': (
    'Masshtabnye modeli', 309, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Militariya': (
    'Militariya', 310, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Medali, znachki, zhetony': (
    'Medali, znachki, zhetony', 311, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Ikony, skladni, metalloplastika': (
    'Ikony, skladni, metalloplastika', 312,
    {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Knigi': (
    'Knigi', 313, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Plastinki': (
    'Plastinki', 314, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Monety Ukrainy iz drag.splavov': (
    'Monety Ukrainy iz drag.splavov', 315,
    {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Hobbi i dosug|Kollekcionirovanie / antikvariat|Kollekcionnoe oruzhie': (
    'Kollekcionnoe oruzhie', 316, {'Kollekcionirovanie / antikvariat': ('Kollekcionirovanie / antikvariat', 60)}),
    'Obshchenie|Poteri': ('Poteri', 'Obshchenie', 135),
    'Obshchenie|Nahodki': ('Nahodki', 'Obshchenie', 136),
    'Obshchenie|Bilety na meropriyatiya': ('Bilety na meropriyatiya', 'Obshchenie', 137),
    'Obshchenie|Poisk poputchikov': ('Poisk poputchikov', 'Obshchenie', 138),
    'Obshchenie|Poisk grupp / muzykantov': ('Poisk grupp / muzykantov', 'Obshchenie', 139),
    'Obshchenie|Poisk lyudej': ('Poisk lyudej', 'Obshchenie', 140),
    'Drugoe|Tovary dlya vzroslyh': ('Tovary dlya vzroslyh', 'Drugoe', 141),
    'Drugoe|Podarki i suveniry': ('Podarki i suveniry', 'Drugoe', 142),
    'Drugoe|Otdam besplatno': ('Otdam besplatno', 'Drugoe', 143),
    'Drugoe|Obmen': ('Obmen', 'Drugoe', 144)}
