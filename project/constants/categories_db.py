#-*- coding: utf-8 -*- 
CATEGORIES = {
    'bytovaya-tehnika': [6, 5, 3, 4, 1, 2, 145],
    'detskij-mir': [34, 35, 36, 37, 38, 39, 40, 41, 42, 44, 31, 32, 33, 43, 149],
    'dom-i-sad': [76, 77, 78, 81, 82, 85, 86, 87, 79, 80, 83, 84, 153],
    'drugoe': [141, 142, 143, 144, 158],
    'elektronika': [16, 18, 7, 8, 9, 10, 11, 12, 14, 15, 17, 13, 146],
    'hobbi-i-dosug': [55, 56, 58, 61, 57, 59, 60, 151],
    'krasota-i-aksessuary': [23, 24, 25, 28, 29, 30, 27, 26, 148],
    'obshchenie': [135, 136, 137, 138, 139, 140, 157],
    'odezhda-obuv': [19, 20, 21, 22, 147],
    'remont-i-stroitelstvo': [88,
                              89,
                              90,
                              91,
                              92,
                              93,
                              94,
                              95,
                              96,
                              97,
                              98,
                              99,
                              100,
                              154],
    'sport-i-zdorove': [52, 53, 54, 45, 46, 47, 48, 50, 51, 49, 150],
    'transport': [102, 104, 106, 108, 109, 110, 101, 105, 103, 107, 155],
    'uslugi': [111,
               113,
               114,
               115,
               116,
               117,
               118,
               120,
               121,
               122,
               123,
               112,
               119,
               124,
               125,
               126,
               127,
               128,
               129,
               130,
               131,
               132,
               133,
               134,
               156],
    'zhivotnye-i-dlya-zhivotnyh': [62,
                                   63,
                                   64,
                                   65,
                                   66,
                                   67,
                                   68,
                                   69,
                                   70,
                                   71,
                                   72,
                                   73,
                                   74,
                                   75,
                                   152]}

SUBCATEGORIES = {
    'bytovaya-tehnika|': 145,
    'bytovaya-tehnika|klimaticheskaya-tehnika': 1,
    'bytovaya-tehnika|prochee-drugaya-tehnika': 6,
    'bytovaya-tehnika|tehnika-dlya-doma': 4,
    'bytovaya-tehnika|tehnika-dlya-kuhni': 3,
    'bytovaya-tehnika|tehnika-dlya-uhoda-za-soboj': 5,
    'bytovaya-tehnika|televizory-videotehnika': 2,
    'detskij-mir|': 149,
    'detskij-mir|aksessuary-dlya-detej': 33,
    'detskij-mir|detskaya-literatura': 35,
    'detskij-mir|detskaya-mebel': 41,
    'detskij-mir|detskaya-obuv': 32,
    'detskij-mir|detskaya-odezhda': 31,
    'detskij-mir|detskie-igrushki-i-igry': 34,
    'detskij-mir|detskie-kolyaski-i-avtokresla': 42,
    'detskij-mir|detskij-transport': 40,
    'detskij-mir|detskoe-pitanie': 39,
    'detskij-mir|dlya-mam': 38,
    'detskij-mir|dlya-samyh-malenkih': 36,
    'detskij-mir|dlya-shkolnikov': 37,
    'detskij-mir|prochee': 44,
    'detskij-mir|zdorove-i-gigiena': 43,
    'dom-i-sad|': 153,
    'dom-i-sad|bytovaya-himiya': 82,
    'dom-i-sad|instrumenty-inventar': 80,
    'dom-i-sad|kanctovary': 86,
    'dom-i-sad|mebel': 79,
    'dom-i-sad|predmety-interera-osveshchenie': 78,
    'dom-i-sad|prochee': 87,
    'dom-i-sad|produkty-pitaniya': 85,
    'dom-i-sad|rasteniya': 83,
    'dom-i-sad|sad-i-ogorod': 84,
    'dom-i-sad|tekstil': 81,
    'dom-i-sad|vse-dlya-kuhni': 76,
    'dom-i-sad|vse-dlya-vannoj-komnaty': 77,
    'drugoe|': 158,
    'drugoe|tovary dlya vzroslyh': 141,
    'drugoe|podarki i suveniry': 142,
    'drugoe|otdam besplatno': 143,
    'drugoe|obmen': 144,
    'elektronika|': 146,
    'elektronika|aksessuary-dlya-telefoniv': 15,
    'elektronika|audiotehnika': 7,
    'elektronika|drugie-telefony-faksy': 18,
    'elektronika|elektronnye-knigi': 13,
    'elektronika|foto-videotehnika': 8,
    'elektronika|igry-pristavki-po': 9,
    'elektronika|komplektuyushchie-i-aksessuary': 10,
    'elektronika|noutbuki': 11,
    'elektronika|pk-i-orgtehnika': 12,
    'elektronika|sim-karty-tarify-nomera': 17,
    'elektronika|stacionarnye-telefony': 16,
    'elektronika|telefony-i-planshety': 14,
    'hobbi-i-dosug|': 151,
    'hobbi-i-dosug|cd-dvd-plastinki': 56,
    'hobbi-i-dosug|knigi-pressa': 55,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat': 60,
    'hobbi-i-dosug|muzykalnye-instrumenty': 57,
    'hobbi-i-dosug|ohota': 58,
    'hobbi-i-dosug|prochee': 61,
    'hobbi-i-dosug|rybolovstvo': 59,
    'krasota-i-aksessuary|': 148,
    'krasota-i-aksessuary|bizhuteriya-i-ukrasheniya': 24,
    'krasota-i-aksessuary|chasy': 25,
    'krasota-i-aksessuary|kosmetika-i-uhod': 27,
    'krasota-i-aksessuary|ochki': 29,
    'krasota-i-aksessuary|parfyumeriya': 28,
    'krasota-i-aksessuary|prochee': 30,
    'krasota-i-aksessuary|sumki-ryukzaki-koshelki': 23,
    'krasota-i-aksessuary|yuvelirnye-izdeliya': 26,
    'obshchenie|': 157,
    'obshchenie|poteri': 135,
    'obshchenie|nahodki': 136,
    'obshchenie|bilety na meropriyatiya': 137,
    'obshchenie|poisk poputchikov ': 138,
    'obshchenie|poisk grupp / muzykantov': 139,
    'obshchenie|poisk lyudej': 140,
    'odezhda-obuv|': 147,
    'odezhda-obuv|muzhskaya-obuv': 22,
    'odezhda-obuv|muzhskaya-odezhda': 20,
    'odezhda-obuv|zhenskaya-obuv': 21,
    'odezhda-obuv|zhenskaya-odezhda': 19,
    'remont-i-stroitelstvo|': 154,
    'remont-i-stroitelstvo|drugie-strojmaterialy': 89,
    'remont-i-stroitelstvo|elektrichestvo': 92,
    'remont-i-stroitelstvo|elementy-krepleniya': 97,
    'remont-i-stroitelstvo|kirpich-beton-penobloki': 95,
    'remont-i-stroitelstvo|lakokrasochnye-materialy': 98,
    'remont-i-stroitelstvo|metalloprokat-armatura': 94,
    'remont-i-stroitelstvo|okna-dveri-stekl-zerkala': 90,
    'remont-i-stroitelstvo|otdelochnye-i-oblicovochnye-materialy': 91,
    'remont-i-stroitelstvo|otoplenie': 88,
    'remont-i-stroitelstvo|pilomaterialy': 96,
    'remont-i-stroitelstvo|santehnika': 93,
    'remont-i-stroitelstvo|uslugi': 100,
    'remont-i-stroitelstvo|ventilyaciya': 99,
    'sport-i-zdorove|': 150,
    'sport-i-zdorove|drugoe': 52,
    'sport-i-zdorove|pribory-i-ustrojstva': 47,
    'sport-i-zdorove|specodezhda-obuv': 51,
    'sport-i-zdorove|sportinventar': 45,
    'sport-i-zdorove|sportivnoe-pitanie': 53,
    'sport-i-zdorove|tovary-dlya-invalidov': 54,
    'sport-i-zdorove|trenazhery': 48,
    'sport-i-zdorove|velosipedy-i-aksessuary-k-nim': 50,
    'sport-i-zdorove|vsydlya-otdyha-turizm': 46,
    'sport-i-zdorove|zdorove': 49,
    'transport|': 155,
    'transport|avtobusy': 109,
    'transport|drugoj-transport': 110,
    'transport|gruzovye-avtomobili': 104,
    'transport|legkovye-avtomobili': 103,
    'transport|moto': 105,
    'transport|pricepy': 108,
    'transport|selhoztehnika': 102,
    'transport|spectehnika': 107,
    'transport|vodnyi-transport': 106,
    'transport|zapchasti-aksessuary': 101,
    'uslugi|': 156,
    'uslugi|bezopasnost-ohrana': 123,
    'uslugi|dostavka': 113,
    'uslugi|finansy-kredit-strahovanie': 117,
    'uslugi|informacionnye-tehnologii-internet': 118,
    'uslugi|nyani-sidelki': 129,
    'uslugi|obrazovanie': 130,
    'uslugi|organizaciya-obsluzhivanie-meropriyatii': 120,
    'uslugi|poligrafiya-izdatelstvo': 125,
    'uslugi|pomoshch-ekstrasensov': 131,
    'uslugi|prochee': 134,
    'uslugi|prokat': 128,
    'uslugi|razvlecheniya-foto-video': 127,
    'uslugi|reklama-marketing': 122,
    'uslugi|remontnye-uslugi': 111,
    'uslugi|selskoe-hozyajstvo': 133,
    'uslugi|sport': 132,
    'uslugi|torgovlya': 124,
    'uslugi|transportnye-uslugi-avtarenda': 114,
    'uslugi|turizm-immigraciya': 121,
    'uslugi|uborka': 112,
    'uslugi|uslugi-perevoda': 115,
    'uslugi|uslugi-puhodu-za-zhivotnymi': 126,
    'uslugi|yuridicheskie-notarialnye-uslugi': 116,
    'uslugi|zdorove-krasota': 119,
    'zhivotnye-i-dlya-zhivotnyh|': 152,
    'zhivotnye-i-dlya-zhivotnyh|akvariumy': 64,
    'zhivotnye-i-dlya-zhivotnyh|byurnahodok': 75,
    'zhivotnye-i-dlya-zhivotnyh|drugie-domashnie-pitomcy': 67,
    'zhivotnye-i-dlya-zhivotnyh|drugoe': 74,
    'zhivotnye-i-dlya-zhivotnyh|koshki': 62,
    'zhivotnye-i-dlya-zhivotnyh|otdam-besplatno': 73,
    'zhivotnye-i-dlya-zhivotnyh|pernatye': 65,
    'zhivotnye-i-dlya-zhivotnyh|pitanie-i-aksessuary': 69,
    'zhivotnye-i-dlya-zhivotnyh|reptilii': 66,
    'zhivotnye-i-dlya-zhivotnyh|selhoz-zhivotnye': 68,
    'zhivotnye-i-dlya-zhivotnyh|sobaki': 63,
    'zhivotnye-i-dlya-zhivotnyh|uhod-za-zhivotnymi': 71,
    'zhivotnye-i-dlya-zhivotnyh|veterinarnye-zootovary': 70,
    'zhivotnye-i-dlya-zhivotnyh|vyazka': 72}

SUBSUBCATEGORIES = {
    'bytovaya-tehnika|klimaticheskaya-tehnika|aksessuary-i-komplektuyushchie': 6,
    'bytovaya-tehnika|klimaticheskaya-tehnika|drugaya-tehnika': 5,
    'bytovaya-tehnika|klimaticheskaya-tehnika|kondicionery': 1,
    'bytovaya-tehnika|klimaticheskaya-tehnika|otoplenie': 3,
    'bytovaya-tehnika|klimaticheskaya-tehnika|ventilyatory': 2,
    'bytovaya-tehnika|klimaticheskaya-tehnika|vozduhoochistiteli-uvlazhniteli': 4,
    'bytovaya-tehnika|tehnika-dlya-doma|aksessuary': 31,
    'bytovaya-tehnika|tehnika-dlya-doma|drugaya-tehnika-dlya-doma': 29,
    'bytovaya-tehnika|tehnika-dlya-doma|filtry-dlya-vody': 28,
    'bytovaya-tehnika|tehnika-dlya-doma|pylesosy': 23,
    'bytovaya-tehnika|tehnika-dlya-doma|shvejnye-mashiny-i-overloki': 26,
    'bytovaya-tehnika|tehnika-dlya-doma|stiralnye-mashiny': 25,
    'bytovaya-tehnika|tehnika-dlya-doma|utyugi': 24,
    'bytovaya-tehnika|tehnika-dlya-doma|vodonagrevateli': 30,
    'bytovaya-tehnika|tehnika-dlya-doma|vyazalnye-mashiny': 27,
    'bytovaya-tehnika|tehnika-dlya-kuhni|drugaya-tehnika-dlya-kuhni': 22,
    'bytovaya-tehnika|tehnika-dlya-kuhni|elektrochajniki': 15,
    'bytovaya-tehnika|tehnika-dlya-kuhni|hlebopechki': 19,
    'bytovaya-tehnika|tehnika-dlya-kuhni|holodilniki': 13,
    'bytovaya-tehnika|tehnika-dlya-kuhni|kofevarki-kofemolki': 16,
    'bytovaya-tehnika|tehnika-dlya-kuhni|kuhonnye-kombajny-i-izmelchiteli': 17,
    'bytovaya-tehnika|tehnika-dlya-kuhni|mikrovolnovye-pechi': 12,
    'bytovaya-tehnika|tehnika-dlya-kuhni|parovarki-multivarki': 18,
    'bytovaya-tehnika|tehnika-dlya-kuhni|plity-pechi': 14,
    'bytovaya-tehnika|tehnika-dlya-kuhni|posudomoechnye-mashiny': 20,
    'bytovaya-tehnika|tehnika-dlya-kuhni|vytyazhki': 21,
    'bytovaya-tehnika|tehnika-dlya-uhoda-za-soboj|britvy-epilyatory-mashinki-dlya-strizhki': 32,
    'bytovaya-tehnika|tehnika-dlya-uhoda-za-soboj|drugaya-tehnika-dlya-individualnoguhoda': 35,
    'bytovaya-tehnika|tehnika-dlya-uhoda-za-soboj|feny-ukladka-volos': 33,
    'bytovaya-tehnika|tehnika-dlya-uhoda-za-soboj|vesy': 34,
    'bytovaya-tehnika|televizory-videotehnika|aksessuary-dlya-tv-videotehniki': 9,
    'bytovaya-tehnika|televizory-videotehnika|drugaya-tv-videotehnika': 11,
    'bytovaya-tehnika|televizory-videotehnika|proigryvateli': 7,
    'bytovaya-tehnika|televizory-videotehnika|sputnikovoe-tv': 10,
    'bytovaya-tehnika|televizory-videotehnika|televizory': 8,
    'detskij-mir|aksessuary-dlya-detej|drugoe': 210,
    'detskij-mir|aksessuary-dlya-detej|galstuki-babochki': 209,
    'detskij-mir|aksessuary-dlya-detej|kosmetika-i-ukrasheniya-dlya-devochek': 212,
    'detskij-mir|aksessuary-dlya-detej|ochki-i-aksessuary': 213,
    'detskij-mir|aksessuary-dlya-detej|remni-poyasa-podtyazhki': 215,
    'detskij-mir|aksessuary-dlya-detej|rezinki-zakolki-obruchi': 214,
    'detskij-mir|aksessuary-dlya-detej|ryukzaki-sumki': 216,
    'detskij-mir|aksessuary-dlya-detej|zonty-dozhdeviki': 211,
    'detskij-mir|detskaya-obuv|drugoe': 203,
    'detskij-mir|detskaya-obuv|krossovki-kedy-mokasiny-butsy': 204,
    'detskij-mir|detskaya-obuv|sandalii-bosonozhki-shlepancy': 205,
    'detskij-mir|detskaya-obuv|sapogi-botinki': 206,
    'detskij-mir|detskaya-obuv|tapochki-cheshki': 207,
    'detskij-mir|detskaya-obuv|tufli': 208,
    'detskij-mir|detskaya-odezhda|dlya-mladencev': 186,
    'detskij-mir|detskaya-odezhda|drugoe': 187,
    'detskij-mir|detskaya-odezhda|futbolki-majki': 198,
    'detskij-mir|detskaya-odezhda|golovnye-ubory-sharfy': 185,
    'detskij-mir|detskaya-odezhda|karnavalnye-kostyumy': 188,
    'detskij-mir|detskaya-odezhda|kofty-reglany': 190,
    'detskij-mir|detskaya-odezhda|kolgotki-noski-golfy': 189,
    'detskij-mir|detskaya-odezhda|kupalniki-plavki': 191,
    'detskij-mir|detskaya-odezhda|nizhnee-bele': 192,
    'detskij-mir|detskaya-odezhda|perchatki-varezhki': 193,
    'detskij-mir|detskaya-odezhda|pidzhaki-rubashki': 194,
    'detskij-mir|detskaya-odezhda|pizhamy-halaty': 195,
    'detskij-mir|detskaya-odezhda|platya-sarafany': 196,
    'detskij-mir|detskaya-odezhda|shkolnaya-forma': 199,
    'detskij-mir|detskaya-odezhda|shorty-bridzhi-kapri': 200,
    'detskij-mir|detskaya-odezhda|shtany': 201,
    'detskij-mir|detskaya-odezhda|sportivnye-kostyumy': 197,
    'detskij-mir|detskaya-odezhda|verhnyaya-odezhda': 184,
    'detskij-mir|detskaya-odezhda|yubki': 202,
    'detskij-mir|zdorove-i-gigiena|drugoe': 221,
    'detskij-mir|zdorove-i-gigiena|gorshki-nakladki-na-unitaz': 219,
    'detskij-mir|zdorove-i-gigiena|gradusniki-termometry-vesy': 220,
    'detskij-mir|zdorove-i-gigiena|krema-masla-prisypka': 222,
    'detskij-mir|zdorove-i-gigiena|podguzniki-salfetki': 223,
    'detskij-mir|zdorove-i-gigiena|vse-dlya-kormleniya': 217,
    'detskij-mir|zdorove-i-gigiena|vse-dlya-kupaniya': 218,
    'dom-i-sad|instrumenty-inventar|benzoinstrument': 327,
    'dom-i-sad|instrumenty-inventar|drugoj-instrument': 326,
    'dom-i-sad|instrumenty-inventar|elektroinstrument': 324,
    'dom-i-sad|instrumenty-inventar|komplektuyushchie-rashodnye-materialy': 328,
    'dom-i-sad|instrumenty-inventar|ruchnoj-instrument': 325,
    'dom-i-sad|mebel|mebel': 321,
    'dom-i-sad|mebel|mebel-dlya-gostinoj': 317,
    'dom-i-sad|mebel|mebel-dlya-prihozhej': 322,
    'dom-i-sad|mebel|mebel-dlya-spalni': 319,
    'dom-i-sad|mebel|mebel-dlya-vannoj-komnaty': 323,
    'dom-i-sad|mebel|mebel-na-zakaz': 318,
    'dom-i-sad|mebel|ofisnaya-mebel': 320,
    'dom-i-sad|rasteniya|gorshki-zemlya-udobreniya': 329,
    'dom-i-sad|rasteniya|rasteniya': 330,
    'dom-i-sad|sad-i-ogorod|drugoe': 332,
    'dom-i-sad|sad-i-ogorod|sadovaya-mebel-shatry-besedki': 333,
    'dom-i-sad|sad-i-ogorod|sadovaya-tehnika': 334,
    'dom-i-sad|sad-i-ogorod|sadovye-svetilniki': 335,
    'dom-i-sad|sad-i-ogorod|sadovyi-instrument': 336,
    'dom-i-sad|sad-i-ogorod|sredstva-ot-nasekomyh': 337,
    'dom-i-sad|sad-i-ogorod|udobreniya-i-himikaty': 338,
    'dom-i-sad|sad-i-ogorod|vse-dlya-poliva': 331,
    'drugoe|none|obmen': 388,
    'drugoe|none|otdam-besplatno': 387,
    'drugoe|none|podarki-i-suveniry': 386,
    'drugoe|none|tovary-dlya-vzroslyh': 385,
    'elektronika|aksessuary-dlya-telefoniv|akkumulyatory': 91,
    'elektronika|aksessuary-dlya-telefoniv|antenny-usiliteli': 92,
    'elektronika|aksessuary-dlya-telefoniv|chehly-i-futlyary': 105,
    'elektronika|aksessuary-dlya-telefoniv|dinamiki-mikrofony': 93,
    'elektronika|aksessuary-dlya-telefoniv|displei-sensornye-ekrany': 94,
    'elektronika|aksessuary-dlya-telefoniv|drugoe': 95,
    'elektronika|aksessuary-dlya-telefoniv|kabeli': 97,
    'elektronika|aksessuary-dlya-telefoniv|karty-pamyati': 98,
    'elektronika|aksessuary-dlya-telefoniv|klaviatury': 99,
    'elektronika|aksessuary-dlya-telefoniv|korpusa': 100,
    'elektronika|aksessuary-dlya-telefoniv|mikroshemy': 101,
    'elektronika|aksessuary-dlya-telefoniv|plenki-dlya-ekranov': 102,
    'elektronika|aksessuary-dlya-telefoniv|provodnye-garnitury-naushniki': 103,
    'elektronika|aksessuary-dlya-telefoniv|razemy-gnezda': 104,
    'elektronika|aksessuary-dlya-telefoniv|shlejfy-perehodniki': 106,
    'elektronika|aksessuary-dlya-telefoniv|zaryadnye-ustrojstva': 96,
    'elektronika|audiotehnika|akusticheskie-sistemy': 39,
    'elektronika|audiotehnika|cd-md-vinilovye-proigryvateli': 44,
    'elektronika|audiotehnika|magnitoly': 37,
    'elektronika|audiotehnika|mp3-pleery': 36,
    'elektronika|audiotehnika|muzykalnye-centry': 38,
    'elektronika|audiotehnika|naushniki': 40,
    'elektronika|audiotehnika|portativnaya-akustika': 42,
    'elektronika|audiotehnika|prochaya-audiotehnika': 45,
    'elektronika|audiotehnika|radiopriemniki': 41,
    'elektronika|audiotehnika|usiliteli-resivery': 43,
    'elektronika|elektronnye-knigi|aksessuary': 87,
    'elektronika|elektronnye-knigi|kpk': 86,
    'elektronika|foto-videotehnika|aksessuary-oborudovanie': 53,
    'elektronika|foto-videotehnika|cifrovye-fotoapparaty': 47,
    'elektronika|foto-videotehnika|drugoe': 56,
    'elektronika|foto-videotehnika|fotoramki-fotoalbomy': 52,
    'elektronika|foto-videotehnika|fotovspyshki': 51,
    'elektronika|foto-videotehnika|obektivy': 49,
    'elektronika|foto-videotehnika|plenochnye-fotoapparaty': 46,
    'elektronika|foto-videotehnika|shtativy-monopody': 50,
    'elektronika|foto-videotehnika|teleskopy-binokli': 54,
    'elektronika|foto-videotehnika|videokamery': 48,
    'elektronika|foto-videotehnika|zapchasti-i-komplektuyushchie': 55,
    'elektronika|igry-pristavki-po|aksessuary': 59,
    'elektronika|igry-pristavki-po|drugoe': 61,
    'elektronika|igry-pristavki-po|igry-dlya-pk': 62,
    'elektronika|igry-pristavki-po|igry-dlya-pristavok': 57,
    'elektronika|igry-pristavki-po|ofisnye-prilozheniya': 64,
    'elektronika|igry-pristavki-po|operacionnye-sistemy': 63,
    'elektronika|igry-pristavki-po|remont-pristavok': 60,
    'elektronika|igry-pristavki-po|tehnika': 58,
    'elektronika|igry-pristavki-po|utility-antivirusy': 65,
    'elektronika|komplektuyushchie-i-aksessuary|bloki-pitaniya': 76,
    'elektronika|komplektuyushchie-i-aksessuary|komplektuyushchie': 66,
    'elektronika|komplektuyushchie-i-aksessuary|korpusa': 73,
    'elektronika|komplektuyushchie-i-aksessuary|materinskie-platy': 70,
    'elektronika|komplektuyushchie-i-aksessuary|moduli-pamyati': 67,
    'elektronika|komplektuyushchie-i-aksessuary|operativnaya-pamyat': 78,
    'elektronika|komplektuyushchie-i-aksessuary|opticheskie-privody': 71,
    'elektronika|komplektuyushchie-i-aksessuary|processory': 74,
    'elektronika|komplektuyushchie-i-aksessuary|sistemy-ohlazhdeniya': 77,
    'elektronika|komplektuyushchie-i-aksessuary|tv-tyunery': 75,
    'elektronika|komplektuyushchie-i-aksessuary|videokarty': 68,
    'elektronika|komplektuyushchie-i-aksessuary|zhestkie-diski': 72,
    'elektronika|komplektuyushchie-i-aksessuary|zvukovye-karty': 69,
    'elektronika|noutbuki|vse-noutbuki': 79,
    'elektronika|pk-i-orgtehnika|fleshki-zhestkie-diski': 83,
    'elektronika|pk-i-orgtehnika|monitory': 85,
    'elektronika|pk-i-orgtehnika|myshki-klaviatura': 82,
    'elektronika|pk-i-orgtehnika|pk': 80,
    'elektronika|pk-i-orgtehnika|printery-skanery-faksy': 81,
    'elektronika|pk-i-orgtehnika|setevoe-oborudovanie': 84,
    'elektronika|sim-karty-tarify-nomera|beeline': 107,
    'elektronika|sim-karty-tarify-nomera|drugoe': 111,
    'elektronika|sim-karty-tarify-nomera|kievstar-djuice': 112,
    'elektronika|sim-karty-tarify-nomera|life': 108,
    'elektronika|sim-karty-tarify-nomera|mts': 113,
    'elektronika|sim-karty-tarify-nomera|peoplenet': 109,
    'elektronika|sim-karty-tarify-nomera|utel': 110,
    'elektronika|telefony-i-planshety|mobilnye-telefony': 90,
    'elektronika|telefony-i-planshety|planshety': 88,
    'elektronika|telefony-i-planshety|remont-i-proshivka': 89,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|bonistika': 304,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|drugie-kategorii': 303,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|filateliya': 306,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|ikony-skladni-metalloplastika': 312,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|kartiny-antikvarnaya-mebel-predmety-interera': 308,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|knigi': 313,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|kollekcionnoe-oruzhie': 316,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|masshtabnye-modeli': 309,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|medali-znachki-zhetony': 311,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|militariya': 310,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|monety-ukrainy-iz-drag.splavov': 315,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|numizmatika': 302,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|plastinki': 314,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|skulptury': 307,
    'hobbi-i-dosug|kollekcionirovanie-antikvariat|stolovoe-serebro-antikvarnaya-kuhonnaya-utvar': 305,
    'hobbi-i-dosug|muzykalnye-instrumenty|aksessuary-dlya-muzykalnyh-instrumentov': 286,
    'hobbi-i-dosug|muzykalnye-instrumenty|akusticheskie-gitary': 289,
    'hobbi-i-dosug|muzykalnye-instrumenty|bas-gitary': 294,
    'hobbi-i-dosug|muzykalnye-instrumenty|drugoe': 285,
    'hobbi-i-dosug|muzykalnye-instrumenty|duhovye': 291,
    'hobbi-i-dosug|muzykalnye-instrumenty|elektrogitary': 288,
    'hobbi-i-dosug|muzykalnye-instrumenty|kombousiliteli': 293,
    'hobbi-i-dosug|muzykalnye-instrumenty|pianin-fortepian-royali': 287,
    'hobbi-i-dosug|muzykalnye-instrumenty|studiinoe-oborudovanie': 284,
    'hobbi-i-dosug|muzykalnye-instrumenty|udarnye-instrumenty': 292,
    'hobbi-i-dosug|muzykalnye-instrumenty|vyhodnoj': 290,
    'hobbi-i-dosug|rybolovstvo|drugoe': 295,
    'hobbi-i-dosug|rybolovstvo|eholoty-radary': 301,
    'hobbi-i-dosug|rybolovstvo|katushki': 296,
    'hobbi-i-dosug|rybolovstvo|lodki-katamarany': 297,
    'hobbi-i-dosug|rybolovstvo|podvodnaya-ohota': 298,
    'hobbi-i-dosug|rybolovstvo|rybackii-inventar': 299,
    'hobbi-i-dosug|rybolovstvo|udilishcha': 300,
    'krasota-i-aksessuary|kosmetika-i-uhod|dekorativnaya-kosmetika': 174,
    'krasota-i-aksessuary|kosmetika-i-uhod|drugoe': 175,
    'krasota-i-aksessuary|kosmetika-i-uhod|kosmetika-solncezashchitnaya-dlya-solyariya': 176,
    'krasota-i-aksessuary|kosmetika-i-uhod|parfyumeriya': 177,
    'krasota-i-aksessuary|kosmetika-i-uhod|pribory-i-aksessuary': 182,
    'krasota-i-aksessuary|kosmetika-i-uhod|sredstva-dlya-britya-depilyacii': 178,
    'krasota-i-aksessuary|kosmetika-i-uhod|sredstva-dlya-volos': 183,
    'krasota-i-aksessuary|kosmetika-i-uhod|uhod-za-licom-i-telom': 180,
    'krasota-i-aksessuary|kosmetika-i-uhod|uhod-za-polostyu-rta': 181,
    'krasota-i-aksessuary|kosmetika-i-uhod|uslugi': 179,
    'krasota-i-aksessuary|kosmetika-i-uhod|vsydlya-nogtej': 173,
    'krasota-i-aksessuary|kosmetika-i-uhod|vsydlya-volos': 172,
    'krasota-i-aksessuary|yuvelirnye-izdeliya|braslety': 167,
    'krasota-i-aksessuary|yuvelirnye-izdeliya|broshi': 169,
    'krasota-i-aksessuary|yuvelirnye-izdeliya|cepochki': 170,
    'krasota-i-aksessuary|yuvelirnye-izdeliya|drugie-yuvelirnye-izdeliya': 171,
    'krasota-i-aksessuary|yuvelirnye-izdeliya|kolca': 165,
    'krasota-i-aksessuary|yuvelirnye-izdeliya|kulony-podveski': 168,
    'krasota-i-aksessuary|yuvelirnye-izdeliya|sergi': 166,
    'obshchenie|none|bilety-na-meropriyatiya': 381,
    'obshchenie|none|nahodki': 380,
    'obshchenie|none|poisk-grupp-muzykantov': 383,
    'obshchenie|none|poisk-lyudej': 384,
    'obshchenie|none|poisk-poputchikov': 382,
    'obshchenie|none|poteri': 379,
    'odezhda-obuv|muzhskaya-obuv|drugaya-obuv': 163,
    'odezhda-obuv|muzhskaya-obuv|golovnye-ubory': 164,
    'odezhda-obuv|muzhskaya-obuv|sandalii-shlepancy': 161,
    'odezhda-obuv|muzhskaya-obuv|sapogi-botinki': 159,
    'odezhda-obuv|muzhskaya-obuv|sportivnaya-obuv': 162,
    'odezhda-obuv|muzhskaya-obuv|tufli-mokasiny': 160,
    'odezhda-obuv|muzhskaya-odezhda|bryuki': 145,
    'odezhda-obuv|muzhskaya-odezhda|drugoe': 148,
    'odezhda-obuv|muzhskaya-odezhda|futbolka': 146,
    'odezhda-obuv|muzhskaya-odezhda|futbolki-majki': 139,
    'odezhda-obuv|muzhskaya-odezhda|kofty': 141,
    'odezhda-obuv|muzhskaya-odezhda|kostyumy-pidzhaki-zhilety': 144,
    'odezhda-obuv|muzhskaya-odezhda|nizhnee-bele-plavki': 138,
    'odezhda-obuv|muzhskaya-odezhda|rubashki': 140,
    'odezhda-obuv|muzhskaya-odezhda|sportivnye-kostyumy': 149,
    'odezhda-obuv|muzhskaya-odezhda|svitera': 142,
    'odezhda-obuv|muzhskaya-odezhda|tolstovki': 143,
    'odezhda-obuv|muzhskaya-odezhda|verhnyaya-odezhda': 147,
    'odezhda-obuv|zhenskaya-obuv|baletki': 150,
    'odezhda-obuv|zhenskaya-obuv|bosonozhki': 153,
    'odezhda-obuv|zhenskaya-obuv|botinki': 155,
    'odezhda-obuv|zhenskaya-obuv|drugoe': 157,
    'odezhda-obuv|zhenskaya-obuv|mokasiny': 151,
    'odezhda-obuv|zhenskaya-obuv|sapogi': 156,
    'odezhda-obuv|zhenskaya-obuv|shlepancy': 154,
    'odezhda-obuv|zhenskaya-obuv|sportivnaya-obuv': 158,
    'odezhda-obuv|zhenskaya-obuv|tufli': 152,
    'odezhda-obuv|zhenskaya-odezhda|bluzki-rubashki': 115,
    'odezhda-obuv|zhenskaya-odezhda|bryuki': 116,
    'odezhda-obuv|zhenskaya-odezhda|dlya-svadby': 137,
    'odezhda-obuv|zhenskaya-odezhda|drugoe': 135,
    'odezhda-obuv|zhenskaya-odezhda|futbolka': 133,
    'odezhda-obuv|zhenskaya-odezhda|futbolki': 131,
    'odezhda-obuv|zhenskaya-odezhda|hudi-tolstovki': 132,
    'odezhda-obuv|zhenskaya-odezhda|kofty': 120,
    'odezhda-obuv|zhenskaya-odezhda|kostyumy': 119,
    'odezhda-obuv|zhenskaya-odezhda|kupalniki-pareo': 121,
    'odezhda-obuv|zhenskaya-odezhda|losiny-legginsy': 122,
    'odezhda-obuv|zhenskaya-odezhda|majki': 123,
    'odezhda-obuv|zhenskaya-odezhda|nizhnee-belyo': 124,
    'odezhda-obuv|zhenskaya-odezhda|odezhda-dlya-beremennyh': 114,
    'odezhda-obuv|zhenskaya-odezhda|pidzhaki': 125,
    'odezhda-obuv|zhenskaya-odezhda|platya': 126,
    'odezhda-obuv|zhenskaya-odezhda|sarafany': 127,
    'odezhda-obuv|zhenskaya-odezhda|sportivnye-kostyumy': 136,
    'odezhda-obuv|zhenskaya-odezhda|svitera': 128,
    'odezhda-obuv|zhenskaya-odezhda|topy': 129,
    'odezhda-obuv|zhenskaya-odezhda|tuniki': 130,
    'odezhda-obuv|zhenskaya-odezhda|verhnyaya-odezhda': 117,
    'odezhda-obuv|zhenskaya-odezhda|yubki': 134,
    'odezhda-obuv|zhenskaya-odezhda|zhilety': 118,
    'sport-i-zdorove|pribory-i-ustrojstva|drugoe': 254,
    'sport-i-zdorove|pribory-i-ustrojstva|filtry': 262,
    'sport-i-zdorove|pribory-i-ustrojstva|kardiopribory-pribory-dlya-pulsa': 255,
    'sport-i-zdorove|pribory-i-ustrojstva|massazhery': 256,
    'sport-i-zdorove|pribory-i-ustrojstva|poyasa-miostimulyatory': 257,
    'sport-i-zdorove|pribory-i-ustrojstva|pribory-magnitoterapii': 258,
    'sport-i-zdorove|pribory-i-ustrojstva|sluhovye-apparaty': 259,
    'sport-i-zdorove|pribory-i-ustrojstva|termometry-gradusniki': 260,
    'sport-i-zdorove|pribory-i-ustrojstva|tonometry': 261,
    'sport-i-zdorove|pribory-i-ustrojstva|vesy': 253,
    'sport-i-zdorove|specodezhda-obuv|drugoe': 281,
    'sport-i-zdorove|specodezhda-obuv|ohota-rybolovstvo': 282,
    'sport-i-zdorove|specodezhda-obuv|turizm': 283,
    'sport-i-zdorove|sportinventar|alpinizm-speleologiya': 224,
    'sport-i-zdorove|sportinventar|arbalety-luki-strely': 225,
    'sport-i-zdorove|sportinventar|bilyard': 226,
    'sport-i-zdorove|sportinventar|boevye-iskusstva': 227,
    'sport-i-zdorove|sportinventar|darts': 230,
    'sport-i-zdorove|sportinventar|drugoe': 231,
    'sport-i-zdorove|sportinventar|fitnes-joga-aerobika-gimnastika': 239,
    'sport-i-zdorove|sportinventar|ganteli-giri-shtangi': 229,
    'sport-i-zdorove|sportinventar|igry-s-myachom': 233,
    'sport-i-zdorove|sportinventar|nastolnye-igry': 234,
    'sport-i-zdorove|sportinventar|pejntbol-strajkbol': 235,
    'sport-i-zdorove|sportinventar|roliki-skejty': 236,
    'sport-i-zdorove|sportinventar|sportivnye-ugolki': 237,
    'sport-i-zdorove|sportinventar|tennis-badminton': 238,
    'sport-i-zdorove|sportinventar|vodnyi-sport': 228,
    'sport-i-zdorove|sportinventar|zimnii-sport': 232,
    'sport-i-zdorove|trenazhery|begovye-dorozhki': 263,
    'sport-i-zdorove|trenazhery|drugie': 265,
    'sport-i-zdorove|trenazhery|orbitreki': 266,
    'sport-i-zdorove|trenazhery|silovye-trenazhyory': 267,
    'sport-i-zdorove|trenazhery|skami-i-stojki': 268,
    'sport-i-zdorove|trenazhery|steppery': 269,
    'sport-i-zdorove|trenazhery|velotrenazhyory': 264,
    'sport-i-zdorove|velosipedy-i-aksessuary-k-nim|drugoe': 277,
    'sport-i-zdorove|velosipedy-i-aksessuary-k-nim|ekipirovka-dlya-velosporta': 280,
    'sport-i-zdorove|velosipedy-i-aksessuary-k-nim|komplektuyushchie': 278,
    'sport-i-zdorove|velosipedy-i-aksessuary-k-nim|smartfony': 276,
    'sport-i-zdorove|velosipedy-i-aksessuary-k-nim|snaryazhenie-aksessuary': 279,
    'sport-i-zdorove|vsydlya-otdyha-turizm|dorozhnye-sumki': 240,
    'sport-i-zdorove|vsydlya-otdyha-turizm|drugoe': 241,
    'sport-i-zdorove|vsydlya-otdyha-turizm|fonari-fonariki': 252,
    'sport-i-zdorove|vsydlya-otdyha-turizm|mangaly-gril-shampura': 242,
    'sport-i-zdorove|vsydlya-otdyha-turizm|metalloiskateli': 243,
    'sport-i-zdorove|vsydlya-otdyha-turizm|naduvnye-bassejny': 244,
    'sport-i-zdorove|vsydlya-otdyha-turizm|nozhi-multituly': 245,
    'sport-i-zdorove|vsydlya-otdyha-turizm|palatki': 246,
    'sport-i-zdorove|vsydlya-otdyha-turizm|pohodnyi-inventar': 248,
    'sport-i-zdorove|vsydlya-otdyha-turizm|posuda-flyagi-termosy': 247,
    'sport-i-zdorove|vsydlya-otdyha-turizm|putyovki-tury': 249,
    'sport-i-zdorove|vsydlya-otdyha-turizm|ryukzaki': 250,
    'sport-i-zdorove|vsydlya-otdyha-turizm|spalnye-meshki-gamaki': 251,
    'sport-i-zdorove|zdorove|aromaterapiya-svechi-lampy': 270,
    'sport-i-zdorove|zdorove|drugoe': 273,
    'sport-i-zdorove|zdorove|lekarstva': 271,
    'sport-i-zdorove|zdorove|netradicionnaya-medicina': 274,
    'sport-i-zdorove|zdorove|ochki-linzy-aksessuary': 275,
    'sport-i-zdorove|zdorove|sredstva-dlya-pohudeniya': 272,
    'transport|legkovye-avtomobili|vse-legkovye-avtomobili': 349,
    'transport|moto|kvadrocikly': 352,
    'transport|moto|mopedy-skutery': 350,
    'transport|moto|mot-drugoe': 353,
    'transport|moto|motocikly': 351,
    'transport|spectehnika|buldozery-traktory': 354,
    'transport|spectehnika|drugaya-spectehnika': 357,
    'transport|spectehnika|ekskavatory': 359,
    'transport|spectehnika|kommunalnaya-tehnika': 360,
    'transport|spectehnika|oborudovanie-dlya-spectehniki': 358,
    'transport|spectehnika|pogruzchiki': 355,
    'transport|spectehnika|samosvaly': 361,
    'transport|spectehnika|stroitelnaya-tehnika': 356,
    'transport|zapchasti-aksessuary|aksessuary-dlya-avto': 341,
    'transport|zapchasti-aksessuary|avtozapchasti': 339,
    'transport|zapchasti-aksessuary|avtozvuk': 344,
    'transport|zapchasti-aksessuary|drugie-zapchasti': 345,
    'transport|zapchasti-aksessuary|gps-navigatory-avtoregistratory': 348,
    'transport|zapchasti-aksessuary|motaksessuary': 346,
    'transport|zapchasti-aksessuary|motozapchasti': 343,
    'transport|zapchasti-aksessuary|shiny-diski': 340,
    'transport|zapchasti-aksessuary|transport-na-zapchasti': 347,
    'transport|zapchasti-aksessuary|zapchasti-dlya-spec-s.h.tehniki': 342,
    'uslugi|uborka|bytovoj-remont-uborka': 364,
    'uslugi|uborka|dizajn-arhitektura': 369,
    'uslugi|uborka|elektrichestvo': 368,
    'uslugi|uborka|gotovye-konstrukcii': 366,
    'uslugi|uborka|okna-dveri-balkony': 365,
    'uslugi|uborka|otdelka-remont': 362,
    'uslugi|uborka|santehnika-kommunikacii': 367,
    'uslugi|uborka|stroitelnye-uslugi': 363,
    'uslugi|uborka|ventilyaciya-kondicionirovanie': 370,
    'uslugi|zdorove-krasota|krasota-zdorove-': 373,
    'uslugi|zdorove-krasota|makiyazh-kosmetologiya-narashchivanie-resnic': 372,
    'uslugi|zdorove-krasota|manikyur-narashchivanie-nogtej': 371,
    'uslugi|zdorove-krasota|massazh': 375,
    'uslugi|zdorove-krasota|medicina': 377,
    'uslugi|zdorove-krasota|prochee': 374,
    'uslugi|zdorove-krasota|strizhki-narashchivanie-volos': 376,
    'uslugi|zdorove-krasota|uslugi-psihologa': 378}


def get_subcategory_id(category_value, subcategory_value):
    return SUBCATEGORIES.get('{0}|{1}'.format(category_value, subcategory_value))


def get_subsubcategory_id(category_value, subcategory_value, subsubcategory_value):
    return SUBSUBCATEGORIES.get('{0}|{1}|{2}'.format(category_value, subcategory_value, subsubcategory_value))
