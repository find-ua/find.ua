"""Transforms find.ua`s parameters to olx links"""
from constants.areas import AREAS
from constants.areas_dict import CITIES
from constants.constants_functions import split_category_and_subcategory


def _city_to_url(city):
    """Transform city or area name from find.ua to olx.u url.

    Args:
        :param city (str): city or area name from find.ua

    Returns:
        :returns: (str)

    Example:
        >>> _city_to_url('bar')
        'bar.vin.'
    """
    for area in AREAS.keys():
        if city in AREAS[area]:
            return "{}.{}.".format(area, city)

    # if not in AREAS then city looks like "rokitnoe_rov" or city is an area (e.g. "ko")
    if len(city.split('_')) == 2:
        city, area = city.split('_')
        return "{}.{}.".format()
    # if city is an area
    elif len(city.split('_')) == 1:
        return "{}.".format(city)


def _category_to_url(category):
    """Transform category name from find.ua to olx.u url.

    Args:
        :param category (str): category name from find.ua

    Returns:
        :returns: (str)

    Example:
        >>> _category_to_url('') # specify format of 'vn'
        'knigi-zhurnaly'
    """
    raw_category = split_category_and_subcategory(category)
    if type(raw_category) == tuple:
        category, subcategory = raw_category





def _filter_to_url(filter):
    pass


# TODO: add filters
def make_olx_url(params):
    """Makes olx.ua`s url from parameters.

    Args:
        :param params (dict): parameters for searching

    Returns:
        :returns : (string) olx.ua url

    Example:
        >>> print make_olx_url({'city': 'bar', 'category': 'elektronika'})
        'http://bar.vin.olx.ua/elektronika/'
    """
    allowed_keys = ['location', 'category', 'subcategory', 'filters']   # allowed keys for params

