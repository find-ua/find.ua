import pprint

from constants.other.filters import FILTERS
from constants.other.cat_com_rus import CATEGORIES


def join_categories_and_filters():
    i = j = k = 0
    key_found = False
    for key in FILTERS:
        # print('key: ', key)
        while i < (len(CATEGORIES)):
            # print('i = ', i)
            j = 0
            while j < len(CATEGORIES[i][1]):
            # for subcategory in CATEGORIES[i][1][0][0]:
            #     print(CATEGORIES[i][1][j][0])
                if key == CATEGORIES[i][1][j][0]:
                    CATEGORIES[i][1][j].append(FILTERS[key])
                    key_found = True
                    break
                if len(CATEGORIES[i][1][j]) > 1:
                    if type(CATEGORIES[i][1][j][1]) is list:
                        while k < len(CATEGORIES[i][1][j][1]):
                            # print(key)
                            # print(CATEGORIES[i][1][j])
                            # print(CATEGORIES[i][1][j][1])
                            # print(CATEGORIES[i][1][j][1][k])
                            # print(CATEGORIES[i][1][j][1][k][0])
                            # print(key)
                            if key == CATEGORIES[i][1][j][1][k][0]:
                                CATEGORIES[i][1][j][1][k].append(FILTERS[key])
                            k += 1
                            # print('j = ', j)
                        k = 0
                j += 1
                # print('j = ', j)
            j = 0
            i += 1
        if key_found:
            key_found = False
            i = 0
            continue
        i = 0
    return CATEGORIES

# print('_____________________________________________________')
# pp = pprint.PrettyPrinter(indent=3)
# pp.pprint(CATEGORIES)

