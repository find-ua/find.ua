#-*- coding: utf-8 -*- 
__author__ = 'acer'

from lxml import html
import requests
import re

LANG = {'ru': "",
        'ua': "uk/"}

def areas():
    XPATH_AREA = './/div[@class="content text"]/div/h2/span/text()'
    XPATH_AREA_CITIES = './/div[@class="content text"]/div[@class="clr marginbott30"]'
    XPATH_CITY = 'ul/li/a/span/span/text()'
    XPATH_CITY_URL = 'ul/li/a/@href'

    page = requests.get("http://olx.ua/uk/sitemap/regions/")
    tree = html.fromstring(page.text)
    areas = []
    boards = []

    for item in tree.xpath(XPATH_AREA_CITIES):
        boards.append(tuple(zip([city for city in item.xpath(XPATH_CITY)],
                                [url for url in item.xpath(XPATH_CITY_URL)])))

    for item in tree.xpath(XPATH_AREA):
        areas.append(item)

    res = list(zip(areas, boards))

    # cities for genum
    with open("genum.txt", 'w') as file:
        for area in res:
            for city in area[1]:
                city = re.search(r'http://(?P<city>[a-z-]+).\w+', city[1]).group('city')   # r'(?<=http://)\w+'
                file.write('{0} = "{1}"\n'.format(city.upper().replace('-', '_'), city))
    print(res)

    # ru cities and areas
    with open("ua.txt", 'w') as file:
        for area in res:
            file.write("('{0}', (\n\t(DB_ENUMS.AREA.TRANSLATE_AREA, 'Вся {1}'),\n".format(area[0], area[0]))
            for city in area[1]:
                file.write('\t(DB_ENUMS.CITY.{0}, "{1} "),\n'.format(
                    re.search(r'http://(?P<city>[a-z-]+).\w+', city[1]).group('city').upper().replace('-', '_'),
                              city[0].title()))
            file.write("\t)\n),\n")


def categories():
    XPATH_COLUMN = './/div[@class ="content"]/div/div'
    XPATH_CATEGORY = 'h3/a'    # '@href' - link   'span/text()' - title  ''
    XPATH_CATEGORY_SUB = 'ul'

    page = requests.get("http://olx.ua/uk/sitemap/")
    tree = html.fromstring(page.text)
    res = []
    categories = []
    subcat = []
    with open("categories_ua.txt", 'w') as file:
        for col in tree.xpath(XPATH_COLUMN):    # choose column
            for cat in col.xpath(XPATH_CATEGORY):   # choose big category such as "Мода и стиль"
                categories.append((cat.xpath('span/text()')[0], cat.xpath('@href')[0]))
                # file.write("{0} {1}\n".format(cat.xpath('a/@href')[0], cat.xpath('a/span/text()')[0]))
            for subs in col.xpath(XPATH_CATEGORY_SUB):
                cat_sub = []
                for sub in subs.xpath('li'):
                    lin_tit = (sub.xpath('a/span/text()')[0], sub.xpath('a/@href')[0])
                    tmp_ = []
                    for subsub in sub.xpath('ul'):
                        tmp = [(t.xpath('span/text()')[0], t.xpath('@href')[0]) for t in subsub.xpath('li/a')]
                        tmp_.append(tmp)
                    cat_sub.append((lin_tit, tmp_))
                subcat.append(cat_sub)
        for i in zip(categories, subcat):
            res.append(i)

        file.write("class CATEGORY:\n")
        for cat in res:
            tmp_c = re.search(r'http://olx.ua/uk/(?P<c>[a-z-]+)/', cat[0][1]).group('c')
            file.write('\t{0} = "{1}"\n'.format(tmp_c.replace('-', '_').upper(), tmp_c))

        file.write("\n\nclass SUBCATEGORY:\n")
        for sub in res:
            for s in sub[1]:
                c = re.search(r'http://olx.ua/uk/[a-z0-9-]+/(?P<c>[a-z0-9-]+)/', s[0][1]).group('c')
                file.write('\n\t{0} = "{1}"\n'.format(c.replace('-', '_').upper(), c))
                for k in s[1]:
                    for p in k:
                        c = re.search(r'http://olx.ua/uk/[a-z0-9-]+/[a-z0-9-]+/(?P<c>[a-z0-9-]+)/', p[1]).group('c')
                            file.write('\t{0} = "{1}"\n'.format(c.replace('-', '_').upper(), c))
                file.write("\n")

        file.write("\n\nclass choices:\n")
        for cat in res:
            file.write('("{0}", (\n\t(DB_ENUMS.CATEGORY.{1}, "Вся {2}"),\n'.format(
                cat[0][0],
                re.search(r'http://olx.ua/uk/(?P<c>[a-z-]+)/', cat[0][1]).group('c').replace('-', '_').upper(),
                cat[0][0].title()))
            for sub in cat[1]:
                if not sub[1]:
                    file.write('\t(DB_ENUMS.SUBCATEGORY.{0}, "{1}"),\n'.format(
                                re.search(r'http://olx.ua/uk/[a-z0-9-]+/(?P<c>[a-z0-9-]+)/',
                                          sub[0][1]).group('c').replace('-', '_').upper(),
                                sub[0][0].title()))

                else:
                    file.write('\t("{0}", (\n\t\t(DB_ENUMS.SUBCATEGORY.{1}, "Вся {2}"),\n'.format(sub[0][0].title(),
                                     re.search(r'http://olx.ua/uk/[a-z0-9-]+/(?P<c>[a-z0-9-]+)/',
                                          sub[0][1]).group('c').replace('-', '_').upper(),
                                          sub[0][0].title()))

                    for ss in sub[1]:
                        for p in ss:
                            file.write('\t\t(DB_ENUMS.SUBCATEGORY.{0}, "{1}"),\n'.format(
                                re.search(r'http://olx.ua/uk/[a-z0-9-]+/[a-z0-9-]+/(?P<c>[a-z0-9-]+)/',
                                          p[1]).group('c').replace('-', '_').upper(),
                                p[0].title()))
                    file.write("\t)\n\t),\n")
            file.write(")\n),\n")
        #print(str(res))
        #file.write(str(res))


categories()
#areas()






