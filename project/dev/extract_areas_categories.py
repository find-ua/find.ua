from pprint import PrettyPrinter

from sqlalchemy import *
from sqlalchemy.orm import sessionmaker


#connection to database
from constants.constants_functions import key_translit

engine = create_engine('postgresql://db_master:db_password@db-find.cvwyq5nhgjhc.eu-central-1.rds.amazonaws.com:5432/db_find')
conn = engine.connect()

#begin the session to work with db
session = sessionmaker()
session.configure(bind=engine)
s = session()

#
# a = Table('area', MetaData(bind=engine), autoload=True)
# c = Table('city', MetaData(bind=engine), autoload=True)
# for area in s.query(a):
#     for city in s.query(c)[area.left-1:area.right]:
#         print("'%s': (%d, {'%s': %d})," % (city.name, city.id, area.name, area.id))
#

# cat = Table('category', MetaData(bind=engine), autoload=True)
# subcat = Table('subcategory', MetaData(bind=engine), autoload=True)
# for category in s.query(cat):
#     if category.left:
#         for subcategory in s.query(subcat)[category.left-1:category.right]:
#             print("'%s|%s|%s': ('%s', %d, {'%s': ('%s', %d)})," % (category.name, category.subcategory_name,
#                                                              subcategory.name, subcategory.name,
#                                                              subcategory.id, category.subcategory_name,
#                                                              category.subcategory_name, category.id))
#     else:
#         print("'%s|%s':('%s', '%s', %d)," % (category.name, category.subcategory_name, category.subcategory_name,
#                                           category.name, category.id))

# constants/categories_db/SUBCATEGORIES
#
# cat = Table('category', MetaData(bind=engine), autoload=True)
# category_dict = {}
# for category in s.query(cat):
#     if category.subcategory_name in category_dict:
#         category_dict[category.subcategory_name].append(category.id)
#     else:
#         category_dict['{0}|{1}'.format(category.name, category.subcategory_name)] = category.id
#

# constants/categories_db/SUBSUBCATEGORIES
#
# cat = Table('category', MetaData(bind=engine), autoload=True)
# subcat = Table('subcategory', MetaData(bind=engine), autoload=True)
# category_dict = {}
# for category in s.query(cat):
#     if category.left:
#         for subcategory in s.query(subcat)[category.left-1:category.right]:
#             category_dict['{0}|{1}|{2}'.format(category.name, category.subcategory_name,
#                                                              subcategory.name)] = subcategory.id
# category_dict = key_translit(category_dict)


# db_num_to_rus_categories_dict
#
# cat = Table('category', MetaData(bind=engine), autoload=True)
# category_dict = {}
# for category in s.query(cat):
#         category_dict[category.id] = (category.name, category.subcategory_name)
#
# p = PrettyPrinter(indent=3)
# p.pprint(category_dict)

def db_num_to_eng_areas_dict():
    a = Table('area', MetaData(bind=engine), autoload=True)
    c = Table('city', MetaData(bind=engine), autoload=True)
    area_dict = {}
    for area in s.query(a):
        for city in s.query(c)[area.left-1:area.right]:
            area_dict[city.id] = (area.name, city.name)
    return area_dict


def extract_areas():
    a = Table('area', MetaData(bind=engine), autoload=True)
    for area in s.query(a):
        print("'{0}': ({1}, {2}),".format(area.short_name, area.left, area.right))


# p = PrettyPrinter()
# p.pprint(db_num_to_eng_areas_dict())
# p.pprint(extract_areas())
# filtr = Table('filter_value', MetaData(bind=engine), autoload=True)
# for fil in s.query(filtr):
#     print("'%s': %d," % (fil.name, fil.id))

            # {<subcategory_name1>: (<subcategory_name>, <num_in_db>,
            # {<subsubcategory1>: (<subsubcategory>, <num_in_db>)})}