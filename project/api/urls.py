#-*- coding: utf-8 -*-
from django.conf.urls import include, url
from api.views import *

urlpatterns = [
    url(r'^search/(?P<location>(\w|-)+)/', include([
        url(r'^$', APISearchView.as_view()),
        url(r'^(?P<category>(\w|-)+)/$', APISearchView.as_view()),
        url(r'^(?P<category>(\w|-)+)/(?P<subcategory>(\w|-)+)/$', APISearchView.as_view()),
        url(r'^(?P<category>(\w|-)+)/(?P<subcategory>(\w|-)+)/(?P<subsubcategory>(\w|-)+)/$', APISearchView.as_view()),
    ]))
]