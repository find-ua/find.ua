from django.shortcuts import render

# Create your views here.
from django.views.generic.base import View
from django.http import HttpResponseBadRequest, JsonResponse
from search.sphinxit_search import MainSearcher
from search.views import process_params


class APISearchView(View):

    def get(self, request, **kwargs):
        params = process_params(request, kwargs)
        if not params:
            raise HttpResponseBadRequest
        ads = MainSearcher.search(params)
        return JsonResponse(ads, safe=False)