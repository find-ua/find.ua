#-*- coding: utf-8 -*- 
__author__ = 'acer'
from collections.abc import MutableMapping
from abc import ABCMeta, abstractmethod
import re

from constants.common import DB_KEYS
from constants.db_enums_ru import DB_ENUMS_RU
from search_engine.utils.search_utils import get_timestamp, timestamp_to_date


class ABCDict(MutableMapping, metaclass=ABCMeta):
    """Dict-like object"""
    @abstractmethod
    def __init__(self):
        self.inner_dict = {}

    def __getitem__(self, key):
        return self.inner_dict[key]

    def __setitem__(self, key, value):
        if not key in self.inner_dict:
            raise AttributeError("Setting new key is forbidden")
        else:
            self.inner_dict[key] = value

    def __delitem__(self, key):
        raise AttributeError("Deletion is forbidden")

    def __len__(self):
        return len(self.inner_dict)

    def __iter__(self):
        for key in self.inner_dict:
            yield key

    def __str__(self):
        return str(self.inner_dict)

    def __repr__(self):
        return str(self.inner_dict)


class AdvertDict(ABCDict):
    """Dict-like object that stores advert"""
    # TODO: Add initialization from dict and named args(if need???)
    # TODO: Find shorter way of initialization
    def __init__(self,
                       title="",
                       url="",
                       category="",
                       location="",
                       price=None,
                       date=None,
                       site=None,
                       photo_urls="",
                       ad_id="",
                       currency=""
                 ):
        self.inner_dict = {
            DB_KEYS.AD.TITLE: title,
            DB_KEYS.AD.URL: url,
            DB_KEYS.AD.CATEGORY: category,
            DB_KEYS.AD.LOCATION: location,
            DB_KEYS.AD.PRICE: price,
            DB_KEYS.AD.DATE: date,
            DB_KEYS.AD.SITE: site,
            DB_KEYS.AD.PHOTO_URLS: photo_urls,
            DB_KEYS.AD.AD_ID: ad_id,
            DB_KEYS.AD.CURRENCY: currency
        }

    @classmethod
    def init_from_xpath(cls, sel, xpath_node):
        """Init AdvertDict dynamically from XPath node

        Parameters:
            xpath_node - XPATH_NODE from search_engine.constants module
            sel - object returned by xpath method from lxml library
        """
        # TODO: catch errors
        # TODO: take into account using re
        title = sel.xpath(xpath_node.TITLE)[0].strip()
        url = sel.xpath(xpath_node.URL)[0].strip()
        category = sel.xpath(xpath_node.CATEGORY)[0].strip()
        location = sel.xpath(xpath_node.LOCATION)[0].strip()
        price = sel.xpath(xpath_node.PRICE)[0].strip()
        date = timestamp_to_date(get_timestamp(' '.join(sel.xpath(xpath_node.DATE)).strip()))
        site = xpath_node.SITE
        photo_urls = sel.xpath(xpath_node.PHOTO_URLS)[0].strip()
        ad_id = sel.xpath(xpath_node.AD_ID)[0].strip()
        currency = "грн" #sel.xpath(xpath_node.CURRENCY)    # temp

        return cls(title=title,
                   url=url,
                   category=category,
                   location=location,
                   price=price,
                   date=date,
                   site=site,
                   photo_urls=photo_urls,
                   ad_id=ad_id,
                   currency=currency
                   )

    # TODO: Rewrite this method
    @classmethod
    def init_from_xpath_olx(cls, sel, xpath_node, parameters, proc={}):
        """Init AdvertDict dynamically from XPath node for olx

        Parameters:
            xpath_node - XPATH_NODE from search_engine.constants module
            sel - object returned by xpath method from lxml library
        """
        # TODO: catch errors
        # TODO: take into account using re
        title = sel.xpath(xpath_node.TITLE)[0].strip()

        url = sel.xpath(xpath_node.URL)[0].strip()

        category = sel.xpath(xpath_node.CATEGORY)[0].strip()

        location = (sel.xpath(xpath_node.LOCATION)[0].strip() if not parameters[DB_KEYS.PARAMS.LOCATION]
                    else parameters[DB_KEYS.PARAMS.LOCATION])   # temp. Must be changed

        price = (sel.xpath(xpath_node.PRICE)[0].strip() if not proc[DB_KEYS.AD.PRICE]
                 else proc[DB_KEYS.AD.PRICE](sel.xpath(xpath_node.PRICE)))

        date = timestamp_to_date(get_timestamp(' '.join(sel.xpath(xpath_node.DATE)).strip())) if \
            sel.xpath(xpath_node.DATE) else ""

        site = "olx.ua"    # TODO: handle using search class consts

        photo_urls = sel.xpath(xpath_node.PHOTO_URLS)[0].strip() if sel.xpath(xpath_node.PHOTO_URLS) else \
            "/static/img/no_image_72x72.jpg"

        ad_id = (sel.xpath(xpath_node.AD_ID)[0].strip() if not proc[DB_KEYS.AD.AD_ID]
                 else proc[DB_KEYS.AD.AD_ID](sel.xpath(xpath_node.AD_ID)))

        currency = "грн" # sel.xpath(xpath_node.CURRENCY)    # temp

        return cls(title=title,
                   url=url,
                   category=category,
                   location=location,
                   price=price,
                   date=date,
                   site=site,
                   photo_urls=photo_urls,
                   ad_id=ad_id,
                   currency=currency
                   )


class ParamsDict(ABCDict):
    """Dict-like object(analogue of Params in DB)"""
    def __init__(self,
                       keys="",
                       site="",
                       category="",
                       location="",
                       price_from=None,
                       price_to=None,
                       condition="",
                       saller_type="",
                 ):
        self.inner_dict = {
            DB_KEYS.PARAMS.KEYS: keys,
            DB_KEYS.PARAMS.SITE: site,
            DB_KEYS.PARAMS.CATEGORY: category,
            DB_KEYS.PARAMS.LOCATION: location,
            DB_KEYS.PARAMS.PRICE_FROM: price_from,
            DB_KEYS.PARAMS.PRICE_TO: price_to,
            DB_KEYS.PARAMS.CONDITION: condition,
            DB_KEYS.PARAMS.SALLER_TYPE: saller_type
        }


class ConstsDict(ABCDict):
    """Dict-like object using for storing constants"""
    def __init__(self, dic=None, **kwargs):
        """Init consts object

        Parameters:
            dic - dict pbject
        """
        if dic and kwargs:
            raise ValueError("Simultaneous init from dict and kwargs is forbidden")    # Maybe some other exception
        elif dic:
            self.inner_dict = dic
        elif kwargs:
            self.inner_dict = kwargs