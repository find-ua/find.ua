#-*- coding: utf-8 -*-
from django.contrib import admin
from .models import *

admin.site.register(Area)
admin.site.register(City)
admin.site.register(Category)
admin.site.register(Subcategory)
admin.site.register(Filter_value)


