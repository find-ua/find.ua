#!/usr/bin/env python

# -*- coding: utf-8 -*-
from django.db import models


# !-------------location------------
class Area(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    name = models.CharField(max_length=200, blank=True, unique=True)
    left = models.SmallIntegerField(blank=True, null=True)
    right = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'area'

    def __str__(self):
        return "%s" % (self.name)


class City(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    name = models.CharField(max_length=200, blank=True, unique=True)

    class Meta:
        managed = False
        db_table = 'city'

    def __str__(self):
        return "%s" % (self.name)


# !-------------categories-------------
class Category(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    subcategory_name = models.CharField(max_length=200, blank=True)
    left = models.SmallIntegerField(blank=True, null=True)
    right = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'category'

    def __str__(self):
        return "%s - %s" % (self.name, self.subcategory_name)


class Subcategory(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    name = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'subcategory'

    def __str__(self):
        return "%s" % (self.name)


# !-------------------filters------------
class Filter_value(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'filter_value'

    def __str__(self):
        return "%s" % (self.name)


# !-------------site--------------
class Site(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    name = models.CharField(max_length=200, unique=True)

    class Meta:
        managed = False
        db_table = 'site'

    def __str__(self):
        return "%s" % (self.name)


from django.db.models import fields
from django.db import connection, migrations
from django.dispatch import receiver, Signal

from psycopg2.extras import register_composite
from psycopg2.extensions import register_adapter, adapt, AsIs
from psycopg2 import ProgrammingError

from django.db.migrations.operations.base import Operation
# from .fields.composite import composite_type_created

# !---------a lot of stuff for composite type-----
_missing_types = {}


class CompositeMeta(type):
    def __init__(cls, name, bases, clsdict):
        super(CompositeMeta, cls).__init__(name, bases, clsdict)
        cls.register_composite()

    def register_composite(cls):
        db_type = cls().db_type(connection)
        if db_type:
            try:
                cls.python_type = register_composite(
                    db_type,
                    connection.cursor().cursor,
                    globally=True
                ).type
            except ProgrammingError:
                _missing_types[db_type] = cls
            else:
                def adapt_composite(composite):
                    return AsIs("(%s)::%s" % (
                        ", ".join([
                                      adapt(getattr(composite, field)).getquoted() for field in composite._fields
                                      ]), db_type
                    ))

                register_adapter(cls.python_type, adapt_composite)


"""
composite_type_created = Signal(providing_args=['name'])

@receiver(composite_type_created)
def register_composite_late(sender, db_type, **kwargs):
    _missing_types.pop(db_type).register_composite()
"""


class CreateCompositeType(Operation):
    def __init__(self, name=None, fields=None):
        self.name = name
        self.fields = fields


"""
    @property
    def reversible(self):
        return True

    def state_forwards(self, app_label, state):
        pass

    def database_forwards(self, app_label, schema_editor, from_state, to_state):
        schema_editor.execute('CREATE TYPE %s AS (%s)' % (
            self.name, ", ".join(["%s %s" % field for field in self.fields])
        ))
        composite_type_created.send(sender=self.__class__, db_type=self.name)

    def state_backwards(self, app_label, state):
        pass

    def database_backwards(self, app_label, schema_editor, from_state, to_state):
        schema_editor.execute('DROP TYPE %s' % self.name)

class Migration(migrations.Migration):
    dependencies = []

    operations = [
        CreateCompositeType(
            name='opening_hours',
            fields=[
                ('start', 'time'),
                ('length', 'interval')
            ],
        ),
    ]
"""


class CompositeField(fields.Field):
    __metaclass__ = CompositeMeta
    """
    A handy base class for defining your own composite fields.

    It registers the composite type.
    """
