# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('basic_classes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.SmallIntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=200)),
            ],
            options={
                'db_table': 'site',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
