# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.SmallIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, max_length=200)),
                ('left', models.SmallIntegerField(blank=True, null=True)),
                ('right', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'area',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.SmallIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('subcategory_name', models.CharField(blank=True, max_length=200)),
                ('left', models.SmallIntegerField(blank=True, null=True)),
                ('right', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'category',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.SmallIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, max_length=200)),
            ],
            options={
                'db_table': 'city',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FilterValue',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'filter_value',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subcategory',
            fields=[
                ('id', models.SmallIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'subcategory',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
