# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('basic_classes', '0002_site'),
    ]

    operations = [
        migrations.CreateModel(
            name='Filter_value',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'filter_value',
                'managed': False,
            },
        ),
    ]
