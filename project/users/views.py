# -*- coding: utf-8 -*-
import datetime

from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse, Http404, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.shortcuts import render_to_response
from django.utils import timezone
from django.views.generic import View, ListView

from constants.common import DB_KEYS
from search.models import Ad
from search.post_processing import ad_post_processing, params_post_processing
from users.forms import *
from users.models import UserProfile, FavParam, FavAd


class UserDashboard(ListView):
    template_name = "users/dashboard.html"

    # @login_required
    def get(self, request, *args, **kwargs):
        print("Entered user dashboard get method ")
        if not request.user.is_authenticated():
            return HttpResponseRedirect('/login/')
        # params = params_post_processing(self.get_queryset(request))
        params = self.get_queryset(request)
        print("Saved params: ", params)
        fav_ads = FavAd.objects.filter(user_id=request.user.id)
        ads = ad_post_processing([obj.__dict__ for obj in [Ad.objects.get(id=f.ad_id) for f in fav_ads]])
        # ads = ad_post_processing([obj.__dict__ for obj in [Ad.objects.get(id=f.ad_id) for f in fav_ads]])
        context = {'params': params, 'ads': ads}
        return render(request, self.template_name, context)

    def get_queryset(self, request):
        print('User id: ', request.user.id)
        print('User: ', request.user)
        try:
            self.user = get_object_or_404(UserProfile, pk=request.user.id)
        except Http404:
            print('lol 404 user profile not found')
            return HttpResponseRedirect('/')
        print('Id exists, user profile exists')
        # pdb.set_trace()
        return FavParam.objects.filter(user=self.user)

    def post(self, request):
        # TODO:check html and change code not to give user_id
        response = {}
        if DB_KEYS.USER_PROFILE.PARAMS_ID in request.POST:
            params_id = request.POST[DB_KEYS.USER_PROFILE.PARAMS_ID]
            return HttpResponseRedirect("/profile/")
        if 'delete-param' in request.POST:
            print("delete param with id %s" % request.POST['param_id'])
            try:
                FavParam.objects.filter(param_id=request.POST['param_id']).delete()
                print('deleted successful')
            except:
                return JsonResponse({'status':'bad'})
            return JsonResponse({'status':'ok'})
        if 'delete-ad' in request.POST:
            try:
                print('delete ad with id {0}'.format(request.POST['ad_id']))
                ad = Ad.objects.filter(ad_id=request.POST['ad_id']).first()
                FavAd.objects.filter(ad_id=ad.id).delete()
            except:
                pass
            return JsonResponse({'status':'ok'})
        return HttpResponseRedirect("/profile/")


class RegistrationView(View):
    form_class = RegForm
    template_name = 'users/reg.html'

    def get(self, request):
        if request.GET.get(''):
            pass
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            #            activation_key = request.COOKIES["sessionid"]
            activation_key = request.session.session_key
            activation_link = "http://127.0.0.1:8000/confirm/%s/" % activation_key
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            # Get user by username
            user = User.objects.get(username=username)

            # Create and save user profile
            new_profile = UserProfile(user=user, activation_key=activation_key,
                                      key_expires=key_expires)
            new_profile.save()

            # Send email with activation key
            email_subject = 'Подтверждение регистрации'
            email_body = "Hey %s, thanks for signing up. To activate your account, click this link within \
                                48hours %s " % (username, activation_link)

            send_mail(email_subject, email_body, 'myemail@example.com',
                      [email], fail_silently=False)
            return render(request, 'users/confirm.html', {'username': username})
        else:
            return HttpResponseRedirect('/registration/')


class SocialRegistrationView(View):
    form_class = SocialRegForm
    template_name = 'users/reg.html'

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            # form.save()
            email = form.cleaned_data['email']
            activation_key = request.session.session_key
            activation_link = "http://127.0.0.1:8000/confirm/%s/" % activation_key
            key_expires = datetime.datetime.today() + datetime.timedelta(2)
            # Get user by username
            user = User.objects.get(id=request.user.id)

            # Create and save user profile
            new_profile = UserProfile(user=user, activation_key=activation_key,
                                      key_expires=key_expires)
            new_profile.save()

            # Send email with activation key
            email_subject = 'Подтверждение регистрации'
            email_body = "Hey %s %s, thanks for signing up. To activate your account, click this link within \
                                48hours %s " % (user.first_name, user.last_name, activation_link)

            send_mail(email_subject, email_body, 'myemail@example.com',
                      [email], fail_silently=False)
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/registration/')


class RegistrationConfirmView(View):
    def get(self, request, activation_key):
        # check if user is already logged in and if he is redirect him to some other url, e.g. home
        if request.user.is_authenticated():
            return HttpResponse("Hello, %s" % request.user.username)

            #        return HttpResponseRedirect(reverse('users:hello'))

        # check if there is UserProfile which matches the activation key (if not then display 404)
        user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

        # check if the activation key has expired, if it hase then render confirm_expired.html
        if user_profile.key_expires > timezone.now():
            return render_to_response('users/confirm_expired.html')
        # if the key hasn't expired save user and set him as active and render some template to confirm activation
        user = user_profile.user
        user.is_active = True
        user.save()
        return HttpResponseRedirect('/profile/')


class LoginView(View):
    template_name = 'users/log.html'

    def get(self, request):
        if request.user.is_authenticated():
            print('aha, user is already in system: ', request.user.id)
            try:
                self.user = get_object_or_404(UserProfile, pk=request.user.id)
            except Http404:
                print('lol 404 user profile not found')
                return HttpResponseRedirect('/login/')
                # return HttpResponseRedirect('/registration/social')
            return HttpResponseRedirect('/profile/')
        form = LogForm()
        context = {'form': form, 'login_error': request.GET.get('login-error'),
                   'password_error': request.GET.get('password-error')}
        return render(request, self.template_name, context)

    def post(self, request):
        form = LogForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            try:
                user = User.objects.get(username=username)
                if user.check_password(password):
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    authenticate(username=username, password=password)
                    login(request, user)
                    return HttpResponseRedirect('/profile/')
                else:
                    return HttpResponseRedirect('/login/?password-error=True')
            except ObjectDoesNotExist:
                return HttpResponseRedirect('/login/?login-error=True')
        else:
            return HttpResponseRedirect('/login/?login-error=True')


class LogOutView(View):
    template_name = 'users/logout.html'

    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')
