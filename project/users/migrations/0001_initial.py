# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('basic_classes', '0003_filter_value'),
        ('search', '0006_ad_adfiltervaluelist'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('user', models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('gender', models.CharField(default='', choices=[('', 'Выбрать...'), ('male', 'Мужчина'), ('female', 'Женщина')], max_length=8)),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('language', models.CharField(default='ua', choices=[('ua', 'Украинский '), ('ru', 'Русский')], max_length=10)),
                ('frequency', models.CharField(default='24 hr', choices=[('24 hr', '24 часа'), ('6 hr', '6 часа'), ('1 min', '1 минута')], max_length=10)),
                ('activation_key', models.CharField(null=True, max_length=100, blank=True)),
                ('key_expires', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'managed': True,
                'db_table': 'user_profile',
            },
        ),
        migrations.CreateModel(
            name='FavAd',
            fields=[
                ('ad', models.ForeignKey(primary_key=True, serialize=False, to='search.Ad')),
                ('user', models.ForeignKey(to='users.UserProfile', primary_key=True)),
                ('description', models.TextField()),
            ],
            options={
                'managed': True,
                'db_table': 'fav_ad',
            },
        ),
        migrations.CreateModel(
            name='FavParam',
            fields=[
                ('param', models.ForeignKey(to='search.Param', primary_key=True)),
                ('user', models.ForeignKey(primary_key=True, serialize=False, to='users.UserProfile')),
                ('last_seen', models.DateTimeField(null=True, blank=True)),
                ('number_of_new_ads', models.IntegerField()),
                ('new_ads', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), size=None)),
            ],
            options={
                'managed': True,
                'db_table': 'fav_param',
            },
        ),
        migrations.AddField(
            model_name='userprofile',
            name='area',
            field=models.ForeignKey(null=True, to='basic_classes.Area', db_column='area', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='city',
            field=models.ForeignKey(null=True, to='basic_classes.City', db_column='city', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='ad_id',
            field=models.ManyToManyField(through='users.FavAd', to='search.Ad'),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='param_id',
            field=models.ManyToManyField(through='users.FavParam', to='search.Param'),
        ),
    ]
