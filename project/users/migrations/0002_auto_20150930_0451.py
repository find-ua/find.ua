# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='favad',
            name='user',
            field=models.ForeignKey(primary_key=True, serialize=False, to='users.UserProfile'),
        ),
        migrations.AlterField(
            model_name='favparam',
            name='user',
            field=models.ForeignKey(to='users.UserProfile', primary_key=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='frequency',
            field=models.CharField(default=b'24 hr', max_length=10, choices=[(b'24 hr', b'24 \xd1\x87\xd0\xb0\xd1\x81\xd0\xb0'), (b'6 hr', b'6 \xd1\x87\xd0\xb0\xd1\x81\xd0\xb0'), (b'1 min', b'1 \xd0\xbc\xd0\xb8\xd0\xbd\xd1\x83\xd1\x82\xd0\xb0')]),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='gender',
            field=models.CharField(default=b'', max_length=8, choices=[(b'', b'\xd0\x92\xd1\x8b\xd0\xb1\xd1\x80\xd0\xb0\xd1\x82\xd1\x8c...'), (b'male', b'\xd0\x9c\xd1\x83\xd0\xb6\xd1\x87\xd0\xb8\xd0\xbd\xd0\xb0'), (b'female', b'\xd0\x96\xd0\xb5\xd0\xbd\xd1\x89\xd0\xb8\xd0\xbd\xd0\xb0')]),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='language',
            field=models.CharField(default=b'ua', max_length=10, choices=[(b'ua', b'\xd0\xa3\xd0\xba\xd1\x80\xd0\xb0\xd0\xb8\xd0\xbd\xd1\x81\xd0\xba\xd0\xb8\xd0\xb9 '), (b'ru', b'\xd0\xa0\xd1\x83\xd1\x81\xd1\x81\xd0\xba\xd0\xb8\xd0\xb9')]),
        ),
    ]
