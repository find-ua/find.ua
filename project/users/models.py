# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.postgres.fields import ArrayField
from datetime import datetime

from basic_classes.models import *
from search.models import Param, Ad
from constants.common import DB_KEYS, DB_ENUMS
from constants.db_enums_ru import DB_ENUMS_RU
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    gender = models.CharField(max_length=8,
                              choices=DB_ENUMS_RU.GENDER_CHOICES,
                              default=DB_ENUMS.GENERAL.EMPTY)
    date_of_birth = models.DateField(auto_now=False,
                                     auto_now_add=False, blank=True, null=True)
    area = models.ForeignKey(Area, db_column='area', blank=True, null=True)
    city = models.ForeignKey(City, db_column='city', blank=True, null=True)
    language = models.CharField(max_length=10,
                                choices=DB_ENUMS_RU.LANGUAGE_CHOICES,
                                default=DB_ENUMS.LANGUAGE.UA)
    frequency = models.CharField(max_length=10,
                                 choices=DB_ENUMS_RU.FREQUENCY_CHOICES,
                                 default=DB_ENUMS.FREQUENCY.HOURS24)
    param_id = models.ManyToManyField(Param,
                                      through='FavParam')
    ad_id = models.ManyToManyField(Ad,
                                   through='FavAd')
    activation_key = models.CharField(max_length=100, blank=True, null=True)
    key_expires = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        managed = True
        db_table = 'user_profile'

    def __unicode__(self):
        return u'%s' % self.user.username


class FavAd(models.Model):
    ad = models.ForeignKey(Ad, primary_key=True)
    user = models.ForeignKey('UserProfile', primary_key=True)
    description = models.TextField(null=False)

    class Meta:
        managed = True
        db_table = 'fav_ad'

    def __str__(self):
        return self.ad.title + '  ' + self.user.user.username


class FavParam(models.Model):
    param = models.ForeignKey(Param, primary_key=True)
    user = models.ForeignKey('UserProfile', primary_key=True)
    last_seen = models.DateTimeField(blank=True, null=True)
    number_of_new_ads = models.IntegerField()
    new_ads = ArrayField(models.IntegerField())

    class Meta:
        managed = True
        db_table = 'fav_param'

    def __str__(self):
        return self.param.keys + ' ' + self.user.user.username
