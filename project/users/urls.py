#-*- coding: utf-8 -*- 
from django.conf.urls import patterns, url
from users.views import *

urlpatterns = patterns('',
    (r'^profile/(?P<user_id>\d+)/$', UserDashboard.as_view()),
    url(r'^profile/$', UserDashboard.as_view()),
    url(r'^login/$', LoginView.as_view(), name='log_in'),
    url(r'^logout/$', LogOutView.as_view(), name='log_out'),
    url(r'^registration/$', RegistrationView.as_view(), name='reg'),
    url(r'^registration/social/$', SocialRegistrationView.as_view(), name='social_reg'),
    url(r'^confirm/(?P<activation_key>\w+)/$', RegistrationConfirmView.as_view()),
)
