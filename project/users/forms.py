# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class LogForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                             'name': 'username',
                                                             'placeholder': 'Логін', }))
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'id': "login-password",
                                                                                   'type': "password",
                                                                                   'class': "form-control",
                                                                                   'name': "password",
                                                                                   'placeholder': "Пароль"}))


class SocialRegForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'type': "text",
                                                            'class': "form-control",
                                                            'name': "email",
                                                            'placeholder': "Електронна пошта"}))


class RegForm(SocialRegForm):
    class Meta:
        model = User
    #        fields = ('username', 'email', 'password1','password2')

    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                             'name': 'username',
                                                             'placeholder': 'Логін', }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'id': "login-password",
                                                                 'type': "password",
                                                                 'class': "form-control",
                                                                 'name': "password",
                                                                 'placeholder': "Пароль"}))

    def clean_username(self):  # check if username dos not exist before
        try:
            User.objects.get(username=self.cleaned_data['username'])  # get user from user model
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError("this user exist already")

    def clean(self):
        return self.cleaned_data

    def save(self, commit=True):
        user = User.objects.create_user(self.cleaned_data['username'],
                                        self.cleaned_data['email'],
                                        self.cleaned_data['password'])
        user.is_active = False
        user.save()
        #        user = super(RegForm, self).save(commit=False)
        #        user.username = self.cleaned_data['username']
        #        user.set_password(self.cleaned_data['password'])
        #        user.email = self.cleaned_data['email']
        #        if commit:
        #            user.is_active = False # not active until he opens activation link
        #            user.save()
        return user
