from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View, TemplateView


class AboutUsView(TemplateView):
    template_name = 'footer/about.html'


class FeedbackView(TemplateView):
    template_name = 'footer/feedback.html'
    # def get(self):
    #     return HttpResponseRedirect('http://feedback.find.org.ua')


class CooperationView(TemplateView):
    template_name = 'footer/cooperation.html'

class ReformalView(TemplateView):
    """
    View for feedback-site reformal.ru
    """
    template_name = "footer/da728da5cd155a5b9474508d.html"
