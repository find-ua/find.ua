#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from footer.views import *

urlpatterns = patterns('',
    url(r'^about/$', AboutUsView.as_view(), name='index'),
    url(r'^feedback/$', FeedbackView.as_view()),
    url(r'^cooperation/$', CooperationView.as_view(), name='log_in'),
    url(r'^da728da5cd155a5b9474508d.html/$', ReformalView.as_view(), name='reformal'),
)
