# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View, ListView

from constants.common import DB_KEYS
from users.models import UserProfile, FavParam, FavAd
from search.models import Param, Ad

from django.core.exceptions import ObjectDoesNotExist
from users.forms import *
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail
import datetime
from django.utils import timezone


class UserList(ListView):
    model = UserProfile
    template_name = "users/index.html"


class UserDashboard(ListView):
    template_name = "users/dashboard.html"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect('/registration/')
        params = self.get_queryset(request)
        fav_ads = FavAd.objects.filter(user_id=request.user.id)
	ads = [Ad.objects.get(id=f.ad_id) for f in fav_ads]
        context = {'params': params, 'ads': ads}
        return render(request, self.template_name, context)

    def get_queryset(self, request):
        self.user = get_object_or_404(UserProfile, pk=request.user.pk)
        return FavParam.objects.filter(user=self.user)
    
    def post(self, request):
        if DB_KEYS.USER_PROFILE.PARAMS_ID in request.POST:
            params_id = request.POST[DB_KEYS.USER_PROFILE.PARAMS_ID]
            return HttpResponseRedirect("/users/{0}/params/{1}/".format(request.user.id, params_id))
        if 'delete-param' in request.POST:
            try:
                FavParam.objects.filter(param_id=request.POST['param_id']).delete()
            except:
                pass
            return HttpResponseRedirect("/users/{0}/".format(request.user.id))
        if 'delete-ad' in request.POST:
            try:
                FavAd.objects.filter(ad_id=request.POST['ad_id']).delete()
            except:
                pass
        return HttpResponseRedirect("/users/{0}/".format(request.user.id))
class UserParams(View):
    """ 
    CBV that provides a call to search engine's function, 
    that checks params with id params_id,
    and displays new ads for these params
    """

    def get(self, request, user_id, params_id):
        # we get params_id from url and load them from DB
        params_obj = Param.objects.get(pk=params_id)
        params = {}
        params[DB_KEYS.PARAMS.KEYS] = params_obj.keys
        params[DB_KEYS.PARAMS.CATEGORY] = params_obj.category
        params[DB_KEYS.PARAMS.LOCATION] = params_obj.location
        params[DB_KEYS.PARAMS.PRICE_FROM] = params_obj.price_from
        params[DB_KEYS.PARAMS.PRICE_TO] = params_obj.price_to
        params[DB_KEYS.PARAMS.SELLER_TYPE] = params_obj.seller_type
        params[DB_KEYS.PARAMS.CONDITION] = params_obj.condition

        fav_ad = FavAd.objects.get(params_id=params_id)

        ads = [Ad.objects.get(id=i.ad_id) for i in fav_id]  # + OlxSearch.find_ads(params)

        # 'ads', 'params' - keywords from templates

        context = {'ads': ads, 'params': params}

        # please, tell me what attribute of Ad class to use 
        # to change colour for new ads in template
        return render(request, 'users/params.html', context)


class RegistrationView(View):
    form_class = RegForm
    template_name = 'users/reg.html'

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
#            activation_key = request.COOKIES["sessionid"]
            activation_key = request.session.session_key
            activation_link = "http://127.0.0.1:8000/confirm/%s/" % activation_key
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            # Get user by username
            user = User.objects.get(username=username)

            # Create and save user profile
            new_profile = UserProfile(user=user, activation_key=activation_key,
                                      key_expires=key_expires)
            new_profile.save()

            # Send email with activation key
            email_subject = 'Подтверждение регистрации'
            email_body = "Hey %s, thanks for signing up. To activate your account, click this link within \
                                48hours %s " % (username, activation_link)

            send_mail(email_subject, email_body, 'myemail@example.com',
                      [email], fail_silently=False)
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/registration/')


class RegistrationConfirmView(View):
    def get(self, request, activation_key):
        # check if user is already logged in and if he is redirect him to some other url, e.g. home
        if request.user.is_authenticated():
            return HttpResponse("Hello, %s" % request.user.username)

            #        return HttpResponseRedirect(reverse('users:hello'))

        # check if there is UserProfile which matches the activation key (if not then display 404)
        user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

        # check if the activation key has expired, if it hase then render confirm_expired.html
        if user_profile.key_expires > timezone.now():
            return render_to_response('users/confirm_expired.html')
        # if the key hasn't expired save user and set him as active and render some template to confirm activation
        user = user_profile.user
        user.is_active = True
        user.save()
        return render_to_response('users/confirm.html')


class LoginView(View):
    template_name = 'users/log.html'

    def get(self, request):
        if request.user.is_authenticated():
            return HttpResponseRedirect('/profile/')
        form = LogForm()
        context = {'form': form,'login_error': request.GET.get('login-error'),
                   'password_error': request.GET.get('password-error')}
        return render(request, self.template_name, context)

    def post(self, request):
        form = LogForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            try:
                user = User.objects.get(username=username)

                if user.check_password(password):
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    authenticate(username=username, password=password)
                    login(request, user)
                    return HttpResponseRedirect('/profile/')
                else:
                    return HttpResponseRedirect('/login/?password-error=True')
            except ObjectDoesNotExist:
                return HttpResponseRedirect('/login/?login-error=True')
        else:
            return HttpResponseRedirect('/login/?login-error=True')


class LogOutView(View):
    template_name = 'users/logout.html'

    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')
