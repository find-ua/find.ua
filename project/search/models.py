# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.postgres.fields import ArrayField
from basic_classes.models import CompositeField, Area, City, Category, Subcategory, Filter_value
from constants.common import DB_KEYS, DB_ENUMS
from constants.db_enums_ru import DB_ENUMS_RU
from basic_classes.models import *


class LinkType(CompositeField):
    site = models.SmallIntegerField()
    link = ArrayField(models.CharField(max_length=200))

    def db_type(self, connection):
        return 'link_type'


class Param(models.Model):
    id = models.AutoField(primary_key=True)
    keys = models.CharField(max_length=200)
    area = models.ForeignKey(Area, db_column='area', blank=True, null=True)
    city = models.ForeignKey(City, db_column='city', blank=True, null=True)
    category = models.ForeignKey(Category, db_column='category', blank=True, null=True)
    subcategory = models.ForeignKey(Subcategory, db_column='subcategory', blank=True, null=True)
    filterValue = models.ManyToManyField(Filter_value,
                                         through='ParamFilterValueList')

    price_from = models.PositiveIntegerField(blank=True, null=True)
    price_to = models.PositiveIntegerField(blank=True, null=True)
    currency = models.CharField(max_length=20,
                                choices=DB_ENUMS_RU.CURRENCY_CHOICES,
                                default=DB_ENUMS.CURRENCY.UAH,
                                blank=True)
    condition = models.CharField(max_length=20,
                                 choices=DB_ENUMS_RU.CONDITION_CHOICES,
                                 default=DB_ENUMS.GENERAL.ALL,
                                 blank=True)
    seller_type = models.CharField(max_length=20,
                                   choices=DB_ENUMS_RU.SELLER_TYPE_CHOICES,
                                   default=DB_ENUMS.GENERAL.ALL,
                                   blank=True)
    site = models.ForeignKey(Site, db_column='site')
    #    site = models.URLField(max_length=200, blank=True)
    link_list = ArrayField(base_field=LinkType())
    date = models.DateTimeField(blank=True, null=True)
    path = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'param'

    def __unicode__(self):
        return self.keys


class ParamFilterValueList(models.Model):
    param = models.ForeignKey(Param, primary_key=True)
    filter_value = models.ForeignKey(Filter_value, primary_key=True)

    class Meta:
        managed = False
        db_table = 'param_filter_value_list'

    def __unicode__(self):
        return self.param.keys + ' || ' + self.filter_value.name


class PriceHistoryType(CompositeField):
    price = models.IntegerField()
    date = models.DateField()

    def db_type(self, connection):
        return 'price_history_type'


class Ad(models.Model, object):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200, null=False)
    area = models.ForeignKey(Area, db_column='area', blank=True, null=True)
    city = models.ForeignKey(City, db_column='city', blank=True, null=True)
    category = models.ForeignKey(Category, db_column='category', blank=False, null=False)
    subcategory = models.ForeignKey(Subcategory, db_column='subcategory', blank=True, null=True)
    filterValue = models.ManyToManyField(Filter_value,
                                         through='AdFilterValueList')
    price = models.IntegerField(blank=True, null=True)
    currency = models.CharField(max_length=50,
                                choices=DB_ENUMS_RU.CURRENCY_CHOICES,
                                default=DB_ENUMS.CURRENCY.UAH)
    url = models.URLField(max_length=200, blank=True)
    site = models.URLField(max_length=200, blank=True)
    ad_id = models.CharField(max_length=200, blank=True)
    photo_urls = models.CharField(max_length=200, blank=True)
    date = models.DateTimeField(blank=True, null=True)
    last_update = models.DateTimeField(blank=True, null=True)
    price_history = ArrayField(base_field=PriceHistoryType())
    price_changed = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'ad'

    def __unicode__(self):
        return u'%s' % self.title


class AdFilterValueList(models.Model):
    ad = models.ForeignKey(Ad, primary_key=True)
    filter_value = models.ForeignKey(Filter_value, primary_key=True)

    class Meta:
        managed = False
        db_table = 'ad_filter_value_list'

    def __unicode__(self):
        return self.ad.title + ' || ' + self.filter_value.name
