# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0003_auto_20150813_1919'),
    ]

    operations = [
        migrations.CreateModel(
            name='Param',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('keys', models.CharField(max_length=200)),
                ('price_from', models.PositiveIntegerField(null=True, blank=True)),
                ('price_to', models.PositiveIntegerField(null=True, blank=True)),
                ('currency', models.CharField(choices=[('uah', 'Гривна '), ('dollar', '$')], blank=True, default='uah', max_length=20)),
                ('condition', models.CharField(choices=[('', 'Все '), ('new', 'Новые'), ('second_hand', 'Б/у')], blank=True, default='', max_length=20)),
                ('seller_type', models.CharField(choices=[('', 'Все'), ('business', 'Бизнес'), ('private', 'Приватные ')], blank=True, default='', max_length=20)),
                ('site', models.URLField(blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
            ],
            options={
                'db_table': 'param',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
