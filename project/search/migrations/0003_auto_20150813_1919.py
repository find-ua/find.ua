# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0002_auto_20150315_1043'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='params',
            options={'managed': False},
        ),
    ]
