# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields
import search.models


class Migration(migrations.Migration):

    dependencies = [
        ('basic_classes', '0003_filter_value'),
        ('search', '0005_paramfiltervaluelist'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ad',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('price', models.IntegerField(null=True, blank=True)),
                ('currency', models.CharField(default='uah', choices=[('uah', 'Гривна '), ('dollar', '$')], max_length=50)),
                ('url', models.URLField(blank=True)),
                ('site', models.URLField(blank=True)),
                ('ad_id', models.CharField(max_length=200, blank=True)),
                ('photo_urls', models.CharField(max_length=200, blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('last_update', models.DateTimeField(null=True, blank=True)),
                ('price_history', django.contrib.postgres.fields.ArrayField(base_field=search.models.PriceHistoryType(), size=None)),
                ('price_changed', models.BooleanField()),
            ],
            options={
                'managed': False,
                'db_table': 'ad',
            },
        ),
        migrations.CreateModel(
            name='AdFilterValueList',
            fields=[
                ('ad', models.ForeignKey(to='search.Ad', primary_key=True)),
                ('filter_value', models.ForeignKey(primary_key=True, serialize=False, to='basic_classes.Filter_value')),
            ],
            options={
                'managed': False,
                'db_table': 'ad_filter_value_list',
            },
        ),
    ]
