# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('basic_classes', '0003_filter_value'),
        ('search', '0004_param'),
    ]

    operations = [
        migrations.CreateModel(
            name='ParamFilterValueList',
            fields=[
                ('param', models.ForeignKey(to='search.Param', primary_key=True)),
                ('filter_value', models.ForeignKey(primary_key=True, serialize=False, to='basic_classes.Filter_value')),
            ],
            options={
                'db_table': 'param_filter_value_list',
                'managed': False,
            },
        ),
    ]
