# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Params',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('category', models.CharField(default='', max_length=50, choices=[('', 'Выберите категорию '), ('Электроника', (('Electronica', 'Вся электроника '), ('Notebooks', 'Ноутбуки'))), ('Транспорт', (('Transport', 'Весь транспорт '), ('Other', 'Прочее ')))], blank=True, db_column='category')),
                ('keys', models.CharField(blank=True, max_length=200, db_column='keys')),
                ('site', models.URLField(blank=True, db_column='site')),
                ('price_from', models.PositiveIntegerField(default=0, db_column='price_from')),
                ('price_to', models.PositiveIntegerField(default=0, db_column='price_to')),
                ('location', models.CharField(default='', max_length=100, choices=[('', 'Выберите расположение '), ('Киевская область', (('Kiev area', 'Вся Киевская область '), ('Kiev', 'Киев '))), ('Полтавская область', (('Poltava area', 'Вся Полтавская область '), ('Poltava', 'Полтава '))), ('Донецкая область', (('Mariupol', 'Мариуполь, Донецкая область'), ('Donetsk', 'Донецк, Донецкая область'), ('Donetsk area', 'Вся Донецкая область')))], blank=True, db_column='location')),
                ('condition', models.CharField(default='', max_length=12, choices=[('', 'Все '), ('new', 'Новые'), ('second_hand', 'Б/у')], blank=True, db_column='condition')),
                ('seller_type', models.CharField(default='', max_length=20, choices=[('', 'Все'), ('business', 'Бизнес'), ('private', 'Приватные ')], blank=True, db_column='seller_type')),
                ('date', models.DateField(blank=True, null=True, db_column='date')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
