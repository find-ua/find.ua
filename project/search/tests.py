#-*- coding: utf-8 -*- 
import datetime

from django.test import TestCase
from django.test.client import RequestFactory

from users.models import User, Params_list

class ParamsSaveTest(TestCase):
    
    def setUp(self):
        self.factory = RequestFactory()
    
    def test_params_save_successfully(self):
        request = self.factory.get('search/?keys=accer&category=&location=&price_from=0&price_to=0&condition=&seller_type=')
        idn=1
        data = Params_list(keys=request.GET.get('keys'),
                        category=request.GET.get('category'),
                        location=request.GET.get('location'),
                        price_from=request.GET.get('price_from'),
                        price_to=request.GET.get('category'),
                        condition=request.GET.get('condition'),
                        seller_type=request.GET.get('seller_type'),
                        last_update=datetime.datetime.now(),
                        user=User.objects.get(pk=idn),
            )
        self.assertEqual(data.save(), True)
        
    def test_simple_params_save(self):
        data = Params_list(keys='accer', user=User.objects.get(pk=1),)
        data.save()
        self.assertEqual(, True)
        
