#-*- coding: utf-8 -*- 
from django.conf.urls import patterns, url
from search.views import *

# DRY?
urlpatterns = patterns('',
    (r'^$', FormView.as_view()),
    (r'^(?P<category>(\w|-)+)/$', FormView.as_view()),
    (r'^(?P<category>(\w|-)+)/(?P<subcategory>(\w|-)+)/$', FormView.as_view()),
    (r'^(?P<category>(\w|-)+)/(?P<subcategory>(\w|-)+)/(?P<subsubcategory>(\w|-)+)/$', FormView.as_view()),
)