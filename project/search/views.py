#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
from copy import deepcopy

from django.db import connection
from django.http import HttpResponse, HttpResponseRedirect, QueryDict, Http404, JsonResponse
from django.shortcuts import render
from django.template.defaulttags import register
from django.utils import timezone
from django.views.generic import View

from basic_classes.models import City, Area, Category, Site
from constants.areas_dict import CITIES
from constants.categories_choices import SUBCATEGORY_CHOICES
from constants.categories_db import SUBCATEGORIES
from constants.common import DB_KEYS, DB_ENUMS, SETS
from constants.constants_functions import find_filters, split_category_and_subcategory, params_to_olx_urls
from constants.filters import extract_filters
from constants.filters_db import FILTERS_DB
from search.forms import MainPageForm, ParamsForm
from search.models import Param, Ad, ParamFilterValueList
from search.sphinxit_search import MainSearcher
from users.models import UserProfile, FavAd, FavParam


class HomePageView(View):
    form_class = MainPageForm
    template_name = "search/index.html"

    def get(self, request):
        form = self.form_class()
        if request.user.is_authenticated():
            auth = '/profile/'
        else:
            auth = '/login/'
        context = {'categories': DB_ENUMS, 'form': form, 'request': request}
        return render(request, self.template_name, context)

    def post(self, request):
        path = '/' + request.POST.get('location', '')
        if DB_KEYS.PARAMS.KEYWORDS in request.POST:
            path += '?q=' + request.POST.get('q', '')
        return HttpResponseRedirect(path)


class FormView(View):
    form_class = ParamsForm
    template_name = "search/results.html"
    response = {}

    def get(self, request, **kwargs):

        params = process_params(request, **kwargs)
        if not params:
            raise Http404
        ads = MainSearcher.search(params)
        # ЯКЩО НЕ ВСТАНОВЛЕНИЙ СФІНКС
        # ads = r.get("http://find-ua.elasticbeanstalk.com/api/search" + request.path + '?' + request.GET.urlencode()).json()

        # ads = Ad.objects.filter(price=26)
        # print("ADS: ", ads)
        filters = find_filters(kwargs)
        print('FILTERS_RU: ', filters)
        form = self.form_class(deepcopy(params), filters=filters)
        # Просто заглушка, щоб щось відображалося
        # ads = [{'site': 'olx.ua','title':'Сумка для ноутбука Qilive 16" (новая)', 'price':'5000 грн',
        #         'location':'Мелітополь, Запорізька область', 'photo_urls':'http://img20.olx.ua/images_slandocomua/234078694_1_261x203_sumka-dlya-noutbuka-qilive-16-novaya-melitopol.jpg',
        #         'date':'Сьогодні 13:59'} for x in range(5)]
        filters_names = [form[key[0]] for key in filters.keys()]
        # print('ads: ', ads)
        # print('filters_names: ', filters_names)
        # todo: зробити порядок фільтрів
        context = {'ads': ads, 'params': params, 'form': form, 'filters': filters_names, 'content': 'content.html'}
        if request.is_ajax():
            return render(request, 'search/content.html', context)
        return render(request, 'search/results.html', context)

    def post(self, request, **kwargs):
        try:
            param_dict = request.POST.copy()
            # правильно закодовуємо українську "і"
            if 'q' in param_dict:
                param_dict['q'] = param_dict['q'].encode("utf-8").decode("UTF-8", "ignore").replace(u'\u0456', u'i')
            print('POST INITIAL', param_dict)

            if 'search' in param_dict:
                path = create_find_ua_url(param_dict)
                return HttpResponseRedirect(path)

            # збереження параметрів
            if 'save-params' in request.POST:
                if not request.user.is_authenticated():
                    print('not auth')
                    return JsonResponse({'status': 'not authentificated'})
                print('----------------')
                print('save params request post: ', request.POST)
                param_dict = {k: v for k, v in request.POST.items() if v}
                category_value = param_dict.get('category', '')
                result = split_category_and_subcategory(category_value)
                # if category or subcategory name is valid
                if result:
                    print('RESULT: ', result)
                    # if 'category' is subcategory
                    if type(result) is tuple:
                        param_dict['category'] = result[0]
                        param_dict['subcategory'] = result[1]
                    # if 'category' is category
                    else:
                        param_dict['category'] = result
                olx_urls = params_to_olx_urls(param_dict)
                if not olx_urls:
                    HttpResponseRedirect('You have chosen incompatible parametrs')
                account = UserProfile.objects.get(user_id=request.user)
                param = Param()
                print("\nparam_dict \n", param_dict)
                # some values can be absent that will throw an errors. we don't need them
                try:
                    param_dict_copy = param_dict.copy()
                    param.path = create_find_ua_url(param_dict_copy)
                    try:
                        param.keys = param_dict.pop('q')
                    except:
                        print('===except q===')
                        pass
                    try:
                        cat = param_dict.pop('category')
                        subcat = ''
                        subcat = param_dict.pop('subcategory')
                        param.category = Category.objects.get(id=SUBCATEGORIES['%s|%s' % (cat, subcat)])
                    except:
                        print('===except category===')
                        pass

                    try:
                        location = param_dict.pop('location')
                        param.city = City.objects.get(name=location)
                        param.area = Area.objects.get(id=list(CITIES[location][1].values())[0])
                    except:
                        print('===except location===')
                        pass
                    try:
                        param.price_from = int(param_dict.pop('pmin'))
                    except:
                        print('===except pmin===')
                        pass
                    try:
                        param.price_to = int(param_dict.pop('pmax'))
                    except:
                        print('===except pmax===')
                        pass        
                    # TODO: while we work only with olx, it's okey, but there should be the way to identify the id of Site
                    param.site = Site.objects.get(id=1) # 1 is for olx.ua
                    param.date = timezone.now()
                except:
                    # because we don't carry for values that absent,
                    # field in this ways will be filled by default
                    print('===except===')
                    pass
                else:
                    param.save()
                cursor = connection.cursor()
                if olx_urls:
                    for url in olx_urls:
                        sql = "UPDATE param SET link_list=link_list|| ROW(1,ARRAY['%s'])::link_type WHERE param.id=%s" % (
                            url, param.id)
                        cursor.execute(sql)
                filters = extract_filters(param_dict)
                if filters:
                    for filter_value in [param_dict[f] for f in filters]:
                        try:
                            f_value_id = FILTERS_DB[filter_value]
                            ParamFilterValueList.objects.create(param_id=param.id, filter_value_id=f_value_id)
                        except:
                            print(filter_value, "not found or another error")
                            self.response['result'] = 'bad'
                            return JsonResponse(self.response)
                print("if you see this line, then params must have been saved")
                FavParam.objects.create(param=param, user=account, last_seen=datetime.datetime.now())
                self.response['result'] = 'ok'
                return JsonResponse(self.response)
        except UnicodeEncodeError:
            print('unicode error')
            pass

        if 'save-ad' in request.POST:
            print('entered save-ad')
            if request.user.is_authenticated():
                print('user authentificated')
                ad = Ad.objects.get(ad_id=request.POST['ad_id'])
                # print(ad.id, ad.title)
                account = UserProfile.objects.get(user_id=request.user.id)
                print(account.user_id)
                FavAd.objects.create(ad=ad, user=account, description=' ')
                print('here must be error')
                self.response['result'] = 'ok'
                return JsonResponse(self.response)
            else:
                return JsonResponse({'status': 'not authentificated'})

        return HttpResponse('BAD: all your requests have been ignored')


def process_params(request, *args, **kwargs):
    query_correct = True
    params = QueryDict('', mutable=True)
    print('kwargs: ', kwargs)
    if len(kwargs) >= 1:
        params.__setitem__('location', kwargs['location'])
        if not kwargs['location'] in SETS.LOCATION:
            print('kwargs location not correct: ', kwargs['location'])
            query_correct = False
            # if query_correct:
            #     if not kwargs['category'] in SETS.CATEGORY:
            #         query_correct = False
    if len(kwargs) >= 2:
        params.__setitem__('category', kwargs['category'])
    if len(kwargs) >= 3:
        params.__setitem__('subcategory', kwargs['subcategory'])
        # if query_correct:
        #     if not kwargs['subcategory'] in SETS.SUBCATEGORY:
        #         query_correct = False
    if len(kwargs) >= 4:
        params.__setitem__('subsubcategory', kwargs['subsubcategory'])
        # if query_correct:
        #     if not kwargs['subsubcategory'] in SETS.SUBCATEGORY:
        #         query_correct = False
    # TODO: переписати з універсальною обробкою і перевіркою позиційних аргументів
    params.update(request.GET)
    print('request.POST', request.POST)
    params.update(request.POST)
    print('params ultimate: ', params)
    if query_correct:
        return params
    else:
        return False


def create_find_ua_url(param_dict):
    """

    Функція перевторює параметри з форми на посилання на нашому сайті

    :param param_dict: cловник параметрів
    :return: відносний url цих параметрів на find.org.ua, наприклад /ukraine/elektronika/noutbuki/?q=accer
    """
    print('Param dict: ', param_dict)
    path = '/' + param_dict.get('location', 'ukraine')
    if 'category' in param_dict and param_dict.get('category'):
        category_value = param_dict.get('category', '')
        subcategory_value = param_dict.get('subcategory', '')
        if subcategory_value:
            # якщо категорія і підкатегорія однозначно в різних полях
            result = (category_value, subcategory_value)
        else:
            # ці пляски з бубном потрібні тому, що в url category/subcategory
            # а у формі category i subcategory в одному полі
            result = split_category_and_subcategory(category_value)
        # if category or subcategory name is valid
        if result:
            print('RESULT: ', result)
            # if 'category' is subcategory
            if type(result) is tuple:
                param_dict['category'] = category = result[0]
                param_dict['subcategory'] = subcategory = result[1]
                path += '/' + category + '/' + subcategory
            # if 'category' is category
            else:
                path += '/' + result
        else:
            print('NOT FOUND !!!!!!!!!!!!!!!!11111')
    # Перевіряємо, чи є підпідкатегорія в запиті і чи відноситься вона до підкатегорії
    if 'subsubcategory' in param_dict:
        subsubcategory_value = param_dict.get('subsubcategory', '')
        print('SUBSUBCATEGORYE: ', subsubcategory_value)
        subsubcategory_choices = SUBCATEGORY_CHOICES.get(
            '{0}|{1}'.format(category, subcategory))
        if subsubcategory_choices:
            eng_subsubcategory_choices = [var[0] for var in subsubcategory_choices]
            if subsubcategory_value in eng_subsubcategory_choices:
                print('SUBSUBCATEGORY VALUE: ', subsubcategory_value)
                path += '/' + subsubcategory_value
    first_filter_in_query = True
    filters = [filter_name[0] for filter_name in find_filters(param_dict)]
    print('FILTERS_RU: ', filters)
    for filtr in param_dict:
        # Перевірка, чи фільтр відповідає категорії/підкатегорії
        # а також, чи це поле непорожнє в запиті (щоб не відображати порожні фільтри в url)
        if (filtr in SETS.FIELDS or filtr in filters) and param_dict.get(filtr):
            if first_filter_in_query:
                path += '/?'
                first_filter_in_query = False
                path += filtr + '=' + param_dict.get(filtr, '')
            else:
                path += '&' + filtr + '=' + param_dict.get(filtr, '')
    # print('REQUEST POST: ', param_dict)
    return path


@register.inclusion_tag('search/content.html')
def show_content(ads):
    return {'ads': ads}