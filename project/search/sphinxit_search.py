# -*- coding: utf-8 -*-
from sphinxit.core.helpers import BaseSearchConfig
from sphinxit.core.processor import Search

from constants.areas import AREAS_NUM
from constants.areas_dict import CITIES
from constants.categories_db import CATEGORIES, get_subsubcategory_id, \
    get_subcategory_id
from constants.common import SETS
from constants.filters_db import FILTERS_DB
from search.post_processing import ad_post_processing


class SphinxitConfig(BaseSearchConfig):
    WITH_STATUS = False


class MainSearcher:
    @staticmethod
    def search(params):
        search_query = Search(indexes=['ad'], config=SphinxitConfig)
        # print('params sphinxit: ', params)
        if 'q' in params:
            search_query = search_query.match(params['q'])
        if 'location' in params:
            location = params['location']
            if location != 'ukraine':
                if location in AREAS_NUM:
                    search_query = search_query.filter(location__between=[AREAS_NUM[location][0],
                                                                          AREAS_NUM[location][1]])
                else:
                    search_query = search_query.filter(location__eq=CITIES[location][0])
        if 'category' in params:
            category_value = params['category']
            # print('category_value', category_value)
            if 'subcategory' in params:
                subcategory_value = params['subcategory']
                subcategory_id = get_subcategory_id(category_value, subcategory_value)
                # print('subcategory_id', subcategory_id)
                search_query = search_query.filter(category__eq=subcategory_id)

                if 'subsubcategory' in params:
                    subsubcategory_id = get_subsubcategory_id(category_value, subcategory_value,
                                                              params['subsubcategory'])
                    search_query = search_query.filter(subcategory__eq=subsubcategory_id)
            else:
                category_id = CATEGORIES[category_value]
                # print('category_id ', category_id)
                search_query = search_query.filter(category__in=category_id)
        if 'pmin' in params or 'pmax' in params:
            if 'pmin' in params and 'pmax' in params:
                search_query = search_query.filter(price__between=[params['pmin'], params['pmax']])
            elif 'pmin' in params:
                search_query = search_query.filter(price__gte=params['pmin'])
            elif 'pmax' in params:
                search_query = search_query.filter(price__lte=params['pmax'])
        for filtr in params:
            # print('FILTR',filtr)
            if filtr not in SETS.NON_FILTERS:
                try:
                    print('filter', params[filtr])
                    filtr_id = FILTERS_DB[params[filtr]]
                    search_query = search_query.filter(filtr__eq=filtr_id)
                except KeyError:
                    print('UNKNOWN KEY IN FILTERS')
        search_query = search_query.limit(int(params.get('page', 0))*40, 40)\
                                   .order_by(params.get('sortby', 'date'), params.get('order', 'desc'))
        search_result = search_query.ask()
        # pp = PrettyPrinter(indent=2)
        # pp.pprint(search_result)
        search_result = search_result['result']['items']
        if not search_result:
            return 'ajax load none'
        result = ad_post_processing(search_result)
        return result
