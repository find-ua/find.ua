#-*- coding: utf-8 -*-
#!/usr/bin/env python
#-*- coding: utf-8 -*-
from copy import deepcopy

from django import forms
from constants.common import DB_KEYS
from constants.db_enums_ru import DB_ENUMS_RU
from constants.categories_choices import CATEGORIES_CHOICES, SUBCATEGORY_CHOICES


class MainPageForm(forms.Form):
    q = forms.CharField(widget=forms.TextInput(attrs={'class': 'word',
                                                      'placeholder': 'Що ви хочете знайти?',
                                                      'id': 'keywords', }))
    location = forms.ChoiceField(choices=DB_ENUMS_RU.AREA_CHOICES,
                                 widget=forms.Select(attrs={'class': 'select',
                                                            'placeholder': 'Оберіть місто',
                                                            'id': 'location', }))


class ParamsForm(forms.Form):
    q = forms.CharField(widget=forms.TextInput(attrs={'class': 'u-full-width res-panel-elements',
                                                      'id': 'keywords',
                                                      'placeholder':'Що ви хочете знайти?'}))
    location = forms.ChoiceField(choices=DB_ENUMS_RU.AREA_CHOICES,
                                 widget=forms.Select(attrs={'class': 'u-full-width res-panel-elements',
                                                            'placeholder': 'Оберіть місто',
                                                            'id': 'location', }))
    pmin = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'price-params res-panel-elements',
                                                              'placeholder': 'Ціна від:',
                                                              'id': 'pmin',}))
    pmax = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'price-params res-panel-elements',
                                                               'placeholder': 'Ціна до:',
                                                              'id':'pmax', }))
    category = forms.ChoiceField(choices=CATEGORIES_CHOICES,
                                 widget=forms.Select(attrs={'class': 'u-full-width  res-panel-elements',
                                                            'id': 'category', }))
    site = forms.MultipleChoiceField(choices=DB_ENUMS_RU.SITE_CHOICES)
    condition = forms.ChoiceField(choices=DB_ENUMS_RU.CONDITION_CHOICES)
    seller = forms.ChoiceField(choices=DB_ENUMS_RU.SELLER_TYPE_CHOICES)

    def __init__(self, *args, **kwargs):

        if 'subcategory' in args[0]:
            # Додаємо поле підпідкатегорії, якщо є
            subcategory_choices = deepcopy(SUBCATEGORY_CHOICES.get(
                '{0}|{1}'.format(args[0]['category'], args[0]['subcategory'])))


            # Щоб у полі категорії відображалася або підкатегорія, або категорія
            # без наступного - тільки категорія
            args[0]['category'] = args[0]['subcategory']

        super(ParamsForm, self).__init__(*args)
        # if subcategory exists and is non-empty
        if 'subcategory_choices' in locals() and subcategory_choices:
            if subcategory_choices[0] != ('', 'Все'):
                subcategory_choices.insert(0, ('', 'Все'))
            self.fields['subsubcategory'] = forms.ChoiceField(choices=subcategory_choices,
                                     widget=forms.Select(attrs={'class': 'u-full-width res-panel-elements',
                                                            'id': 'subsubcategory', }))
        # додаємо фільтри
        filters = kwargs.pop('filters')
        for cat_filter in filters:
            if filters[cat_filter][0] != ('', 'Все'):
                filters[cat_filter].insert(0, ('', 'Все'))
            self.fields[cat_filter[0]] = forms.ChoiceField(choices=filters[cat_filter],
                                    label=filters,
                                    widget=forms.Select(attrs={'class':'res-panel-elements',
                                                               'placeholder': 'urupinsk'}))

            # self.fields['filter{0}'.format(i)] = forms.ChoiceField(choices=filters[cat_filter],
            #                         label=filters,
            #                         # TODO: зробити плейсхолдери для фільтрів
            #                         widget=forms.Select(attrs={'placeholder': 'urupinsk{0}'.format(i)}))


    def clean(self):
        cleaned_data = super(ParamsForm, self).clean()
        keywords = cleaned_data.get(DB_KEYS.PARAMS.KEYWORDS)
        category = cleaned_data.get(DB_KEYS.PARAMS.CATEGORY)

        if not (keywords or category):
            raise forms.ValidationError("Введіть пошуковий рядок або категорію")
