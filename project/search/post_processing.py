import datetime as dt
from ast import literal_eval

from constants.constants_functions import translit_category_to_rus
from constants.db_num_to_rus import DB_NUM_TO_CITY_RUS, DB_NUM_TO_CATEGORIES_SUBCATEGORIES_RUS
from constants.ua import MONTHES_NAMES_UA


def ad_post_processing(ads):
    result = []
    ad_ids = set()
    if ads:
        if 'city_id' in ads[0]:
            for ad in ads:
                ad['location'] = ad.pop('city_id')
                ad['category'] = ad.pop('category_id')
                ad['subcategory'] = ad.pop('subcategory_id')
                ad['date'] = dt.datetime.timestamp(ad.pop('date'))
        for ad in ads:
            if 'і' in ad.get('q', ''):
                ad['q'] = ad['q'].encode("utf-8").decode("UTF-8", "ignore").replace(u'\u0456', u'i')
            ad['location'] = DB_NUM_TO_CITY_RUS[ad['location']][1]
            category, subcategory = DB_NUM_TO_CATEGORIES_SUBCATEGORIES_RUS[ad['category']]
            ad['category'] = category
            ad['subcategory'] = subcategory
            ad['date'] = extract_datetime(dt.datetime.fromtimestamp(ad['date']).strftime('%d-%m-%y %H:%M:%S'))
            ph = extract_price_history(ad['price_history'])
            ad['price_history'] = ph if len(ph) > 1 else None
            if ad['ad_id'] in ad_ids:
                continue
            else:
                ad_ids.add(ad['ad_id'])
                result.append(ad)
            if ad['price'] > 100000000 or ad['price'] < 0:
                ad['price'] = 'Обмен'
    return result


def params_post_processing(params):
    result = []
    for param in params:
        try:
            param.param.area.name = translit_category_to_rus(param.param.area.name)
        except:
            pass
        try:
            print(param.param.category.name)
            param.param.category.name = translit_category_to_rus(param.param.category.name, 'category')
        except:
            pass
        result.append(param)
        # param.param.subcategory =
    return result


def extract_datetime(s):
    d, t = s.split()
    d = d.split('-')
    d.pop(0)
    t = t.split(':')
    t.pop(-1)
    return str(int(d[1])) + ' ' + MONTHES_NAMES_UA[int(d[0])] + ' ' + str(t[0]) + ':' + str(t[1])


def extract_price_history(ph):
    # ph - price_history

    if not ph:
        return []

    ph = ph.replace('{', '[').replace('}', ']').replace('\x81', "'201").replace('\\', '').replace('"(', '('). \
        replace(')"', ')')
    ph = literal_eval(ph)
    ph = list(map(lambda p: (p[0], extract_datetime(p[1])), ph))
    return ph


ph = '{"(1500,\\"2015-10-20 14:14:56.931629\\")"}'
# print(extract_price_history(ph))
# print(extract_datetime(dt.datetime.fromtimestamp(1449174840.0).strftime('%d-%m-%y %H:%M:%S')))
