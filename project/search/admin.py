#-*- coding: utf-8 -*- 
from django.contrib import admin
from search.models import Param, ParamFilterValueList

admin.site.register(Param)
admin.site.register(ParamFilterValueList)
