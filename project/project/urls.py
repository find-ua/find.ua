#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin
from search.views import HomePageView
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view()),
    url(r'', include('social.apps.django_app.urls', namespace='social')),
    url(r'', include('users.urls', namespace="users")),
    url(r'', include('footer.urls', namespace="footer")),
    url(r'^api/', include('api.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^search$', SearchView.as_view()),
    url(r'^(?P<location>(\w|-)+)/', include('search.urls')),
)
