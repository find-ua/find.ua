#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Django settings for project project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

from users.social_auth import *

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
os.environ['S3_USE_SIGV4'] = 'False'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'azk1%k2ozel1use*co!z+8&&i3lygtmjp#=1#64_%a3b92y)p0'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'constants',
    'users',
    'search',
    'footer',
    'api',
    'basic_classes',
    's3_folder_storage',
    'social.apps.django_app.default',
    #  'debug_toolbar',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware'
)

ROOT_URLCONF = 'project.urls'

WSGI_APPLICATION = 'project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': 'db-find.cvwyq5nhgjhc.eu-central-1.rds.amazonaws.com',
        'NAME': 'db_find',
        'USER': 'db_master',
        'PASSWORD': 'db_password',
        'PORT': '5432',
        # 'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

DEFAULT_CHARSET = 'utf-8'

LANGUAGE_CODE = 'ru-Ru'

LANGUAGES = (
    ('ru', 'Русский'),
    ('ua', 'Українська')
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SESSION_SAVE_EVERY_REQUEST = True
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'social.backends.vk.VKOAuth2',
    'social.backends.google.GoogleOpenId',
    'social.backends.google.GoogleOAuth2',
    'social.backends.google.GoogleOAuth',
    'social.backends.facebook.FacebookOAuth2',
)

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'

# STATIC_ROOT = os.path.join(BASE_DIR, 'static_root') # 'python manage.py collectstatic' to use this folder
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

AWS_HEADERS = {  # see http://developer.yahoo.com/performance/rules.html#expires
                 'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
                 'Cache-Control': 'max-age=94608000',
                 }

AWS_STORAGE_BUCKET_NAME = 'find-ua'
AWS_ACCESS_KEY_ID = 'AKIAJKGEKPCEWSZ4RB2A'
AWS_SECRET_ACCESS_KEY = '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'


DEFAULT_FILE_STORAGE = 's3_folder_storage.s3.DefaultStorage'
DEFAULT_S3_PATH = "media"
STATICFILES_STORAGE = 's3_folder_storage.s3.StaticStorage'
STATIC_S3_PATH = "static"
MEDIA_ROOT = '/%s/' % DEFAULT_S3_PATH
MEDIA_URL = '//s3.amazonaws.com/%s/media/' % AWS_STORAGE_BUCKET_NAME
STATIC_ROOT = "/%s/" % STATIC_S3_PATH
STATIC_URL = '//s3.amazonaws.com/%s/static/' % AWS_STORAGE_BUCKET_NAME
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'


# Налаштування для локального сервера, ЗАКОМЕНТУЙТЕ, ЗАЛИВАЮЧИ НА СЕРВЕР!
# STATIC_URL = '/static/'
# Кінець налаштувань, які треба закоментувати


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
           os.path.join(BASE_DIR, "templates")
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_SUBJECT_PREFIX = '[Django] '
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'find.ua.service@gmail.com'
EMAIL_HOST_PASSWORD = '123jkl456jkl789'
EMAIL_PORT = 587

